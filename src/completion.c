// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file  completion.c
\brief Check for rendering completion.

There is no fixed maximum iteration count, and there are multiple glitch
correction passes.
*/

#include "completion.h"
#include "logging.h"

void completion_start(struct completion *completion, int unescaped) {
  completion->almost = false;
  completion->done = false;
  completion->iterations = 0;
  completion->iteration_target = 1;
  completion->unescaped = unescaped;
  completion->escaped = 0;
  completion->escaped_recently = 0;
  debug_message("completion_start(): %d %d %d %d %s\n", completion->unescaped, completion->escaped, completion->escaped_recently, completion->iterations, completion->done ? "DONE" : "");
}

void completion_reset(struct completion *completion, int unescaped) {
  completion->almost = false;
  completion->done = false;
  completion->iterations = 0;
  // don't reset iteration_target
  completion->unescaped = unescaped;
  completion->escaped = 0;
  completion->escaped_recently = 0;
  debug_message("completion_reset(): %d %d %d %d %s\n", completion->unescaped, completion->escaped, completion->escaped_recently, completion->iterations, completion->done ? "DONE" : "");
}

bool completion_update(struct completion *completion, int unescaped, int escaped, int iterations, double sharpness) {
  bool retval = false;
  completion->iterations += iterations;
  completion->unescaped = unescaped;
  completion->escaped += escaped;
  completion->escaped_recently += escaped;
  debug_message("completion_update(): %d %d %d %d << %d %d %d\n", completion->unescaped, completion->escaped, completion->escaped_recently, completion->iterations, unescaped, escaped, iterations);
  if (unescaped == 0) {
    // no more unescaped pixels to iterate, must be done
    completion->done = true;
    retval = true;
  } else if (completion->iterations >= completion->iteration_target) {
    // have pixels ever escaped and is the current escape rate small?
    if (completion->escaped > 0 && completion->escaped_recently <= sharpness * completion->unescaped) {
      // it needs to happen twice, to avoid random failures when the
      // first few pixels of an image escape
      if (completion->almost) {
        completion->done = true;
        debug_message("done\n");
      } else {
        completion->almost = true;
        debug_message("almost\n");
      }
    } else {
      completion->almost = false;
    }
    completion->iteration_target <<= 1;
    completion->escaped_recently = 0;
    retval = true;
  }
  return retval;
}

bool completion_done(struct completion *completion) {
  return completion->done;
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>

#include "parse.h"
#include "parse_ppm.h"

bool parse_ppm(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
  (void) length;
  assert(source);
  char *source2 = strdup(source);
  assert(source2);
  char *s = source2;
  char *sx = 0;
  char *sy = 0;
  char *sz = 0;
  char *line = parse_line(&s);
  if (0 == strcmp("P6", line)) {
    line = parse_line(&s);
    int len = strlen(line);
    if (len > 20) {
      // the total length of a line is surely long enough for each of its
      // shorter substrings
      sx = (char *) malloc(len);
      sy = (char *) malloc(len);
      sz = (char *) malloc(len);
      if (3 == sscanf(line, "# mightymandel %s + %s i @ %s", sx, sy, sz)) {
        mpfr_set_prec(cz, 53);
        mpfr_set_str(cz, sz, 10, MPFR_RNDN);
        if (! radius_is_valid(cz)) {
          free(sx);
          free(sy);
          free(sz);
          free(source2);
          return false;
        }
        mpfr_prec_t p = precision_for_radius(cz);
        mpfr_set_prec(cx, p);
        mpfr_set_prec(cy, p);
        mpfr_set_str(cx, sx, 10, MPFR_RNDN);
        mpfr_set_str(cy, sy, 10, MPFR_RNDN);
        free(sx);
        free(sy);
        free(sz);
        free(source2);
        return true;
      } else {
        free(sx);
        free(sy);
        free(sz);
        free(source2);
        return false;
      }
    } else {
      free(source2);
      return false;
    }
  } else {
    free(source2);
    return false;
  }
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef LOGGING_H
#define LOGGING_H 1

#include <assert.h>
#include <stdio.h>
#include <mpfr.h>

#include "mightymandel.h"

enum log_level_t {
  LOG_OOPS   =  0,
  LOG_FATAL  =  1,
  LOG_ERROR  =  2,
  LOG_WARN   =  3,
  LOG_NOTICE =  4,
  LOG_INFO   =  5,
  LOG_DEBUG  =  6
};

extern enum log_level_t log_level;
extern FILE *log_target;
extern const char *log_prefix[7];

// Use assert() instead of LOG_FATAL when it's caused by a programming error.
// The assertion failure triggers an abort which is catchable in a debugger
// like gdb, allowing inspecting the state of the program to diagnose how the
// error manifested itself in more detail without having to add extra debug log
// message.

#define log_message(level, ...) do{ if (level <= log_level) { assert(log_target); if (level <= LOG_DEBUG) fprintf(log_target, "%s", log_prefix[level]); mpfr_fprintf(log_target, __VA_ARGS__); } }while(0)

#ifdef MIGHTYMANDEL_DEBUG
#define debug_message(...) log_message(LOG_DEBUG, __VA_ARGS__)
#else
#define debug_message(...) do{}while(0)
#endif

enum result_t {
  result_ok      = 0,
  result_glitch  = 1,
  result_parse   = 2,
  result_deep    = 3,
  result_timeout = 4
};

extern const char *result_name[5];

void log_result(const char *filename, enum result_t result, enum render_method_t method, double iterations, double exterior, double interior, double glitch, int passes);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "fp32_fillc.h"
#include "shader.h"
#include "logging.h"
#include "texture.h"

extern const char *fp32_fillc_vert;
extern const char *fp32_fillc_frag;

void fp32_fillc_begin(struct fp32_fillc *s) {
  s->program = compile_program("fp32_fillc", fp32_fillc_vert, 0, fp32_fillc_frag);
  s->aspect = glGetUniformLocation(s->program, "aspect");D;
  s->slice_offset = glGetUniformLocation(s->program, "slice_offset");D;
  s->ida0 = glGetUniformLocation(s->program, "ida0");D;
  s->p = glGetAttribLocation(s->program, "p");D;
  glGenVertexArrays(1, &s->vao);D;
  glGenBuffers(1, &s->vbo);D;
}

void fp32_fillc_end(struct fp32_fillc *s) {
  glDeleteProgram(s->program);D;
  glDeleteBuffers(1, &s->vbo);D;
  glDeleteVertexArrays(1, &s->vao);D;
  glDeleteBuffers(1, &s->vbo);D;
}

void fp32_fillc_do(struct fp32_fillc *s, GLuint vbo, GLuint fbo, GLuint tex, GLuint tex2, int width, int height, int pass, int slice, int slicex, int slicey) {
  debug_message("fp32_fillc_do(%p, %d, %d, %d, %d, %d, %d, %d, %d, %d, %d)\n", s, vbo, fbo, tex, tex2, width, height, pass, slice, slicex, slicey);
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);D;
  if (pass == 0 && slicex == 0 && slicey == 0) {
    glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex, 0);D;
    GLenum buffers[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, buffers);D;
    glViewport(0, 0, width, height);D;
    glClearColor(-1,0,0,0); // FIXME spec says glClearColor clamps to [0,1]
    glClear(GL_COLOR_BUFFER_BIT);
  }
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex2, 0);D;
  GLenum buffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, buffers);D;
  glViewport(0, 0, width >> slice, height >> slice);D;
  glUseProgram(s->program);D;
  glUniform1i(s->ida0, TEX_RAW);D;
  double aspect = width / (double) height;
  glUniform1f(s->aspect, 1.0 / aspect);D;
  double pixel_delta = 2.0 / (height >> slice);
  glUniform2f(s->slice_offset, ((slicex + 0.5) / (1 << slice) - 0.5) * pixel_delta, ((slicey + 0.5) / (1 << slice) - 0.5) * pixel_delta);
  GLfloat quad[] = {
    -aspect, -1, 0, 0,
    -aspect,  1, 0, 1,
     aspect, -1, 1, 0,
     aspect,  1, 1, 1
  };
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, s->vbo);D;
  glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), &quad, GL_STATIC_DRAW);D;
  glVertexAttribPointer(s->p, 4, GL_FLOAT, GL_FALSE, 0, 0);D;
  glEnableVertexAttribArray(s->p);D;
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);D;
  glDisableVertexAttribArray(s->p);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  glUseProgram(0);D;
  // Copy texture to vertex buffer.
  glBindBuffer(GL_PIXEL_PACK_BUFFER, vbo);D;
  glReadPixels(0, 0, width >> slice, height >> slice, GL_RGBA, GL_FLOAT, 0);D;
  glBindBuffer(GL_PIXEL_PACK_BUFFER, 0);D;
  debug_message("VBO fillc: ? -> %d\n", vbo);
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex, 0);D;
  glDrawBuffers(1, buffers);D;
  glViewport(0, 0, width, height);D;
}

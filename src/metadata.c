// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "metadata.h"

struct metadata_node {
  struct metadata_node *next;
  struct metadata_node *prev;
  char *key;
  char *value;
};

struct metadata {
  struct metadata_node head;
  struct metadata_node tail;
};

struct metadata *metadata_new() {
  struct metadata *s = calloc(1, sizeof(struct metadata));
  s->head.next = &s->tail;
  s->tail.prev = &s->head;
  return s;
}

void metadata_delete(struct metadata *s) {
  struct metadata_node *n = s->head.next;
  while (n->next) {
    n->prev->next = n->next;
    n->next->prev = n->prev;
    struct metadata_node *m = n->next;
    free(n->key);
    free(n->value);
    free(n);
    n = m;
  }
}

struct metadata_node *metadata_find(struct metadata *s, const char *key) {
  struct metadata_node *n = s->head.next;
  while (n->next) {
    if (0 == strcmp(n->key, key)) {
      return n;
    }
    n = n->next;
  }
  return 0;
}

const char *metadata_lookup(struct metadata*s, const char *key) {
  struct metadata_node *n = metadata_find(s, key);
  if (n) {
    return n->value;
  }
  return 0;
}

void metadata_update(struct metadata *s, const char *key, const char *value) {
  struct metadata_node *n = metadata_find(s, key);
  if (n) {
    free(n->value);
    n->value = strdup(value);
  } else {
    n = calloc(1, sizeof(struct metadata_node));
    n->key = strdup(key);
    n->value = strdup(value);
    n->next = &s->tail;
    n->prev = s->tail.prev;
    n->next->prev = n;
    n->prev->next = n;
  }
}

void metadata_remove(struct metadata *s, const char *key) {
  struct metadata_node *n = metadata_find(s, key);
  if (! n) {
    return;
  }
  n->prev->next = n->next;
  n->next->prev = n->prev;
  free(n->key);
  free(n->value);
  free(n);
}

int metadata_strlen(const struct metadata *s, const char *prefix) {
  int l = 0;
  struct metadata_node *n = s->head.next;
  while (n->next) {
    l += strlen(prefix) + strlen(n->key) + 1 + strlen(n->value) + 1;
    n = n->next;
  }
  return l;
}

bool metadata_string(const struct metadata *s, const char *prefix, char *string, int length) {
  char *str = string;
  char *end = string + length;
  const struct metadata_node *n = s->head.next;
  while (n->next && str < end) {
    str += snprintf(str, end - str, "%s%s %s\n", prefix, n->key, n->value);
    n = n->next;
  }
  return str <= end;
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_PPM_H
#define PARSE_PPM_H 1

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Parse the original mightymandel `.ppm` metadata file format.

File format example:

    P6
    # mightymandel -0.75 + 0.0 i @ 1.5
    1280 720
    255

followed by binary image data.  The binary image data could contain all kinds
of confusing things, so we don't parse the whole file.

\copydetails parser
*/
bool parse_ppm(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

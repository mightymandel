// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>

#include "parse.h"
#include "parse_kfr.h"

bool parse_kfr(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
  (void) length;
  assert(source);
  char *source2 = strdup(source);
  assert(source2);
  char *s = source2;
  char *sx = 0;
  char *sy = 0;
  char *sz = 0;
  while (s) {
    char *line = parse_line(&s);
    if (0 == strncmp(line, "Re: ",   4)) { sx = line + 4; }
    if (0 == strncmp(line, "Im: ",   4)) { sy = line + 4; }
    if (0 == strncmp(line, "Zoom: ", 6)) { sz = line + 6; }
  }
  if (sx && sy && sz) {
    mpfr_set_prec(cz, 53);
    mpfr_set_str(cz, sz, 10, MPFR_RNDN);
    mpfr_d_div(cz, 2.0, cz, MPFR_RNDN);
    if (! radius_is_valid(cz)) {
      free(source2);
      return false;
    }
    mpfr_prec_t p = precision_for_radius(cz);
    mpfr_set_prec(cx, p);
    mpfr_set_prec(cy, p);
    mpfr_set_str(cx, sx, 10, MPFR_RNDN);
    mpfr_set_str(cy, sy, 10, MPFR_RNDN);
    mpfr_neg(cy, cy, MPFR_RNDN);
    free(source2);
    return true;
  } else {
    free(source2);
    return false;
  }
}

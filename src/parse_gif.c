// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <math.h>
#include <stdio.h>
#include <string.h>

#include "parse.h"
#include "parse_gif.h"
#include "logging.h"

bool parse_gif(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
  // magic header strings
  const char *gif = "GIF";
  const int gif_length = 3;
  const char *fractint001 = "fractint001";
  const int fractint001_length = 11;
  const char *info_id = "Fractal";
  const int info_id_length = 8;
  // magic header offsets
  const int length_b = 1;
  const int version_lsb = 79;
  const int version_msb = 80;
  const int type_lsb = 11;
  const int type_msb = 12;
  const int coord_doubles = 13;
  const int max_offset = 81;
  // magic constants
  const int type_mandelbrot1 = 0;
  const int type_mandelbrot2 = 4;
  // check GIF header
  if (0 != memcmp(source, gif, gif_length)) {
    return false;
  }
  debug_message("fractint gif GIF\n");
  // find extension block
  for (int offset = 0; offset < length - fractint001_length; ++offset) {
    if (0 == memcmp(source + offset, fractint001, fractint001_length)) {
      // found fractint001 extension block
      debug_message("fractint gif fractint001\n");
      // parse info blocks { 1 byte length x, x bytes data }
      // terminated by 0 length
      const unsigned char *info = (const unsigned char *) source + offset + fractint001_length;
      // check for overrun, we read up to max_offset bytes inside the loop
      int info_length;
      while (info < (const unsigned char *) source + length - max_offset && (info_length = info[0])) {
        // parse info block
        if (0 == memcmp((const char *) info + length_b, info_id, info_id_length)) {
          // got Fractal block
          debug_message("fractint gif Fractal\n");
          // check version
          int version = info[version_lsb] | (info[version_msb] << 8);
          debug_message("fractint gif version %d\n", version);
          // check Mandelbrot
          int type = info[type_lsb] | (info[type_msb] << 8);
          debug_message("fractint gif type %d\n", type);
          if (type == type_mandelbrot1 || type == type_mandelbrot2) {
            // copy coords
            double coords[6]; memcpy(coords, info + coord_doubles, sizeof(coords));
            double xmin  = coords[0];
            double xmax  = coords[1];
            double ymin  = coords[2];
            double ymax  = coords[3];
            double creal = coords[4];
            double cimag = coords[5];
            debug_message("fractint gif coords %e %e %e %e %e %e\n", xmin, xmax, ymin, ymax, creal, cimag);
            // cook them
            double x = (xmin + xmax) / 2.0;
            double y = (ymin + ymax) / 2.0;
            double z = fabs(ymax - ymin) / 2.0;
            debug_message("fractint gif xyz %.20e %.20e %e\n", x, y, z);
            if (z > 0.0) {
              // output
              mpfr_set_prec(cx, 53);
              mpfr_set_prec(cy, 53);
              mpfr_set_d(cx, x, MPFR_RNDN);
              mpfr_set_d(cy, y, MPFR_RNDN);
              mpfr_set_d(cz, z, MPFR_RNDN);
              return true;
            }
          }
        }
        // advance to next block
        info += 1 + info_length;
      }
    }
  }
  return false;
}

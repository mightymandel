// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "mightymandel.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

const char *simple_vert =
  "#version 420 core\n"
  "out vec2 texCoord;\n"
  "const vec2 t[4] = vec2[4](vec2(0.0, 0.0), vec2(1.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 1.0));\n"
  "void main() {\n"
  "  gl_Position = vec4(t[gl_VertexID] * 2.0 - 1.0, 0.0, 1.0);\n"
  "  texCoord = t[gl_VertexID];\n"
  "}\n"
  ;

const char *simple_frag =
  "#version 420 core\n"
  "uniform sampler2D tex;\n"
  "in vec2 texCoord;\n"
  "out vec4 colour;\n"
  "int px(vec2 tc) {\n"
  "  vec4 t = texture(tex, tc);\n"
  "  int k = 0;\n"
  "  k += (t.x / 2.0) - floor(t.x / 2.0) >= 0.5 ? 1 : 2;\n"
  "  k += t.z >= 0.0 ? 4 : 8;\n"
  "  k += 16 * int(floor(t.x));\n"
  "  return k;\n"
  "}\n"
  "void main() {\n"
  "  vec2 dx = dFdx(texCoord);\n"
  "  vec2 dy = dFdy(texCoord);\n"
  "  float c = px(texCoord) == px(texCoord + dx + dy) && px(texCoord + dx) == px(texCoord + dy) ? 1.0 : 0.0;\n"
  "  float me = texture(tex, texCoord).x;\n"
  "  colour = vec4(me <= 0.0 ? vec3(1.0, 0.0, 0.0) : vec3(c), 1.0);\n"
  "}\n"
  ;

static void debug_cb(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, GLvoid *user) {
  fprintf(stderr, "%s\n", message);
(void) source;
(void) type;
(void) id;
(void) severity;
(void) length;
(void) user;
}

struct state_s {
  bool should_restart;
  bool should_redraw;
  bool should_save_now;
  bool should_save_when_done;
  mightymandel_options_t *options;
};
typedef struct state_s state_t;

void button_handler(GLFWwindow *window, int button, int action, int mods) {
  if (action == GLFW_PRESS) {
    double x = 0, y = 0;
    state_t *state = glfwGetWindowUserPointer(window);
    glfwGetCursorPos(window, &x, &y);
    mpfr_t cx, cy, r;
    mpfr_init2(cx, 53);
    mpfr_init2(cy, 53);
    mpfr_init2(r, 53);
    mightymandel_options_get_center_re(state->options, cx);
    mightymandel_options_get_center_im(state->options, cy);
    mightymandel_options_get_radius(state->options, r);
    double w = mightymandel_options_get_width(state->options);
    double h = mightymandel_options_get_height(state->options);
    double d = 2 * mpfr_get_d(r, MPFR_RNDN);
    double dx = d * ((x + 0.5) / w - 0.5) * (w / h);
    double dy = d * (0.5 - (y + 0.5) / h);
    switch (button) {
      case GLFW_MOUSE_BUTTON_LEFT:
        mpfr_add_d(cx, cx, dx / 2, MPFR_RNDN);
        mpfr_add_d(cy, cy, dy / 2, MPFR_RNDN);
        mpfr_mul_2si(r, r, -1, MPFR_RNDN);
        mightymandel_options_set_center_re(state->options, cx);
        mightymandel_options_set_center_im(state->options, cy);
        mightymandel_options_set_radius(state->options, r);
        state->should_restart = true;
        break;
      case GLFW_MOUSE_BUTTON_RIGHT:
        mpfr_sub_d(cx, cx, dx, MPFR_RNDN);
        mpfr_sub_d(cy, cy, dy, MPFR_RNDN);
        mpfr_mul_2si(r, r, 1, MPFR_RNDN);
        mightymandel_options_set_center_re(state->options, cx);
        mightymandel_options_set_center_im(state->options, cy);
        mightymandel_options_set_radius(state->options, r);
        state->should_restart = true;
        break;
      case GLFW_MOUSE_BUTTON_MIDDLE:
        mpfr_add_d(cx, cx, dx, MPFR_RNDN);
        mpfr_add_d(cy, cy, dy, MPFR_RNDN);
        mightymandel_options_set_center_re(state->options, cx);
        mightymandel_options_set_center_im(state->options, cy);
        state->should_restart = true;
        break;
    }
  }
(void) mods;
}

void key_handler(GLFWwindow *window, int key, int scancode, int action, int mods) {
  state_t *state = glfwGetWindowUserPointer(window);
  if (action == GLFW_PRESS) {
    switch (key) {
      case GLFW_KEY_Q:
      case GLFW_KEY_ESCAPE:
        glfwSetWindowShouldClose(window, GL_TRUE);
        break;
      case GLFW_KEY_S:
        if (mods & GLFW_MOD_SHIFT) {
          state->should_save_now = true;
        } else {
          state->should_save_when_done = true;
        }
        break;
    }
  }
(void) scancode;
}

int main(int argc, char **argv) {

  glfwInit();
  int win_width = 1280;
  int win_height = 720;
  glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(win_width, win_height, "mightymandel", 0, 0);
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew
  glDebugMessageCallback(debug_cb, 0);
  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  GLuint tex;
  glGenTextures(1, &tex);
  glBindTexture(GL_TEXTURE_2D, tex);
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, win_width, win_height, 0, GL_RGBA, GL_FLOAT, 0);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

  GLuint vao;
  glGenVertexArrays(1, &vao);
  glBindVertexArray(vao);

  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &simple_vert, 0);
    glCompileShader(shader);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &simple_frag, 0);
    glCompileShader(shader);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  {
    char buf[10000];
    int len = 0;
    glGetProgramInfoLog(program, 10000, &len, buf);
    if (len > 0) {
      buf[len] = 0;
      fprintf(stderr, "%s\n", buf);
    }
  }
  glUseProgram(program);

  uint8_t *ppm = malloc(win_width * win_height * 3);
  assert(ppm);

  mightymandel_t *context = mightymandel_new(window, 0 /*debug_cb*/, 0, 0, 0);
  assert(context);

  mightymandel_options_t *options = mightymandel_options_new(context);
  assert(options);

  mightymandel_options_set_width(options, win_width);
  mightymandel_options_set_height(options, win_height);
  mightymandel_options_set_escape_radius(options, 25);
  mightymandel_options_set_sharpness(options, 0.01);
  mightymandel_options_set_results_texture(options, tex);
  mpfr_t z;
  mpfr_init2(z, 53);
  mpfr_set_d(z, -0.75, MPFR_RNDN);
  mightymandel_options_set_center_re(options, z);
  mpfr_set_d(z, 0, MPFR_RNDN);
  mightymandel_options_set_center_im(options, z);
  mpfr_set_d(z, 1.5, MPFR_RNDN);
  mightymandel_options_set_radius(options, z);
  mpfr_clear(z);

  state_t state;
  state.should_restart = false;
  state.should_redraw = false;
  state.should_save_now = false;
  state.should_save_when_done = false;
  state.options = options;
  glfwSetWindowUserPointer(window, &state);
  glfwSetMouseButtonCallback(window, button_handler);
  glfwSetKeyCallback(window, key_handler);

  while (! glfwWindowShouldClose(window)) {

    state.should_restart = false;
    fprintf(stderr, "start\n");
    mightymandel_start(context, options);

    fprintf(stderr, "wait_timeout\n");
    while (! mightymandel_wait_timeout(context, 1.0 / 30.0)) {
      fprintf(stderr, "refresh\n");
      mightymandel_refresh(context);
      glBindTexture(GL_TEXTURE_2D, tex);
      glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
      glfwSwapBuffers(window);

      glfwPollEvents();
      if (glfwWindowShouldClose(window)) {
        break;
      }

      if (state.should_save_now) {
        glReadPixels(0, 0, win_width, win_height, GL_RGB, GL_UNSIGNED_BYTE, ppm);
        printf("P6\n%d %d\n255\n", win_width, win_height);
        fwrite(ppm, win_width * win_height * 3, 1, stdout);
        state.should_save_now = false;
      }

      if (state.should_restart) {
        state.should_restart = false;
       fprintf(stderr, "start\n");
        mightymandel_start(context, options);
      }
      fprintf(stderr, "wait_timeout\n");
    }

    if (glfwWindowShouldClose(window)) {
      break;
    }

    if (state.should_save_when_done) {
      glReadPixels(0, 0, win_width, win_height, GL_RGB, GL_UNSIGNED_BYTE, ppm);
      printf("P6\n%d %d\n255\n", win_width, win_height);
      fwrite(ppm, win_width * win_height * 3, 1, stdout);
      state.should_save_when_done = false;
    }

    while (! state.should_restart) {

      glfwWaitEvents();
      if (glfwWindowShouldClose(window)) {
        break;
      }

      if (state.should_redraw) {
        glBindTexture(GL_TEXTURE_2D, tex);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
        glfwSwapBuffers(window);
      }

    }

  }

  fprintf(stderr, "delete\n");
  mightymandel_delete(context);
  free(ppm);
  glfwDestroyWindow(window);
  glfwTerminate();
  return 0;
(void) argc;
(void) argv;
}

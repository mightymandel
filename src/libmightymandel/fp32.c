// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "mightymandel_private.h"

#include "fp32_init_compute.glsl.c"
#include "fp32_step_compute.glsl.c"
#include "fp32_test_vertex.glsl.c"
#include "fp32_test_fragment.glsl.c"
#include "fp32_extract_compute.glsl.c"
#include "fp32_prefixsum_compute.glsl.c"
#include "fp32_permute_compute.glsl.c"
#include "fp32_copy_compute.glsl.c"
#include "fp32_next_compute.glsl.c"

#include <stdio.h>
#include <stdlib.h>

struct fp32_s {
  GLuint p_init, p_step, p_test, p_extract, p_prefixsum, p_permute, p_copy, p_next;
  GLuint fbo;
  GLuint tex;
  GLuint vao;
  GLuint b_context, b_results, b_state1, b_index, b_state2, b_workgroup;
  GLuint query;
};

void _mightymandel_fp32_init(mightymandel_t *context) {
  _mightymandel_assert(context);
  fp32_t *fp32 = malloc(sizeof(*fp32));
  _mightymandel_assert(fp32);
  fp32->p_init = _mightymandel_compute_shader("fp32_init", _mightymandel_fp32_init_compute);
  fp32->p_step = _mightymandel_compute_shader("fp32_step", _mightymandel_fp32_step_compute);
  fp32->p_test = _mightymandel_vertex_fragment_shader("fp32_test", _mightymandel_fp32_test_vertex, _mightymandel_fp32_test_fragment);
  fp32->p_extract = _mightymandel_compute_shader("fp32_extract", _mightymandel_fp32_extract_compute);
  fp32->p_prefixsum = _mightymandel_compute_shader("fp32_prefixsum", _mightymandel_fp32_prefixsum_compute);
  fp32->p_permute = _mightymandel_compute_shader("fp32_permute", _mightymandel_fp32_permute_compute);
  fp32->p_copy = _mightymandel_compute_shader("fp32_copy", _mightymandel_fp32_copy_compute);
  fp32->p_next = _mightymandel_compute_shader("fp32_next", _mightymandel_fp32_next_compute);

  glGenTextures(1, &fp32->tex);
  glBindTexture(GL_TEXTURE_2D, fp32->tex);
  glObjectLabel(GL_TEXTURE, fp32->tex, -1, "fp32_texture");
  glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, 1, 1, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

  glGenFramebuffers(1, &fp32->fbo);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fp32->fbo);
  glObjectLabel(GL_FRAMEBUFFER, fp32->fbo, -1, "fp32_framebuffer");
  glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, fp32->tex, 0);
  GLenum buf = GL_NONE;
  glDrawBuffers(1, &buf);

  glGenVertexArrays(1, &fp32->vao);
  glBindVertexArray(fp32->vao);
  glObjectLabel(GL_VERTEX_ARRAY, fp32->vao, -1, "fp32_vertex_array");

  glGenQueries(1, &fp32->query);
  glBeginQuery(GL_ANY_SAMPLES_PASSED, fp32->query);
  glEndQuery(GL_ANY_SAMPLES_PASSED);
  glObjectLabel(GL_QUERY, fp32->query, -1, "fp32_query");

  glBindTexture(GL_TEXTURE_2D, 0);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindVertexArray(0);

  context->fp32 = fp32;
}

void _mightymandel_fp32_render(mightymandel_t *context, const mightymandel_options_t *options) {
  _mightymandel_assert(context);
  _mightymandel_assert(options);
  fp32_t *fp32 = context->fp32;
  _mightymandel_assert(fp32);

  // unpack options

  GLuint step_iterations = 64;
  GLuint width = options->width;
  GLuint height = options->height;
  GLuint size = width * height;
  GLfloat center_re = mpfr_get_d(options->center_re, MPFR_RNDN);
  GLfloat center_im = mpfr_get_d(options->center_im, MPFR_RNDN);
  GLfloat radius = mpfr_get_d(options->radius, MPFR_RNDN);
  GLfloat escape_radius_2 = options->escape_radius * options->escape_radius;

  // allocate buffers

  glGenBuffers(1, &fp32->b_context);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_context);
  glObjectLabel(GL_BUFFER, fp32->b_context, -1, "fp32_context");

  glGenBuffers(1, &fp32->b_results);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_results);
  glObjectLabel(GL_BUFFER, fp32->b_results, -1, "fp32_results");

  glGenBuffers(1, &fp32->b_state1);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_state1);
  glObjectLabel(GL_BUFFER, fp32->b_state1, -1, "fp32_state[0]");

  glGenBuffers(1, &fp32->b_index);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_index);
  glObjectLabel(GL_BUFFER, fp32->b_index, -1, "fp32_index");

  glGenBuffers(1, &fp32->b_state2);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_state2);
  glObjectLabel(GL_BUFFER, fp32->b_state2, -1, "fp32_state[1]");

  glGenBuffers(1, &fp32->b_workgroup);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_workgroup);
  glObjectLabel(GL_BUFFER, fp32->b_workgroup, -1, "fp32_workgroup");

  // initialize buffers

  int64_t gpu_mem
    =        2 * sizeof(GLuint) // context
    + size * 4 * sizeof(GLuint) // results
    + size * 4 * sizeof(GLuint) // state 1
    + size * 4 * sizeof(GLuint) // state 2
    + size *     sizeof(GLuint) // index
    +        3 * sizeof(GLuint) // workgroup
    ;
  bool enough_free_gpu_memory = _mightymandel_gpu_alloc(context, gpu_mem);
  _mightymandel_assert(enough_free_gpu_memory);

  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_context);
  GLuint b_context_data[2] = { size, 0 };
  glBufferData(GL_ARRAY_BUFFER, 2 * sizeof(GLuint), &b_context_data[0], GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 1, fp32->b_context);

  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_results);
  glBufferData(GL_ARRAY_BUFFER, size * 4 * sizeof(GLfloat), 0, GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 2, fp32->b_results);

  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_state1);
  glBufferData(GL_ARRAY_BUFFER, size * 4 * sizeof(GLfloat), 0, GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 3, fp32->b_state1);

  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_index);
  glBufferData(GL_ARRAY_BUFFER, size * sizeof(GLuint), 0, GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 4, fp32->b_index);

  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_state2);
  glBufferData(GL_ARRAY_BUFFER, size * 4 * sizeof(GLfloat), 0, GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 5, fp32->b_state2);

  GLuint workgroups = (size + 1023) / 1024;
  workgroups = workgroups == 0 ? 1 : workgroups;
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_workgroup);
  GLuint b_workgroup_data[3] = { workgroups, 1, 1 };
  glBufferData(GL_ARRAY_BUFFER, 3 * sizeof(GLuint), b_workgroup_data, GL_DYNAMIC_COPY);
  glBindBufferBase(GL_SHADER_STORAGE_BUFFER, 6, fp32->b_workgroup);

  glBindVertexArray(fp32->vao);
  glBindBuffer(GL_ARRAY_BUFFER, fp32->b_context);
  glVertexAttribPointer(0, 1, GL_UNSIGNED_INT, GL_FALSE, 0, (void *)(((unsigned char *)0) + 4));
  glEnableVertexAttribArray(0);

  glBindBuffer(GL_DISPATCH_INDIRECT_BUFFER, fp32->b_workgroup);

  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, fp32->fbo);
  glViewport(0, 0, 1, 1);
  glEnable(GL_DEPTH_TEST);
  glDepthFunc(GL_LEQUAL);
  glClearDepth(0.75);

  // initialize uniforms

  glUseProgram(fp32->p_init);
  glUniform3ui(2, width, height, size);
  glUniform4f(3, center_re, center_im, radius, escape_radius_2);

  glUseProgram(fp32->p_step);
  glUniform1ui(1, step_iterations);
  glUniform3ui(2, width, height, size);
  glUniform4f(3, center_re, center_im, radius, escape_radius_2);

  glUseProgram(fp32->p_extract);
  glUniform3ui(2, width, height, size);

  glUseProgram(fp32->p_permute);
  glUniform3ui(2, width, height, size);

  // render

  glUseProgram(fp32->p_init);
  glDispatchComputeIndirect(0);
  glMemoryBarrier(GL_ALL_BARRIER_BITS);

  for (GLuint start_iterations = 0; true; start_iterations += step_iterations) {

    if (_mightymandel_refresh(context, fp32->b_results)) { break; }

    glUseProgram(fp32->p_test);
    glClear(GL_DEPTH_BUFFER_BIT);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glBeginQuery(GL_ANY_SAMPLES_PASSED, fp32->query);
    glDrawArrays(GL_POINTS, 0, 1);
    glEndQuery(GL_ANY_SAMPLES_PASSED);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);

    glBeginConditionalRender(fp32->query, GL_QUERY_WAIT); {

      glUseProgram(fp32->p_extract);
      glDispatchComputeIndirect(0);
      glMemoryBarrier(GL_ALL_BARRIER_BITS);

      for (GLuint passbit = 1; passbit <= size; passbit <<= 1) {

        glUseProgram(fp32->p_prefixsum);
        glUniform1ui(4, passbit);
        glDispatchComputeIndirect(0);
        glMemoryBarrier(GL_ALL_BARRIER_BITS);
      }

      glUseProgram(fp32->p_permute);
      glDispatchComputeIndirect(0);
      glMemoryBarrier(GL_ALL_BARRIER_BITS);

      glUseProgram(fp32->p_copy);
      glDispatchComputeIndirect(0);
      glMemoryBarrier(GL_ALL_BARRIER_BITS);

    } glEndConditionalRender();

    glUseProgram(fp32->p_next);
    glDispatchCompute(1, 1, 1);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);

    glUseProgram(fp32->p_step);
    glUniform1ui(5, start_iterations);
    glDispatchComputeIndirect(0);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);

  }
  _mightymandel_refresh(context, fp32->b_results);

  // reset state

  for (int i = 1; i < 7; ++i) {
    glBindBufferBase(GL_SHADER_STORAGE_BUFFER, i, 0);
  }
  glBindBuffer(GL_DISPATCH_INDIRECT_BUFFER, 0);
  glBindBuffer(GL_ARRAY_BUFFER, 0);
  glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
  glDisable(GL_DEPTH_TEST);
  glUseProgram(0);
  glBindVertexArray(0);

  // deallocate buffers

  glDeleteBuffers(1, &fp32->b_context);
  fp32->b_context = 0;

  glDeleteBuffers(1, &fp32->b_results);
  fp32->b_results = 0;

  glDeleteBuffers(1, &fp32->b_state1);
  fp32->b_state1 = 0;

  glDeleteBuffers(1, &fp32->b_index);
  fp32->b_index = 0;

  glDeleteBuffers(1, &fp32->b_state2);
  fp32->b_state2 = 0;

  glDeleteBuffers(1, &fp32->b_workgroup);
  fp32->b_workgroup = 0;

  _mightymandel_gpu_free(context, gpu_mem);

}

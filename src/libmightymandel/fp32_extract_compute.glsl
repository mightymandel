// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#version 430 core

layout(location = 2) uniform uvec3 size;

layout(std430, binding = 1) buffer b_context {
  readonly restrict uint active_in;
};

layout(std430, binding = 3) buffer b_state {
  readonly restrict uvec4 state[];
};

layout(std430, binding = 4) buffer b_index {
  writeonly restrict uint index[];
};

layout(local_size_x = 1024) in;

void main() {
  if (gl_GlobalInvocationID.x < active_in) {
    index[gl_GlobalInvocationID.x] = state[gl_GlobalInvocationID.x].z < size.z ? 1 : 0;
  }
}

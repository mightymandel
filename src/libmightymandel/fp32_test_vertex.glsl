// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#version 430 core

layout(location = 0) in uint escaped;

void main() {
  gl_Position = vec4(0.0, 0.0, escaped > 0 ? 0.0 : 1.0, 1.0);
}

#!/bin/bash
## mightymandel -- GPU-based Mandelbrot Set explorer
## Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
## License GPL3+ http://www.gnu.org/licenses/gpl.html

echo "const char *_mightymandel_${1} ="
# strip multiline /* .. */ comments, followed by // .. EOL comments, very hacky
#tr '\n' '@' |
#sed 's|/\*[^*]*\*/|\n|g' |
#sed 's|//[^@]*|\n|g' |
#tr '@' '\n' |
#sed 's/^ *//g' |
#tr -s '\n ' |
#sed 's/ = /=/g' |
sed 's|\\|\\\\|g' |
sed 's|"|\\"|g' |
#sed 's|^\(\#.*\)$|\\n\1\\n|' |
sed 's|^|"|' |
sed 's|$|\\n"|'
echo '"\n"'
echo ";"

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef MIGHTYMANDEL_PRIVATE_H
#define MIGHTYMANDEL_PRIVATE_H 1

#include "mightymandel.h"

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <pthread.h>
#include <unistd.h>

struct mightymandel_options_s {
  mightymandel_t *context;
  int64_t width;
  int64_t height;
  double escape_radius;
  double sharpness;
  mpfr_t center_re;
  mpfr_t center_im;
  mpfr_t radius;
  GLuint texture;
};

enum mightymandel_state_e
  { mightymandel_state_idle = 0
  , mightymandel_state_active
  };
typedef enum mightymandel_state_e mightymandel_state_t;

enum mightymandel_renderer_e
  { mightymandel_renderer_none = 0
  , mightymandel_renderer_fp32
  };
typedef enum mightymandel_renderer_e mightymandel_renderer_t;

struct fp32_s;
typedef struct fp32_s fp32_t;

struct mightymandel_s {
  // library thread arguments
  GLFWwindow *parent;
  int64_t cpu_memory_limit;
  int64_t gpu_memory_limit;

  // library thread
  pthread_t thread;

  // big global lock
  pthread_mutex_t mutex;
  pthread_cond_t cond;
  bool init_error;

  // shared state (only write when locked)
  mightymandel_state_t state;
  bool quit_request;
  bool start_request;
  const mightymandel_options_t *start_options;
  bool stop_request;
  bool refresh_request;
  GLsync refresh_sync;

  // GL state
  GLFWwindow *window;
  GLDEBUGPROC debug_callback;
  GLvoid *debug_user;
  int debug_error_limit;
  int debug_error_count;

  // options from last render
  mightymandel_options_t *options;

  // memory management state
  int64_t cpu_memory_used;
  int64_t gpu_memory_used;

  // renderers
  mightymandel_renderer_t renderer;
  fp32_t *fp32;
};

void *_mightymandel_malloc(mightymandel_t *context, int64_t bytes);
void _mightymandel_free(mightymandel_t *context, void *mem, int64_t bytes);
bool _mightymandel_gc(mightymandel_t *context, int64_t bytes);
bool _mightymandel_gpu_alloc(mightymandel_t * context, int64_t bytes);
void _mightymandel_gpu_free(mightymandel_t *context, int64_t bytes);

void *_mightymandel_main(void *arg);
void _mightymandel_init(mightymandel_t *context);
void _mightymandel_render(mightymandel_t *context, const mightymandel_options_t *options);
mightymandel_options_t *mightymandel_options_copy(const mightymandel_options_t *options);

bool _mightymandel_refresh(mightymandel_t *context, GLuint buffer);

GLuint _mightymandel_compute_shader(const char *label, const char *compute_src);
GLuint _mightymandel_vertex_fragment_shader(const char *label, const char *vertex_src, const char *fragment_src);
void _mightymandel_fp32_init(mightymandel_t *context);
void _mightymandel_fp32_render(mightymandel_t *context, const mightymandel_options_t *options);

#define _mightymandel_assert(e) if (! (e)) { \
  char message[1000]; \
  snprintf(message, 1000, "assertion failure: %s:%d %s(): %s", __FILE__, __LINE__, __FUNCTION__, #e); \
  glDebugMessageInsert(GL_DEBUG_SOURCE_THIRD_PARTY, GL_DEBUG_TYPE_ERROR, 0xDEADDEAD, GL_DEBUG_SEVERITY_HIGH, -1, message); \
  sleep(-1); \
  assert(e); \
}

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "mightymandel_private.h"

#include <stdlib.h>
#include <string.h>

void _mightymandel_debug_program(const char *label, GLuint program) {
  GLint status;
  glGetProgramiv(program, GL_LINK_STATUS, &status);
  GLint length = 0;
  glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    _mightymandel_assert(info);
    info[0] = 0;
    glGetProgramInfoLog(program, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    char *message = malloc(1000 + length);
    _mightymandel_assert(message);
    snprintf(message, 1000 + length, "program %s link info:\n%s", label, info ? info : "(no info log)");
    glDebugMessageInsert
      ( GL_DEBUG_SOURCE_THIRD_PARTY
      , status ? GL_DEBUG_TYPE_OTHER : GL_DEBUG_TYPE_ERROR
      , program
      , status ? GL_DEBUG_SEVERITY_NOTIFICATION : GL_DEBUG_SEVERITY_HIGH
      , -1
      , message
      );
    free(message);
  }
  if (info) {
    free(info);
  }
}

void _mightymandel_debug_shader(const char *label, GLuint shader, GLenum type, const char *source) {
  GLint status;
  glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  GLint length = 0;
  glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
  char *info = 0;
  if (length) {
    info = malloc(length + 1);
    _mightymandel_assert(info);
    info[0] = 0;
    glGetShaderInfoLog(shader, length, 0, info);
    info[length] = 0;
  }
  if ((info && info[0]) || ! status) {
    int mlen = 1000 + length + (source ? strlen(source) : 0);
    char *message = malloc(mlen);
    _mightymandel_assert(message);
    const char *type_str = "unknown";
    switch (type) {
      case GL_VERTEX_SHADER: type_str = "vertex"; break;
      case GL_FRAGMENT_SHADER: type_str = "fragment"; break;
      case GL_COMPUTE_SHADER: type_str = "compute"; break;
    }
    snprintf(message, mlen, "%s shader %s compile info:\n%s\nshader source:\n%s", type_str, label, info ? info : "(no info log)", source ? source : "(no source)");
    glDebugMessageInsert
      ( GL_DEBUG_SOURCE_THIRD_PARTY
      , status ? GL_DEBUG_TYPE_OTHER : GL_DEBUG_TYPE_ERROR
      , shader
      , status ? GL_DEBUG_SEVERITY_NOTIFICATION : GL_DEBUG_SEVERITY_HIGH
      , -1
      , message
      );
    free(message);
  }
  if (info) {
    free(info);
  }
}

GLuint _mightymandel_vertex_fragment_shader(const char *label, const char *vert, const char *frag) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(shader, 1, &vert, 0);
    glCompileShader(shader);
    _mightymandel_debug_shader(label, shader, GL_VERTEX_SHADER, vert);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  {
    GLuint shader = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(shader, 1, &frag, 0);
    glCompileShader(shader);
    _mightymandel_debug_shader(label, shader, GL_FRAGMENT_SHADER, frag);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  glObjectLabel(GL_PROGRAM, program, -1, label);
  _mightymandel_debug_program(label, program);
  return program;
}

GLuint _mightymandel_compute_shader(const char *label, const char *comp) {
  GLuint program = glCreateProgram();
  {
    GLuint shader = glCreateShader(GL_COMPUTE_SHADER);
    glShaderSource(shader, 1, &comp, 0);
    glCompileShader(shader);
    _mightymandel_debug_shader(label, shader, GL_COMPUTE_SHADER, comp);
    glAttachShader(program, shader);
    glDeleteShader(shader);
  }
  glLinkProgram(program);
  glObjectLabel(GL_PROGRAM, program, -1, label);
  _mightymandel_debug_program(label, program);
  return program;
}


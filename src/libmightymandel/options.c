// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

// public options API, called from either thread

#include "mightymandel_private.h"

#include <assert.h>
#include <stdlib.h>

extern mightymandel_options_t *mightymandel_options_new(mightymandel_t *context) {
  mightymandel_options_t *options = malloc(sizeof(*options));
  if (! options) {
    return 0;
  }
  options->context = context;
  options->width = 0;
  options->height = 0;
  options->escape_radius = 25.0;
  options->sharpness = 0.01;
  options->texture = 0;
  mpfr_init2(options->center_re, 53);
  mpfr_init2(options->center_im, 53);
  mpfr_init2(options->radius, 53);
  return options;
}

extern void mightymandel_options_delete(mightymandel_options_t *options) {
  assert(options);
  mpfr_clear(options->center_re);
  mpfr_clear(options->center_im);
  mpfr_clear(options->radius);
  free(options);
}

extern void mightymandel_options_set_width(mightymandel_options_t *options, int64_t value) {
  assert(options);
  options->width = value;
}

extern int64_t mightymandel_options_get_width(const mightymandel_options_t *options) {
  assert(options);
  return options->width;
}

extern void mightymandel_options_set_height(mightymandel_options_t *options, int64_t value) {
  assert(options);
  options->height = value;
}

extern int64_t mightymandel_options_get_height(const mightymandel_options_t *options) {
  assert(options);
  return options->height;
}

extern void mightymandel_options_set_escape_radius(mightymandel_options_t *options, double value) {
  assert(options);
  options->escape_radius = value;
}

extern double mightymandel_options_get_escape_radius(const mightymandel_options_t *options) {
  assert(options);
  return options->escape_radius;
}

extern void mightymandel_options_set_sharpness(mightymandel_options_t *options, double value) {
  assert(options);
  options->sharpness = value;
}

extern double mightymandel_options_get_sharpness(const mightymandel_options_t *options) {
  assert(options);
  return options->sharpness;
}

extern void mightymandel_options_set_center_re(mightymandel_options_t *options, const mpfr_t value) {
  assert(options);
  mpfr_set_prec(options->center_re, mpfr_get_prec(value));
  mpfr_set(options->center_re, value, MPFR_RNDN);
}

extern void mightymandel_options_get_center_re(const mightymandel_options_t *options, mpfr_t value) {
  assert(options);
  mpfr_set_prec(value, mpfr_get_prec(options->center_re));
  mpfr_set(value, options->center_re, MPFR_RNDN);
}

extern void mightymandel_options_set_center_im(mightymandel_options_t *options, const mpfr_t value) {
  assert(options);
  mpfr_set_prec(options->center_im, mpfr_get_prec(value));
  mpfr_set(options->center_im, value, MPFR_RNDN);
}

extern void mightymandel_options_get_center_im(const mightymandel_options_t *options, mpfr_t value) {
  assert(options);
  mpfr_set_prec(value, mpfr_get_prec(options->center_im));
  mpfr_set(value, options->center_im, MPFR_RNDN);
}

extern void mightymandel_options_set_radius(mightymandel_options_t *options, const mpfr_t value) {
  assert(options);
  mpfr_set_prec(options->radius, mpfr_get_prec(value));
  mpfr_set(options->radius, value, MPFR_RNDN);
}

extern void mightymandel_options_get_radius(const mightymandel_options_t *options, mpfr_t value) {
  assert(options);
  mpfr_set_prec(value, mpfr_get_prec(options->radius));
  mpfr_set(value, options->radius, MPFR_RNDN);
}

extern void mightymandel_options_set_results_texture(mightymandel_options_t *options, GLuint value) {
  assert(options);
  options->texture = value;
}

extern GLuint mightymandel_options_get_results_texture(const mightymandel_options_t *options) {
  assert(options);
  return options->texture;
}

extern mightymandel_options_t *mightymandel_options_copy(const mightymandel_options_t *options) {
  assert(options);
  assert(options->context);
  mightymandel_options_t *o = mightymandel_options_new(options->context);
  assert(o);
  o->width = options->width;
  o->height = options->height;
  o->escape_radius = options->escape_radius;
  o->sharpness = options->sharpness;
  o->texture = options->texture;
  mightymandel_options_get_center_re(options, o->center_re);
  mightymandel_options_get_center_im(options, o->center_im);
  mightymandel_options_get_radius(options, o->radius);
  return o;
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#version 430 core

layout(location = 4) uniform uint passbit;

layout(std430, binding = 1) buffer b_context {
  readonly restrict uint active_in;
};

layout(std430, binding = 4) buffer b_index {
  restrict uint index[];
};

layout(local_size_x = 1024) in;

void main() {
  uint i = gl_GlobalInvocationID.x;
  if (i < active_in) {
    uint x = index[i];
    if ((i & passbit) == passbit) {
      uint j = (i | (passbit - 1)) - passbit;
      if (j < active_in) {
        x += index[j];
        index[i] = x;
      }
    }
  }
}

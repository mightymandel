// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#version 430 core

layout(location = 2) uniform uvec3 size;

layout(std430, binding = 1) buffer b_context {
  readonly restrict uint active_in;
};

layout(std430, binding = 3) buffer b_input {
  readonly restrict uvec4 iput[];
};

layout(std430, binding = 4) buffer b_index {
  readonly restrict uint index[];
};

layout(std430, binding = 5) buffer b_output {
  writeonly restrict uvec4 ouput[];
};

layout(local_size_x = 1024) in;

void main() {
  uint i = gl_GlobalInvocationID.x;
  if (i < active_in) {
    uvec4 me = iput[i];
    if (me.z < size.z) {
      uint j = index[i] - 1;
      ouput[j] = me;
    }
  }
}

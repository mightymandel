// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#version 430 core

layout(location = 2) uniform uvec3 size;
layout(location = 3) uniform vec4 view;

layout(std430, binding = 1) buffer b_context {
  readonly  restrict uint active_in;
  writeonly restrict uint escaped;
};

layout(std430, binding = 2) buffer b_results {
  writeonly restrict vec4 results[];
};

layout(std430, binding = 3) buffer b_state {
  writeonly restrict uvec4 state[];
};

layout(local_size_x = 1024) in;

void main() {
  uint id = gl_GlobalInvocationID.x;
  if (id < size.z) {
    uint i = id % size.x;
    uint j = id / size.x;
    float x = (float(i) + 0.5) / float(size.x) - 0.5;
    float y = (float(j) + 0.5) / float(size.y) - 0.5;
    vec2 c = vec2(x, y) * 2.0 * vec2(float(size.x) / float(size.y), 1.0) * view.z + view.xy;
    // https://en.wikipedia.org/wiki/Mandelbrot_set#Cardioid_.2F_bulb_checking
    float z = c.x - 0.25;
    float w = c.x + 1.0;
    float q = z * z + c.y * c.y;
    bool interior
      =  q * (q + z) < 0.25 * c.y * c.y // cardioid
      || w * w + c.y * c.y < 0.0625; // circle
    uvec4 me;
    me.x = 0;
    me.y = 0;
    me.z = interior ? size.z : id;
    me.w = 0;
    state[id] = me;
    results[id] = vec4(interior ? -1.0 : 0.0, vec3(0.0));
    if (interior) {
      escaped = 1;
    }
  }
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

// context thread

#include "mightymandel_private.h"

#include <stdlib.h>

#define CHECK(e) do{ \
  int pthreading_error = (e); \
  _mightymandel_assert(! pthreading_error); \
}while(0)

void *_mightymandel_main(void *arg) {
  mightymandel_t *context = arg;
  CHECK(pthread_mutex_lock(&context->mutex));
  _mightymandel_init(context);
fprintf(stderr, "\t\tsignalling startup\n");
  CHECK(pthread_cond_signal(&context->cond));
  bool running = true;
  while (running) {
fprintf(stderr, "\t\tawaiting wakeup\n");
    CHECK(pthread_cond_wait(&context->cond, &context->mutex));
    if (context->quit_request) {
fprintf(stderr, "\t\tquit signal received\n");
      context->quit_request = false;
      running = false;
    } else if (context->start_request) {
fprintf(stderr, "\t\tstart signal received\n");
      context->start_request = false;
      if (context->options) {
        mightymandel_options_delete(context->options);
      }
      context->options = mightymandel_options_copy(context->start_options);
      context->start_options = 0;
      context->state = mightymandel_state_active;
fprintf(stderr, "\t\tsignalling started\n");
      CHECK(pthread_cond_signal(&context->cond));
      CHECK(pthread_mutex_unlock(&context->mutex));

      _mightymandel_render(context, context->options);

      CHECK(pthread_mutex_lock(&context->mutex));
      if (context->stop_request) {
        context->stop_request = false;
fprintf(stderr, "\t\tsignalling stopped\n");
        CHECK(pthread_cond_signal(&context->cond));
      }
      context->state = mightymandel_state_idle;
    }
  }
  glfwMakeContextCurrent(0);
  CHECK(pthread_mutex_unlock(&context->mutex));
fprintf(stderr, "\t\tdone\n");
  return 0;
}

void _mightymandel_debug(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, GLvoid *user) {
  mightymandel_t *context = user;
  const char *source_str = "unknown";
  switch (source) {
    case GL_DEBUG_SOURCE_API:             source_str = "OpenGL";          break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:   source_str = "window system";   break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER: source_str = "shader compiler"; break;
    case GL_DEBUG_SOURCE_THIRD_PARTY:     source_str = "third party";     break;
    case GL_DEBUG_SOURCE_APPLICATION:     source_str = "application";     break;
    case GL_DEBUG_SOURCE_OTHER:           source_str = "application";     break;
  }
  const char *type_str = "unknown";
  switch (type) {
    case GL_DEBUG_TYPE_ERROR:       type_str = "error";       break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR: type_str = "deprecated behavior"; break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:  type_str = "undefined behavior";  break;
    case GL_DEBUG_TYPE_PORTABILITY: type_str = "portability"; break;
    case GL_DEBUG_TYPE_PERFORMANCE: type_str = "performance"; break;
    case GL_DEBUG_TYPE_MARKER:      type_str = "marker";      break;
    case GL_DEBUG_TYPE_PUSH_GROUP:  type_str = "push group";  break;
    case GL_DEBUG_TYPE_POP_GROUP:   type_str = "pop group";   break;
    case GL_DEBUG_TYPE_OTHER:       type_str = "other";       break;
  }
  const char *severity_str = "unknown";
  switch (severity) {
    case GL_DEBUG_SEVERITY_HIGH:         severity_str = "high";         break;
    case GL_DEBUG_SEVERITY_MEDIUM:       severity_str = "medium";       break;
    case GL_DEBUG_SEVERITY_LOW:          severity_str = "low";          break;
    case GL_DEBUG_SEVERITY_NOTIFICATION: severity_str = "notification"; break;
  }
  bool should_print = true;
  if (context && context->debug_callback) {
    context->debug_callback(source, type, id, severity, length, message, context->debug_user);
    should_print = GL_DEBUG_SEVERITY_HIGH && type == GL_DEBUG_TYPE_ERROR;
  }
  if (should_print) {
    fprintf(stderr, "libmightymandel: %s %s %u %s: %s\n", source_str, type_str, id, severity_str, message);
  }
  if (severity == GL_DEBUG_SEVERITY_HIGH && type == GL_DEBUG_TYPE_ERROR) {
    if (++(context->debug_error_count) > context->debug_error_limit) {
      abort();
    }
  }
}

void _mightymandel_init(mightymandel_t *context) {
  assert(context);
  assert(context->window);
  glfwMakeContextCurrent(context->window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError();
  glEnable(GL_DEBUG_OUTPUT);
  glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
  glDebugMessageCallback(_mightymandel_debug, context);
  _mightymandel_fp32_init(context);
}


void _mightymandel_render(mightymandel_t *context, const mightymandel_options_t *options) {
  _mightymandel_assert(context);
  _mightymandel_assert(options);
  _mightymandel_fp32_render(context, options);
}

bool _mightymandel_refresh(mightymandel_t *context, GLuint buffer) {
  _mightymandel_assert(context);
  CHECK(pthread_mutex_lock(&context->mutex));
  bool retval;
  if (context->quit_request || context->stop_request) {
    retval = true;
  } else if (context->refresh_request) {
    retval = false;
    context->refresh_request = false;
    _mightymandel_assert(buffer);
    _mightymandel_assert(context->options);
    _mightymandel_assert(context->options->texture);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, buffer);
    glBindTexture(GL_TEXTURE_2D, context->options->texture);
    glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, context->options->width, context->options->height, GL_RGBA, GL_FLOAT, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindBuffer(GL_PIXEL_UNPACK_BUFFER, 0);
    glMemoryBarrier(GL_ALL_BARRIER_BITS);
    GLsync sync = glFenceSync(GL_SYNC_GPU_COMMANDS_COMPLETE, 0);

    context->refresh_sync = sync;
fprintf(stderr, "\t\tsignalling refreshed\n");
    CHECK(pthread_cond_signal(&context->cond));
  }
  CHECK(pthread_mutex_unlock(&context->mutex));
  return retval;
}

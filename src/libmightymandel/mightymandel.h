// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef MIGHTYMANDEL_H
#define MIGHTYMANDEL_H 1

#include <stdbool.h>
#include <stdint.h>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <mpfr.h>

struct mightymandel_s;
typedef struct mightymandel_s mightymandel_t;

struct mightymandel_options_s;
typedef struct mightymandel_options_s mightymandel_options_t;

extern mightymandel_t *mightymandel_new(GLFWwindow *window, GLDEBUGPROC debug_callback, GLvoid *debug_user, int64_t cpu_memory_limit, int64_t gpu_memory_limit);
extern void mightymandel_delete(mightymandel_t *context);

extern mightymandel_options_t *mightymandel_options_new(mightymandel_t *context);
extern void mightymandel_options_delete(mightymandel_options_t *options);
extern mightymandel_options_t *mightymandel_options_copy(const mightymandel_options_t *options);
extern void mightymandel_options_set_width(mightymandel_options_t *options, int64_t value);
extern int64_t mightymandel_options_get_width(const mightymandel_options_t *options);
extern void mightymandel_options_set_height(mightymandel_options_t *options, int64_t value);
extern int64_t mightymandel_options_get_height(const mightymandel_options_t *options);
extern void mightymandel_options_set_escape_radius(mightymandel_options_t *options, double value);
extern double mightymandel_options_get_escape_radius(const mightymandel_options_t *options);
extern void mightymandel_options_set_sharpness(mightymandel_options_t *options, double value);
extern double mightymandel_options_get_sharpness(const mightymandel_options_t *options);
extern void mightymandel_options_set_center_re(mightymandel_options_t *options, const mpfr_t value);
extern void mightymandel_options_get_center_re(const mightymandel_options_t *options, mpfr_t value);
extern void mightymandel_options_set_center_im(mightymandel_options_t *options, const mpfr_t value);
extern void mightymandel_options_get_center_im(const mightymandel_options_t *options, mpfr_t value);
extern void mightymandel_options_set_radius(mightymandel_options_t *options, const mpfr_t value);
extern void mightymandel_options_get_radius(const mightymandel_options_t *options, mpfr_t value);
extern void mightymandel_options_set_results_texture(mightymandel_options_t *options, GLuint tex);
extern GLuint mightymandel_options_get_results_texture(const mightymandel_options_t *options);

extern void mightymandel_start(mightymandel_t *context, const mightymandel_options_t *options);
extern void mightymandel_stop(mightymandel_t *context);
extern void mightymandel_wait(mightymandel_t *context);
extern bool mightymandel_wait_timeout(mightymandel_t *context, double timeout);
extern void mightymandel_refresh(mightymandel_t *context);

#endif

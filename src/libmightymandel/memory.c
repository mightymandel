// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "mightymandel_private.h"

#include <assert.h>
#include <stdlib.h>

void *_mightymandel_malloc(mightymandel_t *context, int64_t bytes) {
  _mightymandel_assert(context);
  if (context->cpu_memory_limit) {
    if (context->cpu_memory_used + bytes > context->cpu_memory_limit) {
      if (_mightymandel_gc(context, bytes)) {
        return 0;
      }
    }
  }
  void *mem = malloc(bytes);
  if (mem) {
    context->cpu_memory_used += bytes;
  }
  return mem;
}

void _mightymandel_free(mightymandel_t *context, void *mem, int64_t bytes) {
  _mightymandel_assert(context);
  _mightymandel_assert(mem);
  free(mem);
  context->cpu_memory_used -= bytes;
}

bool _mightymandel_gc(mightymandel_t *context, int64_t bytes) {
  return true; // failure
(void) context;
(void) bytes;
}

bool _mightymandel_gpu_alloc(mightymandel_t * context, int64_t bytes) {
  _mightymandel_assert(context);
  if (context->gpu_memory_limit) {
    if (context->gpu_memory_used + bytes > context->gpu_memory_limit) {
      return false;
    }
  }
  context->gpu_memory_used += bytes;
  return true;
}

void _mightymandel_gpu_free(mightymandel_t *context, int64_t bytes) {
  _mightymandel_assert(context);
  context->gpu_memory_used -= bytes;
}

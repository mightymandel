// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#version 430 core

layout(std430, binding = 1) buffer b_context {
            restrict uint active_in;
            restrict uint escaped;
};

layout(std430, binding = 6) buffer b_workgroup {
  writeonly restrict uint workgroup_x;
  writeonly restrict uint workgroup_y;
  writeonly restrict uint workgroup_z;
};

layout(std430, binding = 4) buffer b_index {
  readonly restrict uint index[];
};

layout(local_size_x = 1) in;

void main() {
  if (gl_GlobalInvocationID.x == 0) {
    uint count = 0;
    if (escaped > 0) {
      if (active_in > 0) {
        count = index[active_in - 1];
      }
    } else {
      count = active_in;
    }
    active_in = count;
    escaped = 0;
    uint groups = (count + 1023) / 1024;
    groups = groups == 0 ? 1 : groups;
    workgroup_x = groups;
    workgroup_y = 1;
    workgroup_z = 1;
  }
}

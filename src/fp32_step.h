// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FP32_STEP_H
#define FP32_STEP_H 1

#include <GL/glew.h>

struct fp32_step {
  GLuint program;
  GLint er2;
  GLint cne0;
  GLint zdz0;
  GLuint vao;
};

void fp32_step_begin(struct fp32_step *s);
void fp32_step_end(struct fp32_step *s);
void fp32_step_start(struct fp32_step *s, GLuint vbo, double escaperadius2);
void fp32_step_do(struct fp32_step *s, GLuint *active_count, GLuint vbo, GLuint query);

#endif

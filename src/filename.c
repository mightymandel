// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <string.h>
#include <time.h>

#include "filename.h"

void filename_begin(struct filename *s) {
  time(&s->t);
#ifndef MIGHTYMANDEL_WIN32
  localtime_r(&s->t, &s->tm);
#else
  memset(&s->tm, 0, sizeof(s->tm));
#endif
}

char *filename_name(struct filename *s, const char *ext, int seqno, int tilex, int tiley) {
  char name[1024];
  if (tilex == -1 && tiley == -1) {
    snprintf(name, 1000, "mightymandel_%04d-%02d-%02d_%02d-%02d-%02d_%04d.%s", 1900+s->tm.tm_year, 1+s->tm.tm_mon, s->tm.tm_mday, s->tm.tm_hour, s->tm.tm_min, s->tm.tm_sec, seqno, ext);
  } else {
    snprintf(name, 1000, "mightymandel_%04d-%02d-%02d_%02d-%02d-%02d_%04d_%02d_%02d.%s", 1900+s->tm.tm_year, 1+s->tm.tm_mon, s->tm.tm_mday, s->tm.tm_hour, s->tm.tm_min, s->tm.tm_sec, seqno, tiley, tilex, ext);
  }
  return strdup(name);
}

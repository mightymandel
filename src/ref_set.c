// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file ref_set.c
\brief Collection of reference coordinates.
*/

#include <stdlib.h>

#include "logging.h"
#include "ref_set.h"

/*!
\brief Create a new reference set.

\return A new reference set.
*/
struct ref_set *ref_set_new() {
  struct ref_set *s = (struct ref_set *) calloc(1, sizeof(struct ref_set));
  debug_message("ref_set_new s: %p\n", s);
  return s;
}

/*!
\brief Create a new reference set.

\param s The reference set to delete.
*/
void ref_set_delete(struct ref_set *s) {
  debug_message("ref_set_delete s: %p\n", s);
  struct ref_set_node *node = s->set;
  while (node) {
    struct ref_set_node *next = node->next;
    mpfr_clear(node->x);
    mpfr_clear(node->y);
    free(node);
    node = next;
  }
  free(s);
}

/*!
\brief Insert a point into a reference set.

You should check membership before insertion to avoid duplicates.

\param s The reference set.
\param x The real coordinate of the reference.
\param y The imaginary coordinate of the reference.
*/
void ref_set_insert(struct ref_set *s, const mpfr_t x, const mpfr_t y) {
  debug_message("ref_set_insert x: %Re\n", x);
  debug_message("ref_set_insert y: %Re\n", y);
  struct ref_set_node *node = (struct ref_set_node *) calloc(1, sizeof(struct ref_set_node));
  node->next = s->set;
  mpfr_init2(node->x, mpfr_get_prec(x));
  mpfr_init2(node->y, mpfr_get_prec(y));
  mpfr_set(node->x, x, GMP_RNDN);
  mpfr_set(node->y, y, GMP_RNDN);
  s->set = node;
}

/*!
\brief Check if a point is in a reference set.

\param s The reference set.
\param x The real coordinate of the reference.
\param y The imaginary coordinate of the reference.
\return true when the set contains the reference, false otherwise.
*/
bool ref_set_contains(const struct ref_set *s, const mpfr_t x, const mpfr_t y) {
  struct ref_set_node *node = s->set;
  while (node) {
    if (mpfr_equal_p(x, node->x) && mpfr_equal_p(y, node->y)) {
      return true;
    }
    node = node->next;
  }
  return false;
}

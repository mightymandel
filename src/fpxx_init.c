// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>

#include "fpxx_init.h"
#include "shader.h"
#include "logging.h"
#include "mightymandel.h"

extern const char *fpxx_init_vert;
extern const char *fpxx_init_geom;
extern const char *fpxx_init_frag;
const GLchar *fpxx_init_varyings[] = {"cne", "zdz", "err"};

void fpxx_init_begin(struct fpxx_init *s) {
  s->program = compile_program_tf("fpxx_init", fpxx_init_vert, fpxx_init_geom, fpxx_init_frag, 3, fpxx_init_varyings);
  s->radius = glGetUniformLocation(s->program, "radius");
  s->center = glGetUniformLocation(s->program, "center");
  s->aspect = glGetUniformLocation(s->program, "aspect");
  s->pass = glGetUniformLocation(s->program, "pass");
  s->c = glGetAttribLocation(s->program, "c");
  glGenVertexArrays(1, &s->vao);
}

void fpxx_init_end(struct fpxx_init *s) {
  glDeleteVertexArrays(1, &s->vao);
  glDeleteProgram(s->program);
}

void fpxx_init_do(struct fpxx_init *s, GLuint *active_count, GLuint *vbo, GLuint query, int pass, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius, const mpfr_t refx, const mpfr_t refy) {
  mpfr_t dx, dy;
  mpfr_inits2(mpfr_get_prec(refx), dx, dy, (mpfr_ptr) 0);
  mpfr_sub(dx, centerx, refx, MPFR_RNDN);
  mpfr_sub(dy, centery, refy, MPFR_RNDN);
  debug_message("init dx: %Re\n", dx);
  debug_message("init dy: %Re\n", dy);
  *active_count = width * height;
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);D;
  glUseProgram(s->program);D;
  glUniform1i(s->pass, pass);D;
  glUniform1d(s->radius, mpfr_get_d(radius, MPFR_RNDN));D;
  glUniform2d(s->center, mpfr_get_d(dx, MPFR_RNDN), mpfr_get_d(dy, MPFR_RNDN));D;
  glUniform1d(s->aspect, height / (double) width);D;
  glVertexAttribPointer(s->c, 4, GL_FLOAT, GL_FALSE, 0, 0);D;
  glEnableVertexAttribArray(s->c);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[1]);D;
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);D;
  glBeginTransformFeedback(GL_POINTS);D;
  glDrawArrays(GL_POINTS, 0, *active_count);D;
  glEndTransformFeedback();D;
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
  int before = *active_count;
  glGetQueryObjectuiv(query, GL_QUERY_RESULT, active_count);D;
  int after = *active_count;
  debug_message("init active_count: %d -> %d\n", before, after);
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
  glUseProgram(0);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);
  mpfr_clears(dx, dy, (mpfr_ptr) 0);
  debug_message("VBO init: %d -> %d\n", vbo[0], vbo[1]);
}

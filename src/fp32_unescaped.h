// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FP32_UNESCAPED_H
#define FP32_UNESCAPED_H 1

#include <GL/glew.h>

struct fp32_unescaped {
  GLuint program;
  GLint cne0;
  GLint zdz0;
  GLuint vao;
};

void fp32_unescaped_begin(struct fp32_unescaped *s);
void fp32_unescaped_end(struct fp32_unescaped *s);
void fp32_unescaped_start(struct fp32_unescaped *s, GLuint vbo);
void fp32_unescaped_do(struct fp32_unescaped *s, GLuint *unescaped, GLuint active_count, GLuint vbo, GLuint query);

#endif

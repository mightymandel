// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

uniform double radius;
uniform dvec2  center;
in  vec4 c;
out dvec4 cne1;
#ifdef DE
out dvec4 zdz1;
#else
out dvec2 zdz1;
#endif
out dvec2 err1;
out vec4 c1;
void main() {
  dvec2 c0 = radius * dvec2(c.xy) + center;
  cne1 = dvec4(c0, 0.0, 0.0);
#ifdef DE
  zdz1 = dvec4(c0, 0.0, 0.0);
#else
  zdz1 = dvec2(c0);
#endif
  err1 = dvec2(0.0);
  c1 = c;
}

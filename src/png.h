// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PNG_H
#define PNG_H 1

#include <stdbool.h>

/*!
\brief Determine length of a serialized PNG iTXt chunk.

\param keyword The keyword for the chunk.
\param text The text for the chunk.
\return The length of the chunk.
*/
int png_iTXt_length(const char *keyword, const char *text);

/*!
\brief Serialize a PNG iTXt chunk.

http://www.w3.org/TR/PNG/#5Chunk-layout
http://www.w3.org/TR/PNG/#11iTXt

\param keyword The keyword for the chunk.
\param text The text for the chunk.
\param chunk Buffer for output.
\param length Length of the buffer.
\return `true` if the buffer was large enough, `false` otherwise. 
*/
bool png_iTXt(const char *keyword, const char *text, unsigned char *chunk, int length);

#endif

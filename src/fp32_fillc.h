// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FP32_FILLC_H
#define FP32_FILLC_H 1

#include <GL/glew.h>

struct fp32_fillc {
  GLuint program;
  GLint  aspect;
  GLint  slice_offset;
  GLint  ida0;
  GLint  p;
  GLuint vao;
  GLuint vbo;
};

void fp32_fillc_begin(struct fp32_fillc *s);
void fp32_fillc_end(struct fp32_fillc *s);
void fp32_fillc_do(struct fp32_fillc *s, GLuint vbo, GLuint fbo, GLuint tex, GLuint tex2, int width, int height, int pass, int slice, int slicex, int slicey);

#endif

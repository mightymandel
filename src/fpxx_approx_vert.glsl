// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

uniform double er2;
uniform int iters;
#ifdef DE
uniform dvec2 abcuvw[6];
#else
uniform dvec2 abcuvw[3];
#endif
in dvec2 c0;
out dvec4 cne;
#ifdef DE
out dvec4 zdz;
#else
out dvec2 zdz;
#endif
out dvec2 err;
void main() {
  dvec2 c = c0.xy;
  dvec2 c2 = csqr(c);
  dvec2 c3 = cmul(c, c2);
  dvec2 z = cmul(abcuvw[0], c) + cmul(abcuvw[1], c2) + cmul(abcuvw[2], c3);
#ifdef DE
  dvec2 dz = cmul(abcuvw[3], c) + cmul(abcuvw[4], c2) + cmul(abcuvw[5], c3);
#endif
  cne = dvec4(c, double(iters), 0.0);
#ifdef DE
  zdz = dvec4(z, dz);
#else
  zdz = dvec2(z);
#endif
  err = dvec2(0.0);
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file  atom.c
\brief Mandelbrot set atom finding.
*/

#include <mpfr.h>

#include "atom.h"

/*!
\brief Check if a line segment intersects with the positive real axis.

\param ax The x coordinate of the first point.
\param ay The y coordinate of the first point.
\param bx The x coordinate of the second point.
\param by The y coordinate of the second point.
\return 1 if \f$(a,b)\f$ intersects \f$(0,+\infty)\f$, 0 otherwise.
*/
int crosses_positive_real_axis(const mpfr_t ax, const mpfr_t ay, const mpfr_t bx, const mpfr_t by) {
  if (mpfr_sgn(ay) != mpfr_sgn(by)) {
    mpfr_t dx, dy;
    mpfr_inits2(mpfr_get_prec(ax), dx, dy, (mpfr_ptr) 0);
    // d = b - a
    mpfr_sub(dx, bx, ax, MPFR_RNDN);
    mpfr_sub(dy, by, ay, MPFR_RNDN);
    int s = mpfr_sgn(dy);
    // dy = d `cross` a
    mpfr_mul(dy, dy, ax, MPFR_RNDN);
    mpfr_mul(dx, dx, ay, MPFR_RNDN);
    mpfr_sub(dy, dy, dx, MPFR_RNDN);
    int t = mpfr_sgn(dy);
    return s == t;
  }
  return 0;
}

/*!
\brief Check if a square surrounds the origin.

\param ax The x coordinate of the first point.
\param ay The y coordinate of the first point.
\param bx The x coordinate of the second point.
\param by The y coordinate of the second point.
\param cx The x coordinate of the third point.
\param cy The y coordinate of the third point.
\param dx The x coordinate of the fourth point.
\param dy The y coordinate of the fourth point.
\return 1 if \f$(a,b)(b,c)(c,d)(d,a)\f$ surround \f$0\f$, 0 otherwise.
*/
int surrounds_origin(const mpfr_t ax, const mpfr_t ay, const mpfr_t bx, const mpfr_t by, const mpfr_t cx, const mpfr_t cy, const mpfr_t dx, const mpfr_t dy) {
  return 1 & (
    crosses_positive_real_axis(ax, ay, bx, by) +
    crosses_positive_real_axis(bx, by, cx, cy) +
    crosses_positive_real_axis(cx, cy, dx, dy) +
    crosses_positive_real_axis(dx, dy, ax, ay) );
}

/*!
\brief Check if a point escaped.

This is a cheap inaccurate check using exponents.  Inf/NaN are assumed escaped.

\param x The real part of the point.
\param y The imaginary part of the point.
\return 1 if the point escaped, 0 otherwise.
*/
int did_escaped(const mpfr_t x, const mpfr_t y) {
  return (mpfr_regular_p(x) && mpfr_get_exp(x) > 10) || (mpfr_regular_p(y) && mpfr_get_exp(y) > 10) || ! mpfr_number_p(x) || ! mpfr_number_p(y);
}

/*!
\brief Find the lowest period of any atom inside a box.

Uses the Jordan curve method described here:
<http://mrob.com/pub/muency/period.html>.

\param cx The real part of the box center.
\param cy The imaginary part of the box center.
\param r  The box radius.
\param maxperiod The maximum period to check until.
\return 0 if no period was found, otherwise the found period.
*/
unsigned int boxperiod(const mpfr_t cx, const mpfr_t cy, const mpfr_t r, unsigned int maxperiod, void *abort_data, abort_t abort_fn) {
  // allocate box
  mpfr_prec_t prec = mpfr_get_prec(cx);
  mpfr_t v[18];
  for (int i = 0; i < 18; ++i) {
    mpfr_init2(v[i], prec);
  }
  // initialize box
  for (int i = 0; i < 8; ++i) {
    mpfr_set_ui(v[i], 0, MPFR_RNDN);
  }
  mpfr_sub(v[ 8], cx, r, MPFR_RNDN);
  mpfr_sub(v[ 9], cy, r, MPFR_RNDN);
  mpfr_add(v[10], cx, r, MPFR_RNDN);
  mpfr_sub(v[11], cy, r, MPFR_RNDN);
  mpfr_add(v[12], cx, r, MPFR_RNDN);
  mpfr_add(v[13], cy, r, MPFR_RNDN);
  mpfr_sub(v[14], cx, r, MPFR_RNDN);
  mpfr_add(v[15], cy, r, MPFR_RNDN);
  // iterate
  unsigned int period = 0;
  for (unsigned int p = 1; p <= maxperiod; ++p) {
    for (unsigned int i = 0; i < 8; i += 2) {
      // z = z^2 + c
      mpfr_sqr(v[16], v[i], MPFR_RNDN);
      mpfr_sqr(v[17], v[i+1], MPFR_RNDN);
      mpfr_mul(v[i+1], v[i], v[i+1], MPFR_RNDN);
      mpfr_mul_2ui(v[i+1], v[i+1], 1, MPFR_RNDN);
      mpfr_sub(v[i], v[16], v[17], MPFR_RNDN);
      mpfr_add(v[i], v[i], v[i+8], MPFR_RNDN);
      mpfr_add(v[i+1], v[i+1], v[i+9], MPFR_RNDN);
      // check escaped
      if (did_escaped(v[i], v[i+1])) {
        goto done;
      }
    }
    if (surrounds_origin(v[0], v[1], v[2], v[3], v[4], v[5], v[6], v[7])) {
      period = p;
      goto done;
    }
    if (abort_fn(abort_data)) { break; }
  }
done:
  for (int i = 0; i < 18; ++i) {
    mpfr_clear(v[i]);
  }
  return period;
}

/*!
\brief Find an atom of a given period.

Uses Newton's method as described here:
<http://mathr.co.uk/mandelbrot/bonds.pdf>.
The method is iterated with increased precision until the nucleus and
\f$\frac{1}{2}\f$ bond are distint, which gives a size estimate and
ensures the nucleus has sufficient precision.

\param period The period of the nucleus.
\param x The real part of the nucleus (input: guess, output: nucleus).
\param y The imaginary part of the nucleus (input: guess, output: nucleus).
\param z The size of the nucleus (output valid when 1 is returned)
\return 1 if the nucleus was accurately determined, 0 otherwise.
*/
int muatom(int period, mpfr_t x, mpfr_t y, mpfr_t z, void *abort_data, abort_t abort_fn) {
  const mpfr_prec_t accuracy = 32;
  mpfr_prec_t bits = mpfr_get_prec(x);
#define VARS nx, ny, bx, by, wx, wy, zz, Ax, Ay, Bx, By, Cx, Cy, Dx, Dy, Ex, Ey, t1, t2, t3, t4, t5, t6, t7, t8, t9, t0, t01, t02, t03, t04
  mpfr_t VARS;
  mpfr_inits2(bits, VARS, (mpfr_ptr) 0);
  mpfr_set(nx, x, MPFR_RNDN);
  mpfr_set(ny, y, MPFR_RNDN);
  mpfr_set_prec(zz, mpfr_get_prec(z));
  int n_ok, w_ok, b_ok;
  int r = 0;
  do {
    mpfr_set_prec(t0,  bits - accuracy);
    mpfr_set_prec(t01, bits - accuracy);
    mpfr_set_prec(t02, bits - accuracy);
    mpfr_set_prec(t03, bits - accuracy);
    mpfr_set_prec(t04, bits - accuracy);
    int limit = 100;
    do {
      // refine nucleus
      mpfr_set_ui(Ax, 0, MPFR_RNDN);
      mpfr_set_ui(Ay, 0, MPFR_RNDN);
      mpfr_set_ui(Dx, 0, MPFR_RNDN);
      mpfr_set_ui(Dy, 0, MPFR_RNDN);
      for (int i = 0; i < period; ++i) {
        if (abort_fn(abort_data)) { goto cleanup; }
        // D <- 2AD + 1  : Dx <- 2 (Ax Dx - Ay Dy) + 1 ; Dy = 2 (Ax Dy + Ay Dx)
        mpfr_mul(t1, Ax, Dx, MPFR_RNDN);
        mpfr_mul(t2, Ay, Dy, MPFR_RNDN);
        mpfr_mul(t3, Ax, Dy, MPFR_RNDN);
        mpfr_mul(t4, Ay, Dx, MPFR_RNDN);
        mpfr_sub(Dx, t1, t2, MPFR_RNDN);
        mpfr_mul_2ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add_ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add(Dy, t3, t4, MPFR_RNDN);
        mpfr_mul_2ui(Dy, Dy, 1, MPFR_RNDN);
        // A <- A^2 + n  : Ax <- Ax^2 - Ay^2 + nx ; Ay <- 2 Ax Ay + ny
        mpfr_sqr(t1, Ax, MPFR_RNDN);
        mpfr_sqr(t2, Ay, MPFR_RNDN);
        mpfr_mul(t3, Ax, Ay, MPFR_RNDN);
        mpfr_sub(Ax, t1, t2, MPFR_RNDN);
        mpfr_add(Ax, Ax, nx, MPFR_RNDN);
        mpfr_mul_2ui(t3, t3, 1, MPFR_RNDN);
        mpfr_add(Ay, t3, ny,MPFR_RNDN);
      }
      // n <- n - A / D  = (nx + ny i) - ((Ax Dx + Ay Dy) + (Ay Dx - Ax Dy)i) / (Dx^2 + Dy^2)
      mpfr_sqr(t1, Dx, MPFR_RNDN);
      mpfr_sqr(t2, Dy, MPFR_RNDN);
      mpfr_add(t1, t1, t2, MPFR_RNDN);
  
      mpfr_mul(t2, Ax, Dx, MPFR_RNDN);
      mpfr_mul(t3, Ay, Dy, MPFR_RNDN);
      mpfr_add(t2, t2, t3, MPFR_RNDN);
      mpfr_div(t2, t2, t1, MPFR_RNDN);
      mpfr_sub(t2, nx, t2, MPFR_RNDN);
  
      mpfr_mul(t3, Ay, Dx, MPFR_RNDN);
      mpfr_mul(t4, Ax, Dy, MPFR_RNDN);
      mpfr_sub(t3, t3, t4, MPFR_RNDN);
      mpfr_div(t3, t3, t1, MPFR_RNDN);
      mpfr_sub(t3, ny, t3, MPFR_RNDN);
  
      mpfr_set(t01, t2, MPFR_RNDN);
      mpfr_set(t02, t3, MPFR_RNDN);
      mpfr_set(t03, nx, MPFR_RNDN);
      mpfr_set(t04, ny, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      mpfr_add_ui(t01, t01, 1, MPFR_RNDN);
      mpfr_sub_ui(t01, t01, 1, MPFR_RNDN);
      mpfr_add_ui(t02, t02, 1, MPFR_RNDN);
      mpfr_sub_ui(t02, t02, 1, MPFR_RNDN);
      n_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      mpfr_set(nx, t2, MPFR_RNDN);
      mpfr_set(ny, t3, MPFR_RNDN);
    } while (! n_ok && --limit);
    if (! limit) goto cleanup;
    mpfr_set(wx, nx, MPFR_RNDN);
    mpfr_set(wy, ny, MPFR_RNDN);
    mpfr_set(bx, nx, MPFR_RNDN);
    mpfr_set(by, ny, MPFR_RNDN);
    limit = 100;
    do {
      // refine bond
      mpfr_set(Ax, wx, MPFR_RNDN);
      mpfr_set(Ay, wy, MPFR_RNDN);
      mpfr_set_ui(Bx, 1, MPFR_RNDN);
      mpfr_set_ui(By, 0, MPFR_RNDN);
      mpfr_set_ui(Cx, 0, MPFR_RNDN);
      mpfr_set_ui(Cy, 0, MPFR_RNDN);
      mpfr_set_ui(Dx, 0, MPFR_RNDN);
      mpfr_set_ui(Dy, 0, MPFR_RNDN);
      mpfr_set_ui(Ex, 0, MPFR_RNDN);
      mpfr_set_ui(Ey, 0, MPFR_RNDN);
      for (int i = 0; i < period; ++i) {
        if (abort_fn(abort_data)) { goto cleanup; }
        // E <- 2 ( A E + B D )
        mpfr_mul(t1, Ax, Ex, MPFR_RNDN);
        mpfr_mul(t2, Ay, Ey, MPFR_RNDN);
        mpfr_sub(t1, t1, t2, MPFR_RNDN);
        mpfr_mul(t2, Ax, Ey, MPFR_RNDN);
        mpfr_mul(t3, Ay, Ex, MPFR_RNDN);
        mpfr_add(t2, t2, t3, MPFR_RNDN);
        mpfr_mul(t3, Bx, Dx, MPFR_RNDN);
        mpfr_mul(t4, By, Dy, MPFR_RNDN);
        mpfr_sub(t3, t3, t4, MPFR_RNDN);
        mpfr_add(Ex, t1, t3, MPFR_RNDN);
        mpfr_mul(t3, Bx, Dy, MPFR_RNDN);
        mpfr_mul(t4, By, Dx, MPFR_RNDN);
        mpfr_add(t3, t3, t4, MPFR_RNDN);
        mpfr_add(Ey, t2, t3, MPFR_RNDN);
        mpfr_mul_2ui(Ex, Ex, 1, MPFR_RNDN);
        mpfr_mul_2ui(Ey, Ey, 1, MPFR_RNDN);
        // D <- 2 A D + 1
        mpfr_mul(t1, Ax, Dx, MPFR_RNDN);
        mpfr_mul(t2, Ay, Dy, MPFR_RNDN);
        mpfr_mul(t3, Ax, Dy, MPFR_RNDN);
        mpfr_mul(t4, Ay, Dx, MPFR_RNDN);
        mpfr_sub(Dx, t1, t2, MPFR_RNDN);
        mpfr_mul_2ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add_ui(Dx, Dx, 1, MPFR_RNDN);
        mpfr_add(Dy, t3, t4, MPFR_RNDN);
        mpfr_mul_2ui(Dy, Dy, 1, MPFR_RNDN);
        // C <- 2 ( B^2 + A C )
        mpfr_sqr(t1, Bx, MPFR_RNDN);
        mpfr_sqr(t2, By, MPFR_RNDN);
        mpfr_sub(t1, t1, t2, MPFR_RNDN);
        mpfr_mul(t2, Bx, By, MPFR_RNDN);
        mpfr_mul_2ui(t2, t2, 1, MPFR_RNDN);
        mpfr_mul(t3, Ax, Cx, MPFR_RNDN);
        mpfr_mul(t4, Ay, Cy, MPFR_RNDN);
        mpfr_sub(t3, t3, t4, MPFR_RNDN);
        mpfr_add(t1, t1, t3, MPFR_RNDN);
        mpfr_mul(t3, Ax, Cy, MPFR_RNDN);
        mpfr_mul(t4, Ay, Cx, MPFR_RNDN);
        mpfr_add(t3, t3, t4, MPFR_RNDN);
        mpfr_add(t2, t2, t3, MPFR_RNDN);
        mpfr_mul_2ui(Cx, t1, 1, MPFR_RNDN);
        mpfr_mul_2ui(Cy, t2, 1, MPFR_RNDN);
        // B <- 2 A B
        mpfr_mul(t1, Ax, Bx, MPFR_RNDN);
        mpfr_mul(t2, Ay, By, MPFR_RNDN);
        mpfr_mul(t3, Ax, By, MPFR_RNDN);
        mpfr_mul(t4, Ay, Bx, MPFR_RNDN);
        mpfr_sub(Bx, t1, t2, MPFR_RNDN);
        mpfr_mul_2ui(Bx, Bx, 1, MPFR_RNDN);
        mpfr_add(By, t3, t4, MPFR_RNDN);
        mpfr_mul_2ui(By, By, 1, MPFR_RNDN);
        // A <- A^2 + b
        mpfr_sqr(t1, Ax, MPFR_RNDN);
        mpfr_sqr(t2, Ay, MPFR_RNDN);
        mpfr_mul(t3, Ax, Ay, MPFR_RNDN);
        mpfr_sub(Ax, t1, t2, MPFR_RNDN);
        mpfr_add(Ax, Ax, bx, MPFR_RNDN);
        mpfr_mul_2ui(t3, t3, 1, MPFR_RNDN);
        mpfr_add(Ay, t3, by,MPFR_RNDN);
      }
      // (w) <- (w) - (B-1  D)^-1 (A - w)
      // (b) <- (b)   ( C   E)    (B + 1)
      //
      // det = (B-1)E - CD
      // inv = (E    -D)
      //       (-C (B-1) / det
      //
      // w - (E(A-w) - D(B+1))/det
      // b - (-C(A-w) + (B-1)(B+1))/det   ; B^2 - 1
      //
      // A -= w
      mpfr_sub(Ax, Ax, wx, MPFR_RNDN);
      mpfr_sub(Ay, Ay, wy, MPFR_RNDN);
      // (t1,t2) = B^2 - 1
      mpfr_mul(t1, Bx, Bx, MPFR_RNDN);
      mpfr_mul(t2, By, By, MPFR_RNDN);
      mpfr_sub(t1, t1, t2, MPFR_RNDN);
      mpfr_sub_ui(t1, t1, 1, MPFR_RNDN);
      mpfr_mul(t2, Bx, By, MPFR_RNDN);
      mpfr_mul_2ui(t2, t2, 1, MPFR_RNDN);
      // (t1,t2) -= C(A-w)
      mpfr_mul(t3, Cx, Ax, MPFR_RNDN);
      mpfr_mul(t4, Cy, Ay, MPFR_RNDN);
      mpfr_sub(t3, t3, t4, MPFR_RNDN);
      mpfr_sub(t1, t1, t3, MPFR_RNDN);
      mpfr_mul(t3, Cx, Ay, MPFR_RNDN);
      mpfr_mul(t4, Cy, Ax, MPFR_RNDN);
      mpfr_add(t3, t3, t4, MPFR_RNDN);
      mpfr_sub(t2, t2, t3, MPFR_RNDN);
      // (t3, t4) = (B-1)E - CD
      mpfr_sub_ui(t3, Bx, 1, MPFR_RNDN);
      mpfr_mul(t4, t3, Ex, MPFR_RNDN);
      mpfr_mul(t5, By, Ey, MPFR_RNDN);
      mpfr_sub(t4, t4, t5, MPFR_RNDN);
      mpfr_mul(t5, t3, Ey, MPFR_RNDN);
      mpfr_mul(t6, By, Ex, MPFR_RNDN);
      mpfr_sub(t5, t5, t6, MPFR_RNDN);
      mpfr_mul(t6, Cx, Dx, MPFR_RNDN);
      mpfr_mul(t7, Cy, Dy, MPFR_RNDN);
      mpfr_sub(t6, t6, t7, MPFR_RNDN);
      mpfr_sub(t3, t4, t6, MPFR_RNDN);
      mpfr_mul(t6, Cx, Dy, MPFR_RNDN);
      mpfr_mul(t7, Cy, Dx, MPFR_RNDN);
      mpfr_add(t6, t6, t7, MPFR_RNDN);
      mpfr_sub(t4, t5, t6, MPFR_RNDN);
      // (t3, t4) = 1/(t3, t4)  ; z^-1 = z* / (z z*)
      mpfr_sqr(t5, t3, MPFR_RNDN);
      mpfr_sqr(t6, t4, MPFR_RNDN);
      mpfr_add(t5, t5, t6, MPFR_RNDN);
      mpfr_div(t3, t3, t5, MPFR_RNDN);
      mpfr_div(t4, t4, t5, MPFR_RNDN);
      mpfr_neg(t4, t4, MPFR_RNDN);
      // (t1, t2) *= (t3, t4)
      mpfr_mul(t5, t1, t3, MPFR_RNDN);
      mpfr_mul(t6, t2, t4, MPFR_RNDN);
      mpfr_mul(t7, t1, t4, MPFR_RNDN);
      mpfr_mul(t8, t2, t3, MPFR_RNDN);
      mpfr_sub(t1, t5, t6, MPFR_RNDN);
      mpfr_add(t2, t7, t8, MPFR_RNDN);
  
      // (t1, t2) = b - (t1, t2)
      mpfr_sub(t1, bx, t1, MPFR_RNDN);
      mpfr_sub(t2, by, t2, MPFR_RNDN);
  
      mpfr_set(t01, t1, MPFR_RNDN);
      mpfr_set(t02, t2, MPFR_RNDN);
      mpfr_set(t03, bx, MPFR_RNDN);
      mpfr_set(t04, by, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      mpfr_add_ui(t01, t01, 1, MPFR_RNDN);
      mpfr_sub_ui(t01, t01, 1, MPFR_RNDN);
      mpfr_add_ui(t02, t02, 1, MPFR_RNDN);
      mpfr_sub_ui(t02, t02, 1, MPFR_RNDN);
      b_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      // (t5, t6) = E (A-w)
      mpfr_mul(t5, Ex, Ax, MPFR_RNDN);
      mpfr_mul(t6, Ey, Ay, MPFR_RNDN);
      mpfr_sub(t5, t5, t6, MPFR_RNDN);
      mpfr_mul(t6, Ex, Ay, MPFR_RNDN);
      mpfr_mul(t7, Ey, Ax, MPFR_RNDN);
      mpfr_add(t6, t6, t7, MPFR_RNDN);
      // B += 1
      mpfr_add_ui(Bx, Bx, 1, MPFR_RNDN);
      // (t7, t8) = D (B+1)
      mpfr_mul(t7, Dx, Bx, MPFR_RNDN);
      mpfr_mul(t8, Dy, By, MPFR_RNDN);
      mpfr_sub(t7, t7, t8, MPFR_RNDN);
      mpfr_mul(t8, Dx, By, MPFR_RNDN);
      mpfr_mul(t9, Dy, Bx, MPFR_RNDN);
      mpfr_add(t8, t8, t9, MPFR_RNDN);
      // (t5, t6) -= (t7, t8)
      mpfr_sub(t5, t5, t7, MPFR_RNDN);
      mpfr_sub(t6, t6, t8, MPFR_RNDN);
      // (t7, t8) = (t5, t6) * (t3, t4)
      mpfr_mul(t7, t3, t5, MPFR_RNDN);
      mpfr_mul(t8, t4, t6, MPFR_RNDN);
      mpfr_sub(t7, t7, t8, MPFR_RNDN);
      mpfr_mul(t8, t3, t6, MPFR_RNDN);
      mpfr_mul(t9, t4, t5, MPFR_RNDN);
      mpfr_add(t8, t8, t9, MPFR_RNDN);
  
      // (t3, t4) = w - (t7, t8)
      mpfr_sub(t3, wx, t7, MPFR_RNDN);
      mpfr_sub(t4, wy, t8, MPFR_RNDN);

      mpfr_set(t01, t3, MPFR_RNDN);
      mpfr_set(t02, t4, MPFR_RNDN);
      mpfr_set(t03, wx, MPFR_RNDN);
      mpfr_set(t04, wy, MPFR_RNDN);
      mpfr_sub(t01, t01, t03, MPFR_RNDN);
      mpfr_sub(t02, t02, t04, MPFR_RNDN);
      mpfr_add_ui(t01, t01, 1, MPFR_RNDN);
      mpfr_sub_ui(t01, t01, 1, MPFR_RNDN);
      mpfr_add_ui(t02, t02, 1, MPFR_RNDN);
      mpfr_sub_ui(t02, t02, 1, MPFR_RNDN);
      w_ok = mpfr_zero_p(t01) && mpfr_zero_p(t02);
  
      mpfr_set(bx, t1, MPFR_RNDN);
      mpfr_set(by, t2, MPFR_RNDN);
      mpfr_set(wx, t3, MPFR_RNDN);
      mpfr_set(wy, t4, MPFR_RNDN);
    } while (!(w_ok && b_ok) && --limit);
    if (! limit) goto cleanup;
    // t1 = |n - b|
    mpfr_sub(t1, nx, bx, MPFR_RNDN);
    mpfr_sqr(t1, t1, MPFR_RNDN);
    mpfr_sub(t2, ny, by, MPFR_RNDN);
    mpfr_sqr(t2, t2, MPFR_RNDN);
    mpfr_add(t1, t1, t2, MPFR_RNDN);
    mpfr_sqrt(t1, t1, MPFR_RNDN);
    bits = 2 * bits;
    mpfr_prec_round(nx, bits, MPFR_RNDN);
    mpfr_prec_round(ny, bits, MPFR_RNDN);
    mpfr_prec_round(wx, bits, MPFR_RNDN);
    mpfr_prec_round(wy, bits, MPFR_RNDN);
    mpfr_prec_round(bx, bits, MPFR_RNDN);
    mpfr_prec_round(by, bits, MPFR_RNDN);
    mpfr_set(zz, t1, MPFR_RNDN);
    mpfr_set_prec(Ax, bits);
    mpfr_set_prec(Ay, bits);
    mpfr_set_prec(Bx, bits);
    mpfr_set_prec(By, bits);
    mpfr_set_prec(Cx, bits);
    mpfr_set_prec(Cy, bits);
    mpfr_set_prec(Dx, bits);
    mpfr_set_prec(Dy, bits);
    mpfr_set_prec(Ex, bits);
    mpfr_set_prec(Ey, bits);
    mpfr_set_prec(t1, bits);
    mpfr_set_prec(t2, bits);
    mpfr_set_prec(t3, bits);
    mpfr_set_prec(t4, bits);
    mpfr_set_prec(t5, bits);
    mpfr_set_prec(t6, bits);
    mpfr_set_prec(t7, bits);
    mpfr_set_prec(t8, bits);
    mpfr_set_prec(t9, bits);
  } while (mpfr_zero_p(zz) || bits + mpfr_get_exp(zz) < mpfr_get_prec(zz) + accuracy);

  // copy to output
  r = 1;
cleanup:
  bits = mpfr_get_prec(nx);
  mpfr_set_prec(x, bits);
  mpfr_set_prec(y, bits);
  mpfr_set(x, nx, MPFR_RNDN);
  mpfr_set(y, ny, MPFR_RNDN);
  mpfr_set(z, zz, MPFR_RNDN);
  mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
  return r;
}

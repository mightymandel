// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_GIF_H
#define PARSE_GIF_H 1

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Parse FractInt GIF format.

\copydetails parser
*/
bool parse_gif(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <math.h>
#include <string.h>

#include "logging.h"
#include "interact.h"
#include "utility.h"

void interact_begin(int w, int h, const mpfr_t cx, const mpfr_t cy, const mpfr_t r, double weight, bool show_glitches) {
  memset(&interact, 0, sizeof(struct interact));
  interact.width = w;
  interact.height = h;
  interact.weight = weight;
  interact.show_glitches = show_glitches;
  mpfr_init2(interact.centerx, mpfr_get_prec(cx));
  mpfr_init2(interact.centery, mpfr_get_prec(cy));
  mpfr_init2(interact.radius, mpfr_get_prec(r));
  mpfr_set(interact.centerx, cx, MPFR_RNDN);
  mpfr_set(interact.centery, cy, MPFR_RNDN);
  mpfr_set(interact.radius, r, MPFR_RNDN);
}

void interact_end(void) {
  mpfr_clear(interact.centerx);
  mpfr_clear(interact.centery);
  mpfr_clear(interact.radius);
}

void interact_reset(void) {
  interact.quit = false;
  interact.save_screenshot = false;
  interact.updated = false;
}

static void interact_coord(mpfr_t x, mpfr_t y, double px, double py) {
  pixel_coordinate(x, y, interact.width, interact.height, interact.centerx, interact.centery, interact.radius, px, py);
}

static void interact_center(const mpfr_t x, const mpfr_t y) {
  mpfr_prec_t bits = fmax(53, pxbits(interact.radius, interact.height));
  mpfr_set_prec(interact.centerx, bits);
  mpfr_set(interact.centerx, x, MPFR_RNDN);
  mpfr_set_prec(interact.centery, bits);
  mpfr_set(interact.centery, y, MPFR_RNDN);
  interact.updated = true;
}

static void interact_translate(double dx, double dy) {
  double px = interact.width / 2.0 + dx;
  double py = interact.height / 2.0 + dy;
  mpfr_t x, y;
  mpfr_init2(x, 53);
  mpfr_init2(y, 53);
  pixel_coordinate(x, y, interact.width, interact.height, interact.centerx, interact.centery, interact.radius, px, py);
  interact_center(x, y);
  mpfr_clear(x);
  mpfr_clear(y);
}

static void interact_zoom_about(const mpfr_t x, const mpfr_t y, double factor) {
  double g = pow(sqrt(0.5), factor);
  mpfr_mul_d(interact.radius, interact.radius, g, MPFR_RNDN);
  mpfr_prec_t p = fmax(53, pxbits(interact.radius, interact.height));
  mpfr_prec_round(interact.centerx, p, MPFR_RNDN);
  mpfr_prec_round(interact.centery, p, MPFR_RNDN);
  mpfr_t t;
  mpfr_init2(t, p);
  mpfr_mul_d(t, x, (1 - g), MPFR_RNDN);
  mpfr_mul_d(interact.centerx, interact.centerx, g, MPFR_RNDN);
  mpfr_add(interact.centerx, interact.centerx, t, MPFR_RNDN);
  mpfr_mul_d(t, y, (1 - g), MPFR_RNDN);
  mpfr_mul_d(interact.centery, interact.centery, g, MPFR_RNDN);
  mpfr_add(interact.centery, interact.centery, t, MPFR_RNDN);
  mpfr_clear(t);
  interact.updated = true;
}

static void interact_zoom(double factor) {
  interact_zoom_about(interact.centerx, interact.centery, factor);
}

void interact_mouse(button button, double x, double y, bool shift, bool ctrl) {
  double factor = (shift ? 5 : 1) * (ctrl ? 10 : 1);
  mpfr_t vx, vy;
  mpfr_inits2(53, vx, vy, (mpfr_ptr) 0);
  interact_coord(vx, vy, x, y);
  if (button == b_middle) { interact_center(vx, vy);
  } else if (button == b_left  || button == b_up  ) { interact_zoom_about(vx, vy,  factor);
  } else if (button == b_right || button == b_down) { interact_zoom_about(vx, vy, -factor);
  }
  mpfr_clears(vx, vy, (mpfr_ptr) 0);
}

void interact_keyboard(key key, bool shift, bool ctrl) {
  double factor = (shift ? 5 : 1) * (ctrl ? 10 : 1);
  switch (key) {
    case k_quit: interact.quit = true; break;
    case k_show_glitches:
      interact.show_glitches = ! interact.show_glitches;
      log_message(LOG_NOTICE, "show glitches: %s\n", interact.show_glitches ? "yes" : "no");
      break;
    case k_zoom_in:  interact_zoom( factor); break;
    case k_zoom_out: interact_zoom(-factor); break;
    case k_save_screenshot: interact.save_screenshot = true; break;
    case k_weight_0: interact.weight =  5; break;
    case k_weight_1: interact.weight = -4; break;
    case k_weight_2: interact.weight = -3; break;
    case k_weight_3: interact.weight = -2; break;
    case k_weight_4: interact.weight = -1; break;
    case k_weight_5: interact.weight =  0; break;
    case k_weight_6: interact.weight =  1; break;
    case k_weight_7: interact.weight =  2; break;
    case k_weight_8: interact.weight =  3; break;
    case k_weight_9: interact.weight =  4; break;
    case k_left:  interact_translate(-factor * 10, 0); break;
    case k_right: interact_translate( factor * 10, 0); break;
    case k_up:    interact_translate(0, -factor * 10); break;
    case k_down:  interact_translate(0,  factor * 10); break;
  }
}

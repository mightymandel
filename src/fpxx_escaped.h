// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FPXX_ESCAPED_H
#define FPXX_ESCAPED_H 1

#include <stdbool.h>
#include <GL/glew.h>
#include <mpfr.h>

struct fpxx_escaped {
  GLuint program;
  GLint center;
  GLint radius;
  GLint aspect;
  GLint loger2;
  GLint cne0;
  GLint zdz0;
  GLint err0;
  GLuint vao;
};

void fpxx_escaped_begin(struct fpxx_escaped *s);
void fpxx_escaped_end(struct fpxx_escaped *s);
void fpxx_escaped_start(struct fpxx_escaped *s, GLuint tex, GLuint fbo, GLuint vbo, double escaperadius2, int width, int height, const mpfr_t centerx0, const mpfr_t centery0, const mpfr_t radius0, const mpfr_t refx, const mpfr_t refy);
void fpxx_escaped_do(struct fpxx_escaped *s, GLuint active_count);

#endif

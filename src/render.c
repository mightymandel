// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file render.c
\brief Manage calculation and rendering.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "render.h"
#include "utility.h"
#include "find_ref.h"
#include "atom.h"
#include "logging.h"
#include "mightymandel.h"
#include "image.h"
#include "stopwatch.h"
#include "poll.h"
#include "texture.h"

const char *render_method_name[3] = {
  "fp32",
  "fp64",
  "fpxx"
};

void render_options_init(struct render_options *o) {
  memset(o, 0, sizeof(struct render_options));
  mpfr_init2(o->centerx, 53);
  mpfr_init2(o->centery, 53);
  mpfr_init2(o->radius, 53);
}

void render_options_end(struct render_options *o) {
  mpfr_clear(o->centerx);
  mpfr_clear(o->centery);
  mpfr_clear(o->radius);
}

void render_options_set_location(struct render_options *o, const mpfr_t cx, const mpfr_t cy, const mpfr_t r) {
  int bits = pxbits(r, o->height);
  if (bits < 24) {
    o->method = render_method_fp32;
  } else if (bits < 53) {
    o->method = render_method_fp64;
  } else {
    o->method = render_method_fpxx;
  }
  if (! FP64) {
    o->method = render_method_fp32;
  }
  mpfr_set_prec(o->centerx, mpfr_get_prec(cx));
  mpfr_set_prec(o->centery, mpfr_get_prec(cy));
  mpfr_set_prec(o->radius, mpfr_get_prec(r));
  mpfr_set(o->centerx, cx, MPFR_RNDN);
  mpfr_set(o->centery, cy, MPFR_RNDN);
  mpfr_set(o->radius, r, MPFR_RNDN);
}

void render_options_copy(struct render_options *o, const struct render_options *p) {
  o->method = p->method;
  o->calculate_de = p->calculate_de;
  o->weight = p->weight;
  o->max_glitch = p->max_glitch;
  o->max_blob = p->max_blob;
  o->sharpness = p->sharpness;
  o->show_de = p->show_de;
  o->show_glitches = p->show_glitches;
  o->save_screenshot = p->save_screenshot;
  o->width = p->width;
  o->height = p->height;
  o->win_width = p->win_width;
  o->win_height = p->win_height;
  o->series_approx = p->series_approx;
  o->order = p->order;
  o->slice = p->slice;
  mpfr_set_prec(o->centerx, mpfr_get_prec(p->centerx));
  mpfr_set_prec(o->centery, mpfr_get_prec(p->centery));
  mpfr_set_prec(o->radius, mpfr_get_prec(p->radius));
  mpfr_set(o->centerx, p->centerx, MPFR_RNDN);
  mpfr_set(o->centery, p->centery, MPFR_RNDN);
  mpfr_set(o->radius, p->radius, MPFR_RNDN);
}

void render_begin(struct render *s) {
  if (! s->begun) {
    memset(s, 0, sizeof(struct render));
    render_options_init(&s->options);
    s->render_time = stopwatch_new();

    s->refs = 0;
    s->blobs = 0;

    s->rgb_tex = 0;
    s->tex = 0;
    s->tex2 = 0;
    s->fbo = 0;
    s->query = 0;
    s->vbo[0] = 0;
    s->vbo[1] = 0;
    s->active_count = 0;
    s->escaperadius2 = 65536.0 * 8.0 * log(2.0);

    glClampColor(GL_CLAMP_VERTEX_COLOR, GL_FALSE);glGetError();D;
    glClampColor(GL_CLAMP_READ_COLOR, GL_FALSE);D;
    glClampColor(GL_CLAMP_FRAGMENT_COLOR, GL_FALSE);glGetError();D;
    glGenTextures(1, &s->rgb_tex);D;
    glActiveTexture(GL_TEXTURE0 + TEX_RGB);
    glBindTexture(GL_TEXTURE_2D, s->rgb_tex);D;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);D;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);D;
    glGenTextures(1, &s->tex);D;
    glActiveTexture(GL_TEXTURE0 + TEX_RAW);
    glBindTexture(GL_TEXTURE_2D, s->tex);D;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);D;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);D;
    glGenTextures(1, &s->tex2);D;
    glActiveTexture(GL_TEXTURE0 + TEX_FILLC);
    glBindTexture(GL_TEXTURE_2D, s->tex2);D;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);D;
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);D;
    glGenFramebuffers(1, &s->fbo);D;
    glGenQueries(1, &s->query);D;
    glGenBuffers(2, s->vbo);D;

    fp32_fillc_begin(&s->fp32_fillc);
    fp32_colour_begin(&s->fp32_colour, s->rgb_tex);

    fp32_init_begin(&s->fp32_init);
    fp32_step_begin(&s->fp32_step);
    fp32_unescaped_begin(&s->fp32_unescaped);
    fp32_escaped_begin(&s->fp32_escaped);

    if (FP64) {
      fp64_init_begin(&s->fp64_init);
      fp64_step_begin(&s->fp64_step);
      fp64_unescaped_begin(&s->fp64_unescaped);
      fp64_escaped_begin(&s->fp64_escaped);

      fpxx_init_begin(&s->fpxx_init);
      fpxx_approx_begin(&s->fpxx_approx);
      fpxx_approx2_begin(&s->fpxx_approx2);
      fpxx_step_begin(&s->fpxx_step);
      fpxx_unescaped_begin(&s->fpxx_unescaped);
      fpxx_escaped_begin(&s->fpxx_escaped);
    }

    s->slice_table = slice_table_new();

    s->idle.f = 0;

    s->begun = 1;
  }
}

void render_end(struct render *s) {
  render_options_end(&s->options);
  stopwatch_delete(s->render_time);

  glDeleteTextures(1, &s->rgb_tex);D;
  glDeleteTextures(1, &s->tex);D;
  glDeleteTextures(1, &s->tex2);D;
  glDeleteFramebuffers(1, &s->fbo);D;
  glDeleteQueries(1, &s->query);D;
  glDeleteBuffers(2, s->vbo);D;

  fp32_fillc_end(&s->fp32_fillc);
  fp32_colour_end(&s->fp32_colour);

  fp32_init_end(&s->fp32_init);
  fp32_step_end(&s->fp32_step);
  fp32_unescaped_end(&s->fp32_unescaped);
  fp32_escaped_end(&s->fp32_escaped);

  if (FP64) {
    fp64_init_end(&s->fp64_init);
    fp64_step_end(&s->fp64_step);
    fp64_unescaped_end(&s->fp64_unescaped);
    fp64_escaped_end(&s->fp64_escaped);

    fpxx_init_end(&s->fpxx_init);
    fpxx_approx_end(&s->fpxx_approx);
    fpxx_approx2_end(&s->fpxx_approx2);
    fpxx_step_end(&s->fpxx_step);
    fpxx_unescaped_end(&s->fpxx_unescaped);
    fpxx_escaped_end(&s->fpxx_escaped);
  }

  slice_table_delete(s->slice_table);
}

static void fpX_start(struct render *s);
bool render_reshape(struct render *s, int new_width, int new_height, int new_slice, GLsizei *bytes_allocated) {
  bool reshape = (s->options.width != new_width) || (s->options.height != new_height) || (s->options.slice != new_slice);
  if (reshape) {
#define CHECK assert(vbo_bytes > 0 && "--size is too big for my tiny GLsizei, try increasing --slice")
    assert(sizeof(double) == 8 && "what kind of obscure system are you running on?");
    GLsizei vbo_bytes = (new_width >> new_slice) * (new_height >> new_slice); CHECK;
    vbo_bytes *= (DE ? 10 : 8); CHECK;
    vbo_bytes <<= 1; CHECK;
    vbo_bytes <<= 1; CHECK;
    vbo_bytes <<= 1; CHECK;
    for (int i = 0; i < 2; ++i) {
      glBindBuffer(GL_ARRAY_BUFFER, s->vbo[i]);D;
      glBufferData(GL_ARRAY_BUFFER, vbo_bytes, 0, GL_DYNAMIC_COPY);D;
      glBindBuffer(GL_ARRAY_BUFFER, 0);D;
    }
    GLsizei tex_bytes = 0;
    glActiveTexture(GL_TEXTURE0 + TEX_RGB);D;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, new_width, new_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);D;
    tex_bytes += new_width * new_height * 4;
    glActiveTexture(GL_TEXTURE0 + TEX_RAW);D;
    glTexImage2D(GL_TEXTURE_2D, 0, DE ? GL_RGB32F : GL_RG32F, new_width, new_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);D;
    tex_bytes += new_width * new_height * 4 * (DE ? 3 : 2);
    glActiveTexture(GL_TEXTURE0 + TEX_FILLC);D;
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, new_width >> new_slice, new_height >> new_slice, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);D;
    tex_bytes += (new_width >> new_slice) * (new_height >> new_slice) * 4 * 4;
    if (bytes_allocated) {
      *bytes_allocated = 2 * vbo_bytes + tex_bytes;
    }
  }
  return reshape;
}

static bool render_update_options(struct render *s, const struct render_options *o) {
  bool need_to_restart_rendering = false;
  if (s->options.method != o->method) { need_to_restart_rendering = true; }
  if (s->options.calculate_de > o->calculate_de) { need_to_restart_rendering = true; }
  if (s->options.width != o->width) { need_to_restart_rendering = true; }
  if (s->options.height != o->height) { need_to_restart_rendering = true; }
  if (! mpfr_equal_p(s->options.centerx, o->centerx)) { need_to_restart_rendering = true; }
  if (! mpfr_equal_p(s->options.centery, o->centery)) { need_to_restart_rendering = true; }
  if (! mpfr_equal_p(s->options.radius, o->radius)) { need_to_restart_rendering = true; }
  if (render_reshape(s, o->width, o->height, o->slice, 0)) { need_to_restart_rendering = true; }
  render_options_copy(&s->options, o);
  return need_to_restart_rendering;
}

void render_start(struct render *s, const struct render_options *o) {
  render_options_copy(&s->options, o);
  s->pass = 0;
  s->slice_n = 0;
  s->timeout = false;
  s->slice_done = false;
  s->done = false;
  s->all_done = false;
  fp32_colour_clear(&s->fp32_colour, o->width, o->height);
  s->idle.f = fpX_start;
  debug_message("render_start()\n");
  log_message(LOG_INFO, "pass: %d\n", s->pass + 1);
}

void render_cont(struct render *s) {
  if (s->options.method == render_method_fpxx && s->all_done) {
    s->idle.f = 0;
    return;
  }
  s->pass++;
  s->slice_n = 0;
  s->idle.f = fpX_start;
  debug_message("render_cont()\n");
  log_message(LOG_INFO, "pass: %d\n", s->pass + 1);
}

void render_next_slice(struct render *s) {
  s->idle.f = fpX_start;
  s->slice_n += 1;
  if (s->slice_n == (1 << (s->options.slice << 1))) {
    s->done = true;
    s->idle.f = 0;
  }
}

#define PINGPONG(fp,FP) \
void fp##_pingpong(struct render *s) { \
  if (! (s->active_count == 0)) { \
    fp##_step_do(&s->fp##_step, &s->active_count, s->vbo[1], s->query); \
    GLuint unescaped = 0; \
    fp##_unescaped_do(&s->fp##_unescaped, &unescaped, s->active_count, s->vbo[0], s->query); \
    int escaped = s->active_count - unescaped; \
    if (escaped) { \
      fp##_escaped_do(&s->fp##_escaped, s->active_count); \
    } \
    if (completion_update(&s->completion, unescaped, escaped, 1, s->options.sharpness)) { \
      if (completion_done(&s->completion)) { \
        render_next_slice(s); \
      } \
    } else { \
      s->slice_done = false; \
      s->idle.f = fp##_pingpong; \
    } \
    s->active_count = unescaped; \
  } else { \
    render_next_slice(s); \
  } \
}
PINGPONG(fp32,FP32)
PINGPONG(fp64,FP64)
PINGPONG(fpxx,FPXX)
#undef PINGPONG

void fp32_start(struct render *s) {
  s->slice_done = false;
  int slice_x = 0;
  int slice_y = 0;
  slice_table_coords(s->slice_table, s->options.slice, s->slice_n, &slice_x, &slice_y);
  fp32_fillc_do(&s->fp32_fillc, s->vbo[1], s->fbo, s->tex, s->tex2, s->options.width, s->options.height, s->pass, s->options.slice, slice_x, slice_y);
  fp32_init_do(&s->fp32_init, &s->active_count, s->vbo, s->query, s->options.width >> s->options.slice, s->options.height >> s->options.slice, s->options.centerx, s->options.centery, s->options.radius);
  completion_start(&s->completion, s->active_count);
  fp32_step_start(&s->fp32_step, s->vbo[0], s->escaperadius2);
  fp32_escaped_start(&s->fp32_escaped, s->tex, s->fbo, s->vbo[1], s->escaperadius2, s->options.width, s->options.height, s->options.centerx, s->options.centery, s->options.radius);
  fp32_unescaped_start(&s->fp32_unescaped, s->vbo[1]);
  s->idle.f = fp32_pingpong;
}

void fp64_start(struct render *s) {
  s->slice_done = false;
  int slice_x = 0;
  int slice_y = 0;
  slice_table_coords(s->slice_table, s->options.slice, s->slice_n, &slice_x, &slice_y);
  fp32_fillc_do(&s->fp32_fillc, s->vbo[1], s->fbo, s->tex, s->tex2, s->options.width, s->options.height, s->pass, s->options.slice, slice_x, slice_y);
  fp64_init_do(&s->fp64_init, &s->active_count, s->vbo, s->query, s->options.width >> s->options.slice, s->options.height >> s->options.slice, s->options.centerx, s->options.centery, s->options.radius);
  completion_start(&s->completion, s->active_count);
  fp64_step_start(&s->fp64_step, s->vbo[0], s->escaperadius2, 2.0 * mpfr_get_d(s->options.radius, MPFR_RNDN) / s->options.height);
  fp64_escaped_start(&s->fp64_escaped, s->tex, s->fbo, s->vbo[1], s->escaperadius2, s->options.width, s->options.height, s->options.centerx, s->options.centery, s->options.radius);
  fp64_unescaped_start(&s->fp64_unescaped, s->vbo[1]);
  s->idle.f = fp64_pingpong;
}

bool fpxx_start_atom_abort(void *userdata) {
  struct render *s = userdata;
  switch (poll_ui(s->poll, false)) {
    case poll_abort: s->aborted = true; return true;
    case poll_timeout: s->aborted = true; s->timeout = true; return true;
    case poll_display: return false;
    case poll_continue: return false;
  }
  // poll returned some invalid enum...
  s->aborted = true;
  return true;
}

void fpxx_start(struct render *s) {
  s->idle.f = 0;
  s->slice_done = false;
  int slice_x = 0;
  int slice_y = 0;
  slice_table_coords(s->slice_table, s->options.slice, s->slice_n, &slice_x, &slice_y);
  if (! s->refs) {
    s->refs = ref_set_new();
    if (s->blobs) { blob_set_delete(s->blobs); }
    s->blobs = blob_set_new();
    mpfr_t x, y, r;
    mpfr_inits2(53, x, y, r, (mpfr_ptr) 0);
    pixel_coordinate(x, y, s->options.width, s->options.height, s->options.centerx, s->options.centery, s->options.radius, s->options.width / 2.0 + 0.5, s->options.height / 2.0 + 0.5);
    mpfr_set(r, s->options.radius, MPFR_RNDN);
    unsigned int period = boxperiod(x, y, r, 100000, s, fpxx_start_atom_abort); // FIXME hardcoded maxperiod
    if (s->aborted) {
      mpfr_clears(x, y, r, (mpfr_ptr) 0);
      return;
    }
    if (period > 0) {
      muatom(period, x, y, r, s, fpxx_start_atom_abort);
      if (s->aborted) {
        mpfr_clears(x, y, r, (mpfr_ptr) 0);
        return;
      }
      // may fail, but if it does we just use existing refx,refy anyway
      // but it might return NaN or inf if it fails really badly
      if (! (mpfr_number_p(x) && mpfr_number_p(y))) {
        pixel_coordinate(x, y, s->options.width, s->options.height, s->options.centerx, s->options.centery, s->options.radius, s->options.width / 2.0 + 0.5, s->options.height / 2.0 + 0.5);
      }
    }
    if (! ref_set_contains(s->refs, x, y)) {
      ref_set_insert(s->refs, x, y);
    }
    mpfr_clears(x, y, r, (mpfr_ptr) 0);
  }
  fp32_fillc_do(&s->fp32_fillc, s->vbo[0], s->fbo, s->tex, s->tex2, s->options.width, s->options.height, s->pass, s->options.slice, slice_x, slice_y);
  glBindFramebuffer(GL_FRAMEBUFFER, s->fbo);D;
  fpxx_init_do(&s->fpxx_init, &s->active_count, s->vbo, s->query, s->pass, s->options.width >> s->options.slice, s->options.height >> s->options.slice, s->options.centerx, s->options.centery, s->options.radius, s->refs->set->x, s->refs->set->y);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
  if (s->pass == 0) {
    completion_start(&s->completion, s->active_count);
    if (s->slice_n == 0) {
      completion_start(&s->completion2, s->active_count);
      s->completion2.iteration_target = 4;
      s->last_active_count = s->active_count;
    }
  } else {
    completion_reset(&s->completion, s->active_count);
    if (s->slice_n == (1 << (s->options.slice << 1))) {
      if (completion_update(&s->completion2, s->active_count, s->last_active_count - s->active_count, 1, 0.0)) {
        if (completion_done(&s->completion2)) {
          s->all_done = true;
          return;
        }
      }
      s->last_active_count = s->active_count;
    }
  }
  mpfr_t zx, zy, dzx, dzy, dzx0, dzy0;
  mpfr_inits2(53, zx, zy, dzx, dzy, dzx0, dzy0, (mpfr_ptr) 0);
  mpfr_prec_t p = mpfr_get_prec(s->options.radius);
  mpfr_set_prec(dzx0, p);
  mpfr_set_prec(dzy0, p);
  mpfr_set(dzx0, s->options.radius, MPFR_RNDN);
  mpfr_mul_d(dzx0, dzx0, 2.0 / s->options.height, MPFR_RNDN);
  mpfr_set_ui(dzy0, 0, MPFR_RNDN);
  if (s->options.order) {
    fpxx_approx2_do(&s->fpxx_approx2, &s->active_count, s->vbo, zx, zy, s->options.order, s->pass, s->options.radius, s->refs->set->x, s->refs->set->y, s->slice_n == 0, s, fpxx_start_atom_abort);
  } else {
    fpxx_approx_do(&s->fpxx_approx, &s->active_count, s->vbo, s->query, dzx0, dzy0, zx, zy, dzx, dzy, s->pass, s->options.radius, s->refs->set->x, s->refs->set->y, s->options.series_approx, s->slice_n == 0, s, fpxx_start_atom_abort);
  }
  if (s->aborted) {
    mpfr_clears(zx, zy, dzx, dzy, dzx0, dzy0, (mpfr_ptr) 0);
    return;
  }
  fpxx_step_start(&s->fpxx_step, s->vbo[0], s->escaperadius2, dzx0, dzy0, zx, zy, dzx, dzy, s->refs->set->x, s->refs->set->y, s->slice_n == 0);
  mpfr_clears(zx, zy, dzx, dzy, dzx0, dzy0, (mpfr_ptr) 0);
  fpxx_escaped_start(&s->fpxx_escaped, s->tex, s->fbo, s->vbo[1], s->escaperadius2, s->options.width, s->options.height, s->options.centerx, s->options.centery, s->options.radius, s->refs->set->x, s->refs->set->y);
  fpxx_unescaped_start(&s->fpxx_unescaped, s->vbo[1]);
  s->idle.f = fpxx_pingpong;
}

void fpX_start(struct render *s) {
  log_message(LOG_DEBUG, "slice: %d\n", s->slice_n);
  switch (s->options.method) {
    case render_method_fp32: fp32_start(s); break;
    case render_method_fp64: fp64_start(s); break;
    case render_method_fpxx: fpxx_start(s); break;
  }
  glBindFramebuffer(GL_FRAMEBUFFER, s->fbo);D;
}

bool render_calculate(struct render *s, const struct render_options *o, struct poll *poll) {
  s->poll = poll;
  if (render_update_options(s, o)) {
    stopwatch_reset(s->render_time);
    debug_message("render_calculate() reset time %f\n", stopwatch_elapsed(s->render_time));
    if (s->refs) {
      ref_set_delete(s->refs);
      s->refs = 0;
    }
    render_start(s, o);
  }
  bool more = s->idle.f && ! s->aborted;
  while (more) {
    s->idle.f(s);
    more = s->idle.f && ! s->aborted;
    enum poll_result result = poll_ui(poll, false);
    switch (result) {
      case poll_continue: break;
      case poll_abort: s->aborted = true; return false;
      case poll_display: more |= render_display(s, o, poll); break;
      case poll_timeout: s->timeout = true; render_display(s, o, poll); return false;
    }
  }
  return more;
}

bool render_display(struct render *s, const struct render_options *o, struct poll *poll) {
  double iterations, exterior, interior, glitch;
  enum result_t result;
  debug_message("render_display() %p\n", s->idle.f);
#define COLOUR do{ \
  fp32_colour_do(&s->fp32_colour, o->win_width, o->win_height, o->width, o->height, s->timeout || s->all_done, s->pass > 0 || s->all_done, o->show_glitches, pow(0.5, o->weight), o->slice, s->slice_n); \
  glBindFramebuffer(GL_FRAMEBUFFER, s->fbo);D; \
  poll_swap_buffers(poll); \
}while(0)
  COLOUR;
  if (s->timeout) {
    log_message(LOG_INFO, "timeout\n");
    log_message(LOG_INFO, "elapsed time: %f\n", stopwatch_elapsed(s->render_time));
    result = image_result(o->width, o->height, &iterations, &exterior, &interior, &glitch);
    log_result(o->filename, result_timeout, o->method, iterations, exterior, interior, glitch, o->method == render_method_fpxx ? s->pass + 1 : 0);
    s->slice_done = true;
    s->done = true;
    s->all_done = true;
    s->aborted = true;
    s->idle.f = 0;
    return false;
  }
  if (! s->idle.f) {
    debug_message("render method: %d\n", o->method);
    if (o->method == render_method_fpxx && s->done && ! s->all_done) {
      if (s->pass < 1000) { // FIXME hardcoded max passes
        switch (find_ref(s->refs, s->blobs, o->width, o->height, o->centerx, o->centery, o->radius, o->max_glitch, o->max_blob)) {
          case find_ref_failure:
            log_message(LOG_INFO, "failed to find reference, after passes: %d\n", s->pass + 1);
            log_message(LOG_INFO, "elapsed time: %f\n", stopwatch_elapsed(s->render_time));
            image_result(o->width, o->height, &iterations, &exterior, &interior, &glitch);
            log_result(o->filename, result_glitch, o->method, iterations, exterior, interior, glitch, s->pass + 1);
            s->done = true;
            s->all_done = true;
            COLOUR;
            return false;
          case find_ref_complete:
            image_result(o->width, o->height, &iterations, &exterior, &interior, &glitch);
            if ((! s->options.order && s->fpxx_approx.too_deep) || (s->options.order && s->fpxx_approx2.too_deep)) {
              log_message(LOG_INFO, "possibly errors in image, took passes: %d\n", s->pass + 1);
              log_message(LOG_INFO, "elapsed time: %f\n", stopwatch_elapsed(s->render_time));
              result = result_deep;
            } else {
              log_message(LOG_INFO, "no more errors in image, took passes: %d\n", s->pass + 1);
              log_message(LOG_INFO, "elapsed time: %f\n", stopwatch_elapsed(s->render_time));
              result = result_ok;
            }
            log_result(o->filename, result, o->method, iterations, exterior, interior, glitch, s->pass + 1);
            s->done = true;
            s->all_done = true;
            COLOUR;
            return false;
          case find_ref_success:
            debug_message("found another reference\n");
            log_message(LOG_INFO, "elapsed time: %f\n", stopwatch_elapsed(s->render_time));
            render_cont(s);
            s->slice_done = false;
            s->done = false;
            return true;
        }
      } else {
        log_message(LOG_INFO, "too many passes: %d\n", s->pass);
        log_message(LOG_INFO, "elapsed time: %f\n", stopwatch_elapsed(s->render_time));
        result = image_result(o->width, o->height, &iterations, &exterior, &interior, &glitch);
        log_result(o->filename, result, o->method, iterations, exterior, interior, glitch, s->pass);
        s->done = true;
        s->all_done = true;
        COLOUR;
        return false;
      }
    } else {
      if (! s->all_done) {
        log_message(LOG_INFO, "elapsed time: %f\n", stopwatch_elapsed(s->render_time));
        result = image_result(o->width, o->height, &iterations, &exterior, &interior, &glitch);
        log_result(o->filename, result, o->method, iterations, exterior, interior, glitch, 0);
        s->done = true;
        s->all_done = true;
      }
      return false;
    }
  }
  return s->idle.f;
}

enum render_result render_do(struct render *render, struct render_options *render_options, struct poll *poll) {
  render->aborted = false;
  do {
    while (render_calculate(render, render_options, poll)) {
      // spin
    }
    if (render->aborted) {
      break;
    }
  } while (render_display(render, render_options, poll));
  if (render->aborted) {
    if (render->timeout) {
      return render_complete;
    }
    return render_aborted;
  } else {
    return render_complete;
  }
}

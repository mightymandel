// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FIND_REF_H
#define FIND_REF_H 1

/*!
\file  find_ref.h
\brief Algorithms for finding new reference points.
*/

#include "ref_set.h"
#include "blob_set.h"

/*!
\brief The things that can occur when reference finding.
*/
enum find_ref_t {
  find_ref_failure  = -1, //!< There are glitches but no new reference could be found.
  find_ref_complete =  0, //!< There are very few glitches, so the image is complete.
  find_ref_success  =  1  //!< A new reference was inserted into the ref_set.
};

enum find_ref_t find_ref(struct ref_set *refs, struct blob_set *blobset, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius, double max_glitch, int max_blob);

#endif

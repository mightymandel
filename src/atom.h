// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef ATOM_H
#define ATOM_H 1

/*!
\file  atom.h
\brief Mandelbrot set atom finding.
*/

#include <mpfr.h>
#include "utility.h"

int crosses_positive_real_axis(const mpfr_t ax, const mpfr_t ay, const mpfr_t bx, const mpfr_t by);
int surrounds_origin(const mpfr_t ax, const mpfr_t ay, const mpfr_t bx, const mpfr_t by, const mpfr_t cx, const mpfr_t cy, const mpfr_t dx, const mpfr_t dy);
int did_escaped(const mpfr_t x, const mpfr_t y);
unsigned int boxperiod(const mpfr_t cx, const mpfr_t cy, const mpfr_t r, unsigned int maxperiod, void *abort_data, abort_t abort_fn);
int muatom(int period, mpfr_t x, mpfr_t y, mpfr_t z, void *abort_data, abort_t abort_fn);

#endif

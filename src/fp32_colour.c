// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "fp32_colour.h"
#include "shader.h"
#include "logging.h"
#include "texture.h"

void fp32_colour_begin(struct fp32_colour *s, GLuint rgb_tex) {
  s->program = compile_program("fp32_colour", fp32_colour_vert, 0, fp32_colour_frag);
  s->ida0 = glGetUniformLocation(s->program, "ida0");D;
  s->slice_number = glGetUniformLocation(s->program, "slice_number");D;
  s->slice_coords = glGetUniformLocation(s->program, "slice_coords");D;
  s->update = glGetUniformLocation(s->program, "update");D;
  s->update2 = glGetUniformLocation(s->program, "update2");D;
  s->errs = glGetUniformLocation(s->program, "errs");D;
  s->thickness = glGetUniformLocation(s->program, "thickness");D;
  s->slicing = glGetUniformLocation(s->program, "slicing");D;
  s->pt = glGetAttribLocation(s->program, "pt");D;
  s->program2 = compile_program("fp32_colour2", fp32_colour2_vert, 0, fp32_colour2_frag);
  s->rgb2 = glGetUniformLocation(s->program2, "rgb");D;
  s->pt2 = glGetAttribLocation(s->program2, "pt");D;
  glGenVertexArrays(1, &s->vao);D;
  glGenVertexArrays(1, &s->vao2);D;
  glGenBuffers(1, &s->vbo);D;
  GLfloat quad[] =
    { -1, -1, 0, 0
    ,  1, -1, 1, 0
    , -1,  1, 0, 1
    ,  1,  1, 1, 1
    };
  glBindBuffer(GL_ARRAY_BUFFER, s->vbo);D;
  glBufferData(GL_ARRAY_BUFFER, 16 * sizeof(GLfloat), &quad, GL_STATIC_DRAW);D;
  glBindVertexArray(s->vao);D;
  glVertexAttribPointer(s->pt, 4, GL_FLOAT, GL_FALSE, 0, 0);D;
  glEnableVertexAttribArray(s->pt);D;
  glBindVertexArray(s->vao2);D;
  glVertexAttribPointer(s->pt2, 4, GL_FLOAT, GL_FALSE, 0, 0);D;
  glEnableVertexAttribArray(s->pt2);D;
  glBindVertexArray(0);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glUseProgram(s->program);D;
  glUniform1i(s->ida0, TEX_RAW);D;
  glUniform1i(s->slice_number, TEX_SLICE_NUMBER);D;
  glUniform1i(s->slice_coords, TEX_SLICE_COORDS);D;
  glUseProgram(s->program2);D;
  glUniform1i(s->rgb2, TEX_RGB);D;
  glUseProgram(0);D;
  glGenFramebuffers(1, &s->fbo);D;
  glBindFramebuffer(GL_FRAMEBUFFER, s->fbo);D;
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, rgb_tex, 0);D;
  GLenum buffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, buffers);D;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
}

void fp32_colour_end(struct fp32_colour *s) {
  glDeleteProgram(s->program);D;
  glDeleteProgram(s->program2);D;
  glDeleteVertexArrays(1, &s->vao);D;
  glDeleteVertexArrays(1, &s->vao2);D;
  glDeleteBuffers(1, &s->vbo);D;
  glDeleteFramebuffers(1, &s->fbo);D;
}

/*!
\brief Colour raw data to RGB.

OpenGL state changes: glBindFramebuffer, glActiveTexture
*/
void fp32_colour_do(struct fp32_colour *s, int win_width, int win_height, int width, int height, bool update, bool update2, GLuint errs, float thickness, int slice, int slice_n) {
  // render to texture
  glBindFramebuffer(GL_FRAMEBUFFER, s->fbo);D;
  glViewport(0, 0, width, height);D;
  glUseProgram(s->program);D;
  glUniform1i(s->update, update);D;
  glUniform1i(s->update2, update2);D;
  glUniform1i(s->errs, errs);D;
  glUniform1f(s->thickness, thickness);D;
  glUniform2i(s->slicing, slice_n, slice);
  glBindVertexArray(s->vao);D;
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);D;
  glBindVertexArray(0);D;
  glUseProgram(0);D;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
  glActiveTexture(GL_TEXTURE0 + TEX_RGB);D;
  glGenerateMipmap(GL_TEXTURE_2D);D;
  // draw to screen
  glViewport(0, 0, win_width, win_height);D;
  glUseProgram(s->program2);D;
  glBindVertexArray(s->vao2);D;
  glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);D;
  glBindVertexArray(0);D;
  // reset state
  glViewport(0, 0, width, height);D;
}

void fp32_colour_clear(struct fp32_colour *s, int width, int height) {
  glBindFramebuffer(GL_FRAMEBUFFER, s->fbo);D;
  glViewport(0, 0, width, height);D;
  glClearColor(1.0, 0.7, 0.0, 1.0);D;
  glClear(GL_COLOR_BUFFER_BIT);D;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdlib.h>

#include "logging.h"
#include "stopwatch.h"
#include "startup.h"
#include "render.h"
#include "interact.h"
#include "poll.h"
#include "filename.h"
#include "record.h"

struct poll {
  double timeout;                        //!< Timeout.
  double ui_poll_timeout;                //!< Time between UI polls.
  double display_timeout;                //!< Time between displays.
  struct stopwatch *last_timeout;        //!< Time since last set timeout.
  struct stopwatch *last_ui_poll;        //!< Time since last UI poll.
  struct stopwatch *last_display;        //!< Time since last display.
  GLFWwindow *window;                    //!< Window pointer reference.
  bool interactive;                      //!< Whether to expect UI changes.
  struct render_options *render_options; //!< Render options reference.
  bool save_screenshot;                  //!< Whether to save a screenshot.
  int screenshot_count;                  //!< Number of screenshots saved.
  struct filename *filename;             //!< Filename generator.
  struct record *record;                 //!< Image recorder.
};

struct poll *poll_new(GLFWwindow *window, bool interactive, struct render_options *render_options, double ui_poll_timeout, double display_timeout, struct filename *filename, struct record *record) {
  struct poll *poll = malloc(sizeof(struct poll));
  poll->timeout = 1.0 / 0.0; // infinity
  poll->ui_poll_timeout = ui_poll_timeout;
  poll->display_timeout = display_timeout;
  poll->last_timeout = stopwatch_new();
  poll->last_ui_poll = stopwatch_new();
  poll->last_display = stopwatch_new();
  poll->window = window;
  poll->interactive = interactive;
  poll->render_options = render_options;
  poll->save_screenshot = false;
  poll->screenshot_count = 0;
  poll->filename = filename;
  poll->record = record;
  stopwatch_start(poll->last_timeout);
  stopwatch_start(poll->last_ui_poll);
  stopwatch_start(poll->last_display);
  return poll;
}

void poll_delete(struct poll *poll) {
  stopwatch_delete(poll->last_ui_poll);
  stopwatch_delete(poll->last_display);
  free(poll);
}

enum poll_result poll_ui(struct poll *poll, bool should_wait) {
  debug_message("poll_ui(%p)\n", poll);
  bool redisplay = false;
  if (stopwatch_elapsed(poll->last_timeout) > poll->timeout) {
    debug_message("poll_ui(%p) timeout\n", poll);
    return poll_timeout;
  }
  if (stopwatch_elapsed(poll->last_ui_poll) > poll->ui_poll_timeout) {
    debug_message("poll_ui(%p) polling UI\n", poll);
    stopwatch_reset(poll->last_ui_poll);
    if (poll->interactive) {
      interact_reset();
    }
    if (should_wait) {
      glfwWaitEvents();
    } else {
      glfwPollEvents();
    }
    if (poll->interactive) {
      redisplay |= poll->render_options->weight != interact.weight;
      redisplay |= poll->render_options->show_glitches != interact.show_glitches;
      poll->render_options->weight = interact.weight;
      poll->render_options->show_glitches = interact.show_glitches;
      if (interact.quit) { glfwSetWindowShouldClose(poll->window, GL_TRUE); }
      if (interact.updated) {
        interact.updated = false;
        render_options_set_location(poll->render_options, interact.centerx, interact.centery, interact.radius);
        debug_message("poll_ui(%p) abort (updated)\n", poll);
        return poll_abort;
      }
      if (interact.save_screenshot) {
        poll->save_screenshot = true;
        redisplay = true;
      }
    }
    if (glfwWindowShouldClose(poll->window)) {
      debug_message("poll_ui(%p) abort (quit)\n", poll);
      return poll_abort;
    }
  }
  if (redisplay || stopwatch_elapsed(poll->last_display) > poll->display_timeout) {
    debug_message("poll_ui(%p) display\n", poll);
    stopwatch_reset(poll->last_display);
    return poll_display;
  }
  debug_message("poll_ui(%p) continue\n", poll);
  return poll_continue;
}

struct tiling;
struct zoom;
char *collect_metadata_to_string(const char *prefix, struct render_options *render_options, struct tiling *tiling, struct zoom *zoom, int pass);

void poll_swap_buffers(struct poll *poll) {
  glfwSwapBuffers(poll->window);
  if (poll->save_screenshot) {
    poll->save_screenshot = false;
    char *comment = collect_metadata_to_string("# ", poll->render_options, 0, 0, -1);
    char *name = filename_name(poll->filename, "ppm", poll->screenshot_count++, -1, -1);
    record_do(poll->record, name, poll->render_options->width, poll->render_options->height, comment);
    free(name);
    free(comment);
  }
}

void poll_set_timeout(struct poll *poll, double timeout) {
  poll->timeout = timeout;
  stopwatch_reset(poll->last_timeout);
}

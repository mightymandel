// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FPXX_APPROX2_H
#define FPXX_APPROX2_H 1

#include <GL/glew.h>
#include <mpfr.h>

#include "complex.h"
#include "utility.h"
#include "z2c.h"

struct fpxx_approx2 {
  struct z2c_series *series;
  struct z2c_approx *approx;
  struct z2c_reference *reference;
  int n;
  bool too_deep;
};

void fpxx_approx2_begin(struct fpxx_approx2 *s);
void fpxx_approx2_end(struct fpxx_approx2 *s);
void fpxx_approx2_do(struct fpxx_approx2 *s, GLuint *active_count, GLuint *vbo, mpfr_t zx, mpfr_t zy, int order, int pass, const mpfr_t radius, const mpfr_t refx, const mpfr_t refy, bool initial_slice, void *abort_data, abort_t abort_fn);

#endif

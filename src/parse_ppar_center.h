// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_PPAR_CENTER_H
#define PARSE_PPAR_CENTER_H 1

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Parse preprocessed FractInt `.ppar` file format (center variant).

<https://en.wikibooks.org/wiki/Fractals/fractint#fractint_par_files>

\copydetails parser
*/
bool parse_ppar_center(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

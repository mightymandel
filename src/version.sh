#!/bin/bash
## mightymandel -- GPU-based Mandelbrot Set explorer
## Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
## License GPL3+ http://www.gnu.org/licenses/gpl.html

# false && (
git status > /dev/null 2> /dev/null && (
version="$(git describe)"
changes="$(git status --porcelain . | wc -l)"
if (( ${changes} > 0 ))
then
  version="${version}+$(date -u '+%Y-%m-%d-%H-%M-%S')"
fi
cat > version.c <<EOF
/* automatically generated, do not edit */
#include "version.h"
const char *mightymandel_version = "${version}";
EOF
) || (
version="v16+git"
cat > version.c <<EOF
/* automatically generated, do not edit */
#include "version.h"
const char *mightymandel_version = "${version}";
EOF
)

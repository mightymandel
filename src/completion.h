// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef COMPLETION_H
#define COMPLETION_H 1

/*!
\file  completion.h
\brief Check for rendering completion.
*/

#include <stdbool.h>

struct completion {
  bool almost;
  bool done;
  int iterations;
  int iteration_target;
  int unescaped;
  int escaped;
  int escaped_recently;
};

void completion_start(struct completion *completion, int unescaped);
void completion_reset(struct completion *completion, int unescaped);
bool completion_update(struct completion *completion, int unescaped, int escaped, int iterations, double sharpness);
bool completion_done(struct completion *completion);

#endif

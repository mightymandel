// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

in dvec4 cne0;
#ifdef DE
in dvec4 zdz0;
#else
in dvec2 zdz0;
#endif
in dvec2 err0;
out dvec4 cne1;
#ifdef DE
out dvec4 zdz1;
#else
out dvec2 zdz1;
#endif
out dvec2 err1;
void main() {
  cne1 = cne0;
  zdz1 = zdz0;
  err1 = err0;
}

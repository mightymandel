// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\brief RGB texture sampler.
*/
uniform sampler2D rgb;

/*!
\brief Texture coordinate for the current pixel.
*/
smooth in vec2 t;

/*!
\brief Output RGBA colours.
*/
layout(location = 0, index = 0) out vec4 c;

/*!
\brief Output the colour for the pixel.
*/
void main() {
  c = vec4(texture(rgb, t).rgb, 1.0);
}

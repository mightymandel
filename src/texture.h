// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef TEXTURE_H
#define TEXTURE_H 1

/*!
\file texture.h
\brief OpenGL texture unit semantics.
*/

/*!
\brief Colourized image, RGB 8ui with mipmaps, width * height.
*/
#define TEX_RGB   0

/*!
\brief Raw image, (i,d,a,e) 32f, width * height.
*/
#define TEX_RAW   1

/*!
\brief Used by fillc, (x,y,i,e) 32f, (width >> slice) * (height >> slice).
*/
#define TEX_FILLC 2

/*!
\brief Reference orbit, (re1,re2,im1,im2) 32ui (packed doubles), FPXX_STEP_ITERS * (DE ? 2 : 1).
*/
#define TEX_ORBIT 3

/*!
\brief Slice coordinates to slice number, (n) 16ui, mipmaps (for texelFetch), (1 << SLICE_MAX) * (1 << SLICE_MAX).
*/
#define TEX_SLICE_NUMBER 4

/*!
\brief Slice number to slice coordinates, (i,j) 8ui, mipmaps (for texelFetch), (1 << SLICE_MAX) * (1 << SLICE_MAX).
*/
#define TEX_SLICE_COORDS 5

#endif

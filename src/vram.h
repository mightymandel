// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef VRAM_H
#define VRAM_H 1

/*!
\brief Determine available video memory.

\return Available video memory in kB.
*/
int vram_available();

#endif

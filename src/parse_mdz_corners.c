// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>

#include "parse.h"
#include "parse_mdz_corners.h"

bool parse_mdz_corners(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
  (void) length;
  assert(source);
  char *source2 = strdup(source);
  assert(source2);
  char *s = source2;
  char *sp = 0;
  char *sa = 0;
  char *sx0 = 0;
  char *sx1 = 0;
  char *sy1 = 0;
  while (s) {
    char *line = parse_line(&s);
    if (0 == strncmp(line, "precision ", 10)) { sp  = line + 10; }
    if (0 == strncmp(line, "aspect ",     7)) { sa  = line +  7; }
    if (0 == strncmp(line, "xmin ",       5)) { sx0 = line +  5; }
    if (0 == strncmp(line, "xmax ",       5)) { sx1 = line +  5; }
    if (0 == strncmp(line, "ymax ",       5)) { sy1 = line +  5; }
  }
  if (sp && sa && sx0 && sx1 && sy1) {
    int p = atoi(sp);
    mpfr_set_prec(cx, p);
    mpfr_set_prec(cy, p);
    mpfr_set_prec(cz, 53);
    double a = atof(sa);
    mpfr_t x0, x1, y1;
    mpfr_inits2(p, x0, x1, y1, (mpfr_ptr) 0);
    mpfr_set_str(x0, sx0, 10, MPFR_RNDN);
    mpfr_set_str(x1, sx1, 10, MPFR_RNDN);
    mpfr_set_str(y1, sy1, 10, MPFR_RNDN);
    mpfr_add(cx, x0, x1, MPFR_RNDN);
    mpfr_div_2ui(cx, cx, 1, MPFR_RNDN);
    mpfr_sub(x1, x1, x0, MPFR_RNDN);
    mpfr_div_d(cz, x1, a * 2, MPFR_RNDN);
    mpfr_sub(cy, y1, cz, MPFR_RNDN);
    mpfr_clears(x0, x1, y1, (mpfr_ptr) 0);
    free(source2);
    return true;
  } else {
    free(source2);
    return false;
  }
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FP32_INIT_H
#define FP32_INIT_H 1

#include <GL/glew.h>
#include <mpfr.h>

struct fp32_init {
  GLuint program;
  GLint radius;
  GLint center;
  GLint c;
  GLuint vao;
};

void fp32_init_begin(struct fp32_init *s);
void fp32_init_end(struct fp32_init *s);
void fp32_init_do(struct fp32_init *s, GLuint *active_count, GLuint *vbo, GLuint query, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef TILING_H
#define TILING_H 1

/*!
\file  tiling.h
\brief Handle state related to tiled rendering.
*/

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Tiling state structure.

Should be read-only outside tiling.c.
*/
struct tiling {
  int width;            //!< Tile width.
  int height;           //!< Tile height.
  mpfr_t centerx;       //!< Tile center (real part).
  mpfr_t centery;       //!< Tile center (imaginary part).
  mpfr_t radius;        //!< Tile radius.
  int col;              //!< Tile x coordinate (left is 0)
  int row;              //!< Tile y coordinate (top is 0)
  int tiled_cols;       //!< Tiling width in tiles.
  int tiled_rows;       //!< Tiling height in tiles.
  mpfr_t tiled_centerx; //!< Tiling center (real part).
  mpfr_t tiled_centery; //!< Tiling center (imaginary part).
  mpfr_t tiled_radius;  //!< Tiling radius.
};

void tiling_begin(struct tiling *tiling, int cols, int rows, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius);
void tiling_end(struct tiling *tiling);
bool tiling_next(struct tiling *tiling);

#endif

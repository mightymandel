// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

uniform float radius;
uniform vec2  center;
in  vec4 c;
out vec4 cne;
#ifdef DE
out vec4 zdz;
#else
out vec2 zdz;
#endif
void main() {
  cne = vec4(radius * c.xy + center, 0.0, 0.0);
#ifdef DE
  zdz = vec4(0.0, 0.0, 0.0, 0.0);
#else
  zdz = vec2(0.0, 0.0);
#endif
}

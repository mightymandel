// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file  utility.c
\brief Miscellaneous utility functions.
*/

#include <math.h>
#include <stdio.h>

#include "utility.h"

/*!
\brief Find the minimum of two `int`s.

\param a First number.
\param b Second number.
\return The minimum of the two numbers.
*/
int min(int a, int b) { return a < b ? a : b; }

/*!
\brief Find the maximum of two `int`s.

\param a First number.
\param b Second number.
\return The maximum of the two numbers.
*/
int max(int a, int b) { return a > b ? a : b; }

/*!
\brief Find the next power of two.

\param z A number.
\return The least power of two greater or equal to the input number.
*/
int ceil2n(int z) {
  int n = 1;
  while (0 < n && n < z) {
    n <<= 1;
  }
  return n;
}

/*!
\brief Find the precision needed to represent pixel coordinates in an image.

\param radius The radius of the view.
\param height The height of the view in pixels.
\return The precision for pixels in the view.
*/
int pxbits(const mpfr_t radius, double height) {
  mpfr_t r;
  mpfr_init2(r, 53);
  mpfr_log2(r, radius, MPFR_RNDN);
  int bits = 2 + ceil(log2(height)) - floor(mpfr_get_d(r, MPFR_RNDN));
  mpfr_clear(r);
  return bits;
}

/*!
\brief Find the coordinates corresponding to a pixel.

\param x Output real coordinate.
\param y Output imaginary coordinate.
\param width The image width in pixels.
\param height The image height in pixels.
\param centerx The real coordinate of the view center.
\param centery The imaginary coordinate of the view center.
\param radius The radius of the view.
\param i The pixel x coordinate (0 is left of the image).
\param j The pixel y coordinate (0 is top of the image).
*/
void pixel_coordinate(mpfr_t x, mpfr_t y, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius, double i, double j) {
  mpfr_set_prec(x, mpfr_get_prec(centerx) + 8);
  mpfr_set_prec(y, mpfr_get_prec(centery) + 8);
  mpfr_mul_d(x, radius, 2.0 * (i  - width / 2.0) / height, MPFR_RNDN);
  mpfr_add(x, x, centerx, MPFR_RNDN);
  mpfr_mul_d(y, radius, 2.0 * (height / 2.0 - j) / height, MPFR_RNDN);
  mpfr_add(y, y, centery, MPFR_RNDN);
}

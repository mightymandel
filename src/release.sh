#!/bin/bash
## mightymandel -- GPU-based Mandelbrot Set explorer
## Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
## License GPL3+ http://www.gnu.org/licenses/gpl.html

# prepare
make clean
make SYSTEM=win32 clean
make SYSTEM=win64 clean
# native
make
version="$(./mightymandel --version)"
mv mightymandel "../bin/mightymandel-${version}"
make clean
make DEBUG=-s
mv mightymandel "../bin/mightymandel-${version}-nodebug"
make clean
# win32
make SYSTEM=win32
mv mightymandel.exe "../bin/mightymandel-${version}-win32.exe"
make SYSTEM=win32 clean
make SYSTEM=win32 DEBUG=-s
mv mightymandel.exe "../bin/mightymandel-${version}-win32-nodebug.exe"
make SYSTEM=win32 clean
# win64
make SYSTEM=win64
mv mightymandel.exe "../bin/mightymandel-${version}-win64.exe"
make SYSTEM=win64 clean
make SYSTEM=win64 DEBUG=-s
mv mightymandel.exe "../bin/mightymandel-${version}-win64-nodebug.exe"
make SYSTEM=win64 clean
# zip
cd ../bin
echo -n -e "mightymandel ${version}\r\nSee: http://mightymandel.mathr.co.uk\r\n" > "mightymandel-${version}-windows.txt"
zip -9 "mightymandel-${version}-windows.zip" \
  "mightymandel-${version}-windows.txt" \
  "mightymandel-${version}-win32.exe" \
  "mightymandel-${version}-win32-nodebug.exe" \
  "mightymandel-${version}-win64.exe" \
  "mightymandel-${version}-win64-nodebug.exe"
cd ..
git archive --prefix="mightymandel-${version}/" -o "bin/mightymandel-${version}.tar.gz" -9 HEAD
cd bin
# finish
ls -1sh *"${version}"*
cd ../src

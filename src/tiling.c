// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file  tiling.c
\brief Handle state related to tiled rendering.
*/

#include "tiling.h"
#include "utility.h"

/*!
\brief Initialize a tiling.

\param tiling The tiling to initialize.
\param cols The number of tiles horizontally.
\param rows The number of tiles vertically.
\param width The image width of each tile in pixels.
\param height The image height of each tile in pixels.
\param centerx The real coordinate of the tiled view center.
\param centery The imaginary coordinate of the tiled view center.
\param radius The radius of the tiled view.
*/
void tiling_begin(struct tiling *tiling, int cols, int rows, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius) {
  mpfr_inits2(53, tiling->centerx, tiling->centery, tiling->radius, tiling->tiled_centerx, tiling->tiled_centery, tiling->tiled_radius, (mpfr_ptr) 0);
  tiling->width = width;
  tiling->height = height;
  tiling->col = -1;
  tiling->row = 0;
  tiling->tiled_cols = cols;
  tiling->tiled_rows = rows;
  mpfr_set_prec(tiling->tiled_centerx, mpfr_get_prec(centerx));
  mpfr_set_prec(tiling->tiled_centery, mpfr_get_prec(centery));
  mpfr_set_prec(tiling->tiled_radius, mpfr_get_prec(radius));
  mpfr_set(tiling->tiled_centerx, centerx, MPFR_RNDN);
  mpfr_set(tiling->tiled_centery, centery, MPFR_RNDN);
  mpfr_set(tiling->tiled_radius, radius, MPFR_RNDN);
}

/*!
\brief Clean up a tiling.

\param tiling The tiling to clean up.
*/
void tiling_end(struct tiling *tiling) {
  mpfr_clears(tiling->centerx, tiling->centery, tiling->radius, tiling->tiled_centerx, tiling->tiled_centery, tiling->tiled_radius, (mpfr_ptr) 0);
}

/*!
\brief Advance to the next tile.

Should be called before rendering the first tile.

\param tiling The tiling to advance.
\return Success (true) or failure (false, there are no more tiles).
*/
bool tiling_next(struct tiling *tiling) {
  tiling->col++;
  if (tiling->col >= tiling->tiled_cols) {
    tiling->col = 0;
    tiling->row++;
    if (tiling->row >= tiling->tiled_rows) {
      return false;
    }
  }
  mpfr_prec_t p = 16 + mpfr_get_prec(tiling->tiled_centerx);
  mpfr_set_prec(tiling->centerx, p);
  mpfr_set_prec(tiling->centery, p);
  double i = (tiling->col + 0.5) * tiling->width;
  double j = (tiling->row + 0.5) * tiling->height;
  int width = tiling->width * tiling->tiled_cols;
  int height = tiling->height * tiling->tiled_rows;
  pixel_coordinate(tiling->centerx, tiling->centery, width, height, tiling->tiled_centerx, tiling->tiled_centery, tiling->tiled_radius, i, j);
  mpfr_div_si(tiling->radius, tiling->tiled_radius, tiling->tiled_rows, MPFR_RNDN);
  return true;
}

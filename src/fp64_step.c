// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "fp64_step.h"
#include "shader.h"
#include "logging.h"

extern const char *fp64_step_vert;
const GLchar *fp64_step_varyings[] = {"cne", "zdz"};

void fp64_step_begin(struct fp64_step *s) {
  s->program = compile_program_tf("fp64_step", fp64_step_vert, 0, 0, 2, fp64_step_varyings);
  s->er2 = glGetUniformLocation(s->program, "er2");D;
  s->dz0 = glGetUniformLocation(s->program, "dz0");D;
  s->cne0 = glGetAttribLocation(s->program, "cne0");D;
  s->zdz0 = glGetAttribLocation(s->program, "zdz0");D;
  glGenVertexArrays(1, &s->vao);D;
}

void fp64_step_end(struct fp64_step *s) {
  glDeleteProgram(s->program);D;
  glDeleteVertexArrays(1, &s->vao);D;
}

void fp64_step_start(struct fp64_step *s, GLuint vbo, double escaperadius2, complex double dz0) {
  glUseProgram(s->program);D;
  glUniform1d(s->er2, escaperadius2);D;
  if (DE) {
    glUniform2d(s->dz0, creal(dz0), cimag(dz0));D;
  }
  glUseProgram(0);D;
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
  glEnableVertexAttribArray(s->cne0);D;
  glEnableVertexAttribArray(s->zdz0);D;
  glVertexAttribLPointer(s->cne0, 4, GL_DOUBLE, (DE ? 8 : 6) * sizeof(GLdouble), 0);D;
  glVertexAttribLPointer(s->zdz0, DE ? 4 : 2, GL_DOUBLE, (DE ? 8 : 6) * sizeof(GLdouble), ((GLbyte *)0)+(4*sizeof(GLdouble)));D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  debug_message("VBO step: %d -> ?\n", vbo);
}

void fp64_step_do(struct fp64_step *s, GLuint *active_count, GLuint vbo, GLuint query) {
  glBindVertexArray(s->vao);D;
  glUseProgram(s->program);D;
  glEnable(GL_RASTERIZER_DISCARD);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo);D;
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);D;
  glBeginTransformFeedback(GL_POINTS);D;
  glDrawArrays(GL_POINTS, 0, *active_count);D;
  glEndTransformFeedback();D;
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
  glGetQueryObjectuiv(query, GL_QUERY_RESULT, active_count);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
  glDisable(GL_RASTERIZER_DISCARD);D;
  glUseProgram(0);D;
  glBindVertexArray(0);D;
  debug_message("VBO step: ? -> %d\n", vbo);
}

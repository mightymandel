// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <stdlib.h>

#include "fpxx_step.h"
#include "shader.h"
#include "logging.h"
#include "texture.h"

extern const char *fpxx_step_vert;
const GLchar *fpxx_step_varyings[] = {"cne", "zdz", "err"};

void fpxx_step_begin(struct fpxx_step *s) {
  s->program = compile_program_tf("fpxx_step", fpxx_step_vert, 0, 0, 3, fpxx_step_varyings);
  s->escaped = glGetUniformLocation(s->program, "escaped");D;
  s->er2 = glGetUniformLocation(s->program, "er2");D;
  s->zdz0s = glGetUniformLocation(s->program, "zdz0s");D;
  s->cne0 = glGetAttribLocation(s->program, "cne0");D;
  s->zdz0 = glGetAttribLocation(s->program, "zdz0");D;
  s->err0 = glGetAttribLocation(s->program, "err0");D;
  glGenTextures(1, &s->zdz0t);D;
  glActiveTexture(GL_TEXTURE0 + TEX_ORBIT);D;
  glBindTexture(GL_TEXTURE_2D, s->zdz0t);D;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);D;
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);D;
  glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32UI_EXT, FPXX_STEP_ITERS, DE ? 2 : 1, 0, GL_RGBA_INTEGER, GL_UNSIGNED_INT, 0);D;
  mpfr_inits2(53, s->cx, s->cy, s->zx, s->zy, s->dzx, s->dzy, s->dzx0, s->dzy0, s->t1, s->t2, s->t3, s->t4, (mpfr_ptr) 0);
  glGenVertexArrays(1, &s->vao);D;
  s->orbit = 0;
  s->current = 0;
}

void fpxx_step_end(struct fpxx_step *s) {
  glDeleteProgram(s->program);D;
  s->program = 0;
  glDeleteTextures(1, &s->zdz0t);D;
  s->zdz0t = 0;
  mpfr_clears(s->cx, s->cy, s->zx, s->zy, s->dzx, s->dzy, s->dzx0, s->dzy0, s->t1, s->t2, s->t3, s->t4, (mpfr_ptr) 0);
  glDeleteVertexArrays(1, &s->vao);D;
  s->vao = 0;
  // free old orbit
  struct fpxx_step_orbit *orbit = s->orbit;
  while (orbit) {
    struct fpxx_step_orbit *next = orbit->next;
    free(orbit);
    orbit = next;
  }
  s->orbit = 0;
  s->current = 0;
}

void fpxx_step_start(struct fpxx_step *s, GLuint vbo, double escaperadius2, const mpfr_t dzx0, const mpfr_t dzy0, const mpfr_t zx, const mpfr_t zy, const mpfr_t dzx, const mpfr_t dzy, const mpfr_t refx, const mpfr_t refy, bool initial_slice) {
  if (initial_slice) {
    // free old orbit
    struct fpxx_step_orbit *orbit = s->orbit;
    while (orbit) {
      struct fpxx_step_orbit *next = orbit->next;
      free(orbit);
      orbit = next;
    }
    s->orbit = 0;
    s->current = 0;
    // update reference parameters
    mpfr_prec_t p = mpfr_get_prec(refx);
    mpfr_set_prec(s->cx,  p); mpfr_set(s->cx, refx, MPFR_RNDN);
    mpfr_set_prec(s->cy,  p); mpfr_set(s->cy, refy, MPFR_RNDN);
    mpfr_set_prec(s->zx,  p); mpfr_set(s->zx, zx, MPFR_RNDN);
    mpfr_set_prec(s->zy,  p); mpfr_set(s->zy, zy, MPFR_RNDN);
    mpfr_set_prec(s->dzx, p); mpfr_set(s->dzx, dzx, MPFR_RNDN);
    mpfr_set_prec(s->dzy, p); mpfr_set(s->dzy, dzy, MPFR_RNDN);
    mpfr_set_prec(s->t1,  p);
    mpfr_set_prec(s->t2,  p);
    mpfr_set_prec(s->t3,  p);
    mpfr_set_prec(s->t4,  p);
    // handle pre-scaling
    mpfr_set_prec(s->dzx0, p); mpfr_set(s->dzx0, dzx0, MPFR_RNDN);
    mpfr_set_prec(s->dzy0, p); mpfr_set(s->dzy0, dzy0, MPFR_RNDN);
    mpfr_mul(s->t1, s->dzx, s->dzx0, MPFR_RNDN);
    mpfr_mul(s->t2, s->dzy, s->dzx0, MPFR_RNDN);
    mpfr_mul(s->t3, s->dzx, s->dzy0, MPFR_RNDN);
    mpfr_mul(s->t4, s->dzy, s->dzy0, MPFR_RNDN);
    mpfr_sub(s->dzx, s->t1, s->t4, MPFR_RNDN);
    mpfr_add(s->dzy, s->t2, s->t3, MPFR_RNDN);
    // set up shader uniforms
    glUseProgram(s->program);D;
    glUniform1d(s->er2, escaperadius2);D;
    glUniform1i(s->zdz0s, TEX_ORBIT);D;
    glUseProgram(0);D;
    // set up vertex attributes
    glBindVertexArray(s->vao);D;
    glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
    glEnableVertexAttribArray(s->cne0);D;
    glEnableVertexAttribArray(s->zdz0);D;
    glEnableVertexAttribArray(s->err0);D;
    glVertexAttribLPointer(s->cne0, 4, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), 0);D;
    glVertexAttribLPointer(s->zdz0, DE ? 4 : 2, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), ((GLbyte *)0)+(4*sizeof(GLdouble)));D;
    glVertexAttribLPointer(s->err0, 2, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), ((GLbyte *)0)+((DE ? 8 : 6)*sizeof(GLdouble)));D;
    glBindBuffer(GL_ARRAY_BUFFER, 0);D;
    glBindVertexArray(0);D;
    debug_message("VBO step: %d -> ?\n", vbo);
  }
  s->current = s->orbit;
}

typedef union { double d; GLuint u[2]; } doubleint;

void fpxx_u2(GLuint *o, mpfr_t i) {
  doubleint x;
  x.d = mpfr_get_d(i, MPFR_RNDN);
  o[0] = x.u[0];
  o[1] = x.u[1];
}

void fpxx_step_do(struct fpxx_step *s, GLuint *active_count, GLuint vbo, GLuint query) {
  if (! s->current) {
    // allocate a new block
    s->current = malloc(sizeof(struct fpxx_step_orbit));
    s->current->next = 0;
    // compute a reference block
    for (int i = 0; i < FPXX_STEP_ITERS; ++i) {
      // buffer <- z, dz
      fpxx_u2(&s->current->buffer[0][i][0], s->zx);
      fpxx_u2(&s->current->buffer[0][i][2], s->zy);
      if (DE) {
        fpxx_u2(&s->current->buffer[1][i][0], s->dzx);
        fpxx_u2(&s->current->buffer[1][i][2], s->dzy);
        // dz <- 2 * z * dz + dz0
        mpfr_mul(s->t1, s->zx, s->dzx, MPFR_RNDN);
        mpfr_mul(s->t2, s->zy, s->dzy, MPFR_RNDN);
        mpfr_sub(s->t3, s->t1, s->t2, MPFR_RNDN);
        mpfr_mul(s->t1, s->zx, s->dzy, MPFR_RNDN);
        mpfr_mul(s->t2, s->zy, s->dzx, MPFR_RNDN);
        mpfr_mul_2ui(s->dzx, s->t3, 1, MPFR_RNDN);
        mpfr_add(s->dzy, s->t1, s->t2, MPFR_RNDN);
        mpfr_mul_2ui(s->dzy, s->dzy, 1, MPFR_RNDN);
        mpfr_add(s->dzx, s->dzx, s->dzx0, MPFR_RNDN);
        mpfr_add(s->dzy, s->dzy, s->dzy0, MPFR_RNDN);
      }
      // z <- z * z + c
      mpfr_sqr(s->t1, s->zx, MPFR_RNDN);
      mpfr_sqr(s->t2, s->zy, MPFR_RNDN);
      mpfr_mul(s->zy, s->zx, s->zy, MPFR_RNDN);
      mpfr_mul_2ui(s->zy, s->zy, 1, MPFR_RNDN);
      mpfr_sub(s->zx, s->t1, s->t2, MPFR_RNDN);
      mpfr_add(s->zx, s->zx, s->cx, MPFR_RNDN);
      mpfr_add(s->zy, s->zy, s->cy, MPFR_RNDN);
    }
    // append it to the orbit
    if (! s->orbit) {
      s->orbit = s->current;
    } else {
      struct fpxx_step_orbit *last = s->orbit;
      while (last->next) {
        last = last->next;
      }
      last->next = s->current;
    }
  }
  // upload block reference
  glActiveTexture(GL_TEXTURE0 + TEX_ORBIT);D;
  glTexSubImage2D(GL_TEXTURE_2D, 0, 0, 0, FPXX_STEP_ITERS, DE ? 2 : 1, GL_RGBA_INTEGER, GL_UNSIGNED_INT, &s->current->buffer[0][0][0]);D;
  // advance block reference for next time
  s->current = s->current->next;
  // transform vertices
  glBindVertexArray(s->vao);D;
  glUseProgram(s->program);D;
  glUniform1i(s->escaped, 0);D; // FIXME what about escaping references??
  glEnable(GL_RASTERIZER_DISCARD);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo);D;
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);D;
  glBeginTransformFeedback(GL_POINTS);D;
  glDrawArrays(GL_POINTS, 0, *active_count);D;
  glEndTransformFeedback();D;
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
  int before = *active_count;
  glGetQueryObjectuiv(query, GL_QUERY_RESULT, active_count);D;
  int after = *active_count;
  debug_message("step active_count: %d -> %d\n", before, after);
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
  glDisable(GL_RASTERIZER_DISCARD);D;
  glUseProgram(0);D;
  glBindVertexArray(0);D;
  debug_message("VBO step: ? -> %d\n", vbo);
}

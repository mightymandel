// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

uniform double er2;
#ifdef DE
uniform dvec2 dz0;
#endif
in dvec4 cne0;
#ifdef DE
in dvec4 zdz0;
#else
in dvec2 zdz0;
#endif
out dvec4 cne;
#ifdef DE
out dvec4 zdz;
#else
out dvec2 zdz;
#endif

// interior checking
// https://en.wikipedia.org/wiki/Mandelbrot_set#Cardioid_.2F_bulb_checking
bool interior(dvec2 c) {
  // check period 1 cardioid
  double x = c.x - 0.25;
  double q = x * x + c.y * c.y;
  bool cardioid = q * (q + x) < 0.25 * c.y * c.y;
  if (cardioid) {
    return true;
  }
  // check period 2 circle
  x = c.x + 1.0;
  bool circle = x * x + c.y * c.y < 0.0625;
  if (circle) {
    return true;
  }
  // don't check any others, for now
  return false;
}

void main() {
  dvec2 c = cne0.xy;
  double n = cne0.z;
  double e = cne0.w;
  dvec2 z = zdz0.xy;
#ifdef DE
  dvec2 dz = zdz0.zw;
#endif
  // check interior on first iteration only, use n < 0 as a sentinel later
  if (n == 0.0 && interior(c)) {
    n = -1.0;
  } else if (n >= 0.0) {
    int j = 0;
    for (int i = 0; i < FP64_STEP_ITERS; ++i) {
      if (e <= 0.0) {
        j += 1;
#ifdef DE
        dz = 2.0 * cmul(z, dz) + dz0;
#endif
        z = csqr(z) + c;
        e = cmag2(z) - er2;
      } else {
        break;
      }
    }
    n += double(j);
  }
  cne = dvec4(c, n, e);
#ifdef DE
  zdz = dvec4(z, dz);
#else
  zdz = dvec2(z);
#endif
}

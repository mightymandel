// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

uniform float aspect;
uniform vec2 slice_offset;
in  vec4 p;
smooth out vec2 s;
smooth out vec2 t;
void main() {
  gl_Position = vec4(p.xy * vec2(aspect, 1.0), 0.0, 1.0);
  t = p.xy + slice_offset;
  s = p.zw + slice_offset * 0.5 * vec2(aspect, 1.0);
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>
#include <string.h>

#include "render.h"
#include "logging.h"
#include "image.h"

enum log_level_t log_level = LOG_NOTICE;
FILE *log_target = 0;
const char *log_prefix[7] = {
  "OOPS\t",
  "FATAL\t",
  "ERROR\t",
  "WARN\t",
  "NOTICE\t",
  "INFO\t",
  "DEBUG\t"
};

const char *result_name[5] = {
  "ok",
  "glitch",
  "parse",
  "deep",
  "timeout"
};

void log_result(const char *filename, enum result_t result, enum render_method_t method, double iterations, double exterior, double interior, double glitch, int passes) {
  const char *basename = filename;
  if (basename) { basename = strrchr(basename, '/'); }
  if (basename) { basename++; }
  if (! basename) { basename = filename; }
  if (! basename) { basename = ""; }
  // "# filename,result,method,iterations,exterior,interior,glitch,passes"
  switch (result) {
    case result_parse:
      printf("%s,%s,,,,,,\n", basename, result_name[result]);
      break;
    case result_ok:
    case result_glitch:
    case result_timeout:
      switch (method) {
        case render_method_fp32:
        case render_method_fp64:
          printf("%s,%s,%s,%f,%f,%f,,\n", basename, result_name[result], render_method_name[method], iterations, exterior, interior);
          break;
        case render_method_fpxx:
          printf("%s,%s,%s,%f,%f,%f,%f,%d\n", basename, result_name[result], render_method_name[method], iterations, exterior, interior, glitch, passes);
          break;
      }
      break;
    case result_deep:
      printf("%s,%s,,,,,,\n", basename, result_name[result]);
      break;
  }
}

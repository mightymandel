// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef REF_SET_H
#define REF_SET_H 1

/*!
\file ref_set.h
\brief Collection of reference coordinates.
*/

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief A node in the reference set.

Should be read-only outside ref_set.c.
*/
struct ref_set_node {
  struct ref_set_node *next; //!< The next node in the list, or 0.
  mpfr_t x;                  //!< The real coordinate of the reference.
  mpfr_t y;                  //!< The imaginary coordinate of the reference.
};

/*!
\brief A set of references.

Should be read-only outside ref_set.c.
*/
struct ref_set {
  struct ref_set_node *set;  //!< The list of references, or 0 if empty.
};

struct ref_set *ref_set_new();
void ref_set_delete(struct ref_set *s);
void ref_set_insert(struct ref_set *s, const mpfr_t x, const mpfr_t y);
bool ref_set_contains(const struct ref_set *s, const mpfr_t x, const mpfr_t y);

#endif

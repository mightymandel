// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "zoom.h"

void zoom_begin(struct zoom *zoom, int frames, const mpfr_t centerx, const mpfr_t centery) {
  mpfr_inits2(53, zoom->centerx, zoom->centery, zoom->radius, (mpfr_ptr) 0);
  zoom->frame = -1;
  zoom->zoom_frames = frames;
  mpfr_set_prec(zoom->centerx, mpfr_get_prec(centerx));
  mpfr_set_prec(zoom->centery, mpfr_get_prec(centery));
  mpfr_set(zoom->centerx, centerx, MPFR_RNDN);
  mpfr_set(zoom->centery, centery, MPFR_RNDN);
  mpfr_set_si(zoom->radius, 256, MPFR_RNDN);
}

void zoom_end(struct zoom *zoom) {
  mpfr_clears(zoom->centerx, zoom->centery, zoom->radius, (mpfr_ptr) 0);
}

bool zoom_next(struct zoom *zoom) {
  zoom->frame++;
  if (zoom->frame >= zoom->zoom_frames) {
    return false;
  }
  mpfr_mul_2si(zoom->radius, zoom->radius, -1, MPFR_RNDN);
  return true;
}

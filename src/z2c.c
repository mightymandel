// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdlib.h>

#include "z2c.h"

#include "z2c_4.h"
#include "z2c_6.h"
#include "z2c_8.h"
#include "z2c_12.h"
#include "z2c_16.h"
#include "z2c_24.h"
#include "z2c_32.h"
#include "z2c_48.h"
#include "z2c_64.h"

struct z2c_series {
  int order;
  void *series;
};

struct z2c_series *z2c_series_new(const int order, const mpfr_t cx, const mpfr_t cy) {
  struct z2c_series *s = calloc(1, sizeof(*s));
  if (! s) { return 0; }
  s->order = order;
  switch (order) {
    case  4: s->series =  z2c_4_series_new(cx, cy); break;
    case  6: s->series =  z2c_6_series_new(cx, cy); break;
    case  8: s->series =  z2c_8_series_new(cx, cy); break;
    case 12: s->series = z2c_12_series_new(cx, cy); break;
    case 16: s->series = z2c_16_series_new(cx, cy); break;
    case 24: s->series = z2c_24_series_new(cx, cy); break;
    case 32: s->series = z2c_32_series_new(cx, cy); break;
    case 48: s->series = z2c_48_series_new(cx, cy); break;
    case 64: s->series = z2c_64_series_new(cx, cy); break;
  }
  if (! s->series) {
    free(s);
    return 0;
  }
  return s;
}

void z2c_series_delete(struct z2c_series *s) {
  if (! s) {
    return;
  }
  switch (s->order) {
    case  4:  z2c_4_series_delete(s->series); break;
    case  6:  z2c_6_series_delete(s->series); break;
    case  8:  z2c_8_series_delete(s->series); break;
    case 12: z2c_12_series_delete(s->series); break;
    case 16: z2c_16_series_delete(s->series); break;
    case 24: z2c_24_series_delete(s->series); break;
    case 32: z2c_32_series_delete(s->series); break;
    case 48: z2c_48_series_delete(s->series); break;
    case 64: z2c_64_series_delete(s->series); break;
  }
  free(s);
}

bool z2c_series_step(struct z2c_series *s, const int exponent, const int threshold) {
  if (! s) {
    return false;
  }
  switch (s->order) {
    case  4: return  z2c_4_series_step(s->series, exponent, threshold);
    case  6: return  z2c_6_series_step(s->series, exponent, threshold);
    case  8: return  z2c_8_series_step(s->series, exponent, threshold);
    case 12: return z2c_12_series_step(s->series, exponent, threshold);
    case 16: return z2c_16_series_step(s->series, exponent, threshold);
    case 24: return z2c_24_series_step(s->series, exponent, threshold);
    case 32: return z2c_32_series_step(s->series, exponent, threshold);
    case 48: return z2c_48_series_step(s->series, exponent, threshold);
    case 64: return z2c_64_series_step(s->series, exponent, threshold);
  }
  return false;
}

int z2c_series_get_n(const struct z2c_series *s) {
  if (! s) {
    return 0;
  }
  switch (s->order) {
    case  4: return  z2c_4_series_get_n(s->series);
    case  6: return  z2c_6_series_get_n(s->series);
    case  8: return  z2c_8_series_get_n(s->series);
    case 12: return z2c_12_series_get_n(s->series);
    case 16: return z2c_16_series_get_n(s->series);
    case 24: return z2c_24_series_get_n(s->series);
    case 32: return z2c_32_series_get_n(s->series);
    case 48: return z2c_48_series_get_n(s->series);
    case 64: return z2c_64_series_get_n(s->series);
  }
  return 0;
}

struct z2c_reference *z2c_series_reference_new(const struct z2c_series *s) {
  if (! s) {
    return 0;
  }
  switch (s->order) {
    case  4: return  z2c_4_reference_new(s->series);
    case  6: return  z2c_6_reference_new(s->series);
    case  8: return  z2c_8_reference_new(s->series);
    case 12: return z2c_12_reference_new(s->series);
    case 16: return z2c_16_reference_new(s->series);
    case 24: return z2c_24_reference_new(s->series);
    case 32: return z2c_32_reference_new(s->series);
    case 48: return z2c_48_reference_new(s->series);
    case 64: return z2c_64_reference_new(s->series);
  }
  return 0;
}

struct z2c_approx {
  int order;
  void *approx;
};

struct z2c_approx *z2c_series_approx_new(const struct z2c_series *s, const int exponent) {
  if (! s) {
    return 0;
  }
  struct z2c_approx *a = calloc(1, sizeof(*a));
  a->order = s->order;
  switch (s->order) {
    case  4: a->approx =  z2c_4_approx_new(s->series, exponent); break;
    case  6: a->approx =  z2c_6_approx_new(s->series, exponent); break;
    case  8: a->approx =  z2c_8_approx_new(s->series, exponent); break;
    case 12: a->approx = z2c_12_approx_new(s->series, exponent); break;
    case 16: a->approx = z2c_16_approx_new(s->series, exponent); break;
    case 24: a->approx = z2c_24_approx_new(s->series, exponent); break;
    case 32: a->approx = z2c_32_approx_new(s->series, exponent); break;
    case 48: a->approx = z2c_48_approx_new(s->series, exponent); break;
    case 64: a->approx = z2c_64_approx_new(s->series, exponent); break;
  }
  if (! a->approx) {
    free(a);
    return 0;
  }
  return a;
}

void z2c_approx_delete(struct z2c_approx *a) {
  if (! a) {
    return;
  }
  free(a->approx);  // FIXME check this
  free(a);
}

int z2c_approx_get_order(const struct z2c_approx *a) {
  if (! a) {
    return 0;
  }
  return a->order;
}

int z2c_approx_get_exponent(const struct z2c_approx *a) {
  if (! a) {
    return 0;
  }
  switch (a->order) {
    case  4: return  z2c_4_approx_get_exponent(a->approx);
    case  6: return  z2c_6_approx_get_exponent(a->approx);
    case  8: return  z2c_8_approx_get_exponent(a->approx);
    case 12: return z2c_12_approx_get_exponent(a->approx);
    case 16: return z2c_16_approx_get_exponent(a->approx);
    case 24: return z2c_24_approx_get_exponent(a->approx);
    case 32: return z2c_32_approx_get_exponent(a->approx);
    case 48: return z2c_48_approx_get_exponent(a->approx);
    case 64: return z2c_64_approx_get_exponent(a->approx);
  }
  return 0;
}

const complex double *z2c_approx_get_coefficients(const struct z2c_approx *a) {
  if (! a) {
    return 0;
  }
  switch (a->order) {
    case  4: return  z2c_4_approx_get_coefficients(a->approx);
    case  6: return  z2c_6_approx_get_coefficients(a->approx);
    case  8: return  z2c_8_approx_get_coefficients(a->approx);
    case 12: return z2c_12_approx_get_coefficients(a->approx);
    case 16: return z2c_16_approx_get_coefficients(a->approx);
    case 24: return z2c_24_approx_get_coefficients(a->approx);
    case 32: return z2c_32_approx_get_coefficients(a->approx);
    case 48: return z2c_48_approx_get_coefficients(a->approx);
    case 64: return z2c_64_approx_get_coefficients(a->approx);
  }
  return 0;
}

complex double z2c_approx_do(const struct z2c_approx *a, const complex double dc) {
  if (! a) {
    return 0;
  }
  switch (a->order) {
    case  4: return  z2c_4_approx_do(a->approx, dc);
    case  6: return  z2c_6_approx_do(a->approx, dc);
    case  8: return  z2c_8_approx_do(a->approx, dc);
    case 12: return z2c_12_approx_do(a->approx, dc);
    case 16: return z2c_16_approx_do(a->approx, dc);
    case 24: return z2c_24_approx_do(a->approx, dc);
    case 32: return z2c_32_approx_do(a->approx, dc);
    case 48: return z2c_48_approx_do(a->approx, dc);
    case 64: return z2c_64_approx_do(a->approx, dc);
  }
  return 0;
}


// HACK

struct z2c_reference {
  mpfr_t v[10];
  int n;
};

void z2c_reference_get_z(const struct z2c_reference *ref, mpfr_t zx, mpfr_t zy) {
  mpfr_set_prec(zx, mpfr_get_prec(ref->v[2]));
  mpfr_set_prec(zy, mpfr_get_prec(ref->v[3]));
  mpfr_set(zx, ref->v[2], MPFR_RNDN);
  mpfr_set(zy, ref->v[3], MPFR_RNDN);
}

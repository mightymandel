// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

uniform double radius;
uniform dvec2  center;
in  vec4 c;
out dvec4 cne;
#ifdef DE
out dvec4 zdz;
#else
out dvec2 zdz;
#endif
void main() {
  cne = dvec4(radius * dvec2(c.xy) + center, 0.0, 0.0);
#ifdef DE
  zdz = dvec4(0.0, 0.0, 0.0, 0.0);
#else
  zdz = dvec2(0.0, 0.0);
#endif
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

uniform float loger2;
uniform float pxs;
flat in vec4 cne;
#ifdef DE
flat in vec4 zdz;
#else
flat in vec2 zdz;
#endif
layout(location = 0, index = 0) out vec4 ida;
void main() {
  float z2 = cmag2(zdz.xy);
  float logz2 = log(z2);
  float i = 1.0 + cne.z - clamp(log2(logz2 / loger2), 0.00001, 0.99999);
#ifdef DE
  float dz2 = cmag2(zdz.zw);
  float d = logz2 * sqrt(z2 / dz2) * pxs;
#else
  float d = 0;
#endif
  ida = vec4(i, 0.0, d, 0.0);
}

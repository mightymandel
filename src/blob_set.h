// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef BLOB_SET_H
#define BLOB_SET_H 1

/*!
\file  blob_set.h
\brief Blob extraction and disjoint collection of blobs (specification).
*/

#include <stdbool.h>
#include <stdint.h>

/*!
\brief Blob information.
*/
struct blob {
  int label; //!< Label for union-find (internal use).
  int count; //!< Number of pixels in the blob.
  int64_t i; //!< Total of pixel i coordinates.
  int64_t j; //!< Total of pixel j coordinates.
  int min_i; //!< Minimum pixel i coordinate.
  int max_i; //!< Maximum pixel i coordinate.
  int min_j; //!< Minimum pixel j coordinate.
  int max_j; //!< Maximum pixel j coordinate.
  float error; //!< Blob error count.
};

/*!
\brief Opaque blob set collection structure.
*/
struct blob_set;

/*!
\brief Create a new empty blob set.

\return A blob set.
*/
struct blob_set *blob_set_new();

/*!
\brief Delete a blob set.

\param s A blob set.
*/
void blob_set_delete(struct blob_set *s);

/*!
\brief Insert a blob into a blob set.

\param s A blob set.
\param blob A blob.
*/
void blob_set_insert(struct blob_set *s, const struct blob *blob);

/*!
\brief Check if a blob is contained in a blob set.

\param s A blob set.
\param blob A blob.
\return `true` if the blob is in the set, `false` otherwise.
*/
bool blob_set_contains(const struct blob_set *s, const struct blob *blob);

/*!
\brief Blob extraction strategy.
*/
enum blob_strategy {
  blob_boolean, //!< Consider all glitched pixels equal.
  blob_positive //!< Consider glitched pixels equal only if their glitch counts are equal too.
};

/*!
\brief Extract blobs from an image region using a strategy.

\param blob_count The number of extracted blobs is stored here.
\param width The glitch count image width.
\param height The glitch count image height.
\param glitched The glitch count image.
\param strategy The strategy to use.
\param i0 The lower i coordinate of the image region (eg: 0).
\param i1 The upper i coordinate of the image region (eg: width).
\param j0 The lower j coordinate of the image region (eg: 0).
\param j1 The upper j coordinate of the image region (eg: height).
\return An array of blobs, sorted by count (blob_boolean) or glitch count (blob_positive), largest first.
*/
struct blob *find_blobs0(int *blob_count, int width, int height, const float *glitched, enum blob_strategy strategy, int i0, int i1, int j0, int j1);

/*!
\brief Extract blobs from a whole image using `blob_boolean` strategy.

\param blob_count The number of extracted blobs is stored here.
\param width The glitch count image width.
\param height The glitch count image height.
\param glitched The glitch count image.
\return An array of blobs, sorted by count, largest first.
*/
struct blob *find_blobs1(int *blob_count, int width, int height, const float *glitched);

/*!
\brief Extract sub-blobs from an image using `blob_positive` strategy.

\param blob_count The number of extracted blobs is stored here.
\param width The glitch count image width.
\param height The glitch count image height.
\param glitched The glitch count image.
\param blob The blob determines the region to search within.
\return An array of sub-blobs, sorted by glitch count, largest first.
*/
struct blob *find_blobs2(int *blob_count, int width, int height, const float *glitched, const struct blob *blob);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>

#include "parse.h"
#include "parse_ppar_corners.h"
#include "logging.h"

bool parse_ppar_corners(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
  (void) length;
  char *source2 = strdup(source);
  char *s = source2;
  char *corners = 0; // CORNERS=[xmin/xmax/ymin/ymax[/x3rd/y3rd]]
  while (s) {
    char *line = parse_line(&s);
    if (0 == strncmp(line, "corners=", 8)) {
      corners = line + 8;
      debug_message("parse_ppar: corners = %s\n", corners);
      int len = strlen(line);
      char *sxmin = malloc(len); sxmin[0] = 0; //  string for xmin
      char *sxmax = malloc(len); sxmax[0] = 0; //  string for xmax
      char *symin = malloc(len); symin[0] = 0; //  string for ymin
      char *symax = malloc(len); symax[0] = 0; //  string for ymax
      s = strtok(corners, "/");
      strcpy(sxmin, s);
      int i = 2;
      while(s != NULL) {
        s = strtok(NULL, "/");
        if (s) {
          switch (i) {
            case 2: strcpy(sxmax, s); break;
            case 3: strcpy(symin, s); break;
            case 4: strcpy(symax, s); break;
          }
        }
        i++;
      }
      //check
      debug_message("parse_ppar: sxmin = %s ; sxmax = %s ; symin = %s ; symax = %s\n", sxmin, sxmax, symin, symax);
      // variables
      mpfr_t xmin, xmax, ymin, ymax; // corners
      mpfr_t dx, dy;                 // d# = #max - #min
      // initialize
      mpfr_inits2(256, xmin, xmax, ymin, ymax, dx, dy, (mpfr_ptr) 0); // FIXME hardcoded precision
      mpfr_set_str(xmin, sxmin, 10, MPFR_RNDN);
      mpfr_set_str(xmax, sxmax, 10, MPFR_RNDN);
      mpfr_set_str(ymin, symin, 10, MPFR_RNDN);
      mpfr_set_str(ymax, symax, 10, MPFR_RNDN);
      debug_message("parse_ppar: xmin = %Re\n", xmin);
      debug_message("parse_ppar: xmax = %Re\n", xmax);
      debug_message("parse_ppar: ymin = %Re\n", ymin);
      debug_message("parse_ppar: ymax = %Re\n", ymax);
      // computing:
      // cx = center x = xmin + (xmax - xmin) / 2
      // cy = center y = ymin + radius
      // cz = radius   =        (ymax - ymin) / 2
      mpfr_sub(dx, xmax, xmin, MPFR_RNDN);
      mpfr_sub(dy, ymax, ymin, MPFR_RNDN);
      debug_message("parse_ppar: dx = %Re\n", dx);
      debug_message("parse_ppar: dy = %Re\n", dy);
      mpfr_div_2ui(dy, dy, 1, MPFR_RNDN);
      mpfr_div_2ui(dx, dx, 1, MPFR_RNDN);
      mpfr_abs(cz, dy, MPFR_RNDN);
      debug_message("parse_ppar: radius = cz = %Re\n", cz);
      if (! radius_is_valid(cz)) {
        free(sxmin);
        free(sxmax);
        free(symin);
        free(symax);
        free(source2);
        return false;
      }
      mpfr_prec_t p = precision_for_radius(cz);
      mpfr_set_prec(cy, p);
      mpfr_add(cy, ymin, dy, MPFR_RNDN);
      debug_message("parse_ppar: cy = %Re\n", cy);
      mpfr_set_prec(cx, p);
      mpfr_add(cx, xmin, dx, MPFR_RNDN);
      debug_message("parse_ppar: cx = %Re\n", cx);
      // cleanup
      mpfr_clears(xmin, xmax, ymin, ymax, dx, dy, (mpfr_ptr) 0);
      free(sxmin);
      free(sxmax);
      free(symin);
      free(symax);
      free(source2);
      return true;
    }
  }
  free(source2);
  return false;
}

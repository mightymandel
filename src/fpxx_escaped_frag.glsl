// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

flat in vec4 ida1;
layout(location = 0, index = 0) out vec4 ida;
void main() {
  ida = ida1;
}

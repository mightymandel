// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef COMPLEX_H
#define COMPLEX_H 1

/*!
\file  complex.h
\brief Helpers for complex number arithemetic with mpfr.

All variables must already be `mpfr_init2()`d with appropriate precision.
Implemented as CPP macros as a last resort (the C++ version used reference
parameters).
*/

#include <mpfr.h>

/*!
\brief Real number type.
*/
typedef mpfr_t R;

/*!
\brief Complex number type.
*/
typedef struct {
  R x; //!< Real part.
  R y; //!< Imaginary part.
} C;

/*!
\brief Set the precision of both parts of a complex number.

\param o The complex number.
\param p The precision.
*/
#define c_set_prec(o, p) do{\
  mpfr_set_prec(o.x, p);\
  mpfr_set_prec(o.y, p);\
}while(0)

/*!
\brief Set both parts of a complex number.

\param o The complex number to modify.
\param l The complex number to use as source.
*/
#define c_set(o, l) do{\
  mpfr_set(o.x, l.x, MPFR_RNDN);\
  mpfr_set(o.y, l.y, MPFR_RNDN);\
}while(0)

/*!
\brief Compute complex magnitude squared.

\param o The real number for output.
\param l The complex number input.
\param t1 A real temporary variable.
\param t2 A real temporary variable.
*/
#define c_mag2(o, l, t1, t2) do{\
  mpfr_sqr(t1, l.x, MPFR_RNDN);\
  mpfr_sqr(t2, l.y, MPFR_RNDN);\
  mpfr_add(o, t1, t2, MPFR_RNDN);\
}while(0)

/*!
\brief Add two complex numbers.

\param o The complex number for output.
\param l The first complex number input.
\param r The second complex number input.
*/
#define c_add(o, l, r) do{\
  mpfr_add(o.x, l.x, r.x, MPFR_RNDN);\
  mpfr_add(o.y, l.y, r.y, MPFR_RNDN);\
}while(0)

/*!
\brief Square a complex number.

\param o The complex number for output.
\param l The complex number input.
\param t1 A real temporary variable.
\param t2 A real temporary variable.
\param t3 A real temporary variable.
*/
#define c_sqr(o, l, t1, t2, t3) do{\
  mpfr_sqr(t1, l.x, MPFR_RNDN);\
  mpfr_sqr(t2, l.y, MPFR_RNDN);\
  mpfr_mul(t3, l.x, l.y, MPFR_RNDN);\
  mpfr_sub(o.x, t1, t2, MPFR_RNDN);\
  mpfr_mul_2ui(o.y, t3, 1, MPFR_RNDN);\
}while(0)

/*!
\brief Multiply two complex numbers.

\param o The complex number for output.
\param l The first complex number input.
\param r The first complex number input.
\param t1 A real temporary variable.
\param t2 A real temporary variable.
\param t3 A real temporary variable.
\param t4 A real temporary variable.
*/
#define c_mul(o, l, r, t1, t2, t3, t4) do{\
  mpfr_mul(t1, l.x, r.x, MPFR_RNDN);\
  mpfr_mul(t2, l.y, r.y, MPFR_RNDN);\
  mpfr_mul(t3, l.x, r.y, MPFR_RNDN);\
  mpfr_mul(t4, l.y, r.x, MPFR_RNDN);\
  mpfr_sub(o.x, t1, t2, MPFR_RNDN);\
  mpfr_add(o.y, t3, t4, MPFR_RNDN);\
}while(0)

/*!
\brief Multiply a complex number by a power of two.

\param o The complex number for output.
\param l The complex number input.
\param r The unsigned integer power of two.
*/
#define c_mul_2ui(o, l, r) do{\
  mpfr_mul_2ui(o.x, l.x, r, MPFR_RNDN);\
  mpfr_mul_2ui(o.y, l.y, r, MPFR_RNDN);\
}while(0)

#endif

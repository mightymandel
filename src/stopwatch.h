// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef STOPWATCH_H
#define STOPWATCH_H 1

/*!
\file stopwatch.h
\brief Stopwatch timers for measuring elapsed time.

Typical usage:

    struct stopwatch *t = stopwatch_new();
    stopwatch_reset(t);
    stopwatch_start(t);
    do_stuff();
    stopwatch_stop(t);
    do_other_stuff();
    stopwatch_start(t);
    do_stuff();
    stopwatch_stop(t);
    double s = stopwatch_elapsed(t);
    stopwatch_delete(t);

Then `s` contains the elapsed time in seconds for both `do_stuff()` occasions,
not including the time taken for `do_other_stuff()`.

On POSIX, uses `clock_gettime()`.  On Windows, uses `QueryPerformanceCounter()`.
*/

/*!
\brief Opaque stopwatch type.
*/
struct stopwatch;

/*!
\brief Create a new stopwatch.
\return The new stopwatch.
*/
struct stopwatch *stopwatch_new();

/*!
\brief Delete a stopwatch.
\param t The stopwatch.
*/
void stopwatch_delete(struct stopwatch *t);

/*!
\brief Reset a stopwatch.
\param t The stopwatch.
*/
void stopwatch_reset(struct stopwatch *t);

/*!
\brief Start a stopwatch.
\param t The stopwatch.
*/
void stopwatch_start(struct stopwatch *t);

/*!
\brief Stop a stopwatch.
\param t The stopwatch.
*/
void stopwatch_stop(struct stopwatch *t);

/*!
\brief Get the elapsed time of a stopwatch.
\param t The stopwatch.
\return The total elapsed time in seconds.
*/
double stopwatch_elapsed(struct stopwatch *t);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <math.h>
#include <stdio.h>

#include "fpxx_escaped.h"
#include "shader.h"
#include "logging.h"

extern const char *fpxx_escaped_vert;
extern const char *fpxx_escaped_geom;
extern const char *fpxx_escaped_frag;

void fpxx_escaped_begin(struct fpxx_escaped *s) {
  s->program = compile_program("fpxx_escaped", fpxx_escaped_vert, fpxx_escaped_geom, fpxx_escaped_frag);
  s->center = glGetUniformLocation(s->program, "center");D;
  s->radius = glGetUniformLocation(s->program, "radius");D;
  s->aspect = glGetUniformLocation(s->program, "aspect");D;
  s->loger2 = glGetUniformLocation(s->program, "loger2");D;
  s->cne0 = glGetAttribLocation(s->program, "cne0");D;
  s->zdz0 = glGetAttribLocation(s->program, "zdz0");D;
  s->err0 = glGetAttribLocation(s->program, "err0");D;
  glGenVertexArrays(1, &s->vao);D;
}

void fpxx_escaped_end(struct fpxx_escaped *s) {
  glDeleteProgram(s->program);D;
  glDeleteVertexArrays(1, &s->vao);D;
}

void fpxx_escaped_start(struct fpxx_escaped *s, GLuint tex, GLuint fbo, GLuint vbo, double escaperadius2, int width, int height, const mpfr_t centerx0, const mpfr_t centery0, const mpfr_t radius0, const mpfr_t refx, const mpfr_t refy) {
  // set up target framebuffer
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);D;
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex, 0);
  GLenum buffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, buffers);D;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
  // set up shader uniforms
  glUseProgram(s->program);D;
  mpfr_t dx, dy;
  mpfr_inits2(mpfr_get_prec(refx), dx, dy, (mpfr_ptr) 0);
  mpfr_sub(dx, centerx0, refx, MPFR_RNDN);
  mpfr_sub(dy, centery0, refy, MPFR_RNDN);
  debug_message("escaped dx: %Re\n", dx);
  debug_message("escaped dy: %Re\n", dy);
  double centerx = mpfr_get_d(dx, MPFR_RNDN);
  double centery = mpfr_get_d(dy, MPFR_RNDN);
  double radius = mpfr_get_d(radius0, MPFR_RNDN);
  mpfr_clears(dx, dy, (mpfr_ptr) 0);
  glUniform2d(s->center, centerx, centery);D;
  glUniform1d(s->radius, radius);D;
  glUniform1d(s->aspect, height / (double) width);D;
  glUniform1d(s->loger2, log(escaperadius2));D;
  glUseProgram(0);D;
  // set up vertex pointers
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
  glVertexAttribLPointer(s->cne0, 4, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), 0);D;
  glVertexAttribLPointer(s->zdz0, DE ? 4 : 2, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), ((GLbyte *)0)+(4*sizeof(GLdouble)));D;
  glVertexAttribLPointer(s->err0, 2, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), ((GLbyte *)0)+((DE ? 8 : 6)*sizeof(GLdouble)));D;
  glEnableVertexAttribArray(s->cne0);D;
  glEnableVertexAttribArray(s->zdz0);D;
  glEnableVertexAttribArray(s->err0);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  debug_message("VBO escaped: %d -> ?\n", vbo);
}

void fpxx_escaped_do(struct fpxx_escaped *s, GLuint active_count) {
  glUseProgram(s->program);D;
  glBindVertexArray(s->vao);D;
  glDrawArrays(GL_POINTS, 0, active_count);D;
  debug_message("escaped active_count: %d\n", active_count);
  glBindVertexArray(0);D;
  glUseProgram(0);D;
  debug_message("VBO escaped: ? -> frag\n");
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef STARTUP_H
#define STARTUP_H 1

#include <stdbool.h>
#include <mpfr.h>

#include "logging.h"

struct options {
  bool size;
  int width;
  int height;
  bool geometry;
  int win_width;
  int win_height;
  int slice;
  double weight;
  double max_glitch;
  int max_blob;
  double sharpness;
  double timeout;
  bool render_de;
  bool series_approx;
  int order;
  bool show_glitches;
  enum log_level_t log_level;
  bool version;
  bool help;
  bool interactive;
  bool oneshot;
  bool tile;
  int tile_width;
  int tile_height;
  bool zoom;
  int zoom_frames;
  bool overhead;
  char *file_to_load;
  bool view_supplied;
  mpfr_t centerx;
  mpfr_t centery;
  mpfr_t radius;
};

void print_version(void);
void print_banner(void);
void print_usage(const char *progname);
bool parse_command_line(struct options *options, int argc, char **argv);

#endif

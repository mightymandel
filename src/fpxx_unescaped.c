// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "fpxx_unescaped.h"
#include "shader.h"
#include "logging.h"

extern const char *fpxx_unescaped_vert;
extern const char *fpxx_unescaped_geom;
const GLchar *fpxx_unescaped_varyings[] = {"cne", "zdz", "err"};

void fpxx_unescaped_begin(struct fpxx_unescaped *s) {
  s->program = compile_program_tf("fpxx_unescaped", fpxx_unescaped_vert, fpxx_unescaped_geom, 0, 3, fpxx_unescaped_varyings);
  s->cne0 = glGetAttribLocation(s->program, "cne0");D;
  s->zdz0 = glGetAttribLocation(s->program, "zdz0");D;
  s->err0 = glGetAttribLocation(s->program, "err0");D;
  glGenVertexArrays(1, &s->vao);D;
}

void fpxx_unescaped_end(struct fpxx_unescaped *s) {
  glDeleteProgram(s->program);D;
  s->program = 0;
  glDeleteVertexArrays(1, &s->vao);D;
  s->vao = 0;
}

void fpxx_unescaped_start(struct fpxx_unescaped *s, GLuint vbo) {
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
  glVertexAttribLPointer(s->cne0, 4, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), 0);D;
  glVertexAttribLPointer(s->zdz0, DE ? 4 : 2, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), ((GLbyte *)0)+(4*sizeof(GLdouble)));D;
  glVertexAttribLPointer(s->err0, 2, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), ((GLbyte *)0)+((DE ? 8 : 6)*sizeof(GLdouble)));D;
  glEnableVertexAttribArray(s->cne0);D;
  glEnableVertexAttribArray(s->zdz0);D;
  glEnableVertexAttribArray(s->err0);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  debug_message("VBO unescaped: %d -> ?\n", vbo);
}

void fpxx_unescaped_do(struct fpxx_unescaped *s, GLuint *unescaped, GLuint active_count, GLuint vbo, GLuint query) {
  glBindVertexArray(s->vao);D;
  glUseProgram(s->program);D;
  glEnable(GL_RASTERIZER_DISCARD);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo);D;
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);D;
  glBeginTransformFeedback(GL_POINTS);D;
  glDrawArrays(GL_POINTS, 0, active_count);D;
  glEndTransformFeedback();D;
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
  int before = active_count;
  glGetQueryObjectuiv(query, GL_QUERY_RESULT, unescaped);D;
  int after = *unescaped;
  debug_message("unescaped active_count: %d -> %d\n", before, after);
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
  glDisable(GL_RASTERIZER_DISCARD);D;
  glUseProgram(0);D;
  glBindVertexArray(0);D;
  debug_message("VBO unescaped: ? -> %d\n", vbo);
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef POLL_H
#define POLL_H 1

#include <GL/glew.h>
#include <GLFW/glfw3.h>

// forward declarations
struct render_options;
struct filename;
struct record;

/*!
\brief Opaque polling state structure
*/
struct poll;

/*!
\brief What the renderer should do after polling.
*/
enum poll_result {
  poll_continue,  //!< Carry on calculating.
  poll_display,   //!< Call the display function.
  poll_abort,     //!< Abort immediately.
  poll_timeout    //!< Finish up cleanly and return.
};

/*!
\brief The type of a polling function.
*/
typedef enum poll_result poll_t(struct poll *);

/*!
\brief Create a new polling state structure.

\return The polling state.
*/
struct poll *poll_new(GLFWwindow *window, bool interactive, struct render_options *render_options, double ui_poll_timeout, double display_timeout, struct filename *filename, struct record *record);

/*!
\brief Delete a polling state structure.

\param poll The polling state.
*/
void poll_delete(struct poll *poll);

/*!
\brief Poll the user interface for any events.

\param poll The polling state.
\param should_wait Should be `false` except in interactive `main()`.
\return What the renderer should do next.
*/
enum poll_result poll_ui(struct poll *poll, bool should_wait);

/*!
\brief Display the result.

\param poll The polling state.
*/
void poll_swap_buffers(struct poll *poll);

void poll_set_timeout(struct poll *poll, double timeout);

#endif

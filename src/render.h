// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef RENDER_H
#define RENDER_H 1

/*!
\file render.h
\brief Manage calculation and rendering.
*/

#include "fp32_fillc.h"
#include "fp32_colour.h"
#include "fp32_init.h"
#include "fp32_step.h"
#include "fp32_unescaped.h"
#include "fp32_escaped.h"
#include "fp64_init.h"
#include "fp64_step.h"
#include "fp64_unescaped.h"
#include "fp64_escaped.h"
#include "fpxx_init.h"
#include "fpxx_approx.h"
#include "fpxx_approx2.h"
#include "fpxx_step.h"
#include "fpxx_unescaped.h"
#include "fpxx_escaped.h"
#include "ref_set.h"
#include "blob_set.h"
#include "completion.h"
#include "stopwatch.h"
#include "poll.h"
#include "slice.h"

extern const char *render_method_name[3];

/*!
\brief Render options state structure.

Should be read-only outside render.c.
*/
struct render_options {
  enum render_method_t method; //!< Calculation method.
  bool calculate_de;           //!< Calculate distance estimates.
  bool series_approx;          //!< Use series approximation optimisation.
  int order;                   //!< Order of series approximation (0 is old way).
  mpfr_t centerx;              //!< View center (real coordinate).
  mpfr_t centery;              //!< View center (imaginary coordinate).
  mpfr_t radius;               //!< View radius
  double weight;               //!< Boundary colouring weight.
  double max_glitch;           //!< Maximum glitch percentage.
  int max_blob;                //!< Maximum glitch blob size in pixels.
  double sharpness;            //!< Interior sharpness threshold.
  bool show_de;                //!< Colour using distance estimates.
  bool show_glitches;          //!< Highlight glitches in ugly colours.
  bool save_screenshot;        //!< Save a screenshot on next display.
  int width;                   //!< Image width in pixels.
  int height;                  //!< Image height in pixels.
  int win_width;               //!< Window width in pixels.
  int win_height;              //!< Window height in pixels.
  int slice;                   //!< Slice factor (1 << slice in each dimension).
  char *filename;              //!< The filename that was loaded, or null.
};

void render_options_init(struct render_options *o);
void render_options_end(struct render_options *o);
void render_options_copy(struct render_options *o, const struct render_options *p);
void render_options_set_location(struct render_options *o, const mpfr_t cx, const mpfr_t cy, const mpfr_t r);

struct render; // forward declaration
struct idlefunc_t { void (*f)(struct render *); };

enum render_result {
  render_complete,
  render_aborted
};

/*!
\brief Render state structure.

Should be read-only outside render.c
*/
struct render {
  bool begun;                           //!< Flag for one-time initializations.
  struct render_options options;        //!< Current rendering options.
  struct idlefunc_t idle;               //!< Next task callback.
  double escaperadius2;                 //!< Squared escape radius.
  GLuint rgb_tex;                       //!< Texture to store rgb output.
  GLuint tex;                           //!< Texture to store raw output.
  GLuint tex2;                          //!< Texture to store reference iterations.
  GLuint fbo;                           //!< Frame buffer object.
  GLuint query;                         //!< Query object.
  GLuint vbo[2];                        //!< Vertex buffer objects for ping-pong GPGPU.
  GLuint active_count;                  //!< Number of active pixels.
  struct fp32_fillc fp32_fillc;
  struct fp32_colour fp32_colour;
  struct fp32_init fp32_init;
  struct fp32_step fp32_step;
  struct fp32_unescaped fp32_unescaped;
  struct fp32_escaped fp32_escaped;
  struct fp64_init fp64_init;
  struct fp64_step fp64_step;
  struct fp64_unescaped fp64_unescaped;
  struct fp64_escaped fp64_escaped;
  struct fpxx_init fpxx_init;
  struct fpxx_approx fpxx_approx;
  struct fpxx_approx2 fpxx_approx2;
  struct fpxx_step fpxx_step;
  struct fpxx_unescaped fpxx_unescaped;
  struct fpxx_escaped fpxx_escaped;
  int pass;                             //!< Glitch correction pass count.
  struct ref_set *refs;                 //!< Collection of reference coordinates.
  struct blob_set *blobs;               //!< Collection of blobs.
  struct completion completion;         //!< Completion testing.
  struct completion completion2;        //!< Completion testing for multiple references.
  int last_active_count;                //!< History buffer for multiple references completion.
  bool slice_done;                      //!< Is the rendering complete?
  bool done;                            //!< Is the rendering complete for multiple slices?
  bool all_done;                        //!< Is the rendering complete for multiple references?
  bool aborted;                         //!< Should rendering be aborted?
  bool timeout;                         //!< Should rendering be stopped cleanly?
  struct stopwatch *render_time;        //!< Time since render started.
  struct slice_table *slice_table;      //!< Slice table.
  int slice_n;                          //!< Index of current slice, in [0, 1 << (slice << 1)).
  struct poll *poll;
};

void render_begin(struct render *s);
void render_end(struct render *s);
void render_start(struct render *s, const struct render_options *o);
bool render_calculate(struct render *s, const struct render_options *o, struct poll *poll);
bool render_display(struct render *s, const struct render_options *o, struct poll *poll);
bool render_reshape(struct render *s, int new_width, int new_height, int new_slice, GLsizei *bytes_allocated);

enum render_result render_do(struct render *render, struct render_options *render_options, struct poll *poll);

#endif

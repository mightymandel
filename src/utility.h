// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef UTILITY_H
#define UTILITY_H 1

/*!
\file  utility.h
\brief Miscellaneous utility functions.
*/

#include <stdbool.h>
#include <mpfr.h>

int min(int a, int b);
int max(int a, int b);
int ceil2n(int z);
int pxbits(const mpfr_t radius, double height);
void pixel_coordinate(mpfr_t x, mpfr_t y, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius, double i, double j);

typedef bool abort_t(void *);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef ZOOM_H
#define ZOOM_H 1

#include <stdbool.h>
#include <mpfr.h>

struct zoom {
  mpfr_t centerx;
  mpfr_t centery;
  mpfr_t radius;
  int frame;
  int zoom_frames;
};

void zoom_begin(struct zoom *zoom, int frames, const mpfr_t centerx, const mpfr_t centery);
void zoom_end(struct zoom *zoom);
bool zoom_next(struct zoom *zoom);

#endif

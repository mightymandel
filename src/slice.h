// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef SLICE_H
#define SLICE_H 1

struct slice_table;
struct slice_table *slice_table_new();
void slice_table_delete(struct slice_table *t);
void slice_table_coords(struct slice_table *t, int slice, int n, int *i, int *j);

#endif

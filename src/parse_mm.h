// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_MM_H
#define PARSE_MM_H 1

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Parse the original mightymandel `.mm` file format.

File format example:

    -0.75 +
    0.0 i
    1.5 @

The real part is on a line terminated by `" +"`, the imaginary part is on a
line terminated by `" i"`, and the radius is on a line terminated by `" @"`.

\copydetails parser
*/
bool parse_mm(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_MDZ_CORNERS_H
#define PARSE_MDZ_CORNERS_H 1

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Parse MDZ `.mdz` file format (corners variant).

File format example:

    precision 80
    aspect 1.33333
    xmin -2.5
    xmax 1.5
    ymax 1.5

Converting from corners is tricky, but luckily `.mdz` specifies the precision
needed so we don't have to guess it.  Then:

    cz = (xmax - xmin) / (2 * aspect)
    cx = (xmax + xmin) / 2
    cy = (ymax - cz)

\copydetails parser
*/
bool parse_mdz_corners(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

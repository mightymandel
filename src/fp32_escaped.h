// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FP32_ESCAPED_H
#define FP32_ESCAPED_H 1

#include <stdbool.h>
#include <GL/glew.h>
#include <mpfr.h>

struct fp32_escaped {
  GLuint program;
  GLint center;
  GLint radius;
  GLint aspect;
  GLint loger2;
  GLint pxs;
  GLint cne0;
  GLint zdz0;
  GLuint vao;
};

void fp32_escaped_begin(struct fp32_escaped *s);
void fp32_escaped_end(struct fp32_escaped *s);
void fp32_escaped_start(struct fp32_escaped *s, GLuint tex, GLuint fbo, GLuint vbo, double escaperadius2, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius);
void fp32_escaped_do(struct fp32_escaped *s, GLuint active_count);

#endif

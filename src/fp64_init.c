// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdio.h>

#include "fp64_init.h"
#include "shader.h"
#include "logging.h"

extern const char *fp64_init_vert;
const GLchar *fp64_init_varyings[] = {"cne", "zdz"};

void fp64_init_begin(struct fp64_init *s) {
  s->program = compile_program_tf("fp64_init", fp64_init_vert, 0, 0, 2, fp64_init_varyings);
  s->radius = glGetUniformLocation(s->program, "radius");
  s->center = glGetUniformLocation(s->program, "center");
  s->c = glGetAttribLocation(s->program, "c");
  glGenVertexArrays(1, &s->vao);
}

void fp64_init_end(struct fp64_init *s) {
  glDeleteVertexArrays(1, &s->vao);
  glDeleteProgram(s->program);
}

void fp64_init_do(struct fp64_init *s, GLuint *active_count, GLuint *vbo, GLuint query, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius) {
  *active_count = width * height;
  glEnable(GL_RASTERIZER_DISCARD);D;
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);D;
  glUseProgram(s->program);D;
  glUniform1d(s->radius, mpfr_get_d(radius, MPFR_RNDN));D;
  glUniform2d(s->center, mpfr_get_d(centerx, MPFR_RNDN), mpfr_get_d(centery, MPFR_RNDN));D;
  glVertexAttribPointer(s->c, 4, GL_FLOAT, GL_FALSE, 0, 0);D;
  glEnableVertexAttribArray(s->c);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[0]);D;
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);D;
  glBeginTransformFeedback(GL_POINTS);D;
  glDrawArrays(GL_POINTS, 0, *active_count);D;
  glEndTransformFeedback();D;
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
  glGetQueryObjectuiv(query, GL_QUERY_RESULT, active_count);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
  glUseProgram(0);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  glDisable(GL_RASTERIZER_DISCARD);D;
  debug_message("VBO init: %d -> %d\n", vbo[1], vbo[0]);
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

layout(points) in;
layout(points, max_vertices = 1) out;
in dvec4 cne1[1];
#ifdef DE
in dvec4 zdz1[1];
#else
in dvec2 zdz1[1];
#endif
out dvec4 cne;
#ifdef DE
out dvec4 zdz;
#else
out dvec2 zdz;
#endif
void main() {
  bool escaped = ! (cne1[0].w <= 0.0);
  if (! escaped) {
    cne = cne1[0];
    zdz = zdz1[0];
    EmitVertex();
    EndPrimitive();
  }
}

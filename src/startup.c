// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "logging.h"
#include "parse.h"
#include "utility.h"
#include "startup.h"
#include "version.h"
#include "config.glsl"

void print_version(void) {
  printf("%s\n", mightymandel_version);
}

void print_banner(void) {
  log_message(LOG_INFO, "mightymandel -- GPU-based Mandelbrot Set explorer\n");
  log_message(LOG_INFO, "Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen\n");
  log_message(LOG_INFO, "License GPL3+ http://www.gnu.org/licenses/gpl.html\n");
  log_message(LOG_INFO, "Version %s\n", mightymandel_version);
}

void print_usage(const char *progname) {
  printf("%s [argument]* [filename]\n", progname);
  printf("Arguments:\n");
  printf("\n");
  printf("    --help              show this usage message and exit\n");
  printf("    --version           show the version string and exit\n");
  printf("\n");
  printf("    --view X Y R        set view coordinates to X + i Y @ R\n");
  printf("\n");
  printf("    --verbose level     set the log message verbosity\n");
  printf("                        valid levels (from quiet to noisy) are:\n");
  printf("                        fatal error warn notice info debug\n");
  printf("    --geometry WxH      set window size (default --size)\n");
  printf("    --size WxH          set image size (default --geometry)\n");
  printf("    --de                compute distance estimates (default)\n");
  printf("    --no-de             don't compute distance estimates\n");
  printf("    --no-approx         don't compute series approximation\n");
  printf("                        sometimes that optimisation breaks glitch fixing\n");
  printf("    --order O           order of series approximation\n");
  printf("                        valid orders are: 4 6 8 12 16 24 32 48 64\n");
  printf("\n");
  printf("    --weight W          how dark to make the boundary (default 0.0)\n");
  printf("    --glitch            highlight glitches in ugly colours\n");
  printf("    --no-glitch         try to mask glitches (default)\n");
  printf("    --max-glitch G      percentage of glitches allowed (default 0.02)\n");
  printf("                        lower value gives better image at higher cost\n");
  printf("    --max-blob B        maximum pixels in glitched blob allowed (default 1)\n");
  printf("                        lower value gives better image at higher cost\n");
  printf("    --sharpness S       how sharp to make visible interior (default 0.01)\n");
  printf("                        lower value gives better image at higher cost\n");
  printf("    --timeout T         timeout after T seconds (default infinity)\n");
  printf("    --slice S           split calculations into 4^S blocks\n");
  printf("                        valid range from 0 (default) to %d\n", SLICE_MAX);
  printf("\n");
  printf("    --interactive       start in interactive mode (default)\n");
  printf("    --one-shot          render one image and exit\n");
  printf("    --tile N[xM]        render a large image as NxM tiles\n");
  printf("                        M defaults to N if it isn't specified\n");
  printf("                        final image size is (W*N)x(H*M)\n");
  printf("    --zoom F            render a zoom-in sequence of F frames\n");
  printf("                        can be combined with --tile\n");
  printf("    --overhead          quit after startup without rendering anything\n");
  printf("                        might be useful for benchmarking\n");
  printf("\n");
  printf("Only use at most one of: --interactive --one-shot (--tile --zoom)\n");
  printf("\n");
  printf("If a filename argument is given, it will be loaded to set the\n");
  printf("viewing parameters.\n");
}

bool parse_command_line(struct options *options, int argc, char **argv) {
#define O(flag) (0 == strcmp((flag), argv[i]) && strlen((flag)) == strlen(argv[i]))
#define NEXT do{ i++; if (! (i < argc)) { log_message(LOG_FATAL, "%s: argument expected\n", argv[i-1]); return false; } }while(0)
#define NEXT2(s) do{ i++; if (! (i < argc)) { log_message(LOG_FATAL, "%s: argument expected\n", s); return false; } }while(0)
  for (int i = 1; i < argc; ++i) {
    if (false) {
    } else if (O("-h") || O("-?") || O("-help") || O("--help")) {
      options->help = true;
      return true;
    } else if (O("-v") || O("-V") || O("-version") || O("--version")) {
      options->version = true;
      return true;
    } else if (O("--view")) {
      NEXT2("--view");
      const char *sx = argv[i];
      NEXT2("--view");
      const char *sy = argv[i];
      NEXT2("--view");
      const char *sz = argv[i];
      mpfr_init2(options->radius, 53);
      mpfr_set_str(options->radius, sz, 10, MPFR_RNDN);
      if (! (mpfr_regular_p(options->radius) && mpfr_sgn(options->radius) > 0)) {
        log_message(LOG_FATAL, "--view: invalid radius: %s\n", sz);
        return false;
      }
      mpfr_prec_t bits = precision_for_radius(options->radius);
      mpfr_init2(options->centerx, bits);
      mpfr_init2(options->centery, bits);
      mpfr_set_str(options->centerx, sx, 10, MPFR_RNDN);
      mpfr_set_str(options->centery, sy, 10, MPFR_RNDN);
      options->view_supplied = true;
    } else if (O("--verbose")) {
      NEXT;
      if (false) {
      } else if (O("fatal" )) { options->log_level = LOG_FATAL;
      } else if (O("error" )) { options->log_level = LOG_ERROR;
      } else if (O("warn"  )) { options->log_level = LOG_WARN;
      } else if (O("notice")) { options->log_level = LOG_NOTICE;
      } else if (O("info"  )) { options->log_level = LOG_INFO;
      } else if (O("debug" )) { options->log_level = LOG_DEBUG;
      } else {
        log_message(LOG_FATAL,  "--verbose: invalid level: %s\n", argv[i]);
        log_message(LOG_NOTICE, "--verbose: valid levels: fatal error warn notice info debug\n");
        return false;
      }
    } else if (O("--de")) {
      options->render_de = true;
    } else if (O("-no-de") || O("--no-de")) {
      if (O("-no-de")) {
        log_message(LOG_WARN, "deprecated: -no-de  use --no-de instead\n");
      }
      options->render_de = false;
    } else if (O("--no-approx")) {
      options->series_approx = false;
    } else if (O("--order")) {
      NEXT;
      options->order = atoi(argv[i]);
    } else if (O("-glitch") || O("--glitch")) {
      if (O("-glitch")) {
        log_message(LOG_WARN, "deprecated: -glitch  use --glitch instead\n");
      }
      options->show_glitches = true;
    } else if (O("--no-glitch")) {
      options->show_glitches = false;
    } else if (O("--interactive")) {
      options->interactive = true;
    } else if (O("-bench") || O("--one-shot")) {
      if (O("-bench")) {
        log_message(LOG_WARN, "deprecated: -bench  use --one-shot instead\n");
      }
      options->oneshot = true;
    } else if (O("-tile") || O("--tile")) {
      if (O("-tile")) {
        log_message(LOG_WARN, "deprecated: -tile  use --tile instead\n");
      }
      options->tile = true;
      NEXT;
      if (strchr(argv[i], 'x')) {
        int tile_width = 0;
        int tile_height = 0;
        if (! (2 == sscanf(argv[i], "%dx%d", &tile_width, &tile_height))) {
          log_message(LOG_FATAL,  "--tile: invalid dimensions: %s\n", argv[i]);
          return false;
        }
        if (! (tile_width > 0 && tile_height > 0)) {
          log_message(LOG_FATAL,  "--tile: dimensions must be positive: %s\n", argv[i]);
          return false;
        }
        options->tile_width = tile_width;
        options->tile_height = tile_height;
      } else {
        int tile_count = atoi(argv[i]);
        if (! (tile_count > 0)) {
          log_message(LOG_FATAL,  "--tile: dimension must be positive: %s\n", argv[i]);
          return false;
        }
        options->tile_width = tile_count;
        options->tile_height = tile_count;
      }
    } else if (O("-zoom") || O("--zoom")) {
      if (O("-zoom")) {
        log_message(LOG_WARN, "deprecated: -zoom  use --zoom instead\n");
      }
      options->zoom = true;
      NEXT;
      int zoom_frames = atoi(argv[i]);
      if (! (zoom_frames > 0)) {
        log_message(LOG_FATAL,  "--zoom: frame count must be positive: %s\n", argv[i]);
        return false;
      }
      options->zoom_frames = zoom_frames;
    } else if (O("--overhead")) {
      options->overhead = true;
    } else if (O("--geometry")) {
      NEXT;
      int width = 0;
      int height = 0;
      if (! (2 == sscanf(argv[i], "%dx%d", &width, &height))) {
        log_message(LOG_FATAL,  "--size: invalid dimensions: %s\n", argv[i]);
        return false;
      }
      if (! (width > 0 && height > 0)) {
        log_message(LOG_FATAL,  "--size: dimensions must be positive: %s\n", argv[i]);
        return false;
      }
      options->geometry = true;
      options->win_width = width;
      options->win_height = height;
    } else if (O("-size") || O("--size")) {
      if (O("-size")) {
        log_message(LOG_WARN, "deprecated: -size  use --size instead\n");
      }
      NEXT;
      int width = 0;
      int height = 0;
      if (! (2 == sscanf(argv[i], "%dx%d", &width, &height))) {
        log_message(LOG_FATAL,  "--size: invalid dimensions: %s\n", argv[i]);
        return false;
      }
      if (! (width > 0 && height > 0)) {
        log_message(LOG_FATAL,  "--size: dimensions must be positive: %s\n", argv[i]);
        return false;
      }
      options->size = true;
      options->width = width;
      options->height = height;
    } else if (O("-weight") || O("--weight")) {
      if (O("-weight")) {
        log_message(LOG_WARN, "deprecated: -weight  use --weight instead\n");
      }
      NEXT;
      options->weight = atof(argv[i]);
    } else if (O("--max-glitch")) {
      NEXT;
      options->max_glitch = fmin(fmax(atof(argv[i]), 0.0), 100.0);
    } else if (O("--max-blob")) {
      NEXT;
      options->max_blob = max(atoi(argv[i]), 0);
    } else if (O("--sharpness")) {
      NEXT;
      options->sharpness = fmin(fmax(atof(argv[i]), 0.0), 1.0);
    } else if (O("--timeout")) {
      NEXT;
      options->timeout = atof(argv[i]);
    } else if (O("--slice")) {
      NEXT;
      options->slice = min(max(atoi(argv[i]), 0), SLICE_MAX);
    } else if (! options->file_to_load) {
      options->file_to_load = argv[i];
    } else {
      log_message(LOG_FATAL, "unexpected argument: \n", argv[i]);
      return false;
    }
  }
  return true;
#undef O
#undef NEXT
#undef NEXT2
}

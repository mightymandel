// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FPXX_UNESCAPED_H
#define FPXX_UNESCAPED_H 1

#include <GL/glew.h>

struct fpxx_unescaped {
  GLuint program;
  GLint cne0;
  GLint zdz0;
  GLint err0;
  GLuint vao;
};

void fpxx_unescaped_begin(struct fpxx_unescaped *s);
void fpxx_unescaped_end(struct fpxx_unescaped *s);
void fpxx_unescaped_start(struct fpxx_unescaped *s, GLuint vbo);
void fpxx_unescaped_do(struct fpxx_unescaped *s, GLuint *unescaped, GLuint active_count, GLuint vbo, GLuint query);

#endif

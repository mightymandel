// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef RECORD_H
#define RECORD_H 1

#include <mpfr.h>

struct record {
  unsigned char *buffer;
  int bytes;
};

void record_begin(struct record *s);
void record_end(struct record *s);
void record_do(struct record *s, const char *name, int width, int height, const char *comment);

#endif

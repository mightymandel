// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

in  vec4 pt;
smooth out vec2 t;
void main() {
  gl_Position = vec4(pt.xy, 0.0, 1.0);
  t = pt.zw;
}

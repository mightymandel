// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef SHADER_H
#define SHADER_H 1

/*!
\file shader.h
\brief Compile and link OpenGL shader programs.
*/

#include <GL/glew.h>

GLint compile_program_tf(const char *name, const GLchar *vert, const GLchar *geom, const GLchar *frag, int nvaryings, const GLchar **varyings);
GLint compile_program(const char *name, const GLchar *vert, const GLchar *geom, const GLchar *frag);

#endif

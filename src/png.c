// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <assert.h>
#include <stdbool.h>
#include <string.h>

#include "crc.h"
#include "png.h"

int png_iTXt_length(const char *keyword, const char *text) {
  int keyword_length = strlen(keyword);
  int text_length = strlen(text);
  int data_length = keyword_length + 5 + text_length;
  int chunk_length = 8 + data_length + 4;
  return chunk_length;
}

bool png_iTXt(const char *keyword, const char *text, unsigned char *chunk, int length) {
#define INT(l) { l >> 24 & 0xff, l >> 16 & 0xff, l >> 8 & 0xff, l & 0xff }
  int keyword_length = strlen(keyword);
  int text_length = strlen(text);
  int data_length = keyword_length + 5 + text_length;
  int chunk_length = 8 + data_length + 4;
  if (length < chunk_length) {
    return false;
  }
  int k = 0;
  unsigned char len[4] = INT(data_length);
  chunk[k++] = len[0];                         // chunk length
  chunk[k++] = len[1];
  chunk[k++] = len[2];
  chunk[k++] = len[3];
  const unsigned char iTXt[4] = { 105, 84, 88, 116 };
  chunk[k++] = iTXt[0];                        // chunk type
  chunk[k++] = iTXt[1];
  chunk[k++] = iTXt[2];
  chunk[k++] = iTXt[3];                        // chunk data
  memcpy(chunk + k, keyword, keyword_length);  //   keyword
  k += keyword_length;
  chunk[k++] = 0;                              //   null separator
  chunk[k++] = 0;                              //   compression flag
  chunk[k++] = 0;                              //   compression method
                                               //   language tag
  chunk[k++] = 0;                              //   null separator
                                               //   translated keyword
  chunk[k++] = 0;                              //   null separator
  memcpy(chunk + k, text, text_length);        //   text
  k += text_length;
  unsigned long crc_data = crc(chunk + 4, data_length + 4);
  unsigned char cyclic[4] = INT(crc_data);
  chunk[k++] = cyclic[0];                      // chunk CRC
  chunk[k++] = cyclic[1];
  chunk[k++] = cyclic[2];
  chunk[k++] = cyclic[3];
  assert(k == chunk_length);
  return true;
}

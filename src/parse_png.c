// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file parse_png.c
\brief PNG metadata parser implementation.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>

#include "parse.h"
#include "parse_png.h"
#include "logging.h"
#include "utility.h"
#include "crc.h"

bool parse_png(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
  assert(source);
  const unsigned char *s = (const unsigned char *) source;
  // PNG signature http://www.w3.org/TR/PNG/#5PNG-file-signature
  const unsigned char signature[8] = { 137, 80, 78, 71, 13, 10, 26, 10 };
  if (! (length >= 8 && 0 == memcmp(s, signature, 8))) {
    return false; // not PNG
  }
  s += 8;
  // networkk byte order http://www.w3.org/TR/PNG/#7Integers-and-byte-order
#define INT ((s[0] << 24) | (s[1] << 16) | (s[2] << 8) | (s[3])); s+= 4
  // tEXt chunk http://www.w3.org/TR/PNG/#11tEXt
  const unsigned int tEXt = (116 << 24) | (69 << 16) | (88 << 8) | (116);
  // keyowrds http://www.w3.org/TR/PNG/#11keywords
  // Software used to create the image
  const unsigned char *software = (const unsigned char *) "Software";
  const int software_length = 9; // including null terminator
  // Short (one line) title or caption for image
  const unsigned char *title = (const unsigned char *) "Title";
  const int title_length = 6; // including null terminator
  // Software text string for mightymandel
  const unsigned char *mightymandel = (const unsigned char *) "mightymandel";
  const int mightymandel_length = 12; // NOT including null terminator
  // search through the chunks
  bool have_mightymandel = false;
  char *have_title = 0;
  while (s + 4 <= (const unsigned char *) source + length) {
    // PNG chunks http://www.w3.org/TR/PNG/#5Chunk-layout
    unsigned int chunk_length = INT;
    if (! (s + chunk_length + 4 <= (const unsigned char *) source + length)) {
      log_message(LOG_WARN, "PNG truncated?\n");
      break;
    }
    unsigned long chunk_data_crc = crc(s, 4 + chunk_length);
    unsigned int type = INT;
    debug_message("parse_png type = %08x (want %08x)\n", type, tEXt);
    if (type == tEXt) {
      // Software mightymandel
      if (! have_mightymandel && 0 == memcmp(s, software, software_length)) {
        debug_message("parse_png Software\n");
        s += software_length;
        chunk_length -= software_length;
        if ((int) chunk_length == mightymandel_length && 0 == memcmp(s, mightymandel, mightymandel_length)) {
          debug_message("parse_png Software mightymandel\n");
          have_mightymandel = true;
        }
      }
      // Title -0.75 + 0.0 i @ 1.5
      if (! have_title && 0 == memcmp(s, title, title_length)) {
        debug_message("parse_png Title\n");
        s += title_length;
        chunk_length -= title_length;
        have_title = malloc(chunk_length + 1);
        memcpy(have_title, s, chunk_length);
        have_title[chunk_length] = 0;
        debug_message("parse_png Title '%s'\n", have_title);
      }
    }
    s += chunk_length;
    unsigned long chunk_crc = INT;
    if ((chunk_data_crc & 0xFFffFFff) != (chunk_crc & 0xFFffFFff)) {
      log_message(LOG_WARN, "PNG CRC mismatch: %08x %08x\n", chunk_data_crc, chunk_crc);
    }
    // we only care about two keywords in tEXt chunks
    if (have_mightymandel && have_title) {
      debug_message("parse_png Software mightymandel + Title\n");
      bool retval = false;
      // surely long enough for each substring
      int len = strlen(have_title) + 1;
      char *sx = malloc(len); sx[0] = 0;
      char *sy = malloc(len); sy[0] = 0;
      char *sz = malloc(len); sz[0] = 0;
      // parse title
      if (3 == sscanf(have_title, "%s + %s i @ %s", sx, sy, sz)) {
        debug_message("parse_png sx '%s'\n", sx);
        debug_message("parse_png sy '%s'\n", sy);
        debug_message("parse_png sz '%s'\n", sz);
        mpfr_set_prec(cz, 53);
        mpfr_set_str(cz, sz, 10, MPFR_RNDN);
        if (radius_is_valid(cz)) {
          mpfr_prec_t p = precision_for_radius(cz);
          mpfr_set_prec(cx, p);
          mpfr_set_prec(cy, p);
          mpfr_set_str(cx, sx, 10, MPFR_RNDN);
          mpfr_set_str(cy, sy, 10, MPFR_RNDN);
          retval = true;
        }
      }
      // cleanup
      free(sx);
      free(sy);
      free(sz);
      free(have_title);
      return retval;
    }
  }
  // cleanup, might have found some other title unrelated to mightymandel
  if (have_title) {
    free(have_title);
  }
  return false;
#undef INT
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

in vec4 cne0;
#ifdef DE
in vec4 zdz0;
#else
in vec2 zdz0;
#endif
out vec4 cne1;
#ifdef DE
out vec4 zdz1;
#else
out vec2 zdz1;
#endif
void main() {
  cne1 = cne0;
  zdz1 = zdz0;
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_MDZ_CENTER_H
#define PARSE_MDZ_CENTER_H 1

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Parse MDZ `.mdz` file format (center variant).

File format example:

    aspect 1.77777
    cx -0.75
    cy 0.0
    size 5.33333

Size \f$s\f$ is the real width, convert to radius \f$r\f$ (half imaginary
height) by using the aspect \f$a\f$: \f$r = \frac{s}{2 a}\f$.  Double precision
floating point has enough precision and enough range for any real-world aspect
ratio.

\copydetails parser
*/
bool parse_mdz_center(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

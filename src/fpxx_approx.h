// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FPXX_APPROX_H
#define FPXX_APPROX_H 1

#include <GL/glew.h>
#include <mpfr.h>

#include "complex.h"
#include "utility.h"

struct fpxx_approx {
  GLuint program;
  GLint iters;
  GLint c0;
  GLint abcuvw;
  GLuint vao;
  int n;
  double values[6][2];
  C p, z, a, b, c, u, v, w, z2, a2, b2, c2, u2, v2, w2, t1, t2, t3, t4, t5, t6;
  R delta4;
  bool too_deep;
};

void fpxx_approx_begin(struct fpxx_approx *s);
void fpxx_approx_end(struct fpxx_approx *s);
void fpxx_approx_do(struct fpxx_approx *s, GLuint *active_count, GLuint *vbo, GLuint query, const mpfr_t dzx0, const mpfr_t dzy0, mpfr_t zx, mpfr_t zy, mpfr_t dzx, mpfr_t dzy, int pass, const mpfr_t radius, const mpfr_t refx, const mpfr_t refy, bool series_approx, bool initial_slice, void *abort_data, abort_t abort_fn);

#endif

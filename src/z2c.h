// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef Z2C_H
#define Z2C_H 1

#include <complex.h>
#include <stdbool.h>
#include <mpfr.h>

#include "z2c_ref.h"
void z2c_reference_get_z(const struct z2c_reference *ref, mpfr_t zx, mpfr_t zy);

struct z2c_series; // opaque
struct z2c_approx; // opaque

struct z2c_series *z2c_series_new(const int order, const mpfr_t cx, const mpfr_t cy);
void z2c_series_delete(struct z2c_series *s);
bool z2c_series_step(struct z2c_series *s, const int exponent, const int threshold);
int z2c_series_get_n(const struct z2c_series *s);
struct z2c_reference *z2c_series_reference_new(const struct z2c_series *s);
struct z2c_approx *z2c_series_approx_new(const struct z2c_series *s, const int exponent);
void z2c_approx_delete(struct z2c_approx *a);
int z2c_approx_get_order(const struct z2c_approx *a);
int z2c_approx_get_exponent(const struct z2c_approx *a);
const complex double *z2c_approx_get_coefficients(const struct z2c_approx *a);
complex double z2c_approx_do(const struct z2c_approx *a, const complex double dc);

#endif

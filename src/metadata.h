// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef METADATA_H
#define METADATA_H 1

#include <stdbool.h>

/*!
\brief Opaque data structure for storing metadata.
*/
struct metadata;

/*!
\brief Create a new empty metadata structure.

\return A new empty metadata structure.
*/
struct metadata *metadata_new();

/*!
\brief Deleta a metadata structure.

\param s The metadata structure.
*/
void metadata_delete(struct metadata *s);

/*!
\brief Look up a key in the metadata.

If the `key` is found, return the `value`, otherwise `0`.

\param s The metadata structure.
\param key The key.
\return The value, or 0.
*/
const char *metadata_lookup(struct metadata *s, const char *key);

/*!
\brief Update a key value pair in the metadata.

If the `key` is found, replace the `value`. Otherwise append the key value pair.

\param s The metadata structure.
\param key The key.  Must not contain white space.
\param value The value.  Must not contain newlines.
*/
void metadata_update(struct metadata *s, const char *key, const char *value);

/*!
\brief Remove a key from the metadata.

If the `key` is found, remove it and its `value`.

\param s The metadata structure.
\param key The key.
*/
void metadata_remove(struct metadata *s, const char *key);

/*!
\brief Find the length of the metadata concatenated to a string.

\param s The metadata structure.
\param prefix A prefix to prepend to each line.
\return The length.
*/
int metadata_strlen(const struct metadata *s, const char *prefix);

/*!
\brief Concatenate the metadata to a string.

\param s The metadata structure.
\param prefix A prefix to prepend to each line.
\param string A buffer to write into.
\param length The length of the buffer.
\return `true` if there was enough space, `false` otherwise.
*/
bool metadata_string(const struct metadata *s, const char *prefix, char *string, int length);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file  image.c
\brief Image related functions.
*/

#include <math.h>
#include <stdlib.h>

#include "mightymandel.h"
#include "image.h"
#include "texture.h"

/*!
\brief Compare pixel iteration counts and error counts.

For use in `qsort()`, larger iteration counts compare earlier, same iteration
count has larger error count comparing earlier.

\param a The first pixel.
\param b The second pixel.
\return Whether the first pixel is earlier than
(-1), equal to (0), or later than (1) the second pixel.
*/
int cmp_pixel(const void *a, const void *b) {
  const struct pixel *x = a;
  const struct pixel *y = b;
  if (x->n > y->n) { return -1; }
  if (x->n < y->n) { return  1; }
  if (x->e > y->e) { return -1; }
  if (x->e < y->e) { return  1; }
  return 0;
}

/*!
\brief Download image glitch flags from OpenGL.

Uses the currently bound texture, assumes glitch is in alpha.  The
result should be `free()`d when no longer needed.

\param width The width of the texture.
\param height The height of the texture.
\return The glitch count image, tightly packed in rows from top left to
bottom right.
*/
float *image_get_glitched(int width, int height) {
  float *raw = (float *) malloc(width * height * sizeof(float));
  glActiveTexture(GL_TEXTURE0 + TEX_RAW);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_GREEN, GL_FLOAT, raw);
  float *data = malloc(width * height * sizeof(float));
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      int ki = (height - 1 - j) * width + i;
      int ko = j * width + i;
      data[ko] = raw[ki];
    }
  }
  free(raw);
  return data;
}

/*!
\brief Download image iteration counts from OpenGL.

Uses the currently bound textures, assumes iteration count is in red.
The result should be `free()`d when no longer needed.

\param width The width of the texture.
\param height The height of the texture.
\return The iteration count image, tightly packed in rows from top left
to bottom right.
*/
float *image_get_iterations(int width, int height) {
  float *raw = (float *) malloc(width * height * sizeof(float));
  glActiveTexture(GL_TEXTURE0 + TEX_RAW);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT, raw);
  float *data = malloc(width * height * sizeof(float));
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      int ki = (height - 1 - j) * width + i;
      int ko = j * width + i;
      data[ko] = raw[ki];
    }
  }
  free(raw);
  return data;
}

/*!
\brief Erode a binary image.

See <https://en.wikipedia.org/wiki/Erosion_%28morphology%29>.
The result should be `free()`d when no longer needed.

\param data The input image.
\param width The image width.
\param height The image height.
\return The eroded image.
*/
unsigned char *image_erode(const unsigned char *data, int width, int height) {
  unsigned char *data2 = (unsigned char *) malloc(width * height);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      int k = j * width + i;
      int setcount = 0;
      int totalcount = 0;
      for (int jj = -1; jj <= 1; ++jj) {
        for (int ii = -1; ii <= 1; ++ ii) {
          int jjj = j + jj;
          int iii = i + ii;
          if (0 <= iii && iii < width && 0 <= jjj && jjj < height) {
            int kkk = jjj * width + iii;
            setcount += data[kkk];
            totalcount++;
          }
        }
      }
      data2[k] = totalcount == setcount;
    }
  }
  return data2;
}

/*!
\brief Find the highest iteration count glitched pixels.

The result should be `free()`d when no longer needed.

\param glitched The glitch count image.
\param iterations The iteration count image.
\param width The image width.
\param height The image height.
\param count The number of glitched pixels is stored here.
\return The glitched pixels sorted by error count, then by iteration count
(highest first in both fields).
*/
struct pixel *image_find_peak_glitches(const float *glitched, const float *iterations, int width, int height, int *count) {
  struct pixel *pxs = malloc(width * height * sizeof(struct pixel));
  *count = 0;
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      int k = j * width + i;
      if (glitched[k] > 0.0) {
        pxs[*count].e = glitched[k];
        pxs[*count].n = iterations[k];
        pxs[*count].i = i;
        pxs[*count].j = j;
        (*count)++;
      }
    }
  }
  if (*count) {
    qsort(pxs, *count, sizeof(struct pixel), cmp_pixel);
  }
  return pxs;
}

enum result_t image_result(int width, int height, double *iterations, double *exterior, double *interior, double *glitch) {
  *iterations = 0;
  *exterior = 0;
  *interior = 0;
  *glitch = 0;
  double percent = 100.0 / (width * height);
  float *raw = malloc(width * height * 2 * sizeof(float));
  glActiveTexture(GL_TEXTURE0 + TEX_RAW);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RG, GL_FLOAT, raw);
  for (int j = 0; j < height; ++j) {
    for (int i = 0; i < width; ++i) {
      int k = j * width + i;
      *iterations = fmaxf(*iterations, fabsf(raw[2*k+0]));
      *exterior   += raw[2*k+0] >  0.0;
      *interior   += raw[2*k+0] <= 0.0;
      *glitch     += raw[2*k+1] != 0.0;
    }
  }
  free(raw);
  *exterior *= percent;
  *interior *= percent;
  *glitch   *= percent;
  if (*glitch < 0.02) { // FIXME hardcoded threshold
    return result_ok;
  } else {
    return result_glitch;
  }
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>

#include "parse.h"
#include "parse_ppar_center.h"
#include "logging.h"

bool parse_ppar_center(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
  (void) length;
  char *source2 = strdup(source);
  char *s = source2;
  char *centermag = 0; // CENTER-MAG=[Xctr/Yctr/Mag[/Xmagfactor/Rotation/Skew]]
  while (s) {
    char *line = parse_line(&s);
    if (0 == strncmp(line, "center-mag=", 11)) {
      centermag = line + 11;
      debug_message("parse_ppar1: center-mag = %s\n", centermag);
      char *sx = parse_separator(&centermag, '/');
      char *sy = parse_separator(&centermag, '/');
      char *sz = centermag;
      debug_message("parse_ppar1: sx = %s\n", sx);
      debug_message("parse_ppar1: sy = %s\n", sy);
      debug_message("parse_ppar1: sz = %s\n", sz);
      char *extra = strchr(centermag, '/');
      if (extra) {
        debug_message("parse_ppar1: warning unsupported extra fields in center-mag: %s\n", extra);
        *extra = 0;
      }
      mpfr_set_str(cz, sz, 10, MPFR_RNDN);
      mpfr_si_div(cz, 1, cz, MPFR_RNDN);
      if (! radius_is_valid(cz)) {
        free(source2);
        return false;
      }
      mpfr_prec_t prec = precision_for_radius(cz);
      mpfr_set_prec(cx, prec);
      mpfr_set_prec(cy, prec);
      mpfr_set_str(cx, sx, 10, MPFR_RNDN);
      mpfr_set_str(cy, sy, 10, MPFR_RNDN);
      debug_message("parse_ppar1: cx = %Re\n", cx);
      debug_message("parse_ppar1: cy = %Re\n", cy);
      debug_message("parse_ppar1: cz = %Re\n", cz);
      free(source2);
      return true;
    }
  }
  free(source2);
  return false;
}

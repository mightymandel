// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

flat in vec4 ida;
layout(location = 0, index = 0) out vec4 o;
void main() {
  o = ida;
}

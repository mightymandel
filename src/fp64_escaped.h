// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FP64_ESCAPED_H
#define FP64_ESCAPED_H 1

#include <stdbool.h>
#include <GL/glew.h>
#include <mpfr.h>

struct fp64_escaped {
  GLuint program;
  GLint center;
  GLint radius;
  GLint aspect;
  GLint loger2;
  GLint cne0;
  GLint zdz0;
  GLuint vao;
};

void fp64_escaped_begin(struct fp64_escaped *s);
void fp64_escaped_end(struct fp64_escaped *s);
void fp64_escaped_start(struct fp64_escaped *s, GLuint tex, GLuint fbo, GLuint vbo, double escaperadius2, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius);
void fp64_escaped_do(struct fp64_escaped *s, GLuint active_count);

#endif

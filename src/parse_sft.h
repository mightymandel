// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_SFT_H
#define PARSE_SFT_H 1

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Parse SuperFractalThing `.sft` file format.

File format example:

    r=-0.75
    i=0.0
    s=1.5

That is, essentially the same content as `.mm` but with a prefix syntax.

\copydetails parser
*/
bool parse_sft(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

double cmag2(dvec2 z) {
  return dot(z, z);
}

dvec2 csqr(dvec2 z) {
  return dvec2(z.x * z.x - z.y * z.y, 2.0 * z.x * z.y);
}

dvec2 cmul(dvec2 z, dvec2 w) {
  return dvec2(z.x * w.x - z.y * w.y, z.x * w.y + z.y * w.x);
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FP64_STEP_H
#define FP64_STEP_H 1

#include <GL/glew.h>
#include <complex.h>

struct fp64_step {
  GLuint program;
  GLint er2;
  GLint dz0;
  GLint cne0;
  GLint zdz0;
  GLuint vao;
};

void fp64_step_begin(struct fp64_step *s);
void fp64_step_end(struct fp64_step *s);
void fp64_step_start(struct fp64_step *s, GLuint vbo, double escaperadius2, complex double dz0);
void fp64_step_do(struct fp64_step *s, GLuint *active_count, GLuint vbo, GLuint query);

#endif

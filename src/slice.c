// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <assert.h>
#include <stdlib.h>
#include <GL/glew.h>

#include "slice.h"
#include "texture.h"
#include "config.glsl"

struct slice_coord {
  GLubyte i;
  GLubyte j;
};

struct slice_table {
  // coordinates to slice number
  GLushort s0[1][1];
  GLushort s1[2][2];
  GLushort s2[4][4];
  GLushort s3[8][8];
  GLushort s4[16][16];
  GLushort s5[32][32];
  GLushort s6[64][64];
  GLushort s7[128][128];
  GLushort s8[256][256];
  // slice number to coordinates
  struct slice_coord c0[1][1];
  struct slice_coord c1[2][2];
  struct slice_coord c2[4][4];
  struct slice_coord c3[8][8];
  struct slice_coord c4[16][16];
  struct slice_coord c5[32][32];
  struct slice_coord c6[64][64];
  struct slice_coord c7[128][128];
  struct slice_coord c8[256][256];
  // corresponding textures
  GLuint s_tex;
  GLuint c_tex;
};

struct slice_table *slice_table_new() {
  struct slice_table *t = malloc(sizeof(struct slice_table));
  if (! t) { return 0; }

  t->s0[0][0] = 0;

  t->s1[0][0] = 0;
  t->s1[0][1] = 2;
  t->s1[1][0] = 3;
  t->s1[1][1] = 1;

#define FILL(next,from) do{ \
  for (int i = 0; i < (1 << next); ++i) { \
    for (int j = 0; j < (1 << next); ++j) { \
      int k = t->s##from[i >> 1][j >> 1]; \
      int n = t->s1[i & 1][j & 1]; \
      t->s##next[i][j] = k + n * (1 << (from << 1)); \
    } \
  } \
}while(0)
  FILL(2, 1);
  FILL(3, 2);
  FILL(4, 3);
  FILL(5, 4);
  FILL(6, 5);
  FILL(7, 6);
  FILL(8, 7);
#undef FILL

#define INVERT(next) do{ \
  for (int i = 0; i < (1 << next); ++i) { \
    for (int j = 0; j < (1 << next); ++j) { \
      int k = t->s##next[i][j]; \
      int ii = k >> next; \
      int jj = k - (ii << next); \
      t->c##next[ii][jj].i = i; \
      t->c##next[ii][jj].j = j; \
    } \
  } \
}while(0)
  INVERT(0);
  INVERT(1);
  INVERT(2);
  INVERT(3);
  INVERT(4);
  INVERT(5);
  INVERT(6);
  INVERT(7);
  INVERT(8);
#undef INVERT

  glGenTextures(1, &t->s_tex);
  glActiveTexture(GL_TEXTURE0 + TEX_SLICE_NUMBER);
  glBindTexture(GL_TEXTURE_2D, t->s_tex);
#define UPLOAD(n) glTexImage2D(GL_TEXTURE_2D, SLICE_MAX - n, GL_R16UI, 1 << n, 1 << n, 0, GL_RED_INTEGER, GL_UNSIGNED_SHORT, &t->s##n[0][0])
  UPLOAD(8);
  UPLOAD(7);
  UPLOAD(6);
  UPLOAD(5);
  UPLOAD(4);
  UPLOAD(3);
  UPLOAD(2);
  UPLOAD(1);
  UPLOAD(0);
#undef UPLOAD

  glGenTextures(1, &t->c_tex);
  glActiveTexture(GL_TEXTURE0 + TEX_SLICE_COORDS);
  glBindTexture(GL_TEXTURE_2D, t->c_tex);
#define UPLOAD(n) glTexImage2D(GL_TEXTURE_2D, SLICE_MAX - n, GL_RG8UI, 1 << n, 1 << n, 0, GL_RG_INTEGER, GL_UNSIGNED_BYTE, &t->c##n[0][0].i)
  UPLOAD(8);
  UPLOAD(7);
  UPLOAD(6);
  UPLOAD(5);
  UPLOAD(4);
  UPLOAD(3);
  UPLOAD(2);
  UPLOAD(1);
  UPLOAD(0);
#undef UPLOAD
  return t;
}

void slice_table_delete(struct slice_table *t) {
  assert(t);
  glActiveTexture(GL_TEXTURE0 + TEX_SLICE_NUMBER);
  glBindTexture(GL_TEXTURE_2D, 0);
  glActiveTexture(GL_TEXTURE0 + TEX_SLICE_COORDS);
  glBindTexture(GL_TEXTURE_2D, 0);
  glDeleteTextures(1, &t->s_tex);
  glDeleteTextures(1, &t->c_tex);
  free(t);
}

void slice_table_coords(struct slice_table *t, int slice, int n, int *i, int *j) {
  assert(t);
  assert(0 <= slice);
  assert(slice <= SLICE_MAX);
  assert(0 <= n);
  assert(n < (1 << (slice << 1)));
  assert(i);
  assert(j);
  int ii = n >> slice;
  int jj = n - (ii << slice);
  assert(0 <= ii);
  assert(ii < (1 << slice));
  assert(0 <= jj);
  assert(jj < (1 << slice));
  switch (slice) {
#define CASE(k) case k: *i = t->c##k[ii][jj].i; *j = t->c##k[ii][jj].j; break
    CASE(0);
    CASE(1);
    CASE(2);
    CASE(3);
    CASE(4);
    CASE(5);
    CASE(6);
    CASE(7);
    CASE(8);
#undef CASE
    default: assert(! "invalid slice"); break;
  }
}

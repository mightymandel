// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

uniform double er2;
uniform bool escaped;
uniform usampler2D zdz0s;
in dvec4 cne0;
#ifdef DE
in dvec4 zdz0;
#else
in dvec2 zdz0;
#endif
in dvec2 err0;
out dvec4 cne;
#ifdef DE
out dvec4 zdz;
#else
out dvec2 zdz;
#endif
out dvec2 err;

void main() {
  if (escaped) {
    cne = dvec4(cne0.xy, cne0.z + 1.0, 1.0);
      uvec4 uz0  = texelFetch(zdz0s, ivec2(0, 0), 0);
      dvec2 z0 = dvec2
        ( packDouble2x32(uz0.xy), packDouble2x32(uz0.zw) );
#ifdef DE
      uvec4 udz0 = texelFetch(zdz0s, ivec2(0, 1), 0);
      dvec2 dz0 = dvec2
        ( packDouble2x32(udz0.xy), packDouble2x32(udz0.zw) );
    zdz = zdz0 + dvec4(z0, dz0);
#else
    zdz = zdz0 + z0;
#endif
    err = err0;
  } else {
  dvec2 c = cne0.xy;
  double n = cne0.z;
  double e = cne0.w;
  dvec2 z = zdz0.xy;
#ifdef DE
  dvec2 dz = zdz0.zw;
#endif
  double error = err0.y;
  int j = 0;
  for (int i = 0; i < FPXX_STEP_ITERS; ++i) {
    if (e <= 0.0) {
      uvec4 uz0  = texelFetch(zdz0s, ivec2(i, 0), 0);
      dvec2 z0 = dvec2
        ( packDouble2x32(uz0.xy), packDouble2x32(uz0.zw) );
#ifdef DE
      uvec4 udz0 = texelFetch(zdz0s, ivec2(i, 1), 0);
      dvec2 dz0 = dvec2
        ( packDouble2x32(udz0.xy), packDouble2x32(udz0.zw) );
#endif
      // http://www.fractalforums.com/announcements-and-news/pertubation-theory-glitches-improvement/
      e = cmag2(z0 + z) - er2;
      if (cmag2(z0 + z) < cmag2(z0) * 1.0e-6) {
        error += n + j;
      }
      if (! (e <= 0.0)) {
        z += z0;
#ifdef DE
        dz += dz0;
#endif
        break;
      }
      j += 1;
#ifdef DE
      dz = 2.0 * (cmul(dz0, z) + cmul(dz, z) + cmul(dz, z0));
#endif
      z = 2.0 * cmul(z0, z) + csqr(z) + c;
    } else {
      break;
    }
  }
  n += double(j);
  cne = dvec4(c, n, e);
#ifdef DE
  zdz = dvec4(z, dz);
#else
  zdz = dvec2(z);
#endif
  err = dvec2(err0.x, error);
  }
}

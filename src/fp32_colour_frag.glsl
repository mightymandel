// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\brief Raw iteration results texture.

Has two semantic formats depending whether `DE` is enable:
  - `DE` enabled: vec3(iters, error, de)
  - `DE` disabled: vec2(iters, error)

`iters` is the continuous (smooth) iteration count.
Escaped pixels have `iters > 0`.

`error` is the glitch detection error count (for `fpxx` calculations).
Glitched pixels have `error != 0`.

`de` is the distance estimate relative to pixel size.
Far exterior pixels have `de > 4`.
*/
uniform sampler2D ida0;

/*!
\brief Slicing coordinates to number texture.
*/
uniform usampler2D slice_number;

/*!
\brief Slicing number to coordinates texture.
*/
uniform usampler2D slice_coords;

/*!
\brief Colour uncalculated pixels.

`false`: discard pixels marked as uncalculated.
*/
uniform bool update;

/*!
\brief Use small slice deltas.

`true`: force delta = 1
`false`: compute delta based on slice count
*/
uniform bool update2;

/*!
\brief Highlight glitches.

`true`: highlight glitched pixels in red, colour other pixels normally.

`false`: try to mask glitched pixels by blending neighbouring pixels.
*/
uniform bool errs;

/*!
\brief Boundary thickness.

Lower values give a darker image, high values can make aliasing more visible.
*/
uniform float thickness;

/*!
\brief Slicing parameters.

x : slice_n
y : slice
*/
uniform ivec2 slicing;

/*!
\brief Texture coordinate for the current pixel.
*/
smooth in vec2 t;

/*!
\brief Output RGBA colours.
*/
layout(location = 0, index = 0) out vec4 c;

/*!
\brief Compute slice block local coordinates for a pixel.

\param it Pixel coordinates.
\return Slice block local coordinates.
*/
ivec2 local_coords(ivec2 it) {
  int mask = (1 << slicing.y) - 1;
  int i = it.x & mask;
  int j = it.y & mask;
  return ivec2(i, j);
}

/*!
\brief Compute slice block global coordinates for a pixel.

\param it Pixel coordinates.
\return Slice block global coordinates.
*/
ivec2 global_coords(ivec2 it) {
  int i = (it.x >> slicing.y) << slicing.y;
  int j = (it.y >> slicing.y) << slicing.y;
  return ivec2(i, j);
}

/*!
\brief Look up slice block local coordinates for slice block number.

\param block Slice block number.
\return Slice block local coordinates.
*/
ivec2 block_coords(int block) {
  int i = block >> slicing.y;
  int j = block - (i << slicing.y);
  return ivec2(texelFetch(slice_coords, ivec2(i, j).yx, SLICE_MAX - slicing.y).xy);
}

/*!
\brief Look up slice block number for slice block local coordinates.

\param it Slice block local coordinates.
\param level Mipmap level.
\return Slice block number.
*/
int block_number_raw(ivec2 it, int level) {
  return int(texelFetch(slice_number, it.yx, level).x);
}

/*!
\brief Compute best block number for a pixel.

\param it Slice block local coordinates.
\return Slice block number.
*/
int block_number(ivec2 it) {
  // check if our block is already computed
  int block = block_number_raw(it, SLICE_MAX - slicing.y);
  if (block <= slicing.x) { return block; }
  // slicing.y > 0 because if it were 0, we would have returned already
  int i = it.x;
  int j = it.y;
  // search through mipmap levels
  for (int level = SLICE_MAX - slicing.y; level < SLICE_MAX; ++level) {
    // look up in 2x2 neighbourhood
    int i0 = (i >> 1) << 1;
    int i1 = i0 + 1;
    int j0 = (j >> 1) << 1;
    int j1 = j0 + 1;
    // find highest block number that isn't too big
    block = -1;
    int block00 = block_number_raw(ivec2(i0, j0), level);
    if (block < block00 && block00 <= slicing.x) { block = block00; }
    int block01 = block_number_raw(ivec2(i0, j1), level);
    if (block < block01 && block01 <= slicing.x) { block = block01; }
    int block10 = block_number_raw(ivec2(i1, j0), level);
    if (block < block10 && block10 <= slicing.x) { block = block10; }
    int block11 = block_number_raw(ivec2(i1, j1), level);
    if (block < block11 && block11 <= slicing.x) { block = block11; }
    // if we found one, return it
    if (0 <= block) { return block; }
    // didn't find one, going up a level with a smaller size
    i = i >> 1;
    j = j >> 1;
  }
  // didn't find one, return the block that always exists
  return 0;
}

/*!
\brief Compute pixel delta for neighbours.

\return Pixel delta.
*/
int delta() {
  if (update2) { return 1; }
  int m = 0;
  for (int k = slicing.y; k > 0; --k) {
    int n = 1 << (k << 1);
    if (slicing.x >= n) {
      m = slicing.y - k;
      break;
    }
  }
  return 1 << m;
}

/*!
\brief Rejig the coordinates for a pixel so that they are in a computed slice.

\param it Pixel coordinates.
\return Pixel coordinates in a computed slice.
*/
ivec2 quantize_coords(ivec2 it) {
  if (update2) { return it; }
  ivec2 global_it = global_coords(it);
  ivec2 local_it = local_coords(it);
  int block = block_number(local_it);
  ivec2 block_it = block_coords(block);
  return global_it + block_it;
}

/*!
\brief Compute the colour for a pixel.

If `errs` is enabled, glitched pixels are highlighted in red.

Unescaped pixels are coloured yellow.

Exterior pixels are coloured using distance estimates.
If `DE` is enabled, then it uses the distance estimate to colour.
If `DE` is not enabled, then a fake distance estimate colouring based on
neighbouring pixels iteration counts is used.  See:
<http://mathr.co.uk/blog/2014-12-13_faking_distance_estimate_colouring.html>

\param it0 The texture coordinates.
\param dims The size of the texture.
\return The RGBA colour.  Alpha is 0 when a meaningful colour could not be
computed.
*/
vec4 colour(ivec2 it0, ivec2 dims) {
  ivec2 it = quantize_coords(it0);
  vec4 ida = texelFetch(ida0, it, 0);
  bool escaped = ida.x > 0.0;
  bool glitched = ida.y != 0.0;
  vec3 rgb = vec3(1.0);
  float de = 1.0;
  float alpha = 1.0;

  if (! escaped) {
    rgb = vec3(1.0, 0.7, 0.0);
    alpha = 1.0;
  } else {

#ifdef DE

    de = tanh(clamp(ida.z * thickness / 2.0, 0.0, 4.0));

#else

    int neighbour_count = 0;
    float neighbour_z = thickness * 3.0;
    float neighbour_sum2 = neighbour_z * neighbour_z;
    int d = delta();
    for (int i = -d; i <= d; i += d) {
      for (int j = -d; j <= d; j += d) {
        // want 8-point neighbourhood
        if (i == 0 && j == 0) continue;
        // quantize neighbour's coordinates
        ivec2 it20 = it + ivec2(i, j);
        ivec2 it2 = quantize_coords(it20);
        // check that it's really a different pixel
        if (it2.x == it.x && it2.y == it.y) continue;
        // check image bounds
        if (it2.x < 0 || it2.y < 0) continue;
        if (it2.x >= dims.x || it2.y >= dims.y) continue;
        // compute fake de
        float neighbour = texelFetch(ida0, it2, 0).x;
        // check exterior
        if (neighbour > 0.0) {
          neighbour_count += 1;
          float distance = length(vec2(it2 - it));
          float diff = (ida.x - neighbour) / distance;
          neighbour_sum2 += diff * diff;
        }
      }
    }
    if (neighbour_count > 0) {
      de = neighbour_z / sqrt(neighbour_sum2);
    } else {
      alpha = 0.0;
    }

#endif

  }

  if (errs && glitched) {
    rgb = mix(rgb, vec3(1.0, 0.0, 0.0), tanh(clamp(abs(ida.y) / 16.0, 0.0, 3.0) + 1.0));
  }

  if (escaped) {
    rgb = mix(vec3(0.0), rgb, errs && glitched ? de * 0.5 + 0.5 : de);
  }

  return vec4(rgb * alpha, alpha);
}

/*!
\brief Compute the colour for a pixel, optionally masking glitches.

If `errs` is set, just calls `colour()` for each pixel.
Otherwise, glitched pixels are coloured by merging the `colour()`s from
non-glitched neighbours -- if there are no non-glitched neighbours then
use black as a last resort.
*/
void main() {
  ivec2 dims = textureSize(ida0, 0);
  ivec2 it0 = ivec2(floor(t * vec2(dims)));
  ivec2 it = quantize_coords(it0);
  vec4 ida = texelFetch(ida0, it, 0);
  bool glitched = ida.y != 0.0;
  vec4 rgba = vec4(0.0);
  if (false && ! update && ida.x <= 0.0) {
    discard;
  } else {
    if (! glitched || errs) {
      // show good pixels normally (! glitched)
      // show errors normally (errs)
      rgba = colour(it0, dims);
    } else {
      // mask small glitches by blending neighbouring texels
      int d = delta();
      for (int i = -d; i <= d; i += d) {
        for (int j = -d; j <= d; j += d) {
          // want 8-point neighbourhood
          if (i == 0 && j == 0) continue;
          // quantize neighbour's coordinates
          ivec2 it20 = it + ivec2(i, j);
          ivec2 it2 = quantize_coords(it20);
          // check that it's really a different pixel
          if (it2.x == it.x && it2.y == it.y) continue;
          // check image bounds
          if (it2.x < 0 || it2.y < 0) continue;
          if (it2.x >= dims.x || it2.y >= dims.y) continue;
          // if it's unglitched, accumulate colour weighted by distance
          float glitch2 = texelFetch(ida0, it2, 0).y;
          if (glitch2 == 0.0) {
            float distance = length(vec2(it2 - it));
            rgba += distance * colour(it2, dims);
          }
        }
      }
      if (rgba.a > 0.0) {
        rgba /= rgba.a;
      } else {
        // no neighbouring unglitched pixels, last resort...
        rgba = vec4(vec3(0.0), 1.0);
      }
    }
    if (rgba.a > 0.0) {
      c = vec4(rgba.rgb, 1.0);
    } else {
      discard;
    }
  }
}

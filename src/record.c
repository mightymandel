// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <GL/glew.h>

#include "record.h"
#include "logging.h"
#include "texture.h"

void record_begin(struct record *s) {
  s->buffer = 0;
  s->bytes = 0;
}

void record_end(struct record *s) {
  if (s->buffer) {
    free(s->buffer);
    s->buffer = 0;
    s->bytes = 0;
  }
}

void record_do(struct record *s, const char *name, int width, int height, const char *comment) {
  if (! s->buffer) {
    s->bytes = 3 * width * height;
    s->buffer = (unsigned char *) malloc(s->bytes);
  }
  glActiveTexture(GL_TEXTURE0 + TEX_RGB);
  glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, s->buffer);
  bool ok = true;
  FILE *out = fopen(name, "wb");
  if (! out) {
    log_message(LOG_ERROR, "couldn't open output file: %s\n", name);
    ok = false;
  } else {
    fprintf(out, "P6\n");
    fprintf(out, "%s", comment);
    fprintf(out, "%d %d\n255\n", width, height);
    fflush(out);
    // PPM is rows top to bottom, OpenGL is rows bottom to top
    for (int y = height - 1; y >= 0; --y) {
      if (1 != fwrite(s->buffer + y * width * 3, width * 3, 1, out)) {
        log_message(LOG_ERROR, "error writing to file: %s\n", name);
        ok = false;
        break;
      }
    }
    fflush(out);
    fclose(out);
  }
  if (ok) {
    log_message(LOG_NOTICE, "saved: %s\n", name);
  }
}

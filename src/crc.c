/*!
\file crc.c
\brief [Sample Cyclic Redundancy Code implementation](http://www.w3.org/TR/2003/REC-PNG-20031110/#D-CRCAppendix)

\copyright \htmlonly
<a href="http://www.w3.org/Consortium/Legal/ipr-notice">Copyright</a>
© 2003 <a href="http://www.w3.org/">World Wide Web Consortium</a>,
(<a href="http://www.lcs.mit.edu/">Massachusetts Institute of Technology</a>,
<a href="http://www.ercim.org/">European Research Consortium for Informatics
and Mathematics</a>, <a href="http://www.keio.ac.jp/">Keio University</a>,
<a href="http://ev.buaa.edu.cn/">Beihang</a>). All Rights Reserved. This work
is distributed under the
<a href="http://www.w3.org/Consortium/Legal/2002/copyright-software-20021231">W3C<sup>®</sup>
Software License</a> in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
A PARTICULAR PURPOSE.
\endhtmlonly \ref COPYINGW3C
*/

/* Table of CRCs of all 8-bit messages. */
unsigned long crc_table[256];

/* Flag: has the table been computed? Initially false. */
int crc_table_computed = 0;

/* Make the table for a fast CRC. */
void make_crc_table(void)
{
 unsigned long c;
 int n, k;

 for (n = 0; n < 256; n++) {
   c = (unsigned long) n;
   for (k = 0; k < 8; k++) {
     if (c & 1)
       c = 0xedb88320L ^ (c >> 1);
     else
       c = c >> 1;
   }
   crc_table[n] = c;
 }
 crc_table_computed = 1;
}


/* Update a running CRC with the bytes buf[0..len-1]--the CRC
  should be initialized to all 1's, and the transmitted value
  is the 1's complement of the final running CRC (see the
  crc() routine below). */

/* {{{ modified by Claude Heiland-Allen 2015-01-08 (added const) */
unsigned long update_crc(unsigned long crc, const unsigned char *buf,
/* }}} modified by Claude Heiland-Allen 2015-01-08 (added const) */
                        int len)
{
 unsigned long c = crc;
 int n;

 if (!crc_table_computed)
   make_crc_table();
 for (n = 0; n < len; n++) {
   c = crc_table[(c ^ buf[n]) & 0xff] ^ (c >> 8);
 }
 return c;
}

/* Return the CRC of the bytes buf[0..len-1]. */
/* {{{ modified by Claude Heiland-Allen 2015-01-08 (added const) */
unsigned long crc(const unsigned char *buf, int len)
/* }}} modified by Claude Heiland-Allen 2015-01-08 (added const) */
{
 return update_crc(0xffffffffL, buf, len) ^ 0xffffffffL;
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef CRC_H
#define CRC_H 1

/*!
\brief Compute CRC (cyclic redundancy code) as specified by PNG
\param buf The data to checksum.
\param len The length of the data.
\return The CRC value.
*/
unsigned long crc(const unsigned char *buf, int len);

#endif

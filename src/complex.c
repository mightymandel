// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file  complex.c
\brief Helpers for complex number arithemetic with mpfr.
*/

/*!
\brief Random hack.

ISO C99 doesn't allow empty translation units, so it was easier to add a dummy
variable than to fix the Makefile to allow headers without corresponding
implementation files.
*/
int complex_c_cant_be_empty = 0;

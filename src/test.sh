#!/bin/bash
## mightymandel -- GPU-based Mandelbrot Set explorer
## Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
## License GPL3+ http://www.gnu.org/licenses/gpl.html

changes="$(git status --porcelain . | wc -l)"
if (( ${changes} > 0 ))
then
  git status .
  echo "please commit changes before running the test suite"
  exit 1
fi
make clean 1>&2
make 1>&2
if [[ -x mightymandel ]]
then
  mkdir -p ../bin
  mightymandel="../bin/mightymandel-$(./mightymandel --version)"
  cp -avi mightymandel "${mightymandel}"
  now="$(date --iso=s)"
  version="$("${mightymandel}" --version)"
  stem="${version}"
  ( time (
    echo "# mightymandel ${version} test suite at ${now}"
    echo "# filename,result,method,iterations,exterior,interior,glitched,passes"
    cd "../examples"
    exclude="../test/EXCLUDE.md"
    if [[ -e "${exclude}" ]]
    then
      echo     "                excluding files from '${exclude}'" 1>&2
    else
      exclude="/dev/null"
    fi
    mkdir -p "../test/${stem}"
    total="$(find * -type f |grep -v README | wc -l)"
    find * -type f |
    grep -v README |
    xargs -n 1 basename |
    sort |
    sed 's|^\(.*\)\.\([^.]*\)|\2/\1.\2|' |
    cat -n |
    while read index example
    do
      file="$(basename "${example}")"
      echo "                ${file} ( ${index} / ${total} )" 1>&2
      grep -q -F "${file}" "${exclude}" ||
      (
        timeout 310s "${mightymandel}" --timeout 300 --verbose warn --one-shot --no-approx --weight -1.5 --max-blob 4 --glitch --size "640x360" "${example}"
        retval="$?"
        if (( ${retval} == 124 ))
        then
          echo "${file},timeout,,,,,,"
        else
          count="$(ls -1 mightymandel*.ppm 2>/dev/null | wc -l)"
          if (( ${count} == 1 ))
          then
            mv mightymandel*.ppm "${example}.ppm" &&
            ../extra/ppm2png.sh "${example}.ppm" &&
            mv "${example}.png" "../test/${stem}/${file}.png" &&
            rm "${example}.ppm"
          elif (( ${count} > 0 ))
          then
            echo "                more than one output file" 1>&2
            rm mightymandel*.ppm
          elif (( ${retval} == 0 ))
          then
            echo "                no output, exit 0" 1>&2
          elif (( ${retval} == 1 ))
          then
            echo "                no output, exit 1" 1>&2
          else
            echo "${file},crash,,,,,,"
          fi
        fi
      )
    done
    echo       "                DONE" 1>&2
    echo       "                now inspect the result images for undetected corrupt images" 1>&2
    echo       "                they can be found in 'test/${stem}/'" 1>&2
  ) ) | tee "../test/${stem}.csv" | ts
else
  echo       "mightymandel failed to build - are you in src/ ?" 1>&2
  exit 1
fi

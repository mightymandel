// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "fp64_unescaped.h"
#include "shader.h"
#include "logging.h"

extern const char *fp64_unescaped_vert;
extern const char *fp64_unescaped_geom;
const GLchar *fp64_unescaped_varyings[] = {"cne", "zdz"};

void fp64_unescaped_begin(struct fp64_unescaped *s) {
  s->program = compile_program_tf("fp64_unescaped", fp64_unescaped_vert, fp64_unescaped_geom, 0, 2, fp64_unescaped_varyings);
  s->cne0 = glGetAttribLocation(s->program, "cne0");D;
  s->zdz0 = glGetAttribLocation(s->program, "zdz0");D;
  glGenVertexArrays(1, &s->vao);D;
}

void fp64_unescaped_end(struct fp64_unescaped *s) {
  glDeleteProgram(s->program);D;
  glDeleteVertexArrays(1, &s->vao);D;
}

void fp64_unescaped_start(struct fp64_unescaped *s, GLuint vbo) {
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
  glEnableVertexAttribArray(s->cne0);D;
  glEnableVertexAttribArray(s->zdz0);D;
  glVertexAttribLPointer(s->cne0, 4, GL_DOUBLE, (DE ? 8 : 6) * sizeof(GLdouble), 0);D;
  glVertexAttribLPointer(s->zdz0, DE ? 4 : 2, GL_DOUBLE, (DE ? 8 : 6) * sizeof(GLdouble), ((GLbyte *)0)+(4*sizeof(GLdouble)));D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  debug_message("VBO unescaped: %d -> ?\n", vbo);
}

void fp64_unescaped_do(struct fp64_unescaped *s, GLuint *unescaped, GLuint active_count, GLuint vbo, GLuint query) {
  glBindVertexArray(s->vao);D;
  glUseProgram(s->program);D;
  glEnable(GL_RASTERIZER_DISCARD);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo);D;
  glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);D;
  glBeginTransformFeedback(GL_POINTS);D;
  glDrawArrays(GL_POINTS, 0, active_count);D;
  glEndTransformFeedback();D;
  glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
  glGetQueryObjectuiv(query, GL_QUERY_RESULT, unescaped);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
  glDisable(GL_RASTERIZER_DISCARD);D;
  glUseProgram(0);D;
  glBindVertexArray(0);D;
  debug_message("VBO unescaped: ? -> %d\n", vbo);
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FILENAME_H
#define FILENAME_H 1

#include <time.h>

struct filename {
  time_t t;
  struct tm tm;
};

void filename_begin(struct filename *s);
char *filename_name(struct filename *s, const char *ext, int seqno, int tilex, int tiley);

#endif

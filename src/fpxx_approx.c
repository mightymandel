// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "fpxx_approx.h"
#include "shader.h"
#include "logging.h"
#include "utility.h"
#include "mightymandel.h"
#include "texture.h"

extern const char *fpxx_approx_vert;
const GLchar *fpxx_approx_varyings[] = {"cne", "zdz", "err"};

void fpxx_approx_begin(struct fpxx_approx *s) {
  s->program = compile_program_tf("fpxx_approx", fpxx_approx_vert, 0, 0, 3, fpxx_approx_varyings);
  s->abcuvw = glGetUniformLocation(s->program, "abcuvw");D;
  s->iters = glGetUniformLocation(s->program, "iters");D;
  s->c0 = glGetAttribLocation(s->program, "c0");D;
  glGenVertexArrays(1, &s->vao);
  mpfr_inits2(53
    , s->p.x, s->p.y, s->z.x, s->z.y, s->z2.x, s->z2.y, s->delta4
    , s->a.x, s->a.y, s->b.x, s->b.y, s->c.x, s->c.y
    , s->u.x, s->u.y, s->v.x, s->v.y, s->w.x, s->w.y
    , s->a2.x, s->a2.y, s->b2.x, s->b2.y, s->c2.x, s->c2.y
    , s->u2.x, s->u2.y, s->v2.x, s->v2.y, s->w2.x, s->w2.y
    , s->t1.x, s->t1.y, s->t2.x, s->t2.y
    , s->t3.x, s->t3.y, s->t4.x, s->t4.y
    , s->t5.x, s->t5.y, s->t6.x, s->t6.y
    , (mpfr_ptr) 0);
}

void fpxx_approx_end(struct fpxx_approx *s) {
  glDeleteVertexArrays(1, &s->vao);
  glDeleteProgram(s->program);D;
  mpfr_clears
    ( s->p.x, s->p.y, s->z.x, s->z.y, s->z2.x, s->z2.y, s->delta4
    , s->a.x, s->a.y, s->b.x, s->b.y, s->c.x, s->c.y
    , s->u.x, s->u.y, s->v.x, s->v.y, s->w.x, s->w.y
    , s->a2.x, s->a2.y, s->b2.x, s->b2.y, s->c2.x, s->c2.y
    , s->u2.x, s->u2.y, s->v2.x, s->v2.y, s->w2.x, s->w2.y
    , s->t1.x, s->t1.y, s->t2.x, s->t2.y
    , s->t3.x, s->t3.y, s->t4.x, s->t4.y
    , s->t5.x, s->t5.y, s->t6.x, s->t6.y
    , (mpfr_ptr) 0);
}

void fpxx_approx_do(struct fpxx_approx *s, GLuint *active_count, GLuint *vbo, GLuint query, const mpfr_t dzx0, const mpfr_t dzy0, mpfr_t zx, mpfr_t zy, mpfr_t dzx, mpfr_t dzy, int pass, const mpfr_t radius, const mpfr_t refx, const mpfr_t refy, bool series_approx, bool initial_slice, void *abort_data, abort_t abort_fn) {
  if (initial_slice) {
    s->too_deep = false;
    mpfr_sqr(s->delta4, radius, MPFR_RNDN);
    mpfr_sqr(s->delta4, s->delta4, MPFR_RNDN);
    mpfr_prec_t p = mpfr_get_prec(refx);
    c_set_prec(s->p, p);
    c_set_prec(s->z, p);
    c_set_prec(s->a, p);
    c_set_prec(s->b, p);
    c_set_prec(s->c, p);
    c_set_prec(s->u, p);
    c_set_prec(s->v, p);
    c_set_prec(s->w, p);
    c_set_prec(s->z2, p);
    c_set_prec(s->a2, p);
    c_set_prec(s->b2, p);
    c_set_prec(s->c2, p);
    c_set_prec(s->u2, p);
    c_set_prec(s->v2, p);
    c_set_prec(s->w2, p);
    c_set_prec(s->t1, p);
    c_set_prec(s->t2, p);
    c_set_prec(s->t3, p);
    c_set_prec(s->t4, p);
    c_set_prec(s->t5, p);
    c_set_prec(s->t6, p);
    mpfr_set(s->p.x, refx, MPFR_RNDN); mpfr_set(s->p.y, refy, MPFR_RNDN);
    mpfr_set(s->z.x, refx, MPFR_RNDN); mpfr_set(s->z.y, refy, MPFR_RNDN);
    mpfr_set_ui(s->a.x, 1, MPFR_RNDN); mpfr_set_ui(s->a.y, 0, MPFR_RNDN);
    mpfr_set_ui(s->b.x, 0, MPFR_RNDN); mpfr_set_ui(s->b.y, 0, MPFR_RNDN);
    mpfr_set_ui(s->c.x, 0, MPFR_RNDN); mpfr_set_ui(s->c.y, 0, MPFR_RNDN);
    if (DE) {
      mpfr_set_ui(s->u.x, 0, MPFR_RNDN); mpfr_set_ui(s->u.y, 0, MPFR_RNDN);
      mpfr_set_ui(s->v.x, 0, MPFR_RNDN); mpfr_set_ui(s->v.y, 0, MPFR_RNDN);
      mpfr_set_ui(s->w.x, 0, MPFR_RNDN); mpfr_set_ui(s->w.y, 0, MPFR_RNDN);
    }
    unsigned int n = 1;
    bool accurate = true;
    while (accurate) {
      if (abort_fn(abort_data)) {
        return;
      }
      s->values[0][0] = mpfr_get_d(s->a.x, MPFR_RNDN);
      s->values[0][1] = mpfr_get_d(s->a.y, MPFR_RNDN);
      s->values[1][0] = mpfr_get_d(s->b.x, MPFR_RNDN);
      s->values[1][1] = mpfr_get_d(s->b.y, MPFR_RNDN);
      s->values[2][0] = mpfr_get_d(s->c.x, MPFR_RNDN);
      s->values[2][1] = mpfr_get_d(s->c.y, MPFR_RNDN);
      if (DE) {
        mpfr_set(s->t5.x, dzx0, MPFR_RNDN);
        mpfr_set(s->t5.y, dzy0, MPFR_RNDN);
        c_mul(s->t6, s->t5, s->u, s->t1.x, s->t1.y, s->t2.x, s->t2.y);
        s->values[3][0] = mpfr_get_d(s->t6.x, MPFR_RNDN);
        s->values[3][1] = mpfr_get_d(s->t6.y, MPFR_RNDN);
        c_mul(s->t6, s->t5, s->v, s->t1.x, s->t1.y, s->t2.x, s->t2.y);
        s->values[4][0] = mpfr_get_d(s->t6.x, MPFR_RNDN);
        s->values[4][1] = mpfr_get_d(s->t6.y, MPFR_RNDN);
        c_mul(s->t6, s->t5, s->w, s->t1.x, s->t1.y, s->t2.x, s->t2.y);
        s->values[5][0] = mpfr_get_d(s->t6.x, MPFR_RNDN);
        s->values[5][1] = mpfr_get_d(s->t6.y, MPFR_RNDN);
      }
      if (! series_approx) {
        break;
      }
      n += 1;
      if (DE) {
        // w <- 2 (a c + z w + a v + b u)
        c_mul(s->w2, s->z,  s->w, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
        c_mul(s->t1, s->a,  s->c, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
        c_add(s->w2, s->w2, s->t1);
        c_mul(s->t1, s->a,  s->v, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
        c_add(s->w2, s->w2, s->t1);
        c_mul(s->t1, s->b,  s->u, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
        c_add(s->w2, s->w2, s->t1);
        c_mul_2ui(s->w, s->w, 1);
        // v <- 2 (a b + z v + a u)
        c_mul(s->v2, s->z,  s->v, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
        c_mul(s->t2, s->a,  s->u, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
        c_add(s->v2, s->v2, s->t2);
        c_mul(s->t2, s->a,  s->b, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
        c_add(s->v2, s->v2, s->t2);
        c_mul_2ui(s->v2, s->v2, 1);
        // u <- 2 (a a + z u)
        c_mul(s->u2, s->z, s->u, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
        c_sqr(s->t1, s->a, s->t3.y, s->t4.x, s->t4.y);
        c_add(s->u2, s->u2, s->t1);
        c_mul_2ui(s->u2, s->u, 1);
      } else {
        // a b
        c_mul(s->t2, s->a, s->b, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
        // a a
        c_sqr(s->t1, s->a, s->t3.y, s->t4.x, s->t4.y);
      }
      // c <- 2 (a b + z c)
      c_mul(s->c2, s->z, s->c, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
      c_add(s->c2, s->t2, s->c2);
      c_mul_2ui(s->c2, s->c2, 1);
      // b <- 2 z b + a a
      c_mul(s->b2, s->z, s->b, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
      c_mul_2ui(s->b2, s->b2, 1);
      c_add(s->b2, s->t1, s->b2);
      // a2 <- 2 z a + 1
      c_mul(s->a2, s->z, s->a, s->t3.x, s->t3.y, s->t4.x, s->t4.y);
      c_mul_2ui(s->a2, s->a2, 1);
      mpfr_add_ui(s->a2.x, s->a2.x, 1, MPFR_RNDN);
      // z2 <- z z + p
      c_sqr(s->z2, s->z, s->t5.y, s->t6.x, s->t6.y);
      c_add(s->z2, s->z2, s->p);
      // accurate <- |c|^2 d^4 << |a|^2 && |w|^2 d^4 << |u|^2
      c_mag2(s->t1.x, s->c2, s->t4.x, s->t4.y);
      c_mag2(s->t1.y, s->a2, s->t4.x, s->t4.y);
      mpfr_div(s->t1.x, s->t1.x, s->t1.y, MPFR_RNDN);
      mpfr_mul(s->t1.x, s->t1.x, s->delta4, MPFR_RNDN);
      if (DE) {
        c_mag2(s->t2.x, s->w2, s->t4.x, s->t4.y);
        c_mag2(s->t2.y, s->u2, s->t4.x, s->t4.y);
        mpfr_div(s->t2.x, s->t2.x, s->t2.y, MPFR_RNDN);
        mpfr_mul(s->t2.x, s->t2.x, s->delta4, MPFR_RNDN);
      }
      c_mag2(s->t4.y, s->z2, s->t3.x, s->t3.y);
      const double eps = 1e-40;
      accurate = mpfr_get_d(s->t1.x, MPFR_RNDN) < eps && ! (mpfr_get_d(s->t4.y, MPFR_RNDN) > 5); // && (DE ? mpfr_get_d(s->t2.x, MPFR_RNDN) < eps : 1);
      if (accurate) {
        c_set(s->z, s->z2);
        c_set(s->a, s->a2);
        c_set(s->b, s->b2);
        c_set(s->c, s->c2);
        if (DE) {
          c_set(s->u, s->u2);
          c_set(s->v, s->v2);
          c_set(s->w, s->w2);
        }
      }
    }
    n -= 1;
    s->n = n;
    log_message(LOG_INFO, "approx skip: %d\n", n);
  }
  bool ok = true;
  for (int i = 0; i < (DE ? 6 : 3); ++i) {
    for (int j = 0; j < 2; ++j) {
      if (fabs(s->values[i][j]) > 1.0e144 || isnan(s->values[i][j])) {
        ok = false;
      }
    }
  }
  mpfr_set_prec(dzx, mpfr_get_prec(s->a.x));
  mpfr_set_prec(dzy, mpfr_get_prec(s->a.y));
  mpfr_set_prec(zx, mpfr_get_prec(s->z.x));
  mpfr_set_prec(zy, mpfr_get_prec(s->z.y));
  mpfr_set(dzx, s->a.x, MPFR_RNDN);
  mpfr_set(dzy, s->a.y, MPFR_RNDN);
  mpfr_set(zx, s->z.x, MPFR_RNDN);
  mpfr_set(zy, s->z.y, MPFR_RNDN);
  if (! (*active_count > 0)) {
    return;
  }
  if (ok) {
    debug_message("approx GPU\n");
    glEnable(GL_RASTERIZER_DISCARD);D;
    glBindVertexArray(s->vao);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);D;
    glUseProgram(s->program);D;
    glUniform2dv(s->abcuvw, DE ? 6 : 3, &s->values[0][0]);D;
    glUniform1i(s->iters, s->n);D;
    glVertexAttribLPointer(s->c0, 2, GL_DOUBLE, (DE ? 10 : 8) * sizeof(GLdouble), 0);D;
    glEnableVertexAttribArray(s->c0);D;
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[0]);D;
    glBeginQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN, query);D;
    glBeginTransformFeedback(GL_POINTS);D;
    glDrawArrays(GL_POINTS, 0, *active_count);D;
    glEndTransformFeedback();D;
    glEndQuery(GL_TRANSFORM_FEEDBACK_PRIMITIVES_WRITTEN);D;
    int before = *active_count;
    glGetQueryObjectuiv(query, GL_QUERY_RESULT, active_count);D;
    int after = *active_count;
    debug_message("approx active_count: %d -> %d\n", before, after);
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
    glDisableVertexAttribArray(s->c0);D;
    glUseProgram(0);D;
    glBindBuffer(GL_ARRAY_BUFFER, 0);D;
    glBindVertexArray(0);
    glDisable(GL_RASTERIZER_DISCARD);D;
    debug_message("VBO approx: %d -> %d\n", vbo[1], vbo[0]);
  } else {
    debug_message("approx CPU (overflow risk)\n");
    debug_message("VBO approx: %d -> %d\n", vbo[1], vbo[0]);
    glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);D;
    glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[0]);D;
    const double *in = glMapBufferRange(GL_ARRAY_BUFFER, 0, *active_count * (DE ? 10 : 8) * sizeof(GLdouble), GL_MAP_READ_BIT);D;
    bool ok;
    if (in) {
      const double *q = in;
      if (pass == 0 && ! (q[0] != 0) && ! (q[1] != 0)) {
        log_message(LOG_WARN, "approx delta(z)_0 == 0, rendering will probably fail\n");
        s->too_deep = true;
      }
      double *out = glMapBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, *active_count * (DE ? 10 : 8) * sizeof(GLdouble), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);D;
      if (out) {
        double *p = out;
        // initialize variables
        C c, c2, c3, z, dz, t;
        R t0, t1, t2, t3;
#define VARS c.x, c.y, c2.x, c2.y, c3.x, c3.y, z.x, z.y, dz.x, dz.y, t.x, t.y, t0, t1, t2, t3
        mpfr_inits2(mpfr_get_prec(refx), VARS, (mpfr_ptr) 0);
        for (unsigned int k = 0; k < *active_count; ++k) {
          mpfr_set_d(c.x, q[0], MPFR_RNDN);
          mpfr_set_d(c.y, q[1], MPFR_RNDN);
          q += (DE ? 10 : 8);
          *p++ = mpfr_get_d(c.x, MPFR_RNDN);
          *p++ = mpfr_get_d(c.y, MPFR_RNDN);
          *p++ = s->n;
          *p++ = 0; // escaped flag
          // approximation cubic: z = A c + B c c + C c c c
          c_mul(z, s->a, c, t0, t1, t2, t3);
          c_sqr(c2, c, t0, t1, t3);
          c_mul(t, s->b, c2, t0, t1, t2, t3);
          c_add(z, z, t);
          c_mul(c3, c, c2, t0, t1, t2, t3);
          c_mul(t, s->c, c3, t0, t1, t2, t3);
          c_add(z, z, t);
          *p++ = mpfr_get_d(z.x, MPFR_RNDN);
          *p++ = mpfr_get_d(z.y, MPFR_RNDN);
          if (DE) {
            // approximation cubic: z = U c + U c c + U c c c
            c_mul(dz, s->u, c, t0, t1, t2, t3);
            c_mul(t, s->v, c2, t0, t1, t2, t3);
            c_add(dz, dz, t);
            c_mul(t, s->w, c3, t0, t1, t2, t3);
            c_add(dz, dz, t);
            *p++ = mpfr_get_d(dz.x, MPFR_RNDN);
            *p++ = mpfr_get_d(dz.y, MPFR_RNDN);
          }
          *p++ = 0; // error flag
        }
        if (DE) {
          debug_message("approx out: c %e %e z %e %e dz %e %e\n", out[0], out[1], out[4], out[5], out[6], out[7]);
        } else {
          debug_message("approx out: c %e %e z %e %e\n", out[0], out[1], out[4], out[5]);
        }
        mpfr_clears(VARS, (mpfr_ptr) 0);
#undef VARS
        out = 0;
        p = 0;
        ok = glUnmapBuffer(GL_TRANSFORM_FEEDBACK_BUFFER);D;
        glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
      } else {
        ok = false;
      }
      in = 0;
      q = 0;
      ok &= glUnmapBuffer(GL_ARRAY_BUFFER);D;
      glBindBuffer(GL_ARRAY_BUFFER, 0);D;
    } else {
      ok = false;
    }
    if (! ok) {
      log_message(LOG_ERROR, "CPU-based series approximation initialisation failed\n");
    }
  }
}

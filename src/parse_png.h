// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_PNG_H
#define PARSE_PNG_H 1

/*!
\file parse_png.h
\brief PNG metadata parser specification.
*/

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Parse the original mightymandel `.png` metadata file format.

PNG has `tEXt` chunks with keyword `Software set to `mightymandel` and `Title`
set to the view coordinates (in the same format as `parse_ppm()`).

\copydetails parser
*/
bool parse_png(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

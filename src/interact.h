// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef INTERACT_H
#define INTERACT_H 1

#include <stdbool.h>
#include <mpfr.h>

struct interact {
  mpfr_t centerx;
  mpfr_t centery;
  mpfr_t radius;
  double weight;
  bool show_glitches;
  bool save_screenshot;
  bool quit;
  bool updated;
  int width;
  int height;
};

typedef enum {
  k_quit,
  k_show_glitches,
  k_zoom_in,
  k_zoom_out,
  k_save_screenshot,
  k_weight_0, k_weight_1, k_weight_2, k_weight_3, k_weight_4, k_weight_5, k_weight_6, k_weight_7, k_weight_8, k_weight_9,
  k_left, k_right, k_up, k_down
} key;
typedef enum { b_left, b_right, b_middle, b_up, b_down } button;

struct interact interact;

void interact_begin(int w, int h, const mpfr_t cx, const mpfr_t cy, const mpfr_t r, double weight, bool show_glitches);
void interact_end(void);
void interact_reset(void);
void interact_mouse(button button, double x, double y, bool shift, bool ctrl);
void interact_keyboard(key key, bool shift, bool ctrl);

#endif

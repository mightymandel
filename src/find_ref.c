// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file  find_ref.c
\brief Algorithms for finding new reference points.
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpfr.h>

#include "image.h"
#include "logging.h"
#include "utility.h"
#include "find_ref.h"

/*!
\brief Find a new reference for a possibly glitched image.

The new reference is inserted into the ref_set, failure or success is signalled
by return.
*/
enum find_ref_t find_ref(struct ref_set *refs, struct blob_set *blobset, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius, double max_glitch, int max_blob) {
  float *glitched = image_get_glitched(width, height);
  int blob_count = 0;
  struct blob *blobs = find_blobs1(&blob_count, width, height, glitched);
  enum find_ref_t retval = find_ref_failure;
  if (blobs) {
    int count = 0;
    for (int i = 0; i < blob_count; ++i) {
      count += blobs[i].count;
    }
    double percent = 100 * count / (double) (width * height);
    log_message(LOG_INFO, "glitched pixels: %8d/%d (%f%%)\n", count, width * height, percent);
    log_message(LOG_INFO, "largest blob: %d\n", blobs[0].count);
    if (percent <= max_glitch || blobs[0].count <= max_blob) {
      retval = find_ref_complete;
    } else {
      mpfr_t cx, cy;
      mpfr_inits2(53, cx, cy, (mpfr_ptr) 0);
      for (int i = 0; i < blob_count; ++i) {
        int blob_count2 = 0;
        struct blob *blobs2 = find_blobs2(&blob_count2, width, height, glitched, &blobs[i]);
        if (blobs2) {
          for (int j = 0; j < blob_count2; ++j) {
            if (! blob_set_contains(blobset, &blobs2[j])) {
              blob_set_insert(blobset, &blobs2[j]);
              int x = round(blobs2[j].i / (double) blobs2[j].count);
              int y = round(blobs2[j].j / (double) blobs2[j].count);
              pixel_coordinate(cx, cy, width, height, centerx, centery, radius, x + 0.5, y + 0.5);
              if (! ref_set_contains(refs, cx, cy)) {
                ref_set_insert(refs, cx, cy);
                retval = find_ref_success;
                break;
              }
            }
          }
          free(blobs2);
        }
        if (retval == find_ref_success) {
          break;
        }
      }
      mpfr_clears(cx, cy, (mpfr_ptr) 0);
    }
    free(blobs);
  } else {
    retval = find_ref_complete;
  }
  free(glitched);
  return retval;
}

#if 0
  // find highest iteration glitched pixels in image
  float *glitched = image_get_glitched(width, height);
  float *iterations = image_get_iterations(width, height);
  int count = 0;
  struct pixel *pxs = image_find_peak_glitches(glitched, iterations, width, height, &count);
  double percent = 100 * count / (double) (width * height);
  log_message(LOG_INFO, "glitched pixels: %8d/%d (%f%%)\n", count, width * height, percent);
  free(glitched);
  free(iterations);
  // find a glitched pixel not already used as a reference
  enum find_ref_t retval = find_ref_failure;
  if (count) {
    mpfr_t cx, cy;
    mpfr_inits2(53, cx, cy, (mpfr_ptr) 0);
    for (int k = 0; k < count; ++k) {
      pixel_coordinate(cx, cy, width, height, centerx, centery, radius, pxs[k].i + 0.5, pxs[k].j + 0.5);
      if (! ref_set_contains(refs, cx, cy)) {
        ref_set_insert(refs, cx, cy);
        retval = find_ref_success;
        break;
      }
    }
    mpfr_clears(cx, cy, (mpfr_ptr) 0);
  } else {
    retval = find_ref_complete;
  }
  free(pxs);
  return retval;
}
#endif

#if 0
  // find reference points in blobs, largest first
  enum find_ref_t retval;
  if (index) {
    qsort(blobs, index, sizeof(struct blob), cmp_blob_count_desc);
    if (blobs[0].count > 0) {
      retval = find_ref_failure;
      for (int i = 0; i < index; ++i) {
        if (blob_set_contains(blobset, blobs[i].count, blobs[i].i, blobs[i].j)) {
          continue;
        }
        if (blobs[i].count > 0) {
          blob_set_insert(blobset, blobs[i].count, blobs[i].i, blobs[i].j);
          mpfr_t cx, cy, r;
          mpfr_inits2(53, cx, cy, r, (mpfr_ptr) 0);
          int px = round(blobs[i].i / (double) blobs[i].count);
          int py = round(blobs[i].j / (double) blobs[i].count);
          pixel_coordinate(cx, cy, width, height, centerx, centery, radius, px + 0.5, py + 0.5);
          if (! ref_set_contains(refs, cx, cy)) {
            ref_set_insert(refs, cx, cy);
            retval = find_ref_success;
            break;
          }
          mpfr_clears(cx, cy, r, (mpfr_ptr) 0);
        } else {
          break;
        }
        if (retval == find_ref_success) {
          break;
        }
      }
    } else {
      retval = find_ref_complete;
    }
  } else {
    retval = find_ref_complete;
  }
  free(blobs); blobs = 0;
  return retval;
#endif

#if 0
  int pixels = width * height;
  // find highest iteration glitched pixel in highest iteration glitched region
  int M = width / 8;
  int N = height / 8;
  struct pixel *reg = malloc((width + M - 1)/(M/2) * (height + N - 1)/(N/2) * sizeof(struct pixel));
  int regs = 0;
  for (int j = 0; j < height; j += N/2) {
    for (int i = 0; i < width; i += M/2) {
      int count = 0;
      double local_total = 0;
      double local_max = -1;
      int local_i = 0;
      int local_j = 0;
      for (int jj = 0; jj < N; ++jj) {
        int jjj = j + jj;
        if (! (jjj < height)) { continue; }
        for (int ii = 0; ii < M; ++ii) {
          int iii = i + ii;
          if (! (iii < width)) { continue; }
          int kkk = jjj * width + iii;
          if (data[kkk]) {
            count += 1;
            local_total += data_n[kkk];
            if (data_n[kkk] > local_max) {
              local_max = data_n[kkk];
              local_i = i;
              local_j = j;
            }
          }
        }
      }
      if (count) {
        //local_total;// /= count;
        reg[regs].n = local_total;
        reg[regs].i = local_i;
        reg[regs].j = local_j;
        regs++;
      }
    }
  }
  log_message(LOG_NOTICE, "%d\n", regs);
#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_H
#define PARSE_H 1

/*!
\file  parse.h
\brief Parse parameter file formats.

This module handles loading parameter files, which are often line-based text
formats containing coordinates and other information for specific programs.
We are only interested in extracting the view, as center and radius.

A typical parser implementation might:

  - split a copy of the input into lines (for line-based formats)
  - check each line against known keywords and extract needed substrings
  - if the radius (or something convertible, like magnification) is present
      - compute the radius from its string, storing it in cz
      - p = precision_for_radius(cz) (precision needed to represent cx, cy)
      - mpfr_set_prec(cx, p), mpfr_set_prec(cy, p)
      - also set the precision of any required temporary variables as needed
      - compute the view center, store real and imaginary parts in cx, cy
  - otherwise (eg corners representation)
      - TODO: re-parse at successively higher precisions until radius > 0

If a format has variations, it might be clearer to write two parsers than
try to combine them into one.  Commonly needed functionality can be factored
out into other functions.
*/

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief The type of parsers for a format.  This typedef isn't actually used.
It's purely to avoid boilerplate copying and pasting in the documentation
markup.

\param source A null-terminated input file already loaded into memory.
\param length Length of the input file (without the extra null terminator).
\param cx For output of the real part of the view center.  It is already `mpfr_init2()`'d.
\param cy For output of the imaginary part of the view center.  It is already `mpfr_init2()`'d.
\param cz For output of the view radius.  It is already `mpfr_init2()`'d.
\return Success (`true`) or failure (`false`).
*/
typedef bool parser(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

/*!
\brief Check if a radius is valid.  A radius \f$r\f$ is valid when
\f$0 < r < +\infty\f$.  NaN is not a valid radius.
\param radius The radius to check.
\return whether the radius is valid.
*/
bool radius_is_valid(const mpfr_t radius);

/*!
\brief Calculate a reasonable precision that suffices to accurately represent
the center of a view of a given radius \f$r\f$.

Typically \f$r < 1\f$, which makes \f$\log_2 r < 0\f$.  As logarithms compress
a lot (\f$\log_2 10^{-300} > -1000\f$), using `mpfr_get_d` is safe (overflow
is exceedingly unlikely, maybe even impossible).
\param radius A valid radius.
\return A sufficient precision for the center.
*/
mpfr_prec_t precision_for_radius(const mpfr_t radius);

/*!
\brief Load a file's contents into memory.

Load a file's contents into a newly allocated null-terminated buffer.
The buffer may also have embedded nulls (for example when loading a binary
image file).
\param filename The file to load.
\param length The file size is stored here.
\return The file contents in memory, or null on failure.
*/
char *load_file(const char *filename, int *length);

/*!
\brief Split a string at the first occurence of a separator character.

If the separator is found then the source pointer is updated to the following
character, and the found separator is replaced with 0.  This makes the original
source pointer a null terminated string ending where the separator was found.
The original source pointer is returned, and the source pointer is updated as
described if the separator was found, or set to 0 otherwise.  It is an error to
pass in null or pointer to null as the source pointer.
\param source Pointer to source string pointer.  Updated as described above.
\param separator Charactor to split the string on.
\return Pointer to the first substring.
*/
char *parse_separator(char **source, int separator);

/*!
\brief Split a string at line endings and strip BOM.

Split a string at the end of the line `"\n"`.  If the previous character is
carriage return `"\r"`, delete it by replacing with null.  This shortens the
string length by one character.  The purpose is to handle different line
ending conventions on different platforms: Linux and OS X use `"\n"`, Windows
uses `"\n\r"`, aka `LF` and `CRLF`.  Also, some files in UTF-8 end up having a
byte order marker (BOM) inserted even if the content is in the pure ASCII subset
of UTF-8.  This usually only occurs once, at the start of the file, but it's
possible for it to occur anywhere.  Here we strip the BOM from the start of
lines, because that's the most likely place for it to occur when files are
appended to each other.
\param source Pointer to source string pointer.  Updated as described in
`parse_separator()`.
\return Pointer to the first line.
*/
char *parse_line(char **source);

/*!
\brief Parse a parameter file by trying all parsers in turn.

Parse format-agnostically by trying every parser in in turn and picking the
first that succeeds.  Most formats don't overlap enough to cause any problems.
Parse binary formats first, followed by text formats.  Binary format parsers
should be more reliable in file format identification, so will just fail on
text input, while text format parsers might encounter things they don't expect
amidst a jumble of binary data.  In any case, parsing arbitrary binary data
might fail in weird ways, so be wary.

\copydetails parser
*/
bool parse(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

/*!
\brief Load a parameter file and parse it.

\param filename The name of a file to load and parse.
\param cx For output of the real part of the view center.  It is already `mpfr_init2()`'d.
\param cy For output of the imaginary part of the view center.  It is already `mpfr_init2()`'d.
\param cz For output of the view radius.  It is already `mpfr_init2()`'d.
\return Success (`true`) or failure (`false`).
*/
bool load_parameter_file(const char *filename, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <math.h>
#include <stdio.h>

#include "fp32_escaped.h"
#include "shader.h"
#include "logging.h"

extern const char *fp32_escaped_vert;
extern const char *fp32_escaped_geom;
extern const char *fp32_escaped_frag;

void fp32_escaped_begin(struct fp32_escaped *s) {
  s->program = compile_program("fp32_escaped", fp32_escaped_vert, fp32_escaped_geom, fp32_escaped_frag);
  s->center = glGetUniformLocation(s->program, "center");D;
  s->radius = glGetUniformLocation(s->program, "radius");D;
  s->aspect = glGetUniformLocation(s->program, "aspect");D;
  s->loger2 = glGetUniformLocation(s->program, "loger2");D;
  s->pxs = glGetUniformLocation(s->program, "pxs");D;
  s->cne0 = glGetAttribLocation(s->program, "cne0");D;
  s->zdz0 = glGetAttribLocation(s->program, "zdz0");D;
  glGenVertexArrays(1, &s->vao);D;
}

void fp32_escaped_end(struct fp32_escaped *s) {
  glDeleteProgram(s->program);D;
  glDeleteVertexArrays(1, &s->vao);D;
}

void fp32_escaped_start(struct fp32_escaped *s, GLuint tex, GLuint fbo, GLuint vbo, double escaperadius2, int width, int height, const mpfr_t centerx0, const mpfr_t centery0, const mpfr_t radius0) {
  double centerx = mpfr_get_d(centerx0, MPFR_RNDN);
  double centery = mpfr_get_d(centery0, MPFR_RNDN);
  double radius  = mpfr_get_d(radius0,  MPFR_RNDN);
  double aspect = width / (double) height;
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);D;
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex, 0);D;
  GLenum buffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, buffers);D;
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
  glUseProgram(s->program);D;
  glUniform1f(s->loger2, log(escaperadius2));D;
  glUniform2f(s->center, centerx, centery);D;
  glUniform1f(s->radius, radius);D;
  glUniform1f(s->aspect, 1 / aspect);D;
  glUniform1f(s->pxs, height / (2.0 * radius));D;
  glEnableVertexAttribArray(s->cne0);D;
  glEnableVertexAttribArray(s->zdz0);D;
  glVertexAttribPointer(s->cne0, 4, GL_FLOAT, GL_FALSE, (DE ? 8 : 6) * sizeof(GLfloat), 0);D;
  glVertexAttribPointer(s->zdz0, DE ? 4 : 2, GL_FLOAT, GL_FALSE, (DE ? 8 : 6) * sizeof(GLfloat), ((GLbyte *)0)+(4*sizeof(GLfloat)));D;
  glBindVertexArray(0);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
  debug_message("VBO escaped: %d -> ?\n", vbo);
}

void fp32_escaped_do(struct fp32_escaped *s, GLuint active_count) {
  glUseProgram(s->program);D;
  glBindVertexArray(s->vao);D;
  glDrawArrays(GL_POINTS, 0, active_count);D;
  glBindVertexArray(0);D;
  glUseProgram(0);D;
  debug_message("VBO escaped: ? -> frag\n");
}

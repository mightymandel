// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FP64_UNESCAPED_H
#define FP64_UNESCAPED_H 1

#include <GL/glew.h>

struct fp64_unescaped {
  GLuint program;
  GLint cne0;
  GLint zdz0;
  GLuint vao;
};

void fp64_unescaped_begin(struct fp64_unescaped *s);
void fp64_unescaped_end(struct fp64_unescaped *s);
void fp64_unescaped_start(struct fp64_unescaped *s, GLuint vbo);
void fp64_unescaped_do(struct fp64_unescaped *s, GLuint *unescaped, GLuint active_count, GLuint vbo, GLuint query);

#endif

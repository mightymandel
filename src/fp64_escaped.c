// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <math.h>
#include <stdio.h>

#include "fp64_escaped.h"
#include "shader.h"
#include "logging.h"

extern const char *fp64_escaped_vert;
extern const char *fp64_escaped_geom;
extern const char *fp64_escaped_frag;

void fp64_escaped_begin(struct fp64_escaped *s) {
  s->program = compile_program("fp64_escaped", fp64_escaped_vert, fp64_escaped_geom, fp64_escaped_frag);
  s->center = glGetUniformLocation(s->program, "center");
  s->radius = glGetUniformLocation(s->program, "radius");
  s->aspect = glGetUniformLocation(s->program, "aspect");
  s->loger2 = glGetUniformLocation(s->program, "loger2");
  s->cne0 = glGetAttribLocation(s->program, "cne0");
  s->zdz0 = glGetAttribLocation(s->program, "zdz0");
  glGenVertexArrays(1, &s->vao);D;
}

void fp64_escaped_end(struct fp64_escaped *s) {
  glDeleteProgram(s->program);
  glDeleteVertexArrays(1, &s->vao);D;
}

void fp64_escaped_start(struct fp64_escaped *s, GLuint tex, GLuint fbo, GLuint vbo, double escaperadius2, int width, int height, const mpfr_t centerx0, const mpfr_t centery0, const mpfr_t radius0) {
  // set up target framebuffer
  glBindFramebuffer(GL_FRAMEBUFFER, fbo);D;
  glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, tex, 0);
  GLenum buffers[1] = { GL_COLOR_ATTACHMENT0 };
  glDrawBuffers(1, buffers);
  glBindFramebuffer(GL_FRAMEBUFFER, 0);D;
  // set up shader uniforms
  glUseProgram(s->program);D;
  double centerx = mpfr_get_d(centerx0, MPFR_RNDN);
  double centery = mpfr_get_d(centery0, MPFR_RNDN);
  double radius  = mpfr_get_d(radius0,  MPFR_RNDN);
  double aspect = width / (double) height;
  glUniform2d(s->center, centerx, centery);D;
  glUniform1d(s->radius, radius);D;
  glUniform1d(s->aspect, 1 / aspect);D;
  glUniform1d(s->loger2, log(escaperadius2));D;
  glUseProgram(0);D;
  // set up vertex pointers
  glBindVertexArray(s->vao);D;
  glBindBuffer(GL_ARRAY_BUFFER, vbo);D;
  glVertexAttribLPointer(s->cne0, 4, GL_DOUBLE, (DE ? 8 : 6) * sizeof(GLdouble), 0);D;
  glVertexAttribLPointer(s->zdz0, DE ? 4 : 2, GL_DOUBLE, (DE ? 8 : 6) * sizeof(GLdouble), ((GLbyte *)0)+(4*sizeof(GLdouble)));D;
  glEnableVertexAttribArray(s->cne0);D;
  glEnableVertexAttribArray(s->zdz0);D;
  glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  glBindVertexArray(0);D;
  debug_message("VBO escaped: %d -> ?\n", vbo);
}

void fp64_escaped_do(struct fp64_escaped *s, GLuint active_count) {
  glUseProgram(s->program);D;
  glBindVertexArray(s->vao);D;
  glDrawArrays(GL_POINTS, 0, active_count);D;
  glBindVertexArray(0);D;
  glUseProgram(0);D;
  debug_message("VBO escaped: ? -> frag\n");
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

layout(points) in;
layout(points, max_vertices = 1) out;
uniform int pass;
uniform dvec2 center;
uniform double radius;
uniform double aspect;
in  dvec4 cne1[1];
#ifdef DE
in  dvec4 zdz1[1];
#else
in  dvec2 zdz1[1];
#endif
in  dvec2 err1[1];
in  vec4 c1[1];
flat out dvec4 cne;
#ifdef DE
flat out dvec4 zdz;
#else
flat out dvec2 zdz;
#endif
flat out dvec2 err;
flat out vec4 ida;
void main() {
  bool glitched = ! (c1[0].w == 0.0);
  bool escaped = c1[0].z > 0.0;
  if (pass == 0 || (escaped && glitched)) {
    cne = cne1[0];
    zdz = zdz1[0];
    err = dvec2(c1[0].w, 0.0);
    ida = vec4(-abs(c1[0].z), 0.0, 0.0, 0.0);
    gl_Position = vec4(c1[0].xy * vec2(aspect, 1.0), 0.0, 1.0);
    EmitVertex();
    EndPrimitive();
  }
}

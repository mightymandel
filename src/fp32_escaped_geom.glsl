// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

layout(points) in;
layout(points, max_vertices = 1) out;
uniform vec2 center;
uniform float radius;
uniform float aspect;
in  vec4 cne1[1];
#ifdef DE
in  vec4 zdz1[1];
#else
in  vec2 zdz1[1];
#endif
flat out vec4 cne;
#ifdef DE
flat out vec4 zdz;
#else
flat out vec2 zdz;
#endif
void main() {
  bool escaped = ! (cne1[0].w <= 0);
  if (escaped) {
    cne = cne1[0];
    zdz = zdz1[0];
    gl_Position = vec4((cne1[0].xy - center) / radius * vec2(aspect, 1.0), 0.0, 1.0);
    EmitVertex();
    EndPrimitive();
  }
}

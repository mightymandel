// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include "stopwatch.h"

#ifndef MIGHTYMANDEL_WIN32
#ifndef __MACH__

// posix with rt

#include <stdlib.h>
#include <time.h>

/*!
\brief Find the difference between two times.

\param t1 The later time.
\param t0 The earlier time.
\return The time difference in seconds computed as `t1 - t0`.
*/
double time_difference(const struct timespec *t1, const struct timespec *t0) {
  return (t1->tv_sec - t0->tv_sec) + (t1->tv_nsec - t0->tv_nsec) / 1000000000.0;
}

struct stopwatch {
  struct timespec start;
  struct timespec stop;
  double elapsed;
};

struct stopwatch *stopwatch_new() {
  return calloc(1, sizeof(struct stopwatch));
}

void stopwatch_start(struct stopwatch *t) {
  clock_gettime(CLOCK_MONOTONIC, &t->start);
}

void stopwatch_stop(struct stopwatch *t) {
  clock_gettime(CLOCK_MONOTONIC, &t->stop);
  t->elapsed += time_difference(&t->stop, &t->start);
}

#else

// http://stackoverflow.com/questions/11680461/monotonic-clock-on-osx

#include <stdlib.h>
#include <mach/clock.h>
#include <mach/mach.h>

/*!
\brief Find the difference between two times.

\param t1 The later time.
\param t0 The earlier time.
\return The time difference in seconds computed as `t1 - t0`.
*/
double time_difference(const mach_timespec_t *t1, const mach_timespec_t *t0) {
  return (t1->tv_sec - t0->tv_sec) + (t1->tv_nsec - (double) t0->tv_nsec) / 1000000000.0;
}

struct stopwatch {
  mach_timespec_t start;
  mach_timespec_t stop;
  double elapsed;
};

struct stopwatch *stopwatch_new() {
  return calloc(1, sizeof(struct stopwatch));
}

void stopwatch_start(struct stopwatch *t) {
  clock_serv_t cclock;
  host_get_clock_service(mach_host_self(), SYSTEM_CLOCK, &cclock);
  clock_get_time(cclock, &t->start);
  mach_port_deallocate(mach_task_self(), cclock);
}

void stopwatch_stop(struct stopwatch *t) {
  clock_serv_t cclock;
  host_get_clock_service(mach_host_self(), SYSTEM_CLOCK, &cclock);
  clock_get_time(cclock, &t->stop);
  mach_port_deallocate(mach_task_self(), cclock);
  t->elapsed += time_difference(&t->stop, &t->start);
}

#endif
#else

// http://www.decompile.com/cpp/faq/windows_stopwatch_api.htm

#include <stdlib.h>
#include <windows.h>

/*!
\brief Find the difference between two times.

\param t1 The later time in ticks.
\param t0 The earlier time in ticks.
\param ticks Ticks per second.
\return The time difference in seconds computed as `t1 - t0`.
*/
double time_difference(const LARGE_INTEGER *t1, const LARGE_INTEGER *t0, const LARGE_INTEGER *ticks) {
  LARGE_INTEGER delta;
  delta.QuadPart = t1->QuadPart - t0->QuadPart;
  return delta.QuadPart / (double) ticks->QuadPart;
}

struct stopwatch {
  LARGE_INTEGER start;
  LARGE_INTEGER stop;
  double elapsed;
  LARGE_INTEGER ticks;
};

struct stopwatch *stopwatch_new() {
  struct stopwatch *t = calloc(1, sizeof(struct stopwatch));
  QueryPerformanceFrequency(&t->ticks);
  return t;
}

void stopwatch_start(struct stopwatch *t) {
  QueryPerformanceCounter(&t->start);
}

void stopwatch_stop(struct stopwatch *t) {
  QueryPerformanceCounter(&t->stop);
  t->elapsed += time_difference(&t->stop, &t->start, &t->ticks);
}

#endif

void stopwatch_delete(struct stopwatch *t) {
  free(t);
}

void stopwatch_reset(struct stopwatch *t) {
  stopwatch_stop(t);
  stopwatch_start(t);
  t->elapsed = 0;
}

double stopwatch_elapsed(struct stopwatch *t) {
  stopwatch_stop(t);
  stopwatch_start(t);
  return t->elapsed;
}

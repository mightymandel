// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FP32_COLOUR_H
#define FP32_COLOUR_H 1

#include <stdbool.h>
#include <GL/glew.h>

/*!
\brief State structure for colour module.
*/
struct fp32_colour {
  GLuint program;  //!< Shader program.
  GLint aspect;    //!< Shader uniform aspect ratio.
  GLint ida0;      //!< Shader uniform raw results texture sampler.
  GLint slice_number; //!< Shader uniform slice number texture sampler.
  GLint slice_coords; //!< Shader uniform slice coords texture sampler.
  GLint update;    //!< Shader uniform update uncalculated pixels.
  GLint update2;   //!< Shader uniform use small slice deltas.
  GLint errs;      //!< Shader uniform show glitches flag.
  GLint thickness; //!< Shader uniform colouring thickness.
  GLint slicing;   //!< Shader uniform slicing parameters.
  GLint pt;        //!< Shader attribute vertex coordinates.
  GLuint vao;      //!< Vertex array object.
  GLuint program2; //!< Pass-through shader program.
  GLint aspect2;   //!< Pass-through shader uniform aspect ratio.
  GLint rgb2;      //!< Pass-through shader uniform rgb texture sampler.
  GLint pt2;       //!< Pass-through shader attribute vertex coordinates.
  GLuint vao2;     //!< Pass-through vertex array object.
  GLuint vbo;      //!< Vertex buffer object for full screen triangle strip.
  GLuint fbo;      //!< Frame buffer object.
};

/*!
\brief Source from `fp32_colour_vert.glsl`.
*/
extern const char *fp32_colour_vert;

/*!
\brief Source from `fp32_colour_frag.glsl`.
*/
extern const char *fp32_colour_frag;

/*!
\brief Source from `fp32_colour2_vert.glsl`.
*/
extern const char *fp32_colour2_vert;

/*!
\brief Source from `fp32_colour2_frag.glsl`.
*/
extern const char *fp32_colour2_frag;

/*!
\brief Initialize.

\param s The `fp32_colour` state.
\param rgb_tex The RGB colour texture.
*/
void fp32_colour_begin(struct fp32_colour *s, GLuint rgb_tex);

/*!
\brief Cleanup.

\param s The `fp32_colour` state.
*/
void fp32_colour_end(struct fp32_colour *s);

/*!
\brief Perform colouring from a texture to the window.

\param s The `fp32_colour` state.
\param win_width The window width.
\param win_height The window height.
\param width The image width.
\param height The image height.
\param update Whether to update uncalculated pixels or discard them.
\param update2 Whether to use small slice deltas.
\param errs Whether to highlight glitches or try to mask them.
\param thickness The boundary colouring thickness.
\param slice The slicing level.
\param slice_n The current slice index.
*/
void fp32_colour_do(struct fp32_colour *s, int win_width, int win_height, int width, int height, bool update, bool update2, GLuint errs, float thickness, int slice, int slice_n);

void fp32_colour_clear(struct fp32_colour *s, int width, int height);

#endif

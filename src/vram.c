// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <GL/glew.h>
#include <stdio.h>

#include "utility.h"
#include "logging.h"

/*!
\brief Determine available video memory on NVIDIA systems.

<https://www.opengl.org/registry/specs/NVX/gpu_memory_info.txt>

\return Available video memory in kB.
*/
int vram_nvidia() {
  GLint mem = 0;
  glGetIntegerv(0x9049, &mem);
  int e = glGetError();
  if (e == GL_NO_ERROR) {
    return mem;
  } else if (e == GL_INVALID_ENUM) {
    return -1;
  } else {
    log_message(LOG_WARN, "OpenGL error %d in vram_nvidia()\n", e);
    return -2;
  }
}

/*!
\brief Determine available video memory on AMD/ATi systems.

<https://www.opengl.org/registry/specs/ATI/meminfo.txt>

\return Available video memory in kB.
*/
int vram_ati() {
  GLint mem[4] = { 0, 0, 0, 0 };
  glGetIntegerv(0x87FB, &mem[0]);
  int e = glGetError();
  if (e == GL_NO_ERROR) {
    return mem[1];
  } else if (e == GL_INVALID_ENUM) {
    return -1;
  } else {
    log_message(LOG_WARN, "OpenGL error %d in vram_ati()\n", e);
    return -2;
  }
}

int vram_available() {
  int nvx = vram_nvidia();
  int ati = vram_ati();
  return max(nvx, ati);
}

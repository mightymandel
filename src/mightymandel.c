// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>

#include "startup.h"
#include "interact.h"
#include "render.h"
#include "parse.h"
#include "tiling.h"
#include "zoom.h"
#include "record.h"
#include "utility.h"
#include "filename.h"
#include "stopwatch.h"
#include "metadata.h"
#include "vram.h"
#include "poll.h"
#include "version.h"
#include "image.h"

// initialize global state, see mightymandel.h for details
int opengl_error_printout_count = 0;
bool FP64 = true;
bool DE = true;

// not all glfw3 callbacks include mouse coordinates, but we need them sometimes
// they are saved here by motion_handler() and read in the other *_handler()s
static double mouse_x = 0;
static double mouse_y = 0;

// handle input from mouse motion
static void motion_handler(GLFWwindow *w, double x, double y) {
  (void) w;
  mouse_x = x;
  mouse_y = y;
}

// handle input from mouse button presses
static void button_handler(GLFWwindow *w, int button, int action, int mods) {
  (void) w;
  if (action == GLFW_PRESS) {
    int shift = mods & GLFW_MOD_SHIFT;
    int ctrl = mods & GLFW_MOD_CONTROL;
    switch (button) {
      case GLFW_MOUSE_BUTTON_LEFT:   interact_mouse(b_left,   mouse_x, mouse_y, shift, ctrl); break;
      case GLFW_MOUSE_BUTTON_MIDDLE: interact_mouse(b_middle, mouse_x, mouse_y, shift, ctrl); break;
      case GLFW_MOUSE_BUTTON_RIGHT:  interact_mouse(b_right,  mouse_x, mouse_y, shift, ctrl); break;
    }
  }
}

// handle input from mouse scroll wheel actions
static void scroll_handler(GLFWwindow *w, double xoffset, double yoffset) {
  (void) w;
  (void) xoffset;
  int direction = yoffset > 0 ? 1 : yoffset < 0 ? -1 : 0;
  bool shift = glfwGetKey(w, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS || glfwGetKey(w, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS;
  bool ctrl = glfwGetKey(w, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS || glfwGetKey(w, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS;
  switch (direction) {
    case  1: interact_mouse(b_up,   mouse_x, mouse_y, shift, ctrl); break;
    case -1: interact_mouse(b_down, mouse_x, mouse_y, shift, ctrl); break;
    default: break;
  }
}

// handle input from keyboard key presses
static void key_press_handler(GLFWwindow *w, int key, int scancode, int action, int mods) {
  (void) w;
  (void) scancode;
  bool shift = mods & GLFW_MOD_SHIFT;
  bool ctrl = mods & GLFW_MOD_CONTROL;
  if (action == GLFW_PRESS) {
    switch (key) {
      case GLFW_KEY_Q: case GLFW_KEY_ESCAPE: interact_keyboard(k_quit, shift, ctrl); break;
      case GLFW_KEY_E: interact_keyboard(k_show_glitches, shift, ctrl); break;
      case GLFW_KEY_PAGE_UP:   interact_keyboard(k_zoom_in, shift, ctrl); break;
      case GLFW_KEY_PAGE_DOWN: interact_keyboard(k_zoom_out, shift, ctrl); break;
      case GLFW_KEY_S: interact_keyboard(k_save_screenshot, shift, ctrl); break;
      case GLFW_KEY_0: interact_keyboard(k_weight_0, shift, ctrl); break;
      case GLFW_KEY_1: interact_keyboard(k_weight_1, shift, ctrl); break;
      case GLFW_KEY_2: interact_keyboard(k_weight_2, shift, ctrl); break;
      case GLFW_KEY_3: interact_keyboard(k_weight_3, shift, ctrl); break;
      case GLFW_KEY_4: interact_keyboard(k_weight_4, shift, ctrl); break;
      case GLFW_KEY_5: interact_keyboard(k_weight_5, shift, ctrl); break;
      case GLFW_KEY_6: interact_keyboard(k_weight_6, shift, ctrl); break;
      case GLFW_KEY_7: interact_keyboard(k_weight_7, shift, ctrl); break;
      case GLFW_KEY_8: interact_keyboard(k_weight_8, shift, ctrl); break;
      case GLFW_KEY_9: interact_keyboard(k_weight_9, shift, ctrl); break;
      case GLFW_KEY_UP:    interact_keyboard(k_up,    shift, ctrl); break;
      case GLFW_KEY_DOWN:  interact_keyboard(k_down,  shift, ctrl); break;
      case GLFW_KEY_LEFT:  interact_keyboard(k_left,  shift, ctrl); break;
      case GLFW_KEY_RIGHT: interact_keyboard(k_right, shift, ctrl); break;
    }
  }
}

// create a window with the desired OpenGL core profile version
static GLFWwindow* create_window(enum log_level_t level, int major, int minor, int width, int height) {
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, major);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, minor);
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  glfwWindowHint(GLFW_RESIZABLE, GL_FALSE);
  GLFWwindow *window = glfwCreateWindow(width, height, "mightymandel", 0, 0);
  if (! window) {
    log_message(level, "couldn't create window with OpenGL core %d.%d context\n", major, minor);
  }
  return window;
}

char *collect_metadata_to_string(const char *prefix, struct render_options *render_options, struct tiling *tiling, struct zoom *zoom, int pass) {
#define ADD(key,format,...) do{ \
  if (0 <= mpfr_asprintf(&s, format, __VA_ARGS__)) { \
    metadata_update(meta, key, s); \
    mpfr_free_str(s); \
  } \
} while(0)
  struct metadata *meta = metadata_new();
  char *s;
  metadata_update(meta, "software", "mightymandel");
  metadata_update(meta, "version", mightymandel_version);
  if (render_options->filename) {
    s = strrchr(render_options->filename, '/');
    metadata_update(meta, "filename", s ? s + 1 : render_options->filename);
  }
  ADD("view.real", "%Re", render_options->centerx);
  ADD("view.imag", "%Re", render_options->centery);
  ADD("view.radius", "%Re", render_options->radius);
  ADD("view.aspect", "%f", render_options->width / (double) render_options->height);
  ADD("image.width", "%d", render_options->width);
  ADD("image.height", "%d", render_options->height);
  ADD("image.pixels", "%d", render_options->width * render_options->height);
  metadata_update(meta, "calc.escaperadius", "602.833931527923"); /* FIXME hardcoded */
  double pxs = 2.0 * mpfr_get_d(render_options->radius, MPFR_RNDN) / render_options->height; /* FIXME precision */
  ADD("calc.pixelspacing", "%.16e", pxs);
  metadata_update(meta, "calc.fp", render_options->method == render_method_fp32 ? "fp32" : "fp64");
  metadata_update(meta, "calc.perturb", render_options->method == render_method_fpxx ? "yes" : "no");
  if (render_options->method == render_method_fpxx) {
    metadata_update(meta, "calc.perturb.approx", render_options->series_approx ? "yes" : "no");
    ADD("calc.perturb.refcount", "%d", render_options->method == render_method_fpxx ? pass + 1 : 0);
    ADD("calc.perturb.maxglitch","%.16e", render_options->max_glitch);
    ADD("calc.perturb.maxblob","%d", render_options->max_blob);
  }
  ADD("calc.sharpness","%.16e", render_options->sharpness);
  metadata_update(meta, "calc.distanceestimate", render_options->calculate_de ? "yes" : "no");
  metadata_update(meta, "colour.distanceestimate", render_options->show_de ? "yes" : "no");
  ADD("colour.weight","%.16e", render_options->weight);
  metadata_update(meta, "colour.showglitches", render_options->show_glitches ? "yes" : "no");
  metadata_update(meta, "zoom", zoom ? "yes" : "no");
  if (zoom) {
    ADD("zoom.real", "%Re", zoom->centerx);
    ADD("zoom.imag", "%Re", zoom->centery);
    ADD("zoom.radius", "%Re", zoom->radius);
    ADD("zoom.frame", "%d", zoom->frame);
    ADD("zoom.frames", "%d", zoom->zoom_frames);
  }
  metadata_update(meta, "tiling", tiling ? "yes" : "no");
  if (tiling) {
    ADD("tiling.real", "%Re", tiling->tiled_centerx);
    ADD("tiling.imag", "%Re", tiling->tiled_centery);
    ADD("tiling.radius", "%Re", tiling->tiled_radius);
    ADD("tiling.cols", "%d", tiling->tiled_cols);
    ADD("tiling.rows", "%d", tiling->tiled_rows);
    ADD("tiling.col", "%d", tiling->col);
    ADD("tiling.row", "%d", tiling->row);
  }
  double iterations = 0, exterior = 0, interior = 0, glitch = 0;
  enum result_t result = image_result(render_options->width, render_options->height, &iterations, &exterior, &interior, &glitch);
  ADD("stats.result", "%s", result_name[result]);
  ADD("stats.maxiters", "%f", iterations);
  ADD("stats.exterior", "%f", exterior);
  ADD("stats.interior", "%f", interior);
  ADD("stats.glitch", "%f", glitch);
  int n = mpfr_snprintf(0, 0, "%smightymandel %Re + %Re i @ %Re\n", prefix, render_options->centerx, render_options->centery, render_options->radius);
  int m = metadata_strlen(meta, prefix);
  s = malloc(n + m + 1);
  mpfr_snprintf(s, n + m + 1, "%smightymandel %Re + %Re i @ %Re\n", prefix, render_options->centerx, render_options->centery, render_options->radius);
  metadata_string(meta, prefix, s + n, m + 1);
  metadata_delete(meta);
  return s;
#undef ADD
}

// main program entrypoint
int main(int argc, char **argv) {
  log_target = stderr;
  srand(time(0));

  // set default options
  struct options options;
  memset(&options, 0, sizeof(struct options));
  options.width = 1280;
  options.height = 720;
  options.win_width = 1280;
  options.win_height = 720;
  options.render_de = true;
  options.series_approx = true;
  options.order = 0; // old method
  options.log_level = LOG_NOTICE;
  options.max_glitch = 0.02;
  options.max_blob = 1;
  options.sharpness = 0.01;

  // update options from arguments
  if (! parse_command_line(&options, argc, argv)) {
    log_message(LOG_FATAL, "couldn't parse command line\n");
    return 1;
  }
  int sanity = options.interactive + options.oneshot + (options.tile || options.zoom);
  if (sanity > 1) {
    log_message(LOG_FATAL, "incompatible argument combination\n");
    log_message(LOG_FATAL, "at most one of --interactive --one-shot (--tile || -zoom) can be used\n");
    return 1;
  } else if (sanity == 0) {
    options.interactive = true;
  }
  if (options.tile && ! options.render_de) {
    log_message(LOG_WARN, "tiling with --no-de is prone to seams at tile boundaries\n");
  }
  if (options.tile && options.render_de && ! options.show_glitches) {
    log_message(LOG_WARN, "tiling with --no-glitch is prone to seams at tile boundaries\n");
  }
  if (options.size && ! options.geometry) {
    options.win_width = options.width;
    options.win_height = options.height;
  }
  if (! options.size && options.geometry) {
    options.width = options.win_width;
    options.height = options.win_height;
  }
  if (options.width != (options.width >> options.slice) << options.slice) {
    log_message(LOG_FATAL, "width is not a multiple of slice factor %d\n", 1 << options.slice);
    return 1;
  }
  if (options.height != (options.height >> options.slice) << options.slice) {
    log_message(LOG_FATAL, "height is not a multiple of slice factor %d\n", 1 << options.slice);
    return 1;
  }
  switch (options.order) {
    case  0:
      break;
    case  4:
    case  6:
    case  8:
    case 12:
    case 16:
    case 24:
    case 32:
    case 48:
    case 64:
      if (options.render_de) {
        log_message(LOG_FATAL, "new series approximation doesn't support distance estimation\n");
        return 1;
      }
      break;
    default:
      log_message(LOG_FATAL, "invalid series approximation order\n");
      return 1;
  }

  // transfer command line options to renderer options
  struct render_options render_options;
  render_options_init(&render_options);
  render_options.show_glitches = options.show_glitches;
  render_options.calculate_de = options.render_de;
  render_options.show_de = options.render_de;
  render_options.series_approx = options.series_approx;
  render_options.order = options.order;
  render_options.weight = options.weight;
  render_options.max_glitch = options.max_glitch;
  render_options.max_blob = options.max_blob;
  render_options.sharpness = options.sharpness;
  render_options.weight = options.weight;
  render_options.width = options.width;
  render_options.height = options.height;
  render_options.win_width = options.win_width;
  render_options.win_height = options.win_height;
  render_options.slice = options.slice;
  if (options.view_supplied) {
    render_options_set_location(&render_options, options.centerx, options.centery, options.radius);
  } else {
    render_options.method = render_method_fp32;
    mpfr_set_d(render_options.centerx, -0.75, MPFR_RNDN);
    mpfr_set_d(render_options.centery, 0, MPFR_RNDN);
    mpfr_set_d(render_options.radius, 2, MPFR_RNDN);
  }
  render_options.filename = options.file_to_load;

  // transfer options to legacy global state
  DE = options.render_de;

  // print startup messages
  if (options.version) {
    log_target = stdout;
    print_version();
    return 0;
  }
  if (options.help) {
    log_target = stdout;
    print_usage(argv[0]);
    return 0;
  }
  log_level = options.log_level;
  print_banner();

  // load parameter file from the command line argument if any
  if (options.file_to_load) {
    mpfr_t cx, cy, cz;
    mpfr_inits2(53, cx, cy, cz, (mpfr_ptr) 0);
    if (! load_parameter_file(options.file_to_load, cx, cy, cz)) {
      log_message(LOG_FATAL, "failed to load parameters: %s\n", options.file_to_load);
      log_result(options.file_to_load, result_parse, render_method_fp32, 0, 0, 0, 0, 0);
      return 1;
    }
    render_options_set_location(&render_options, cx, cy, cz);
    mpfr_clears(cx, cy, cz, (mpfr_ptr) 0);
  }

  // create OpenGL window context
  if (! glfwInit()) {
    log_message(LOG_FATAL, "couldn't initialize glfw\n");
    return 1;
  }
  int major, minor;
  GLFWwindow *    window = create_window(LOG_WARN,  major = 4, minor = 1, options.win_width, options.win_height);
  if (! window) { window = create_window(LOG_ERROR, major = 3, minor = 3, options.win_width, options.win_height); }
  FP64 = major >= 4;
  if (! window) {
    log_message(LOG_FATAL, "couldn't create window\n");
    return 1;
  }
  if (! FP64) {
    log_message(LOG_WARN, "no FP64 support available, zoom depth will be limited\n");
  }
  glfwMakeContextCurrent(window);
  glewExperimental = GL_TRUE;
  glewInit();
  glGetError(); // discard common error from glew

  // check VRAM is enough, part 1...
  int vram = vram_available();

  // set up renderer
  struct render render;
  render.begun = false;
  render_begin(&render);
  GLsizei bytes_allocated = 0;
  render_reshape(&render, options.width, options.height, options.slice, &bytes_allocated);

  // check VRAM is enough, part 2...
  int kb_allocated = (bytes_allocated + 1023) / 1024;
  if (vram > 0) {
    if (vram < kb_allocated) {
      log_message(LOG_WARN, "not enough video memory available, performance may suffer\n");
      log_message(LOG_WARN, "increasing --slice could reduce video memory consumption\n");
      log_message(LOG_WARN, "current --slice value: %d\n", options.slice);
    }
    log_message(LOG_INFO, "available video memory: %10d kB\n", vram);
  } else {
    log_message(LOG_WARN, "available video memory: unknown\n");
  }
  log_message(LOG_INFO, "video memory we alloc:  %10d kB\n", kb_allocated);

  // set up interaction
  // interact uses internal global state because
  // glfw3 doesn't provide a user pointer that it passes to callbacks
  if (options.interactive) {
    log_message(LOG_INFO, "interactive begin\n");
    glfwSetKeyCallback(window, key_press_handler);
    glfwSetMouseButtonCallback(window, button_handler);
    glfwSetScrollCallback(window, scroll_handler);
    glfwSetCursorPosCallback(window, motion_handler);
    interact_begin(options.win_width, options.win_height, render_options.centerx, render_options.centery, render_options.radius, render_options.weight, render_options.show_glitches);
  }

  // set up zooming
  struct zoom zoom;
  if (options.zoom) {
    log_message(LOG_INFO, "zoom begin\n");
    zoom_begin(&zoom, options.zoom_frames, render_options.centerx, render_options.centery);
    zoom_next(&zoom);
    render_options_set_location(&render_options, zoom.centerx, zoom.centery, zoom.radius);
  }

  // set up tiling (do this after zoom so that zoom and tiling can be combined)
  struct tiling tiling;
  if (options.tile) {
    log_message(LOG_INFO, "tile begin\n");
    tiling_begin(&tiling, options.tile_width, options.tile_height, render_options.width, render_options.height, render_options.centerx, render_options.centery, render_options.radius);
    if (tiling_next(&tiling)) {
      render_options_set_location(&render_options, tiling.centerx, tiling.centery, tiling.radius);
    } else {
      glfwSetWindowShouldClose(window, GL_TRUE);
    }
  }

  // set up one-shot
  if (options.oneshot) {
    log_message(LOG_INFO, "one-shot begin\n");
  }

  // set up recording
  struct filename filename;
  filename_begin(&filename);
  struct record record;
  record_begin(&record);

  // main loop
  if (options.overhead) {
    // nothing to do
  } else if (options.interactive) {
    struct poll *poll = poll_new(window, options.interactive, &render_options, 0.01, 0.1, &filename, &record);
    // state for location printing
    struct render_options last_render_options;
    render_options_init(&last_render_options);
    render_options_copy(&last_render_options, &render_options);
    bool first = true;
    // loop until quit
    while (! glfwWindowShouldClose(window)) {
      // print location info if changed
      if (first || ! mpfr_equal_p(last_render_options.centerx, render_options.centerx)) {
        log_message(LOG_NOTICE, "view.real %Re\n", render_options.centerx);
      }
      if (first || ! mpfr_equal_p(last_render_options.centery, render_options.centery)) {
        log_message(LOG_NOTICE, "view.imag %Re\n", render_options.centery);
      }
      if (first || ! mpfr_equal_p(last_render_options.radius, render_options.radius)) {
        log_message(LOG_NOTICE, "view.radius %Re\n", render_options.radius);
      }
      if (first || last_render_options.method != render_options.method) {
        log_message(LOG_NOTICE, "calc.fp %s\n", render_options.method == render_method_fp32 ? "fp32" : "fp64");
        log_message(LOG_NOTICE, "calc.perturb %s\n", render_options.method == render_method_fpxx ? "yes" : "no");
      }
      first = false;
      render_options_copy(&last_render_options, &render_options);
      // render
      enum render_result result = render_do(&render, &render_options, poll);
      // idle when completed
      if (! glfwWindowShouldClose(window) && result == render_complete) {
        while (poll_continue == poll_ui(poll, true)) {
          // nothing to do
        }
      }
    }
    poll_delete(poll);
  } else {
    struct poll *poll = poll_new(window, options.interactive, &render_options, 0.1, 1.0, &filename, &record);
    int frames = options.zoom ? options.zoom_frames : 1;
    int cols = options.tile ? options.tile_width : 1;
    int rows = options.tile ? options.tile_height : 1;
    double timeout = options.timeout > 0.0 ? options.timeout : 1.0 / 0.0;
    struct stopwatch *tile_time = stopwatch_new();
    for (int frame = 0; ! glfwWindowShouldClose(window) && frame < frames; ++frame) {
      if (options.zoom) {
        log_message(LOG_NOTICE, "zoom %4d/%d\n", frame + 1, frames);
      }
      for (int row = 0; ! glfwWindowShouldClose(window) && row < rows; ++row) {
        for (int col = 0; ! glfwWindowShouldClose(window) && col < cols; ++col) {
          if (options.tile) {
            log_message(LOG_NOTICE, "tile %4d/%d\n", col + row * cols + 1, rows * cols);
          }
          stopwatch_reset(tile_time);
          poll_set_timeout(poll, timeout);
          enum render_result result = render_do(&render, &render_options, poll);
          double tile_took = stopwatch_elapsed(tile_time);
          timeout = fmax(timeout, 1.1 * tile_took); // FIXME hardcoded factor
          if (! glfwWindowShouldClose(window) && result == render_complete) {
            char *comment = collect_metadata_to_string("# ", &render_options, options.tile ? &tiling : 0, options.zoom ? &zoom : 0, render_options.method == render_method_fpxx ? render.pass + 1 : 0);
            char *name = filename_name(&filename, "ppm", frame, col, row);
            record_do(&record, name, render_options.width, render_options.height, comment);
            free(name);
            free(comment);
            if (options.tile) {
              tiling_next(&tiling);
              render_options_set_location(&render_options, tiling.centerx, tiling.centery, tiling.radius);
            }
          }
        }
      }
      if (! glfwWindowShouldClose(window) && options.zoom) {
        zoom_next(&zoom);
        render_options_set_location(&render_options, zoom.centerx, zoom.centery, zoom.radius);
      }
      if (! glfwWindowShouldClose(window) && options.tile) {
        tiling_end(&tiling);
        tiling_begin(&tiling, options.tile_width, options.tile_height, render_options.width, render_options.height, render_options.centerx, render_options.centery, render_options.radius);
        tiling_next(&tiling);
        render_options_set_location(&render_options, tiling.centerx, tiling.centery, tiling.radius);
      }
    }
  }

  // cleanup and exit
  if (options.interactive) {
    interact_end();
  }
  if (options.tile) {
    tiling_end(&tiling);
  }
  if (options.zoom) {
    zoom_end(&zoom);
  }
  render_end(&render);
  record_end(&record);
  glfwTerminate();
  return 0;
}

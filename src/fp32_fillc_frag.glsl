// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

uniform sampler2D ida0;
smooth in vec2 s;
smooth in vec2 t;
layout(location = 0, index = 0) out vec4 o;
void main() {
  vec4 ida = texture(ida0, s);
  o = vec4(t, ida.x, ida.y);
}

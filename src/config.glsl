// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#define FP32_STEP_ITERS 512
#define FP64_STEP_ITERS 1024
#define FPXX_STEP_ITERS 256

#define SLICE_MAX 8

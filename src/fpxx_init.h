// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FPXX_INIT_H
#define FPXX_INIT_H 1

#include <GL/glew.h>
#include <mpfr.h>

struct fpxx_init {
  GLuint program;
  GLint radius;
  GLint center;
  GLint aspect;
  GLint pass;
  GLint c;
  GLuint vao;
};

void fpxx_init_begin(struct fpxx_init *s);
void fpxx_init_end(struct fpxx_init *s);
void fpxx_init_do(struct fpxx_init *s, GLuint *active_count, GLuint *vbo, GLuint query, int pass, int width, int height, const mpfr_t centerx, const mpfr_t centery, const mpfr_t radius, const mpfr_t refx, const mpfr_t refy);

#endif

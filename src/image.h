// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef IMAGE_H
#define IMAGE_H 1

/*!
\file  image.h
\brief Image related functions.
*/

#include "logging.h"

/*!
\brief A pixel storage structure.
*/
struct pixel {
  float e; //!< Error count.
  float n; //!< Iteration count.
  int i;   //!< Pixel x coordinate.
  int j;   //!< Pixel y coordinate.
};

int cmp_pixel(const void *a, const void *b);
float *image_get_glitched(int width, int height);
float *image_get_iterations(int width, int height);
unsigned char *image_erode(const unsigned char *data, int width, int height);
struct pixel *image_find_peak_glitches(const float *glitched, const float *iterations, int width, int height, int *count);

enum result_t image_result(int width, int height, double *iterations, double *exterior, double *interior, double *glitch);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

/*!
\file shader.c
\brief Compile and link OpenGL shader programs.
*/

#include <stdlib.h>

#include "shader.h"
#include "logging.h"
#include "mightymandel.h"

extern const char *fp32_preamble; //!< fp32_preamble.glsl
extern const char *fp64_preamble; //!< fp64_preamble.glsl
extern const char *fp32_complex;  //!< fp32_complex.glsl
extern const char *fp64_complex;  //!< fp64_complex.glsl
extern const char *config;        //!< config.glsl

/*!
\brief Check program link status and output shader compile logs.

\param program The program to debug.
\param name The name of the progam for log messages.
*/
void debug_program(GLuint program, const char *name) {
  if (program) {
    GLint linked = GL_FALSE;
    glGetProgramiv(program, GL_LINK_STATUS, &linked);D;
    if (linked != GL_TRUE) {
      log_message(LOG_ERROR, "%s: OpenGL shader program link failed\n", name);
    }
    GLint length = 0;
    glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);D;
    char *buffer = (char *) malloc(length + 1);
    glGetProgramInfoLog(program, length, NULL, buffer);D;
    buffer[length] = 0;
    if (buffer[0]) {
      log_message(LOG_WARN, "%s: OpenGL shader program info log\n", name);
      if (LOG_WARN <= log_level) {
        fprintf(log_target, "%s\n", buffer);
      }
    }
    free(buffer);
  } else {
    log_message(LOG_ERROR, "%s: OpenGL shader program creation failed\n", name);
  }
}

/*!
\brief Check shader status and output shader compile logs.

\param shader The shader to debug.
\param type The shader type.
\param name The name of the progam for log messages.
*/
void debug_shader(GLuint shader, GLenum type, const char *name) {
  const char *tname = 0;
  switch (type) {
    case GL_VERTEX_SHADER:   tname = "vertex";   break;
    case GL_GEOMETRY_SHADER: tname = "geometry"; break;
    case GL_FRAGMENT_SHADER: tname = "fragment"; break;
    default:                 tname = "unknown";  break;
  }
  if (shader) {
    GLint compiled = GL_FALSE;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);D;
    if (compiled != GL_TRUE) {
      log_message(LOG_ERROR, "%s: OpenGL %s shader compile failed\n", name, tname);
    }
    GLint length = 0;
    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);D;
    char *buffer = (char *) malloc(length + 1);
    glGetShaderInfoLog(shader, length, NULL, buffer);D;
    buffer[length] = 0;
    if (buffer[0]) {
      log_message(LOG_WARN, "%s: OpenGL %s shader info log\n", name, tname);
      if (LOG_WARN <= log_level) {
        fprintf(log_target, "%s\n", buffer);
      }
    }
    free(buffer);
  } else {
    log_message(LOG_ERROR, "%s: OpenGL %s shader creation failed\n", name, tname);
  }
}

/*!
\brief Compile a shader.

Automatically adds extra sources (preamble, complex number functions,
define for DE).

\param program The program to link the shader to.
\param type The type of shader.
\param source The glsl source code.
*/
void compile_shader(GLint program, GLenum type, const char *name, const GLchar *source) {
  const GLchar *fp32_sources[5] = { fp32_preamble, config, DE ? "#define DE 1\n" : "", fp32_complex,               source };
  const GLchar *fp64_sources[6] = { fp64_preamble, config, DE ? "#define DE 1\n" : "", fp32_complex, fp64_complex, source };
  GLuint shader = glCreateShader(type);D;
  glShaderSource(shader, FP64 ? 6 : 5, FP64 ? fp64_sources : fp32_sources, NULL);D;
  glCompileShader(shader);D;
  debug_shader(shader, type, name);
  glAttachShader(program, shader);D;
  glDeleteShader(shader);D;
}

/*!
\brief Compile a program with transform feedback.

\param name The name of the program for log messages.
\param vert The vertex shader source, or null.
\param geom The geometry shader source, or null.
\param frag The fragment shader source, or null.
\param nvaryings The number of varyings to capture in transform feedback, or 0.
\param varyings The names of the varyings to capture in transform feedback, or 0.
\return The compiled program.
*/
GLint compile_program_tf(const char *name, const GLchar *vert, const GLchar *geom, const GLchar *frag, int nvaryings, const GLchar **varyings) {
  GLint program = glCreateProgram();D;
  if (vert) { compile_shader(program, GL_VERTEX_SHADER  , name, vert); }
  if (geom) { compile_shader(program, GL_GEOMETRY_SHADER, name, geom); }
  if (frag) { compile_shader(program, GL_FRAGMENT_SHADER, name, frag); }
  if (nvaryings) {
    glTransformFeedbackVaryings(program, nvaryings, varyings, GL_INTERLEAVED_ATTRIBS);D;
  }
  glLinkProgram(program);D;
  debug_program(program, name);
  return program;
}

/*!
\brief Compile a program without transform feedback.

\param name The name of the program for log messages.
\param vert The vertex shader source, or null.
\param geom The geometry shader source, or null.
\param frag The fragment shader source, or null.
\return The compiled program.
*/
GLint compile_program(const char *name, const GLchar *vert, const GLchar *geom, const GLchar *frag) {
  return compile_program_tf(name, vert, geom, frag, 0, 0);
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <assert.h>
#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>

#include "parse.h"
#include "logging.h"

#include "parse_gif.h"
#include "parse_kfr.h"
#include "parse_mdz_center.h"
#include "parse_mdz_corners.h"
#include "parse_mm.h"
#include "parse_png.h"
#include "parse_ppar_center.h"
#include "parse_ppar_corners.h"
#include "parse_ppm.h"
#include "parse_sft.h"

bool radius_is_valid(const mpfr_t radius) {
  return 0 < mpfr_sgn(radius) && mpfr_regular_p(radius);
}

mpfr_prec_t precision_for_radius(const mpfr_t radius) {
  assert(radius_is_valid(radius));
  mpfr_t t;
  mpfr_init2(t, 53);
  mpfr_log2(t, radius, MPFR_RNDN);
  mpfr_prec_t p = ceil(fmax(53, 16 - mpfr_get_d(t, MPFR_RNDN)));
  mpfr_clear(t);
  return p;
}

char *load_file(const char *filename, int *length) {
  assert(filename);
  char *b = 0;
  FILE *f = fopen(filename, "rb");
  if (f) {
    // calculate the size of the file
    fseek(f, 0, SEEK_END);
    *length = ftell(f);
    fseek(f, 0, SEEK_SET);
    // allocate a buffer with space for an additional null terminator character
    b = (char *) malloc(*length + 1);
    if (b) {
      if (1 != fread(b, *length, 1, f)) {
        free(b);
        fclose(f);
        return 0;
      }
      // add null terminator
      b[*length] = 0;
    }
    fclose(f);
  }
  return b;
}

char *parse_separator(char **source, int separator) {
  if (! source) { return 0; }
  if (! *source) { return 0; }
  char *delim = strchr(*source, separator);
  char *result = *source;
  if (delim) {
    *delim = 0;
    *source = delim + 1;
  } else {
    *source = 0;
  }
  return result;
}

char *parse_line(char **source) {
  char *result = parse_separator(source, '\n');
  char *cr = strrchr(result, '\r');
  if (cr) {
    *cr = 0;
  }
  // strip UTF-8 byte order marker if present
  if (0 == strncmp("\xEF\xBB\xBF", result, 3)) {
    result += 3;
  }
  return result;
}

bool parse(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
#define P(p) if (parse_##p(source, length, cx, cy, cz)) return true
  // parse binary data first in case of mishaps
  P(ppm);
  P(png);
  P(gif);
  // now parse text formats
  P(mm);
  P(ppar_corners);
  P(ppar_center);
  P(kfr);
  P(sft);
  P(mdz_corners);
  P(mdz_center);
  return false;
#undef P
}

bool load_parameter_file(const char *filename, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
  int length = 0;
  char *b = load_file(filename, &length);
  if (! b) {
    log_message(LOG_ERROR, "failed to load file: %s\n", filename);
    return false;
  }
  if (parse(b, length, cx, cy, cz)) {
    log_message(LOG_NOTICE, "loaded parameter file: %s\n", filename);
    free(b);
    return true;
  } else {
    log_message(LOG_ERROR, "failed to parse file: %s\n", filename);
    free(b);
    return false;
  }
}

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpfr.h>

#include "parse.h"
#include "parse_mdz_center.h"

bool parse_mdz_center(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz) {
  (void) length;
  assert(source);
  char *source2 = strdup(source);
  assert(source2);
  char *s = source2;
  char *sa = 0;
  char *sx = 0;
  char *sy = 0;
  char *sz = 0;
  while (s) {
    char *line = parse_line(&s);
    if (0 == strncmp(line, "aspect ", 7)) { sa = line + 7; }
    if (0 == strncmp(line, "cx ",     3)) { sx = line + 3; }
    if (0 == strncmp(line, "cy ",     3)) { sy = line + 3; }
    if (0 == strncmp(line, "size ",   5)) { sz = line + 5; }
  }
  if (sa && sx && sy && sz) {
    double a = atof(sa);
    mpfr_set_prec(cz, 53);
    mpfr_set_str(cz, sz, 10, MPFR_RNDN);
    mpfr_div_d(cz, cz, 2 * a, MPFR_RNDN);
    mpfr_abs(cz, cz, MPFR_RNDN);
    if (! radius_is_valid(cz)) {
      free(source2);
      return false;
    }
    mpfr_prec_t p = precision_for_radius(cz);
    mpfr_set_prec(cx, p);
    mpfr_set_prec(cy, p);
    mpfr_set_str(cx, sx, 10, MPFR_RNDN);
    mpfr_set_str(cy, sy, 10, MPFR_RNDN);
    free(source2);
    return true;
  } else {
    free(source2);
    return false;
  }
}

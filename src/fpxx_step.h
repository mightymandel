// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef FPXX_STEP_H
#define FPXX_STEP_H 1

#include <GL/glew.h>
#include <mpfr.h>

#include "mightymandel.h"

struct fpxx_step_orbit {
  struct fpxx_step_orbit *next;
  GLuint buffer[2][FPXX_STEP_ITERS][4];
};

struct fpxx_step {
  GLuint program;
  GLint er2;
  GLint escaped;
  GLint cne0;
  GLint zdz0;
  GLint err0;
  GLint zdz0s;
  GLuint zdz0t;
  struct fpxx_step_orbit *orbit;
  struct fpxx_step_orbit *current;
  mpfr_t cx, cy, zx, zy, dzx, dzy, dzx0, dzy0, t1, t2, t3, t4;
  GLuint vao;
};

void fpxx_step_begin(struct fpxx_step *s);
void fpxx_step_end(struct fpxx_step *s);
void fpxx_step_start(struct fpxx_step *s, GLuint vbo, double escaperadius2, const mpfr_t dzx0, const mpfr_t dzy0, const mpfr_t zx, const mpfr_t zy, const mpfr_t dzx, const mpfr_t dzy, const mpfr_t refx, const mpfr_t refy, bool initial_slice);
void fpxx_step_do(struct fpxx_step *s, GLuint *active_count, GLuint vbo, GLuint query);

#endif

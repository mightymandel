// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

layout(points) in;
layout(points, max_vertices = 1) out;
uniform double loger2;
uniform dvec2 center;
uniform double radius;
uniform double aspect;
in  dvec4 cne1[1];
#ifdef DE
in  dvec4 zdz1[1];
#else
in  dvec2 zdz1[1];
#endif
in  dvec2 err1[1];
flat out vec4 ida1;
void main() {
  bool escaped = ! (cne1[0].w <= 0.0);
  if (escaped) {
    double z2 = cmag2(zdz1[0].xy);
    float logz2 = log(float(z2));
    float i = float(1.0 + cne1[0].z - clamp(double(log2(float(logz2 / loger2))), 0.00001, 0.99999));
#ifdef DE
    double dz2 = cmag2(zdz1[0].zw);
    float d = float(logz2 * sqrt(z2 / dz2));
#else
    float d = 0.0;
#endif
    float e = err1[0].y > 0.0 ? float(err1[0].x + err1[0].y) : 0.0;
    ida1 = vec4(i, e, d, 0.0);
    dvec2 pos = (cne1[0].xy - center) / radius * dvec2(aspect, 1.0);
    gl_Position = vec4(pos, 0.0, 1.0);
    EmitVertex();
    EndPrimitive();
  }
}

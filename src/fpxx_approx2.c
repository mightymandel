// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "fpxx_approx2.h"
#include "shader.h"
#include "logging.h"
#include "utility.h"
#include "mightymandel.h"
#include "texture.h"

void fpxx_approx2_begin(struct fpxx_approx2 *s) {
  (void) s;
}

void fpxx_approx2_end(struct fpxx_approx2 *s) {
  if (s->series) {
    z2c_series_delete(s->series);
  }
  if (s->approx) {
    z2c_approx_delete(s->approx);
  }
  if (s->reference) {
    z2c_reference_delete(s->reference);
  }
}

void fpxx_approx2_do(struct fpxx_approx2 *s, GLuint *active_count, GLuint *vbo, mpfr_t zx, mpfr_t zy, int order, int pass, const mpfr_t radius, const mpfr_t refx, const mpfr_t refy, bool initial_slice, void *abort_data, abort_t abort_fn) {
  if (initial_slice) {
    s->too_deep = false;
    int exponent = 4 + mpfr_get_exp(radius);
    if (s->series) {
      z2c_series_delete(s->series);
    }
    s->series = z2c_series_new(order, refx, refy);
    while (z2c_series_step(s->series, exponent, 53/*threshold*/)) {
      if (abort_fn(abort_data)) {
        return;
      }
    }
    s->n = z2c_series_get_n(s->series);
    log_message(LOG_INFO, "approx2 skip: %d\n", s->n);
    if (s->approx) {
      z2c_approx_delete(s->approx);
    }
    s->approx = z2c_series_approx_new(s->series, exponent);
    if (s->reference) {
      z2c_reference_delete(s->reference);
    }
    s->reference = z2c_series_reference_new(s->series);
  }
  if (! (*active_count > 0)) {
    return;
  }
  z2c_reference_get_z(s->reference, zx, zy);

  debug_message("approx2 CPU\n");
  debug_message("VBO approx2: %d -> %d\n", vbo[1], vbo[0]);
  glBindBuffer(GL_ARRAY_BUFFER, vbo[1]);D;
  glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, vbo[0]);D;
  const double *in = glMapBufferRange(GL_ARRAY_BUFFER, 0, *active_count * (DE ? 10 : 8) * sizeof(GLdouble), GL_MAP_READ_BIT);D;
  bool ok;
  if (in) {
    if (pass == 0 && ! (in[0] != 0) && ! (in[1] != 0)) {
      log_message(LOG_WARN, "approx2 delta(z)_0 == 0, rendering will probably fail\n");
      s->too_deep = true;
    }
    double *out = glMapBufferRange(GL_TRANSFORM_FEEDBACK_BUFFER, 0, *active_count * (DE ? 10 : 8) * sizeof(GLdouble), GL_MAP_WRITE_BIT | GL_MAP_INVALIDATE_BUFFER_BIT);D;
    if (out) {
      #pragma omp parallel for
      for (unsigned int k = 0; k < *active_count; ++k) {
        const double *q = in + k * (DE ? 10 : 8);
        double *p = out + k * (DE ? 10 : 8);
        p[0] = q[0];
        p[1] = q[1];
        p[2] = s->n;
        p[3] = 0; // escaped flag
        complex double z = z2c_approx_do(s->approx, q[0] + I * q[1]);
        p[4] = creal(z);
        p[5] = cimag(z);
        if (DE) {
          // FIXME should never happen
          p[6] = 0;
          p[7] = 0;
          p[8] = q[8]; // error flag
          p[9] = q[9];
        } else {
          p[6] = q[6]; // error flag
          p[7] = q[7];
        }
      }
      if (DE) {
        debug_message("approx2 out: c %e %e z %e %e dz %e %e\n", out[0], out[1], out[4], out[5], out[6], out[7]);
      } else {
        debug_message("approx2 out: c %e %e z %e %e\n", out[0], out[1], out[4], out[5]);
      }
      out = 0;
      ok = glUnmapBuffer(GL_TRANSFORM_FEEDBACK_BUFFER);D;
      glBindBufferBase(GL_TRANSFORM_FEEDBACK_BUFFER, 0, 0);D;
    } else {
      ok = false;
    }
    in = 0;
    ok &= glUnmapBuffer(GL_ARRAY_BUFFER);D;
    glBindBuffer(GL_ARRAY_BUFFER, 0);D;
  } else {
    ok = false;
  }
  if (! ok) {
    log_message(LOG_ERROR, "CPU-based series approximation initialisation failed\n");
  }

}

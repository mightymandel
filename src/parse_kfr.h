// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef PARSE_KFR_H
#define PARSE_KFR_H 1

#include <stdbool.h>
#include <mpfr.h>

/*!
\brief Parse Kalles Fraktaler `.kfr` file format.

File format example:

    Re: -0.75
    Im: 0.0
    Zoom: 1E0

Line endings are typically CRLF as Kalles Fraktaler is a Windows program.  The
zoom field \f$z\f$ contains a number that gets larger as you zoom in, but it is
easy enough to convert to a radius \f$r\f$: \f$r = \frac{2}{z}\f$.  Also,
Kalles Fraktaler displays upside down.  Rendered images look like they do in
Kalles Fraktaler when the imaginary coordinate is inverted, so we do that too.

\copydetails parser
*/
bool parse_kfr(const char *source, int length, mpfr_t cx, mpfr_t cy, mpfr_t cz);

#endif

// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#ifndef MIGHTYMANDEL_H
#define MIGHTYMANDEL_H 1

/*!
\file  mightymandel.h
\brief Global constants and OpenGL debugging.
*/

#include <stdbool.h>
#include <GL/glew.h>

#include "config.glsl"

/*!
\brief Counts how many OpenGL errors have been printed, for flood control.

`opengl_error_printout_count` is a global variable that should not be modified
anywhere apart from `main()` at startup, and the `D` macro.
*/
extern int opengl_error_printout_count;

/*!
\brief A macro that prints out any OpenGL errors that occur, with source
location.

Does nothing when `MIGHTYMANDEL_DEBUG` is not defined.

Important note: if a header file included later uses `D`, expect chaos.  So
include this file as late as possible (certainly after headers from outside
the mightymandel source code).
*/
#ifdef MIGHTYMANDEL_DEBUG

#define D do{ int e = glGetError(); if (e != 0) { opengl_error_printout_count++; if (opengl_error_printout_count == 50) { \
    log_message(LOG_WARN, "too many OpenGL errors, not printing any more\n"); \
  } else if (opengl_error_printout_count < 50) { \
    log_message(LOG_WARN, "OpenGL error %d in %s() (%s:%d)\n", e, __FUNCTION__, __FILE__, __LINE__); \
  } \
} }while(0)

#else

#define D do{ }while(0)

#endif

/*!
\brief True if 64bit double precision floating point is available on GPU.

`FP64` is a global variable that should not be modified anywhere apart from
`main()` during startup (ie, before initializing shaders, or anything else that
needs it).
*/
extern bool FP64;

/*!
\brief True if rendering should use distance estimates.

`DE` is a global variable that should not be modified anywhere apart from
`main()` during startup (ie, before initializing shaders, or anything else that
needs it).
*/
extern bool DE;

/*!
\def FP32_STEP_ITERS
\brief Number of iterations in each GPU pass (for single precision calculations).

Should be a power of two.  Higher values should increase throughput, reducing
total calculation times (except for low iteration count regions).  Setting it
too high may make the graphical user interface (or even the whole system) have
a laggy response, setting it far too high might even crash the driver.  See the
BENCHMARKS file for a performance comparison and additional notes.
*/

/*!
\def FP64_STEP_ITERS
\brief Number of iterations in each GPU pass (for double precision calculations).

\copydetails FP32_STEP_ITERS
*/

/*!
\def FPXX_STEP_ITERS
\brief Number of iterations in each GPU pass (for double precision calculations
with perturbation).

\copydetails FP32_STEP_ITERS
*/

/*!
\brief The type of rendering calculations to use.
*/
enum render_method_t {
  render_method_fp32 = 0, //!< Plain single precision floating point.
  render_method_fp64 = 1, //!< Plain double precision floating point.
  render_method_fpxx = 2  //!< Perturbed double precision floating point.
};

#endif

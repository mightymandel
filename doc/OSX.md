OS X {#OSX}
====

Compiling on OS X is possible, once the dependencies are installed.


\section osxbuild Building mightymandel on OS X

To build mightymandel on OS X:

    cd ~/code/mightymandel # or where you downloaded mightymandel
    cd src
    make SYSTEM=osx clean
    make SYSTEM=osx

If all went well, you should be able to run it:

    ./mightymandel


\section osxbugs Bugs in mightymandel on OS X

- dual-GPU machines (mainly laptops?) need to have the most powerful GPU
  activated, otherwise there is a risk of a *complete system crash* (system log
  filled with GPU reset errors)
- sometimes shaders fail to compile with minimal information posted to stderr
- window screen size might not match window framebuffer size, and weird things
  might happen when dragging window between monitors with different DPI settings
  <http://www.glfw.org/faq.html#why-is-my-output-in-the-lower-left-corner-of-the-window>
  <http://www.glfw.org/docs/latest/window.html#window_fbsize>

CHANGES {#CHANGES}
=======

- \ref changesnext
- \ref changesv16
- \ref changesv15
- \ref changesv14
- \ref changesv13
- \ref changesv12
- \ref changesolder

\section changesnext not released yet

Rendering improvements:

* Higher-order series approximations:
  * Parallelized code generated at compile-time.
  * `examples/kfr/Xxxxxx.kfr` with `--order 12` is 5x faster.
  * `examples/kfr/tick-tock.kfr` with `--order 24` is 3x faster.
  * Accuracy determination based on algorithm described by Botond Kósa:
    <http://www.fractalforums.com/mandel-machine/mandel-machine/msg74200/#msg74200>
  * Example of the speed trade-offs involved by Botond Kósa:
    <http://www.fractalforums.com/mandel-machine/mandel-machine/msg74201/#msg74201>
* Distance estimator works at deeper zooms (but needs `--no-approx`).

Build system improvements:

* Makefiles changed from `make -f Makefile.system` to `make SYSTEM=system`.
* Valid `system` include `linux` (default), `win32`, `win64`, `osx`.
* Use `make GLFW=--static` if you compiled a static glfw3.


\section changesv16 mightymandel v16 stable release

User interface improvements:

* new parser for pre-processed FractInt .ppar center-mag variant
* new parser for FractInt .gif embedded metadata
* new parser for mightymandel .png embedded metadata
* `--interactive` mode waits for events instead of polling when rendering is
  finished, to reduce idle CPU load
* `extra/zoom` assembler has an optional frames-per-second argument (default 25)
* much more metadata is saved in PPM files and preserved by `extra/ppm2png.sh`
* available video memory and an estimate of the amount mightymandel allocated
  is reported on startup with `--verbose info`
* decoupled window size `--geometry` from image size `--size`
  * render HD zoom image sequence with a small window while you do other stuff
  * have HD window with a small image to speed up calculations while exploring
* new `--timeout` flag (0 for infinite, >0 for that many seconds, default 0)
  sets initial timeout
  * useful for tiled rendering where some tiles may be 100% interior
  * also used by the test suite to save output images while still timing out
* new `--max-blob` flag sets maximum size (in pixels) of glitched blobs to
  ignore (default 1, set to 0 if you want previous behaviour)

Rendering improvements:

* `--slice` option (range from 0 to 8, default 0) splits calculation into
  batches
  * allows rendering larger images in the same amount of video memory without
    the potential seams from tiling
  * provides progressive display with lofi image appearing quickly, gradually
    increasing in quality to final hifi image
  * number of iterates with `--slice S` is number of pixels / 4^S
  * per-iterate video memory usage is 160 bytes (--de) and 128 bytes (--no-de)
* per-pixel video memory usage reduced from 20 bytes to 16 bytes (--de) and
  12 bytes (--no-de)
* interior checking for period 1 cardioid and period 2 circle (fp32 and fp64)
  * halves time taken for fp64-seahorse-valley.mm
* increased the default number of iterations performed in each GPU pass
  * time taken for fp32-large-minibrot.mm reduced from 0.879s to 0.749s
  * time taken for fp64-large-minibrot.mm reduced from 9.982s to 7.262s
  * time taken for fpxx-large-minibrot.mm reduced from 392.4s to 306.6s
* glitch correction reference finding improved
  * should use fewer references for most images
  * uses a two-pass blob extraction method
  * first pass extracts glitched blobs
  * second pass extracts the most glitched sub-blobs within each blob
* `--no-glitch` masking defaults to black if all neighbours are glitched too
* `extra/zoom` assembler adds motion blur
  * variable shutter speed (range from 0.0 to 1.0, default 0.5, set to 0.0 if
    you want previous behaviour)
  * reduces unpleasant strobing at standard frame rates for fast zooms
  * improves compressibility (better quality at lower bitrates)

Documentation improvements:

* documentation available online at <http://mightymandel.mathr.co.uk>
* reformatted in doxygen-flavoured markdown
* `make -C doc` includes the user documentation too
  * output in `doc/mightymandel/index.html`, requires javascript
  * tested with doxygen-1.8.1.2-2 from Debian Wheezy
* documentation files renamed with `.md` extension and moved to `doc/`

Build system improvements:

* compiling from a release tarball works without git
* extra libraries needed on some systems are linked automatically
* version string is always up to date (regenerated each build)
* `make DEBUG=-s`compiles without debugging support (but `make clean` first)
* cross-compiling from Linux to Windows 32bit and 64bit (see \ref WINDOWS)
* compiling on OS X might be possible (untested stopwatch implementation)
* `release.sh` builds a few variants at once, tagging them by version

Internal changes:

* refactored parsers into separate files
* new stopwatch module for time measuring functionality
* new filename module for generating suitable filenames
* new metadata module for storing metadata about images
* new vram module for checking available video memory
* new slice module for generating slice ordering tables for progressive display
* refactored render engine core to poll for events
* static texture unit assignment simplifies code and reduces rebinding


\section changesv15             mightymandel v15 stable release

User interface improvements:

* `--tile` and `--zoom` can be combined
* `--zoom` saves separate files instead of piping to stdout
* `--tile` supports non-square tilings
* keyboard controls for translation (cursor keys with shift and control mods)
* all user interface actions (apart from close window) are disabled when not
  in interactive mode (to avoid accidentally messing up a render)
* new parser for preprocessed .ppar FractInt files (thanks Adam Majewski)
  (only "corners" format is supported, "center-mag" is coming soon)
* new script `extra/split2ppar.sh` to pre-process FractInt .par to .ppar

Rendering improvements:

* very important fix to a bug that caused severe image corruption (an update
  to a variable was inside an if when it should have been outside)
* `--no-de` uses 8 neighbours for smoother appearance (previously 4)
* `--no-glitch` uses 8 neighbours for smoother appearance (previously 4)
* `--no-approx` flag to disable series approximation in case of corrupt images
* atom finding (used for initial reference point) more robust against failure
* completion checking simplified, with a `--sharpness` flag to determine how
  sharp interior regions are
* for perturbation rendering, a new `--max-glitch` flag sets a threshold of
  how many pixels are allowed to be glitched
* glitch correction algorithm changed (now picks the most erroneous pixel
  with the highest iteration count)
* tiled rendering has an automatic timeout that prevents infinite loop if a
  tile is 100% interior
* `extra/zoom` sequence assembler output quality boost with simpler code

Documentation improvements:

* updated all the user documetation
* added a change log (this file)
* classified the todo list by difficulty
* added a hacking guide containing tips for hacking on mightymandel
* some source code documentation in doxygen markup (incomplete so far)
* more examples from <http://www.chillheimer.de/kallesfraktaler/glitches.zip>
* more examples from <http://reocities.com/CapeCanaveral/5003/par.htm>
* more examples from Adam Majewski and myself (Claude)
* archived test suite results in test/

Build system improvements:

* automatic version string generation using `git describe`
* link with -lm
* source code documentation to HTML extraction (`make -C src doc`)
* test suite generates a nice tidy CSV file (`make -C src test`)
* test suite warns to commit changes first (so version will be correct)
* strip comments from glsl when building (C99 string constant must be <4095)

Internal changes (the big de-inversion of control):

* struct view is gone, had too many unrelated things in one place
* struct S is gone, had to much shared global mutable state for understanding
* state module is gone, too much centralized interlocking confusions
* new interact module that handles all the semantics of keyboard/mouse/etc
* mightymandel glfw callbacks now delegate to interact module
* new logging module that handles output at various levels of detail
* new startup module that handles command line input (version, usage, banner,
  argument parsing)
* record module simplified with a saner file name template, and the date is
  set once at program startup instead of changing with every save
* new tiling module to handle all tiling state
* new zoom module to handle all zooming state
* render.h defines a struct render_options for all control over rendering, it
  has a flag set if paramters have been changed that require rendering to
  restart, otherwise the new visualisation parameters are applied live

Other internal changes:

* `fp___escaped` "flush" flag removed, now the necessary functionality is in
  fp32_colour "done" flag and fpxx_init without rasterizer discard
* don't split iteration count into int/float parts for `--no-de` to simplify
  the colouring code amongst other things
* uniformized all the stuff that needs to transform pixels to coordinates or
  the reverse to use center/radius/aspect representations
* fpxx_escape uses precision of reference (instead of center) to initialize
* usage of VBOs validated, no longer risking image corruption
* skip check for escaping reference
* lots of LOG_DEBUG messages added
* factored out window creation to a new function


\section changesv14             mightymandel v14 stable release

Build system improvements:

* ported from C++11 to C99
* new Makefile flags `EXTRA_COMPILE_FLAGS` and `EXTRA_LINK_FLAGS`
  which may be useful for compiling on interesting environments
  without patching the Makefile itself

Rendering improvements:

* `-weight` now works with DE
* `-no-de` weight changed to match DE appearance
* glitched interiors no longer stay glitched
* interiors coloured yellow instead of white

User interface improvements:

* fixed buggy mouse button handling
* inverted weight key control order
* `-version` flag prints version and exits

Documentation improvements:

* fixed git instructions in documentation
* documented PageUp/PageDown zoom key controls
* added hardware comparison benchmarks


\section changesv13             mightymandel v13 stable release

Reduced dependencies:

* glm replaced by simple maths
* glib regex replaced by new parsers

New parsers:

* new support for ppm as output by mightymandel
* new support for mdz xmin/xmax/ymax variant
* all supported formats render the right size
  (assuming window aspect ratio is correct)

New weight control for `-no-de`:

* can make lines lighter or darker if desired
* corresponding feature for DE coming soon

Improved series approximation:

* for deep zooms it is initialized on CPU to
  avoid overflow on GPU
* warning if image is likely to fail due to
  insufficient precision

Miscellaneous:

* zoom assembler updated, now can pipe direct
* new script to convert ppm to png preserving
  mightymandel metadata with location information
* new script to make assembling tiled images easy

Since v12:
* 57 files changed, 1198 insertions(+), 420 deletions(-)


\section changesv12             mightymandel v12 stable release

Massive speed improvements by simplifying reference point
finding algorithm (uses atom finding for first reference
only, then uses glitch blob centers).

Large library of example parameters, and a test suite that
renders all of them.

Documentation improvements and miscellaneous bug fixes.

Changes since v11:
  143 files changed, 8680 insertions(+), 491 deletions(-)

Source code line count:

* C++    3597
* GLSL    883
* other    97
* total  4577


\section changesolder ancient history

* v11             use OpenGL 4.1 to avoid proprietary stuff, other misc cleanups
* v10             OpenGL Core Profile support and version autodetection
* v9              high resolution tiled rendering support
* v8              automatic glitch correction
* v7              switched from gtk2 to glfw3
* v6              pseudo-de fake distance estimate colouring option
* v5              command line flag disables fp64 double usage
* v4              no longer needs anansi
* v3              improved usability and appearance
* v2              use float or double as required
* v1              dancing angels

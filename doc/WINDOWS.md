WINDOWS {#WINDOWS}
=======

Cross-compiling from Linux to Windows 32bit and 64bit is possible.  The
following is tested on (amd64) Debian Wheezy with some packages from Jessie.
These pacakges are needed for sure, but others might be too:

    sudo aptitude install \
      mingw-w64 \
      mingw-w64-tools

For testing in `wine`, I installed `libgl1-nvidia-glx:i386` as well (depends on
your system if this is appropriate).

The build system assumes all your win32 and win64 stuff is in `~/win32`
and `~/win64` so first that needs to get set up:

    mkdir ~/win32 ~/win32/bin ~/win32/src ~/win64 ~/win64/bin ~/win64/src

Then you need to install the dependencies:
- \ref wingmp
- \ref winmpfr
- \ref winglew
- \ref winglfw
- \ref winbuild
- \ref winbugs

\section wingmp GMP

[GMP](https://gmplib.org) cross-compiles without any problems:

    cd ~/win32/src
    wget https://gmplib.org/download/gmp/gmp-6.0.0a.tar.lz
    tar xf gmp-6.0.0a.tar.lz
    cd gmp-6.0.0
    ./configure --prefix=${HOME}/win32 --host=i686-w64-mingw32 --enable-cxx
    make -j6
    make install
    cd ~/win64/src
    cp -ai ~/win32/src/gmp-6.0.0a.tar.lz .
    tar xf gmp-6.0.0a.tar.lz
    cd gmp-6.0.0
    ./configure --prefix=${HOME}/win64 --host=x86_64-w64-mingw32 --enable-cxx
    make -j6
    make install


\section winmpfr MPFR

[MPFR](http://www.mpfr.org) also cross-compiles without any problems after you
have GMP built:

    cd ~/win32/src
    wget http://www.mpfr.org/mpfr-current/mpfr-3.1.2.tar.xz
    wget http://www.mpfr.org/mpfr-current/allpatches
    cp -ai allpatches mpfr-3.1.2.patch
    tar xf mpfr-3.1.2.tar.xz
    cd mpfr-3.1.2
    patch -N -Z -p1 < ../mpfr-3.1.2.patch
    ./configure --prefix=${HOME}/win32 --host=i686-w64-mingw32 --with-gmp-build=${HOME}/win32/src/gmp-6.0.0
    make -j6
    make install
    cd ~/win64/src
    cp -ai ~/win32/src/mpfr-3.1.2.tar.xz .
    cp -ai ~/win32/src/allpatches mpfr-3.1.2.patch
    tar xf mpfr-3.1.2.tar.xz
    cd mpfr-3.1.2
    patch -N -Z -p1 < ../mpfr-3.1.2.patch
    ./configure --prefix=${HOME}/win64 --host=x86_64-w64-mingw32 --with-gmp-build=${HOME}/win64/src/gmp-6.0.0
    make -j6
    make install


\section winglew GLEW

[GLEW](http://glew.sourceforge.net) has binaries for windows, but the download
page has a countdown with lots of annoying adverts:

<http://sourceforge.net/projects/glew/files/glew/1.11.0/glew-1.11.0-win32.zip/download>

Download it to `~/win32/src`, then:

    cd ~/win32/src
    unzip glew-1.11.0-win32.zip
    cd glew-1.11.0
    cp -ai lib/*/Win32/*.lib ~/win32/lib
    cp -ai bin/*/Win32/*.dll ~/win32/bin
    cp -ai bin/*/Win32/*.exe ~/win32/bin
    cp -ai include/GL ~/win32/include
    cd ~/win64/src
    cp -ai ~/win32/src/glew-1.11.0-win32.zip .
    unzip glew-1.11.0-win32.zip
    cd glew-1.11.0
    cp -ai lib/*/x64/*.lib ~/win64/lib
    cp -ai bin/*/x64/*.dll ~/win64/bin
    cp -ai bin/*/x64/*.exe ~/win64/bin
    cp -ai include/GL ~/win64/include


\section winglfw GLFW

[GLFW](http://www.glfw.org) also has windows binaries, also with countdown and
annoying adverts:

<http://sourceforge.net/projects/glfw/files/glfw/3.0.4/glfw-3.0.4.bin.WIN32.zip/download>

<http://sourceforge.net/projects/glfw/files/glfw/3.0.4/glfw-3.0.4.bin.WIN64.zip/download>

Download them to `~/win32/src` and `~/win64/src` respectively, then:

    cd ~/win32/src
    unzip glfw-3.0.4.bin.WIN32.zip
    cd glfw-3.0.4.bin.WIN32
    cp -ai lib-mingw/* ~/win32/lib
    cp -ai include/GLFW ~/win32/include
    cd ~/win64/src
    unzip glfw-3.0.4.bin.WIN64.zip
    cd glfw-3.0.4.bin.WIN64
    cp -ai lib-mingw/* ~/win64/lib
    cp -ai include/GLFW ~/win64/include


\section winbuild Building mightymandel.exe

Now you can build `mightymandel.exe`.  To build the 32bit version:

    cd ~/code/mightymandel # or where you downloaded mightymandel
    cd src
    make SYSTEM=win32 clean
    make SYSTEM=win32

To build the 64bit version:

    cd ~/code/mightymandel # or where you downloaded mightymandel
    cd src
    make SYSTEM=win64 clean
    make SYSTEM=win64

If you installed the dependencies in a different location, you can:

    make SYSTEM=win32 WINPREFIX=/some/path

or

    make SYSTEM=win64 WINPREFIX=/some/path

If `wine` is set up correctly, you should be able to run it:

    ./mightymandel.exe


\section winbugs Bugs in mightymandel.exe

- `localtime_r()` isn't working, so I ifdef'd that out.  All saved files
  have the same stem, so be aware that each mightymandel session will overwrite
  PPM saved from a previous session:

      NOTICE  saved: mightymandel_1900-01-00_00-00-00_0000_00_00.ppm

- I don't know what happens on real windows wrt stderr or stdout messages.

- I don't know what happens on real windows wrt current working directory of a
  program started by a double click on an icon.  So images could get saved to
  strange places (or saving could fail due to permission issues).

- 1926 lines of error messages are printed on stderr at launch (this may be an
  issue with `wine` instead of `mightymandel.exe`).

      err:wgl:is_extension_supported No OpenGL extensions found, check if your OpenGL setup is correct!

BUGS {#BUGS}
====

- \ref bugde
- \ref bugglitch
- \ref bugparsers
- \ref bugother


\section bugde Distance Estimator

* series approximation goes wrong beyond ~1e-55
  * test case `down-the-rabbit-hole.mm` (too light)
* not possible to switch between `--de` and `--no-de` at runtime
* occasional NaNs with fp32 at low zoom
  * test case `examples/mm/fp32-nan-de.mm`
* `--no-de` gives visible seams with tiled rendering
  ![no-de-seam-1](no-de-seam-1.png)
  ![no-de-seam-2](no-de-seam-2.png)
  ![no-de-seam-3](no-de-seam-3.png)


\section bugglitch Glitch Correction

* bad glitches without `--no-approx`
  * test case zoom into `emndl_perturbation_test_2013-06-14.mm`
* warn: delta(z)_0 == 0, rendering will probably fail
  * all pixels have the same delta(z)_0, insufficient precision in double
  * leads to bad images
* `--no-glitch` gives minor visible seams with tiled rendering


\section bugparsers Parsers

* `parse_ppar_corners()` uses a fixed precision (256 bits)
* parsers assume ASCII (or other 8bit encoding)
  * UTF-8 is a multi-byte encoding where some bytes of multi-byte characters
    might clash with ASCII. (possibly false: see
    <http://htmlpurifier.org/docs/enduser-utf8.html#migrate-variablewidth>)
* parsers don't check success of `mpfr_set_str()`
* parsers should use `strtod()` instead of `atof()` to be able to detect errors
* fractint .gif parser doesn't read extended precision metadata


\section bugother Other

* if image is 100% interior, it will never finish rendering.
  possible algorithm to check this case before starting rendering:
 * first exclude problematic corner points
  * boundary points exactly representable as binary floating point
  * perhaps just exclude real axis and +/- i, are there others?
  * maybe perturb points if this is the case (make rectangle 0.5px smaller?)
 * for each corner, check if it is interior
  * if all corners are interior to the same component
   * if shape estimate of component is cardioid (non-convex)
    * (*) calculate interior distance estimates for each point
    * if there are any regions not excluded by the distance estimates
     * compute the interior|exterior distance estimate for a point in the
       middle of such a regions
     * if interior and same component, exclude another region and goto (*)
     * otherwise rectangle has exterior points and it's safe to render
   * else rectangle is 100% interior to a convex circular bulb
  might be easier to check a region in a previous frame,
  at least for `--interactive` and `--zoom`.
  for `--tile` perhaps render a small preview of the view to check interior
  tiles.  `--timeout` mitigates the problem somewhat.
* `render.c` has a hardcoded maximum period (100000) for finding the first reference
* `render.c` has a hardcoded maximum pass count (1000) for number of references
* test suite assumes example parameters are in directories matching extension
* need a way to turn view radius into zoom frame count
* need a way to render a subsection of a zoom sequence
  * perhaps start radius / end radius, which are rounded outwards to 2^n values
* timeout calculations are broken if a render is stopped and restarted with
  ctrl-Z and later fg
* `extra/zoom` needlessly uses double-size textures
* `extra/zoom` needlessly uses two passes for cubic interpolation
* `extra/zoom` needs to have window visible or it pauses rendering
* `--slice 0` saves unneeded reference orbit in `fpxx_step.c`, wasting system
  memory

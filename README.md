mightymandel
============

GPU-based Mandelbrot set explorer
<http://mightymandel.mathr.co.uk>

Documentation can be found in `doc/`.

Excluded Tests {#testexclude}
==============

Files to exclude from test suite runs.

- \ref excludegif
- \ref excludekfr
- \ref excludemdz
- \ref excludemm
- \ref excludepar
- \ref excludeppar


\section excludegif gif

The GIF parser has insufficient precision for some parameters.
Some give a 100% interior image and timeout:
  * 1_33.gif

Others give a 0.0 radius and fail to parse:
  * 2_33.gif
  * 2_34.gif
  * 2_40.gif
  * 2_41.gif
  * 2_42.gif
  * 2_49.gif
  * 4_22.gif


\section excludekfr kfr

Some parameters are too deep for the mightymandel renderer to handle:
  * e1000.kfr (1e1000)
  * glitch33.kfr (1e1277
  * Ssssssssss.kfr (4e533)
  * the-complexity-of-a-line.kfr (6e784)


\section excludemdz mdz

This parameter has a very thin aspect ratio very deep in a cusp (so timeout or
considered 100% interior):
  * north_facing_spike.mdz


\section excludemm mm

This parameter is quite deep and dense and fails glitchily with series
approximation enabled:
  * emndl_perturbation_test_2013-06-14.mm


\section excludepar par

There is no parser for .par format yet, so all files are excluded here:
  * 1_01.par 1_02.par 1_03.par 1_04.par 1_05.par 1_06.par 1_07.par 1_08.par 1_09.par 1_10.par
    1_11.par 1_12.par 1_13.par 1_14.par 1_15.par 1_16.par 1_17.par 1_18.par 1_19.par 1_20.par
    1_21.par 1_22.par 1_23.par 1_24.par 1_25.par 1_26.par 1_27.par 1_28.par 1_29.par 1_30.par
    1_31.par 1_32.par 1_33.par 1_34.par 1_35.par 1_36.par 1_37.par 1_38.par 1_39.par 1_40.par
    1_41.par 1_42.par 1_43.par 1_44.par 1_45.par 1_46.par 1_47.par 1_48.par 1_49.par 1_50.par
    2_01.par 2_02.par 2_03.par 2_04.par 2_05.par 2_06.par 2_07.par 2_08.par 2_09.par 2_10.par
    2_11.par 2_12.par 2_13.par 2_14.par 2_15.par 2_16.par 2_17.par 2_18.par 2_19.par 2_20.par
    2_21.par 2_22.par 2_23.par 2_24.par 2_25.par 2_26.par 2_27.par 2_28.par 2_29.par 2_30.par
    2_31.par 2_32.par 2_33.par 2_34.par 2_35.par 2_36.par 2_37.par 2_38.par 2_39.par 2_40.par
    2_41.par 2_42.par 2_43.par 2_44.par 2_45.par 2_46.par 2_47.par 2_48.par 2_49.par 2_50.par
    3_01.par 3_02.par 3_03.par 3_04.par 3_05.par 3_06.par 3_07.par 3_08.par 3_09.par 3_10.par
    3_11.par 3_12.par 3_13.par 3_14.par 3_15.par 3_16.par 3_17.par 3_18.par 3_19.par 3_20.par
    3_21.par 3_22.par 3_23.par 3_24.par 3_25.par 3_26.par 3_27.par 3_28.par 3_29.par 3_30.par
    3_31.par 3_32.par 3_33.par 3_34.par 3_35.par 3_36.par 3_37.par 3_38.par 3_39.par 3_40.par
    3_41.par 3_42.par 3_43.par 3_44.par 3_45.par 3_46.par 3_47.par 3_48.par 3_49.par 3_50.par
    4_01.par 4_02.par 4_03.par 4_04.par 4_05.par 4_06.par 4_07.par 4_08.par 4_09.par 4_10.par
    4_11.par 4_12.par 4_13.par 4_14.par 4_15.par 4_16.par 4_17.par 4_18.par 4_19.par 4_20.par
    4_21.par 4_22.par 4_23.par 4_24.par 4_25.par 4_26.par 4_27.par 4_28.par 4_29.par 4_30.par
    4_31.par 4_32.par 4_33.par 4_34.par 4_35.par 4_36.par 4_37.par 4_38.par 4_39.par 4_40.par
    4_41.par 4_42.par 4_43.par 4_44.par 4_45.par 4_46.par 4_47.par 4_48.par 4_49.par 4_50.par 


\section excludeppar ppar

One parameters gives a 100% interior image and times out:
  * 1_33.ppar

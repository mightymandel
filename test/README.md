Result Format {#testformat}
=============

Files should be named by version.  CSV (comma separated values) with a two line
header prefixed with #.  The first line contains version and date details.  The
second line contains the field specification.  Example header:

    # mightymandel v14-123-g8f76147 test suite at 2014-12-30T18:36:53+0000
    # filename,result,method,iterations,exterior,interior,glitched,passes

Example data line:

    dragons.kfr,ok,fpxx,25200.839107,100.000000,0.000000,0.010417,2

Descriptions of the fields:

  - Parameter file.
    The basename of the parameter file (without directory, but with
    extension).

  - Result.
    The overall rendering result.  Should be one of:

      - ok
        The rendering completed and the image is fine.

      - timeout
        Rendering didn't complete within the time limit.

      - glitch
        The image has an unacceptably high proportion of detected glitches.

      - parse
        The parameter file failed to parse.

      - deep
        The view is too deep for mightymandel to handle.

      - crash
        mightymandel crashed (or otherwise exited unexpectedly).

      - corrupt
        The image is bad: undetected glitches, wrong appearance, other.  Usually
        no way of knowing without manual inspection of images.

      - excluded
        The test was not run (perhaps a known failure and wanting to save time).

  - Method.
    The rendering method used.  Should be one of:

      - fp32
        Plain single precision floating point.

      - fp64
        Plain double precision floating point.

      - fpxx
        Perturbed double precision floating point.

  - Iterations.
    Maximum iteration count of any escaped pixel.

    - This has changed, it used to be:
      The total average iterations per pixel (final result, not recomputed
      glitches).

  - Exterior.
    The percentage of exterior pixels.

  - Interior.
    The percentage of interior pixels.

  - Glitched.
    The percentage of glitched pixels (fpxx only).

  - Passes.
    The number of glitch correction passes (fpxx only).

Any unknown field should be left blank.

The file (apart from the header lines) should be sorted in case-insentive
alphabetical order.

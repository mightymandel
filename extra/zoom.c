// mightymandel -- GPU-based Mandelbrot Set explorer
// Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
// License GPL3+ http://www.gnu.org/licenses/gpl.html

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <GL/glew.h>
#include <GL/glut.h>

// image sizes
static int IWIDTH = 0;
static int IHEIGHT = 0;
static int OWIDTH = 0;
static int OHEIGHT = 0;
static int WWIDTH = 0;
static int WHEIGHT = 0;

// speed of zooming
static int IFRAMES = 0;
static int OFPS = 25;
static double OLENGTH = 0;
static double increment = 0;

// motion blur
static double shutter = 0.5;
static int motion_count = 1;

// image buffers
static unsigned char *ibuffer = 0;
static unsigned char *ybuffer = 0;
static unsigned char *ubuffer = 0;
static unsigned char *vbuffer = 0;

static char oheader[1024];

// variables
static double phase = 0;
static int which = 0;

// textures
enum texid {
  texi11a = 0,
  texi11b = 1,
  tex22a,
  tex22b,
  tex22c,
  tex21a,
  tex21b,
  tex11a,
  tex11b,
  tex10a,
  tex00a,
  // total number of textures
  texCount
};
static GLuint tex[texCount];

// frame buffer
static GLuint fbo = 0;

// shaders and uniform locations
static int cubic_prog = 0;
static int cubic_tex = 0;
static int cubic_delta = 0;
static int blur_prog = 0;
static int blur_tex = 0;
static int blur_alpha = 0;
static int merge_prog = 0;
static int merge_tex0 = 0;
static int merge_tex1 = 0;
static int merge_g = 0;
static int yuv_prog = 0;
static int yuv_tex = 0;

// read raw image data
static int read_image(void) {
  char header[256];
  if ('P' != fgetc(stdin)) { fprintf(stderr, "expected P\n"); return 0; }
  if ('6' != fgetc(stdin)) { fprintf(stderr, "expected 6\n"); return 0; }
  if ('\n' != fgetc(stdin)) { fprintf(stderr, "expected newline\n"); return 0; }
  // FIXME assume header only has comments on lines beginning with #
  int c = fgetc(stdin);
  while ('#' == c) { while (c != '\n') { if (c == -1) { fprintf(stderr, "EOF in comment\n"); return 0; } c = fgetc(stdin); } c = fgetc(stdin); }
  ungetc(c, stdin);
  int w = 0;
  int h = 0;
  if (2 != fscanf(stdin, "%d %d 255", &w, &h)) { fprintf(stderr, "couldn't parse width height 255\n"); return 0; }
  if (! (w == IWIDTH && h == IHEIGHT)) { fprintf(stderr, "size mismatch\n"); return 0; }
  if ('\n' != fgetc(stdin)) { fprintf(stderr, "expected newline\n"); return 0; }
  return 1 == fread(ibuffer, IHEIGHT * IWIDTH * 3, 1, stdin);
}

// upload an image to the GPU
static void upload_image(GLuint t, int w, int h) {
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, t);
  glTexSubImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, 0, 0, w, h, GL_RGB, GL_UNSIGNED_BYTE, ibuffer);
}

// interpolate
static void cubic(GLuint to, GLuint from, double zoomx, double zoomy, double x, double y, int w, int h) {
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, 0, 0);
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, from);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, to, 0);
  glUseProgramObjectARB(cubic_prog);
  glUniform1iARB(cubic_tex, 0);
  glUniform2fARB(cubic_delta, x, y);
  glBegin(GL_QUADS); {
    glTexCoord2f(w*0.5 - w*0.5*zoomx, h*0.5 - h*0.5*zoomy); glVertex2f(0, 0);
    glTexCoord2f(w*0.5 + w*0.5*zoomx, h*0.5 - h*0.5*zoomy); glVertex2f(1, 0);
    glTexCoord2f(w*0.5 + w*0.5*zoomx, h*0.5 + h*0.5*zoomy); glVertex2f(1, 1);
    glTexCoord2f(w*0.5 - w*0.5*zoomx, h*0.5 + h*0.5*zoomy); glVertex2f(0, 1);
  } glEnd();
  glUseProgramObjectARB(0);
}
static void cubicX(GLuint to, GLuint from, double zoomx, double zoomy, int w, int h) {
  cubic(to, from, zoomx, zoomy, 1, 0, w, h);
}
static void cubicY(GLuint to, GLuint from, double zoomx, double zoomy, int w, int h) {
  cubic(to, from, zoomx, zoomy, 0, 1, w, h);
}

// blur
static void blur(GLuint to, GLuint from, double alpha, int w, int h) {
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, 0, 0);
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, from);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, to, 0);
  glUseProgramObjectARB(blur_prog);
  glUniform1iARB(blur_tex, 0);
  glUniform1fARB(blur_alpha, alpha);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glBegin(GL_QUADS); {
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(w, 0); glVertex2f(1, 0);
    glTexCoord2f(w, h); glVertex2f(1, 1);
    glTexCoord2f(0, h); glVertex2f(0, 1);
  } glEnd();
  glDisable(GL_BLEND);
  glUseProgramObjectARB(0);
}

// merge two textures
static void merge(GLuint to, GLuint l, GLuint r, double g, int w, int h) {
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, 0, 0);
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, l);
  glActiveTexture(GL_TEXTURE1);
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, r);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, to, 0);
  glUseProgramObjectARB(merge_prog);
  glUniform1iARB(merge_tex0, 0);
  glUniform1iARB(merge_tex1, 1);
  glUniform1fARB(merge_g, g);
  glBegin(GL_QUADS); {
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(w, 0); glVertex2f(1, 0);
    glTexCoord2f(w, h); glVertex2f(1, 1);
    glTexCoord2f(0, h); glVertex2f(0, 1);
  } glEnd();  
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
  glActiveTexture(GL_TEXTURE0);
  glUseProgramObjectARB(0);
}

// convert RGB to YUV
static void toyuv(GLuint to, GLuint from, int w, int h) {
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, 0, 0);
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, from);
  glFramebufferTexture2DEXT(GL_FRAMEBUFFER_EXT, GL_COLOR_ATTACHMENT0_EXT, GL_TEXTURE_RECTANGLE_ARB, to, 0);
  glUseProgramObjectARB(yuv_prog);
  glUniform1iARB(yuv_tex, 0);
  glBegin(GL_QUADS); {
    glTexCoord2f(0, 0); glVertex2f(0, 0);
    glTexCoord2f(w, 0); glVertex2f(1, 0);
    glTexCoord2f(w, h); glVertex2f(1, 1);
    glTexCoord2f(0, h); glVertex2f(0, 1);
  } glEnd();  
  glUseProgramObjectARB(0);
}

// download Y data from the GPU
static void download_y(void) {
  glReadPixels(0, 0, OWIDTH, OHEIGHT, GL_RED,   GL_UNSIGNED_BYTE, ybuffer);
}

// download UV data from the GPU
static void download_uv(void) {
  glReadPixels(0, 0, OWIDTH / 2, OHEIGHT / 2, GL_GREEN, GL_UNSIGNED_BYTE, ubuffer);
  glReadPixels(0, 0, OWIDTH / 2, OHEIGHT / 2, GL_BLUE,  GL_UNSIGNED_BYTE, vbuffer);
}

// write planar YUV420
static int write_image(void) {
  if (1 != fwrite("FRAME\n", 6, 1, stdout)) { return 0; }
  if (1 != fwrite(ybuffer, OWIDTH * OHEIGHT    , 1, stdout)) { return 0; }
  if (1 != fwrite(ubuffer, OWIDTH * OHEIGHT / 4, 1, stdout)) { return 0; }
  if (1 != fwrite(vbuffer, OWIDTH * OHEIGHT / 4, 1, stdout)) { return 0; }
  return 1;
}

// separable binomial filter is not needed
// source is upscaled with cubic interpolation, so linear interpolated lookup..
// ..when downscaling by a factor of two should be just fine
static const char *blur_frag_src =
"#version 130\n"
"uniform sampler2DRect tex;\n"
"uniform float alpha;\n"
"\n"
"void main() {\n"
"  gl_FragColor = vec4(texture2DRect(tex, gl_TexCoord[0].xy).rgb, alpha);\n"
"}\n";

// separable cubic interpolation
static const char *cubic_frag_src =
"#version 130\n"
"uniform sampler2DRect tex;\n"
"uniform vec2 delta;\n" // (1,0) or (0,1)
"\n"
"vec4 interp(float t, mat4 a) {\n"
"  const mat4 m = mat4(0.0,2.0,0.0,0.0, -1.0,0.0,1.0,0.0, 2.0,-5.0,4.0,-1.0, -1.0,3.0,-3.0,1.0);\n"
"  vec4 f = vec4(1.0, t, t*t, t*t*t);\n"
"  return 0.5 * f * transpose(m) * transpose(a);\n"
"}\n"
"\n"
"void main() {\n"
"  vec2 p = gl_TexCoord[0].xy;\n"
"  vec2 i = floor(p);\n"
"  vec2 f = p - i;\n"
"  gl_FragColor =\n"
"    interp(dot(f, delta), mat4(\n"
"      texture2DRect(tex, i -       delta),\n"
"      texture2DRect(tex, i              ),\n"
"      texture2DRect(tex, i +       delta),\n"
"      texture2DRect(tex, i + 2.0 * delta)));\n"
"}\n";

// mixer
static const char *merge_frag_src =
"#version 130\n"
"uniform sampler2DRect tex0;\n"
"uniform sampler2DRect tex1;\n"
"uniform float g;\n"
"\n"
"void main() {\n"
"  vec2 p = gl_TexCoord[0].xy;\n"
"  gl_FragColor = mix(texture2DRect(tex0, p), texture2DRect(tex1, p), g);\n"
"}\n";

// rgb to yuv
static const char *yuv_frag_src =
"#version 130\n"
"uniform sampler2DRect tex;\n"
"\n"
"void main() {\n"
"  const mat3 m = mat3(\n"
"    0.299, 0.587, 0.114,\n"
"    -0.168736, -0.331264, 0.5,\n"
"    0.5, -0.418688, -0.081312);\n"
"  const vec3 s = vec3(219.0, 224.0, 224.0);\n"
"  const vec3 o = vec3(16.5, 128.5, 128.5);\n"
"  vec2 p = gl_TexCoord[0].xy;\n"
"  vec3 rgb = texture2DRect(tex, p).rgb;\n"
"  vec3 yuv = (rgb * m * s + o) / 255.0;\n"
"  gl_FragColor = vec4(yuv, 1.0);\n"
"}\n";

// display callback
static void display(void) {
  int w = IWIDTH;
  int h = IHEIGHT;
  int motion = 0;
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, fbo);
  for (motion = 0; motion < motion_count; ++motion) {
    double motion_alpha = 1.0 / (motion + 1.0);
    while (phase >= 1) {
      // read next image
      if (read_image()) {
        upload_image(tex[which], w, h);
        which = 1 - which;
        phase -= 1;
      } else {
        fprintf(stderr, "couldn't read image\n"); 
        exit(0);
      }
    }
    if (motion <= shutter * motion_count) {
    // scale and blend
    double f0 = pow(0.5, phase + 1);
    double f1 = pow(0.5, phase);
    double g = (1 - pow(4, -phase)) / (pow(4, 1 - phase) - pow(4, -phase));
    glViewport(0, 0, w * 2, h    ); cubicX(tex[tex21a], tex[    which], f0, 1,       w    , h    );
                                    cubicX(tex[tex21b], tex[1 - which], f1, 1,       w    , h    );
    glViewport(0, 0, w * 2, h * 2); cubicY(tex[tex22a], tex[tex21a], 1, f0,          w * 2, h    );
                                    cubicY(tex[tex22b], tex[tex21b], 1, f1,          w * 2, h    );
                                    merge( tex[tex22c], tex[tex22a], tex[tex22b], g, w * 2, h * 2);
    glViewport(0, 0, w,     h    ); blur ( tex[tex11a], tex[tex22c], motion_alpha,   w * 2, h * 2);
    }
    // advance
    phase += increment / motion_count;
  }
  // download output
                                    toyuv( tex[tex11b], tex[tex11a],                 w    , h    );
                                    download_y();
    glViewport(0, 0, w / 2, h / 2); blur ( tex[tex00a], tex[tex11b], 1.0,            w    , h    );
                                    download_uv();
  // output
  write_image();
  // display current output
  glBindFramebufferEXT(GL_FRAMEBUFFER_EXT, 0);
  glViewport(0, 0, WWIDTH, WHEIGHT);
  glUseProgramObjectARB(0);
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tex[tex11a]);
  glBegin(GL_QUADS); {
    // flipped vertically to display correctly
    glTexCoord2f(     0,       0); glVertex2f(0, 1);
    glTexCoord2f(OWIDTH,       0); glVertex2f(1, 1);
    glTexCoord2f(OWIDTH, OHEIGHT); glVertex2f(1, 0);
    glTexCoord2f(     0, OHEIGHT); glVertex2f(0, 0);
  } glEnd();  
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, 0);
  glutSwapBuffers();
  glutReportErrors();
}

// allocate an RGB texture
static void textureb(int tid, int w, int h) {
  glBindTexture(GL_TEXTURE_RECTANGLE_ARB, tex[tid]);
  glTexImage2D(GL_TEXTURE_RECTANGLE_ARB, 0, GL_RGBA, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, 0);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_RECTANGLE_ARB, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
}

// initialize a shader
static int shader(const char *src) {
  int p = glCreateProgramObjectARB();
  int f = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
  glShaderSourceARB(f, 1, (const GLcharARB **) &src, 0);
  glCompileShaderARB(f);
  glAttachObjectARB(p, f);
  glLinkProgramARB(p);
  int success;
  glGetObjectParameterivARB(p, GL_OBJECT_LINK_STATUS_ARB, &success);
  if (!success) { fprintf(stderr, "failed to compile shader\n%s\n", src); exit(1); }
  return p;
}

// GLUT timer callback
static void timerf(int x) {
  glutTimerFunc(10, timerf, x+1);
  glutPostRedisplay();
}

// main program
extern int main(int argc, char **argv) {
  if (! (argc >= 5)) {
    fprintf(stderr, "usage: %s iwidth iheight iframes olength [ofps [oshutter]] < stream.ppm > stream.y4m\n", argv[0]);
    return 1;
  }
  IWIDTH = atoi(argv[1]);
  IHEIGHT = atoi(argv[2]);
  IFRAMES = atoi(argv[3]);
  OWIDTH = IWIDTH;
  OHEIGHT = IHEIGHT;
  OLENGTH = atof(argv[4]);
  OFPS = 25;
  if (argc >= 6) {
    OFPS = atoi(argv[5]);
  }
  if (argc >= 7) {
    shutter = fmin(fmax(atof(argv[6]), 0.0), 1.0);
  }
  increment = (IFRAMES - 1.0) / (OLENGTH * OFPS);
  /*
  zoom0 = 0.5 ** phase
  zoom1 = 0.5 ** (phase + increment / motion_count)
  texcoord = (width/2 + width/2 * zoom , height/2 + height/2 * zoom)
  assume phase = 0
  texcoord0 = (width, height)
  texcoord1 = (width, height) * (1 + 0.5 ** (increment / motion_count)) / 2
  texcoord1 - texcoord2 = (width, height) * [(1 + 0.5 ** (increment / motion_count)) / 2 - 1]
  | tex1 - tex2 | = sqrt(w^2+h^2) * [(1 + 0.5 ** (inc / mot)) / 2 - 1]
  | tex1 - tex2 | == 1  for smooth motion blur
  2*[1/sqrt(w^2+h^2) + 1] - 1 = 0.5 ** (inc / mot)
  2/sqrt(w^2 + h^2) + 1 = 0.5**(inc/mot)
  log (2 / sqrt (w^2 + h^2) + 1) = (inc / mot) log 0.5
  mot = inc * log 0.5 / log (2 / sqrt(w^2 + h^2) + 1)
  */
  motion_count = fmax(1.0, ceil(fabs(increment * log(0.5) / log(2.0 / sqrt(OWIDTH * OWIDTH + OHEIGHT * OHEIGHT) + 1.0))));
  fprintf(stderr, "zoom: motion_count = %d (%d)\n", motion_count, (int) ceil(shutter * motion_count));
  ibuffer = malloc(IWIDTH * IHEIGHT * 3);
  ybuffer = malloc(OWIDTH * OHEIGHT);
  ubuffer = malloc(OWIDTH/2 * OHEIGHT/2);
  vbuffer = malloc(OWIDTH/2 * OHEIGHT/2);
  snprintf(oheader, 1024, "YUV4MPEG2 W%d H%d F%d:1 Ip A1:1 C420mpeg2\n", OWIDTH, OHEIGHT, OFPS);
  // initialize
  double aspect = OWIDTH / (double) OHEIGHT;
  WWIDTH = 640;
  WHEIGHT = 640 / aspect;
  glutInitWindowSize(WWIDTH, WHEIGHT);
  glutInit(&argc, argv);
  glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
  glutCreateWindow("zoom");
  glewInit();
  // shaders
  cubic_prog  = shader(cubic_frag_src);
  cubic_tex   = glGetUniformLocationARB(cubic_prog, "tex");
  cubic_delta = glGetUniformLocationARB(cubic_prog, "delta");
  merge_prog  = shader(merge_frag_src);
  merge_tex0  = glGetUniformLocationARB(merge_prog, "tex0");
  merge_tex1  = glGetUniformLocationARB(merge_prog, "tex1");
  merge_g     = glGetUniformLocationARB(merge_prog, "g");
  blur_prog   = shader(blur_frag_src);
  blur_tex    = glGetUniformLocationARB(blur_prog,  "tex");
  blur_alpha  = glGetUniformLocationARB(blur_prog,  "alpha");
  yuv_prog    = shader(yuv_frag_src);
  yuv_tex     = glGetUniformLocationARB(yuv_prog,   "tex");
  // universal view
  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  gluOrtho2D(0, 1, 0, 1);
  // textures
  glShadeModel(GL_FLAT);
  glEnable(GL_TEXTURE_RECTANGLE_ARB);
  glGenTextures(texCount, tex);
  int w = IWIDTH;
  int h = IHEIGHT;
  textureb(texi11a, w * 1, h * 1);
  textureb(texi11b, w * 1, h * 1);
  textureb(tex22a, w * 2, h * 2);
  textureb(tex22b, w * 2, h * 2);
  textureb(tex22c, w * 2, h * 2);
  textureb(tex21a, w * 2, h * 1);
  textureb(tex21b, w * 2, h * 1);
  textureb(tex11a, w * 1, h * 1);
  textureb(tex11b, w * 1, h * 1);
  textureb(tex10a, w * 1, h / 2);
  textureb(tex00a, w / 2, h / 2);
  // frame buffer
  glGenFramebuffersEXT(1, &fbo);
  // initialize state
  which = 0;
  phase = 2;
  // write output header
  if (1 != fwrite(oheader, strlen(oheader), 1, stdout)) { fprintf(stderr, "failed to write header\n"); return 1; }
  // start processing frames
  glutDisplayFunc(display);
  glutTimerFunc(10, timerf, 0);
  glutReportErrors();
  glutMainLoop();
  return 0;
}

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <jack/jack.h>

#define pi2 6.283185307179586
#define sr 48000.0
#define bpm 144.0
#define decay 0.995
#define count 16

double mtof(double m) {
  return 440 * pow(2, (m - 69) / 12);
}

struct voice {
  int ee;
  double pp;
  double dp;
  double vv;
  double dv;
  double zx;
  double zy;
  double cx;
  double cy;
};

void voice_step(struct voice *v) {
  if (! v-> ee) {
    double x = v->zx * v->zx - v->zy * v->zy + v->cx;
    double y = 2 * v->zx * v->zy + v->cy;
    double m = x*x + y*y;
    v->ee = m > 4;
    v->zx = x;
    v->zy = y;
    m = 1 / (1 + m * m);
    v->dv = pow(decay, m);
    v->vv = tanh(m);
    v->dp = mtof(60 + 12 * atan2(y, x) / pi2) / sr;
    v->pp = 0;
  }
}

double voice_dsp(struct voice *v) {
  if (v->ee) {
    return 0;
  } else {
    v->vv *= v->dv;
    v->pp = fmod(v->pp + v->dp, 1);
    return v->vv * sin(pi2 * v->pp);
  }
}

struct voice voices[count][count];

void voices_init(double cx, double cy, double r) {
  for (int j = 0; j < count; ++j) {
    for (int i = 0; i < count; ++i) {
      struct voice *v = &voices[j][i];
      v->ee = 0;
      v->pp = 0;
      v->dp = 0;
      v->vv = 1;
      v->zx = 0;
      v->zy = 0;
      v->cx = 2 * (i + 0.5 - count / 2.0) / count * r + cx;
      v->cy = 2 * (j + 0.5 - count / 2.0) / count * r + cy;
    }
  }
}

double voices_dsp(void) {
  double o = 0;
  double s = 1;
  for (int j = 0; j < count; ++j) {
    for (int i = 0; i < count; ++i) {
      o += voice_dsp(&voices[j][i]);
      s += ! voices[j][i].ee;
    }
  }
  return tanh(4 * o / s);
}

void voices_step(void) {
  for (int j = 0; j < count; ++j) {
    for (int i = 0; i < count; ++i) {
      voice_step(&voices[j][i]);
    }
  }
}

volatile int sample, samples;
jack_client_t *client;
jack_port_t *port;

int processcb(jack_nframes_t nframes, void *arg) {
  jack_default_audio_sample_t *out;
  out = (jack_default_audio_sample_t *) jack_port_get_buffer(port, nframes);
  for (int i = 0; i < nframes; ++i, ++sample) {
    out[i] = voices_dsp();
    if (sample >= samples) {
      voices_step();
      sample -= samples;
    }
  }
  return 0;
}


void atexitcb(void) {
  jack_client_close(client);
}

int main(int argc, char **argv) {
  voices_init(0, 0, 2 * sqrt(2));
  sample = 0;
  samples = sr / (4 * bpm / 60);
  fprintf(stderr, "%d\n", samples);

  if ((client = jack_client_new("mightymandel"))) {
    atexit(atexitcb);
    jack_set_process_callback(client, processcb, 0);
    port = jack_port_register(client, "output", JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput, 0);
    if (! jack_activate(client)) {
      jack_connect(client, "mightymandel:output", "system:playback_1");
      jack_connect(client, "mightymandel:output", "system:playback_2");
    }
  }

  char buffer[1024];
  const int buflen = 1000;
  while(! feof(stdin)) {
    double cx, cy, r;
    buffer[0] = 0;
    fgets(buffer, buflen, stdin);
    if (3 == sscanf(buffer, "%lf + %lf i @ %lf", &cx, &cy, &r)) {
      sample = 0;
      voices_init(cx, cy, r);
      for (int i = 0; i < 1000; ++i) {
        voices_step();
      }
      for (int j = 0; j < count; ++j) {
        for (int i = 0; i < count; ++i) {
          voices[j][i].zx = 0;
          voices[j][i].zy = 0;
          voices[j][i].vv = 0;
        }
      }
      fprintf(stderr, "%f %f %f\n", cx, cy, r);
    }
  }
  return 0;
}

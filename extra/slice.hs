overheads = 0.1
vram = 1024^3 -- 1GB
bytes_per_pixel = 4 * 1 + 2 * 4 -- rgba8UI (colour) + rg32F (raw, optimized for --no-de)
bytes_per_iter = 2 * 7 * 8 + 4 * 4 -- 2x vbo 7x64F (--no-de) + fillc rgba32F
aspect = 16/9

bytes_sliced n = bytes_per_pixel + bytes_per_iter / n
pixels_sliced n = (1 - overheads) * vram / bytes_sliced n
dimensions n = (floor (pixels * sqrt aspect), floor(pixels / sqrt aspect))
  where pixels = sqrt (pixels_sliced n)

iters n = floor (fromIntegral (uncurry (*) (dimensions n)) / n)

main = mapM_ (\n -> print (n, dimensions (4^n), iters (4^n))) [0..10]

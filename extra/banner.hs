-- input parameters
width = 1920.0    -- for full-screen browsers on typical systems
height = 92.0     -- what height the header is on my browser
left = 368.0      -- just to the right of the version
mleft = -2.0      -- put the tip there
mradius = 0.025   -- period 3 minibrot is nicely sized

-- calculate the view center
halfwidth = width / 2
aspect = width / height
right = width
center = width/2
mhalfwidth = mradius * aspect
mright = (right - left) / halfwidth * mhalfwidth + mleft
mcenter = (center - left) / (right - left) * (mright - mleft) + mleft

-- print it out in mightymandel .mm format
main = do
  putStrLn $ show mcenter ++ " +"
  putStrLn $ "0.0 i"
  putStrLn $ show mradius ++ " @"

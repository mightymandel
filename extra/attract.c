/*
#include <complex>
using namespace std;

Attract mode.

:d utility
double walk(double x, double mi, double ma, double d) {
  double p = (x - mi) / (ma - mi);
  double s = rand() / (double) RAND_MAX;
  return x + d * (s - p);
}

double exteriordistance(double x, double y, int n) {
  double px = 0, py = 0, cx = x, cy = y, dx = 1, dy = 0;
  double r2 = escaperadius2;
  double px1, py1, dx1, dy1, px2, py2, pxy, d2, p, d;
  for (int i = 1; i < n; ++i) {
    px2 = px * px;
    py2 = py * py;
    d2 = px2 + py2;
    if (d2 > r2) {
      p = sqrt(px * px + py * py);
      d = sqrt(dx * dx + dy * dy);
      return 2 * p * log(p) / d;
    }
    pxy = px * py;
    px1 = px2 - py2 + cx;
    py1 = 2 * pxy + cy;
    dx1 = 2 * (px * dx - py * dy) + 1;
    dy1 = 2 * (dx * py + dy * px);
    px = px1; py = py1; dx = dx1; dy = dy1;
  }
  return -1;
}

double targetx = 0;
double targety = 0;
double targetz = 1;
double targetradius = 2;
void picktarget() {
  while (1) {
//    fprintf(stderr, ".");
    double x = rand() / (double) RAND_MAX * 3 - 2;
    double y = rand() / (double) RAND_MAX * 2.5 - 1.25;
    double e = exteriordistance(x, y, 1000);
    if (0 < e && e < 0.00001) {
      targetx = x;
      targety = y;
      targetz = -40; //walk(targetz, -40, 1, 2);
      targetradius = pow(2, targetz);
//      fprintf(stderr, "%f %f %f %f\n", targetx, targety, targetradius, targetz);
      return;
    }
  }
}

double poincaredistance(double x1r, double x1i, double y1, double x2r, double x2i, double y2) {
  double dxr = x2r - x1r;
  double dxi = x2i - x1i;
  double dy = y2 - y1;
  return acosh(1 + (dxr * dxr + dxi * dxi + dy * dy) / (2 * y1 * y2));
}

void poincaregeodesic(
  double t,
  double x1r, double x1i, double y1,
  double x2r, double x2i, double y2,
  double *tr, double *ti, double *ty
) {
  double dxr = x2r - x1r;
  double dxi = x2i - x1i;
  double mdx = sqrt(dxr * dxr + dxi * dxi);
  if (!(mdx > 0)) {
    *tr = x1r * (1 - t) + t * x2r;
    *ti = x1i * (1 - t) + t * x2i;
    *ty = y1 * pow(y2 / y1, t);
  } else {
    double dt = poincaredistance(x1r, x1i, y1, x2r, x2i, y2);
//    fprintf(stderr, "%f\n", t);
    t = exp(t * dt);
    double eT = exp(dt);
    double c = y2 - y1 * eT;
    double d = -mdx * eT;
    double a =  d * y1;
    double b = -c * y1;
//    fprintf(stderr, "%f %f %f %f %f %f %f\n", dt, t, eT, a, b, c, d);
    complex<double> g = complex<double>(b, a * t) / complex<double>(d, c * t);
    double x = g.real();
    double y = g.imag();
    *tr = x1r + x * dxr / mdx;
    *ti = x1i + x * dxi / mdx;
    *ty = y;
  }
}
/|*
-- hyperbolic geodesic
-- the parameter t in [0..1] interpolates between the endpoints
geodesic p1@(Poincare x1 y1) p2@(Poincare x2 y2)
  | x1 == x2  = \t -> Poincare x1 (y1 * (y2 / y1) ** t)
  | otherwise = \t -> let x :+ y = go (exp (t * dt)) in Poincare (x1 + (x :+ 0) * dir) y
  where
    go t = (b :+ (a * t)) / (d :+ (c * t))   -- interpolate geodesic
    a =  d * y1
    b = -c * y1
    [c, d] = toList . nullVector $ (2><2)[ eT * mdx, y2 - y1 * eT, y1 - eT * y2, mdx ]
    eT = exp dt
    dt = distance p1 p2    -- hyperbolic distance between viewports
    dx = x2 - x1           -- vector between viewport centers
    mdx = magnitude dx     -- distance between viewport centers
    dir = cis (phase dx)   -- unit vector between viewport centers
*|/

void movetowardstarget(double t) {
  double d = poincaredistance(centerx, centery, radius, targetx, targety, targetradius);
//  fprintf(stderr, "d = %f\n", d);
  if (d < 0.1) {
    picktarget();
    movetowardstarget(t);
  } else {
    poincaregeodesic(t / d, centerx, centery, radius, targetx, targety, targetradius, &centerx, &centery, &radius);
  }
}
:
*/

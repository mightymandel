#!/bin/bash
## mightymandel -- GPU-based Mandelbrot Set explorer
## Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
## License GPL3+ http://www.gnu.org/licenses/gpl.html

for X in "$@"
do
  echo -e "Title\t$(head -n 2 "${X}" | tail -c +19)\nSoftware\tmightymandel" > "${X}.txt" &&
  tail -n +3 "${X}" | while read prefix line
  do
    if [[ "${prefix}" == "#" ]]
    then
      if [[ "${line}" == "software mightymandel" ]]
      then
        echo -n "mightymandel"
      else
        echo " ${line}"
      fi
    else
      break
    fi
  done >> "${X}.txt"
  pnmtopng -interlace -force -compression 9 -text "${X}.txt" "${X}" > "${X%ppm}png" &&
  rm "${X}.txt"
done

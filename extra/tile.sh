#!/bin/bash
## mightymandel -- GPU-based Mandelbrot Set explorer
## Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
## License GPL3+ http://www.gnu.org/licenses/gpl.html

n="${1}"
file="$(readlink -e "${2}")"
mm=$(readlink -e "${3}")
tmp="$(mktemp -d --tmpdir=. tile.XXXXXXXX)"
pushd "${tmp}"
"${mm}" --tile "${n}" "${file}" "${@:4}" 2>&1 | tee "../${tmp}.log"
if [[ "${n}" =~ (.*)x(.*) ]]
then
  w="${BASH_REMATCH[1]}"
  h="${BASH_REMATCH[2]}"
else
  w="${n}"
  h="${n}"
fi
echo "${w} x ${h}"
ls *.ppm | xargs -n "${w}" pnmcat -lr | pnmsplit
ls image* | sort -V | xargs pnmcat -tb > "../${tmp}.ppm"
popd
du -hsc "${tmp}"*

#!/bin/bash
tmp="$(mktemp -d --tmpdir=. split2ppar.XXXXXXXX)"
for file in "${@}"
do
  name="$(basename "${file}")"
  file="$(readlink -e "${file}")"
  pushd "${tmp}"
  awk "/{/{n++}{print > \"${name}_\"n\".par\" }" "${file}"
  popd
done
pushd "${tmp}"
for file in *.par
do
  ident="$(head -n 1 "${file}" | sed 's/ .*$//')"
  mv "${file}" "${ident}.par"       # rename input
  cat < "${ident}.par" |            # read input
  sed 's/;.*$//' |                  # delete ; comments
  tr '\n' ' ' |                     # join on one line with spaces
  sed 's/\\ *//g' |                 # merge lines ended with '\'
  tr -s ' ' |                       # compress multiple ' ' to single ' '
  sed 's/^.*{\([^}]*\)}.*$/\1/' |   # extract the part between { }
  tr ' ' '\n' |                     # split into separate lines
  cat > "${ident}.ppar"             # write output
done
popd
ls "${tmp}/"*

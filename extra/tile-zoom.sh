#!/bin/bash
## mightymandel -- GPU-based Mandelbrot Set explorer
## Copyright (C) 2012,2013,2014,2015 Claude Heiland-Allen
## License GPL3+ http://www.gnu.org/licenses/gpl.html

stem="${1}"
w="${2}"
h="${3}"
n0="${4}"
n1="${5}"
mkdir -p junk
seq "${n0}" "$(( n1 - 1 ))" |
while read f
do
  frame="$(printf %04d "${f}")"
  seq 0 "$(( h - 1 ))" |
  while read y
  do
    tiley="$(printf %02d "${y}")"
    pnmcat -lr "${stem}_${frame}_${tiley}"_*.ppm > "${stem}_${frame}_${tiley}.ppm" &&
    mv -t junk "${stem}_${frame}_${tiley}"_*.ppm
  done
  pnmcat -tb "${stem}_${frame}_"??.ppm > "${stem}_${frame}.ppm" &&
  mv -t junk "${stem}_${frame}_"??.ppm
done
